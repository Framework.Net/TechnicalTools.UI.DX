﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace TechnicalTools.UI.DX
{
    public partial class InputBox : EnhancedXtraForm
    {
        /// <summary></summary>
        /// <param name="owner">A control that owns the input box</param>
        /// <param name="result">reference to the data to fill</param>
        /// <param name="msg">Display a message above the input control</param>
        /// <param name="title">Title of the form</param>
        /// <param name="usePasswordChar">Allow to mask user entry with star character</param>
        /// <param name="remarks">Display a remarks below the input control</param>
        /// <param name="validateOnEnter">Make a click on Ok if user press enter. if true user won't be able to enter newline in a friendly way.</param>
        /// <param name="validate">Check the value and return empty or null string if value is validated. otherwise returns the reason why the value is not valid.</param>
        /// <returns>Return DialogResult.OK or DialogResult.Cancel</returns>
        public static DialogResult ShowDialog(IWin32Window owner, ref string result, string msg, string title = null, bool usePasswordChar = false, string remarks = null, bool validateOnEnter = false, Func<string, string> validate = null)
        {
            return GenericShowDialog(owner, ref result, msg, title, usePasswordChar, remarks, validateOnEnter, validate, str => str);
        }

        /// <summary>See other overload</see></summary>
        public static DialogResult ShowDialog(IWin32Window owner, ref long? result, string msg, string title = null, bool usePasswordChar = false, string remarks = null, bool validateOnEnter = false, Func<long?, string> validate = null)
        {
            return GenericShowDialog(owner, ref result, msg, title, usePasswordChar, remarks, validateOnEnter, validate, str =>
            {
                if (string.IsNullOrWhiteSpace(str))
                    return null;
                if (long.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out long rr))
                    return rr;
                throw new UserUnderstandableException("Not a real number!", null);
            });
        }

        /// <summary>See other overload</see></summary>
        public static DialogResult ShowDialog(IWin32Window owner, ref decimal? result, string msg, string title = null, bool usePasswordChar = false, string remarks = null, bool validateOnEnter = false, Func<decimal?, string> validate = null)
        {
            return GenericShowDialog(owner, ref result, msg, title, usePasswordChar, remarks, validateOnEnter, validate, str =>
            {
                if (string.IsNullOrWhiteSpace(str))
                    return null;
                if (decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal rr))
                    return rr;
                throw new UserUnderstandableException("Not a real number!", null);
            });
        }

        static DialogResult GenericShowDialog<T>(IWin32Window owner, ref T result, string msg, string title, bool usePasswordChar, string remarks, bool validateOnEnter, Func<T, string> validate, Func<string, T> convert)
        {
            var frm = new InputBox() { _convert = str => convert(str), _validate = obj => validate?.Invoke((T)obj) };
            frm.Text = title ?? Application.ProductName;
            frm.lblRemark.Text = remarks ?? " "; // we use " " otherwise DevExpress displays the name of the control

            frm.lueMemo_LayoutItem.Text = frm.lueText_LayoutItem.Text = msg;
            (usePasswordChar ? frm.lueMemo_LayoutItem : frm.lueText_LayoutItem).Visibility = LayoutVisibility.Never;
            frm.mmoValue.Text = frm.txtValue.Text = result == null ? "" : result.ToString();
            frm.txtValue.Properties.PasswordChar = '*';
            if (validateOnEnter)
            {
                void onKeyDown(object _, KeyEventArgs e)
                {
                    if (e.KeyCode == Keys.Enter)
                    {
                        e.Handled = true;
                        frm.btnSelect.PerformClick();
                    }
                }
                frm.mmoValue.KeyDown += onKeyDown;
                frm.txtValue.KeyDown += onKeyDown;
            }

            frm.FitToContent();

            var status = frm.ShowDialog(owner);
            if (status == DialogResult.OK)
                result = (T)frm._result;
            return status;
        }
        

        protected InputBox() // protected for VS designer
        {
            InitializeComponent();
        }
        object               _result;
        Func<string, object> _convert;
        Func<object, string> _validate;

        // Resize the form to fit the content
        void FitToContent()
        {
            var sizeBefore = ClientSize;
            var size = layoutControl1.GetPreferredSize(Minimum);
            ClientSize = size;
            var loc = Location;
            loc.Offset((sizeBefore.Width - ClientSize.Width) / 2,
                       (sizeBefore.Height - ClientSize.Height) / 2);
            Location = loc;
        }
        static readonly Size Minimum = new Size(1, 1);

        void btnSelect_Click(object sender, EventArgs e)
        {
            BaseEdit be = lueMemo_LayoutItem.Visibility == LayoutVisibility.Always ? mmoValue : txtValue;
            string invalidReason;
            try
            {
                var value = _convert(be.Text);
                invalidReason = _validate?.Invoke(value);
                if (string.IsNullOrWhiteSpace(invalidReason))
                {
                    _result = value;
                    DialogResult = DialogResult.OK;
                    return;
                }
            }
            catch (Exception ex)
            {
                if (!(ex is IUserUnderstandableException))
                    ExceptionManager.Instance.NotifyException(ex, UnexpectedExceptionManager.eExceptionKind.Ignored);
                invalidReason = ex.Message;
            }
            lblInvalidReason.Text = invalidReason;
            lblInvalidReason.Visibility = LayoutVisibility.Always;
            FitToContent();
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
