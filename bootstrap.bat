@echo off

setlocal 
set nopause=0

:parsing_arg_loop
      :: Thanks to https://stackoverflow.com/a/34552964
      ::-------------------------- has argument ?
	  :: in %~1 - the ~ removes any wrapping " or '.
      if ["%~1"]==[""] (
        goto parsing_arg_end
      )
      ::-------------------------- argument exist ?
	  if ["%~1"]==["--nopause"] set nopause=1

      ::--------------------------
      shift
      goto parsing_arg_loop


:parsing_arg_end


echo.
echo  ** Loading git submodule (if any)
echo git submodule update --init --recursive
git submodule update --init --recursive

REM cd SubProjectifNeeded
REM call bootstrap.bat --nopause
REM cd ..

echo ** Make all git repositories "standalone" **
"Git Submodule undo absorbgitdirs (Gui).exe" --noGui
echo ** Generating "No DX" version of solution/project files **
"Generate No DX solution files (Gui).exe" --noGui

REM If there is diamond dependency between projects, for example <this project> => A => M and <this project> => B => M
REM It is simpler to manage only one version of M on hard drive and make A and B deal with it... (but only if "undo absorbgitdirs" is done)
REM IF NOT EXIST "Dependencies\B\Dependencies\M" (
REM   echo.
REM   echo ** Create symlink needed by nuget in some submodules so solution can compile
REM   echo mklink /j "Dependencies\B\Dependencies\M" "Dependencies\A\Dependencies\M"
REM        mklink /j "Dependencies\B\Dependencies\M" "Dependencies\A\Dependencies\M"
REM )

REM Any nested project that has nuget dependencies, the outer project don't references, will cause problems at compile time
REM Because Visual studio expects to have only one "packages" folder at the root of solution.
REM To keep project non-dependant, we create this symlink.
REM IF NOT EXIST "Dependencies\M\packages" (
REM   echo.
REM   echo ** Create symlink needed by nuget in some submodules so solution can compile
REM   echo mklink /j "Dependencies\M\packages" "packages"
REM        mklink /j "Dependencies\M\packages" "packages"
REM )

if ["%nopause%"] == ["0"] (
  echo DONE! Press any key to leave this script
  pause
)
endlocal
