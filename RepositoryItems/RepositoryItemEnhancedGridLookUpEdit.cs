﻿using System;
using System.ComponentModel;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;


namespace TechnicalTools.UI.DX.RepositoryItems
{
    // The attribute that points to the registration method 
    [UserRepositoryItem(nameof(EnhancedGridLookUpEditName))]
    public class RepositoryItemEnhancedGridLookUpEdit : RepositoryItemGridLookUpEdit
    {
        // Static constructor should call registration method
        static RepositoryItemEnhancedGridLookUpEdit()
        {
            RegisterCustomGridLookUpEdit();
        }
        public RepositoryItemEnhancedGridLookUpEdit() : base()
        {
        }

        // Unique name for custom control
        public static readonly string EnhancedGridLookUpEditName = nameof(EnhancedGridLookUpEdit);
        public override string EditorTypeName
        {
            get
            {
                return EnhancedGridLookUpEditName;
            }
        }

        public static void RegisterCustomGridLookUpEdit()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EnhancedGridLookUpEditName, typeof(EnhancedGridLookUpEdit), typeof(RepositoryItemEnhancedGridLookUpEdit), typeof(GridLookUpEditBaseViewInfo), new ButtonEditPainter(), true));
        }

        protected override DevExpress.XtraGrid.GridControl CreateGrid()
        {
            return new EnhancedGridControl();
        }

        protected override DevExpress.XtraGrid.Views.Base.ColumnView CreateViewInstance()
        {
            switch (ViewType)
            {
                case GridLookUpViewType.Default:
                case GridLookUpViewType.GridView:
                    {
                        var v = new EnhancedGridView();
                        v.CopyPaste_Unconfigure(); // otherwise user click does not select row
                        return v;
                    }
                case GridLookUpViewType.BandedView:
                    {
                        var v = new EnhancedBandedGridView();
                        v.CopyPaste_Unconfigure(); // otherwise user click does not select row
                        return v;
                    }
                //case GridLookUpViewType.AdvBandedView:
            }
            return base.CreateViewInstance();
        }
    }

    internal class EnhancedGridLookUpEdit : GridLookUpEdit
    {
        static EnhancedGridLookUpEdit()
        {
            RepositoryItemEnhancedGridLookUpEdit.RegisterCustomGridLookUpEdit();
        }

        public override string EditorTypeName
        {
            get
            {
                return RepositoryItemEnhancedGridLookUpEdit.EnhancedGridLookUpEditName;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemEnhancedGridLookUpEdit Properties
        {
            get
            {
                return base.Properties as RepositoryItemEnhancedGridLookUpEdit;
            }
        }
    }

}
