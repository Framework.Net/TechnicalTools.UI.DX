﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Model;
using TechnicalTools.UI.DX.Helpers;


namespace TechnicalTools.UI.DX
{
    public partial class GenericGridViewerForm : EnhancedXtraForm
    {
        readonly object _datasource;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public GridView View { get { return gvData; } }

        public GenericGridViewerForm(object datasource = null)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            _datasource = datasource;
            gvData.BestFitMaxRowCount = 100;
        }

        private void GenericGridViewerForm_Load(object sender, EventArgs e)
        {
            CenterToParent();
            var datasource = _datasource is IList lst ? lst 
                           : _datasource is IEnumerable enumerable ? enumerable.Cast<object>().ToList()
                           : throw new TechnicalException("Unhandled datasource!", null);
            // Here is the trick : Gridview use the first row object to identify all column types
            // So we do the same but we maximise the chance to get relevant result
            var typeWithLotOfProperties = datasource.Cast<object>().NotNull()
                                                    .Take(gvData.BestFitMaxRowCount)
                                                    .Select(obj => obj.GetType())
                                                    .Distinct()
                                                    // a type with of hight level of inheritance is expected to have more properties than other
                                                    .OrderByDescending(t => t.GetLevelOfHierarchy()) 
                                                    .FirstOrDefault();
            if (typeWithLotOfProperties != null)
                gvData.PopulateColumnsFromDataAnnotations(typeWithLotOfProperties);
            gcData.DataSource = _datasource;
            gvData.BestFitColumns();

            SetCustomizedFormMenuItems(Tuple.Create("Rename", (Action)(() =>
            {
                string newText = Text;
                if (DialogResult.OK == InputBox.ShowDialog(this, ref newText, "Enter the new name of this window", "Changing window's name..."))
                    Text = newText;
            })));
        }
    }
}
