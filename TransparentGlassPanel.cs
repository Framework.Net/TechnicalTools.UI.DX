﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.UI;


namespace TechnicalTools.UI.DX
{
    // TODO : A mettre ne confrontation avec les techniques suivantes : 
    // http://www.c-sharpcorner.com/article/making-transparent-controls-using-gdi-and-C-Sharp/
    // http://www.c-sharpcorner.com/article/making-transparent-controls-using-gdi-and-C-Sharp/
    /// <summary>
    /// Equivalent de TransparentPanel mais géré en faisant nous meme le boutot (ie : demandant a la forme de se redessiner sans les TransparentGlassPanel) 
    /// et se servir de cette image comme d'un fond par defaut pour peindre le controle
    /// Probleme de cette classe :
    ///
    /// - Lorsque les panel bouge, le decors (texte, control, etc), en fait totue l'image de la form utilisé pour dessiner le fond, semble trembler
    ///   Plutot moche  
    ///   Je pense, a confirmer, que cela s'explique parce windows bouge d'abord l'image du composant avant de demande un invalidate)
    ///   
    /// - Soit des TransparentGlassPanel 1, 2 et 3, Si 1 est inclu dans 2, et que 3 est au dessus de 1 (et donc de 2) alors 
    ///   quand 1 bouge le fond de 3 n'est pas updaté. Il faut donc invlaider tous les panels de celui le plus au fond a celui le plus en haut.
    ///   
    /// - Des fois les TransparentGlassPanel apparaissent avec une opacité complete dans le designer (c'est pas trop grave a la limite)
    /// 
    /// - Si l'utilisateur selectionne un controle derriere un transparente panel (par exemple un textbox) et interagit avec, les modificaitons visuel (le texte) ne sont pas reflete sur l'image du transparente panel
    /// 
    /// A noter que le deplacement de la partie d'un TransparentGlassPanel situé sous un autre TransparentGlassPanel ont un mouvement fluide ! Le decors ne tremble pas
    ///
    /// Pour finir il y a sans doute des améliorations à faire, certain des probleme pouvant etre resolu avec de la patience et des tests
    /// </summary>
    [DesignTimeVisible(true)]
    public partial class TransparentGlassPanel : BaseUserControl
    {
        #region Properties

        [DefaultValue(0)]
        [Category("My")]
        [Browsable(false)]
        public byte Opacity      { get { return _Opacity; } set { _Opacity = value; RebuildOpacityBrush(); } }
               byte _Opacity = 0;

        [DefaultValue(255)]
        [Category("My")]
        public byte Transparency { get { return (byte)(255 - Opacity); } set { Opacity = (byte)(255 - value); } }

        #region Only BackColor
        [Category("My")]
        public override Color BackColor { get { return base.BackColor; } set { base.BackColor = value; } }
        // 3 Methode magiques que le designer regarde afin de bien fonctionner (dans ce type de cas c'est mieux que DefaultValue]
        // CF : http://stackoverflow.com/a/20838280/685341
        // a method that should return false if the value of the property should not be persisted.
        [EditorBrowsable(EditorBrowsableState.Never)]
        private bool ShouldSerializeBackColor() { return !BackColor.Equals(DefaultBackColor); }
        // a method that can run when the user selects the Reset context menu item (deja definit dans la classe parent)
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //private void ResetBackColor() { base.ResetBackColor(); BackColor = DefaultBackColor; }
        // A property with just a getter that returns the default value (deja definit dans la classe parent)
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //private static Color DefaultBackColor { get { return System.Windows.Forms.Control.DefaultBackColor; } }
        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            RebuildOpacityBrush();
        }
        void RebuildOpacityBrush()
        {
            if (_opacityBrush != null)
                _opacityBrush.Dispose();
            _opacityBrush = new SolidBrush(Color.FromArgb(Opacity, BackColor.R, BackColor.G, BackColor.B));
            Invalidate();
        }
        SolidBrush _opacityBrush;
        #endregion Only BackColor
        #endregion Properties

        public TransparentGlassPanel()
        {
            InitializeComponent();
            BackColor = System.Windows.Forms.Control.DefaultBackColor;
            DoubleBuffered = true; // evite le clignotement
            RebuildOpacityBrush();
            InitMovingMngmt();

            ParentChanging += ParentMngmt_ParentChanging;
            Disposed += ParentMngmt_Disposed;
            Disposed += OtherControl_Disposed;
        }


        static Dictionary<Control, List<TransparentGlassPanel>> _instance_parents = new Dictionary<Control, List<TransparentGlassPanel>>();

        // TODO : Pour gerer la superpostion des classes, il faudrait : 
        // - stocker toutes les instances (static List<GlassLayer> ...)
        // - Lors d'un draw d'un glass, trouver tous les autres GlassLayer du meme parent
        // Et faire en sorte qu'ils soient dessine dans l'ordre de celui qui est le plus en 
        //    dessous (zindex >> 0) a celui qui est top most (z-index proche de 0).
        //   (pour recuperer les zindex, utiliser GetChildIndex (http://msdn.microsoft.com/en-us/library/1fz293fh.aspx))
        // Pour tester, mettre deux instances Arrow l'une sur l'autre et bouger celle en dessous pour voir si celle au dessus prend en compte son image.

        protected override void OnMove(EventArgs e)
        {
            Invalidate(); // A effectuer après d'autres controles qui aurait pu s'inscrire à l'evenement "Move"
            base.OnMove(e);
            Refresh();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            Invalidate(); // A effectuer après d'autre controles qui aurait pu s'inscrire à l'evenement "Resize"
        }

        #region Gestion du parent
        //
        // S'effectue après d'autre controles qui aurait pu s'inscrire à l'evenement "ParentChanged"
        void ParentMngmt_ParentChanging(object sender, ParentChangingEventArgs e)
        {
            if (_drawing)
                return;
            if (e.OldParent != null)
            {
                e.OldParent.Resize -= ParentMngmt_Resize;
                e.OldParent.ControlAdded -= ParentMngmt_ControlAdded;
                e.OldParent.ControlRemoved -= ParentMngmt_ControlRemoved;

                foreach (Control ctrl in e.OldParent.Controls)
                    ParentMngmt_ControlRemoved(e.OldParent, new ControlEventArgs(ctrl));

                _instance_parents[e.OldParent].Remove(this);
                if (_instance_parents[e.OldParent].Count == 0)
                    _instance_parents.Remove(e.OldParent);
            }

            if (Parent != null)
            {
                Parent.Resize += ParentMngmt_Resize;
                Parent.ControlAdded += ParentMngmt_ControlAdded;
                Parent.ControlRemoved += ParentMngmt_ControlRemoved;

                foreach (Control ctrl in Parent.Controls)
                    if (ctrl != this)
                        ParentMngmt_ControlAdded(Parent, new ControlEventArgs(ctrl));
                if (!_instance_parents.ContainsKey(Parent))
                    _instance_parents.Add(Parent, new List<TransparentGlassPanel>());
                _instance_parents[Parent].Add(this);

                // TODO ajouter un event invalidated sur tous les control de parent
                Form form = FindForm();
                if (form != null) // est a null lorsque this est mis dans son container mais que son container n'est pas encore dans la form
                    form.Load += Form_Load;

                Invalidate();
            }
        }
        void ParentMngmt_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        // Gere aussi les appels a BringToFront et SendToBack
        void ParentMngmt_ControlAdded(object sender, ControlEventArgs e)
        {
            if (e.Control == this)
                return;
            if (_drawing)
                return;
            OtherControl_AddAsBrotherControl(e.Control);
        }

        // Gere aussi les appels a BringToFront et SendToBack
        void ParentMngmt_ControlRemoved(object sender, ControlEventArgs e)
        {
            if (e.Control == this)
                return;
            if (_drawing)
                return;
            OtherControl_RemoveAsBrotherControl(e.Control);
        }



        void Form_Load(object sender, EventArgs e)
        {
            Invalidate();
            (sender as Form).Load -= Form_Load;
        }

        void ParentMngmt_Disposed(object sender, EventArgs e)
        {
            Parent = null; // Provoque du code necessaire
        }

        #endregion Gestion du parent

        #region Gestion des autres controles

        void OtherControl_AddAsBrotherControl(Control ctrl)
        {
            SubscribeToControlEvent(ctrl);
            OtherControl_OnAnyMoveOrSizeChange(ctrl, EventArgs.Empty);
        }
        void SubscribeToControlEvent(Control ctrl)
        {
            //UnsubscribeToControlEvent(ctrl); // to be sure
            if (!_ctrls_old_bounds.ContainsKey(ctrl))
                _ctrls_old_bounds.Add(ctrl, new BoundReference() { Bounds = ctrl.Bounds });
            _ctrls_old_bounds[ctrl].ReferencingInstances.Add(this);

            ctrl.Move += OtherControl_OnAnyMoveOrSizeChange;
            ctrl.Resize += OtherControl_OnAnyMoveOrSizeChange;
            ctrl.Disposed += OtherControl_Disposed;
        }

        void OtherControl_RemoveAsBrotherControl(Control ctrl)
        {
            UnsubscribeToControlEvent(ctrl, false);
            OtherControl_OnAnyMoveOrSizeChange(ctrl, EventArgs.Empty);
            // If ctrl is a TransparentGlassPanel and have been remove from parent Control, the ParentChange event has been fired 
            // causing ctrl to be removed in _ctrls_old_bounds
            if (_ctrls_old_bounds.ContainsKey(ctrl) &&
                _ctrls_old_bounds[ctrl].ReferencingInstances.Count == 0)
                _ctrls_old_bounds.Remove(ctrl);
        }
        void UnsubscribeToControlEvent(Control ctrl, bool remove_ctrl_if_no_reference = true)
        {
            if (!_ctrls_old_bounds.ContainsKey(ctrl))
                return;
            ctrl.Move -= OtherControl_OnAnyMoveOrSizeChange;
            ctrl.Resize -= OtherControl_OnAnyMoveOrSizeChange;
            ctrl.Disposed -= OtherControl_Disposed;
            if (ctrl is TransparentGlassPanel)
                (ctrl as TransparentGlassPanel).OnBackgroundRedrawn -= TransparentGlassPanel_OnBackgroundRedrawn;

            _ctrls_old_bounds[ctrl].ReferencingInstances.Remove(this);
            if (remove_ctrl_if_no_reference && 
                _ctrls_old_bounds[ctrl].ReferencingInstances.Count == 0)
                _ctrls_old_bounds.Remove(ctrl);
        }

        void OtherControl_OnAnyMoveOrSizeChange(object sender, EventArgs e)
        {
            var ctrl = sender as Control;
            // L'autre control est ou a ete sur ou en dessous la zone du TransparentGlassPanel actuel
            if (Bounds.IntersectsWith(ctrl.Bounds) || Bounds.IntersectsWith(_ctrls_old_bounds[ctrl].Bounds))
                Invalidate();
            // If ctrl is a TransparentGlassPanel and have been remove from parent Control, the ParentChange event has been fired 
            // causing ctrl to be removed in _ctrls_old_bounds
            if (_ctrls_old_bounds.ContainsKey(ctrl)) 
                _ctrls_old_bounds[ctrl].Bounds = ctrl.Bounds; // TODO
        }

        void OtherControl_Disposed(object sender, EventArgs e)
        {
            OtherControl_RemoveAsBrotherControl(sender as Control);
        }

        void OtherControl_ThisIsDisposed(object sender, EventArgs e)
        {
            foreach (var kvp in _ctrls_old_bounds)
                if (kvp.Key.Parent == OldParent)
                    UnsubscribeToControlEvent(kvp.Key);
            Debug.Assert(_ctrls_old_bounds.Values.All(br => !br.ReferencingInstances.Contains(this)));
        }

        void TransparentGlassPanel_OnBackgroundRedrawn(object sender, EventArgs e)
        {
            Debug.Assert((sender as Control).Parent == Parent);
            OtherControl_OnAnyMoveOrSizeChange(sender, e);
        }

        class BoundReference
        {
            public Rectangle Bounds;
            public List<TransparentGlassPanel> ReferencingInstances = new List<TransparentGlassPanel>();
        }
        static Dictionary<Control, BoundReference> _ctrls_old_bounds = new Dictionary<Control, BoundReference>();

        #endregion Gestion des autres controles

        #region Gestion du raffraichissement du background

        public /*protected */event EventHandler OnBackgroundRedrawn;

        //// Get Other transparent glass layer, in the same parent container,
        //// that cover partially or not the current transparent Glass layer, ordered by Z-index (top most first)
        List<TransparentGlassPanel> GetOverlappingOtherGlassLayer(Control parent, Rectangle bounds, Rectangle old_bounds)
        {
            List<TransparentGlassPanel> lst = new List<TransparentGlassPanel>();
            if (parent != null)
                lst.AddRange(parent.Controls.OfType<TransparentGlassPanel>()
                                        .Where(c => bounds.IntersectsWith(c.Bounds) || old_bounds.IntersectsWith(c.Bounds))
                                        .OrderBy(tgl => tgl.Parent.Controls.GetChildIndex(tgl))); // get others from top most to bottom (zindex= 0 => topmost)
            return lst;
        }

        List<DrawSettings> FindAllGlassLayerToUpdate(Control ctrl, Rectangle old_Bounds)
        {
            var queue = new List<TransparentGlassPanel>();
            var results = new List<DrawSettings>();

            int i = 0;
            while (true)
            {
                foreach (var tgl in GetOverlappingOtherGlassLayer(ctrl.Parent, ctrl.Bounds, old_Bounds))
                    if (!queue.Contains(tgl))
                        queue.Add(tgl);
                if (i >= queue.Count)
                    break;
                ctrl = queue[i];
                var oldParent = ((TransparentGlassPanel)ctrl).OldParent;
                old_Bounds = ((TransparentGlassPanel)ctrl).OldBounds;
                
                if (ctrl.Parent != null)
                    results.Add(new DrawSettings()
                        {
                            TGL = (TransparentGlassPanel)ctrl,
                            Parent = ctrl.Parent,
                            Zindex = ctrl.Parent.Controls.GetChildIndex(ctrl)
                        });
                ++i;
            }
            return results;
        }

        class DrawSettings
        {
            public TransparentGlassPanel TGL;
            public Control Parent;
            public int Zindex;
        }



        public Bitmap _background;
        static bool _drawing;

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
            if (_drawing)
                return;
            if (_background != null)
                _background.Dispose();
            _background = null;
        }

        // To override, see Draw(...) !
        protected sealed override void OnPaintBackground(PaintEventArgs e)
        {
            // C'est vraiment étrange de devoir dessiner dans OnPaintBackground plutot que OnPaint.
            // Mais sans ça le control n'est pas bien redessiné (BackColor est affiché sans transparence sans raison !)
            // ON affiche dans le cas contraite de OnPaint
            if (_drawing)
                DrawBackground(e.Graphics, e);
        }
        // To override, see Draw(...) !
        protected sealed override void OnPaint(PaintEventArgs e)
        {
            // Evite la recursivité puisque Draw fait appel à GetBackgroundImage qui va, via Parent.DrawToBitmap(...)
            // refaire des appels à OnPaint et OnPaintBackgroud
            if (!_drawing) 
                DrawBackground(e.Graphics, e);
        }

        /// <summary>
        /// La méthode qui dessine renvoie simplement le fond bufferisé
        /// </summary>
        protected void DrawBackground(Graphics g, PaintEventArgs e)
        {
            if (GetBackgroundImage() == null)
                return;

            g.DrawImage(GetBackgroundImage(), new Rectangle(Point.Empty, Size), new Rectangle(ClientLocation.Add(Location), Size), GraphicsUnit.Pixel);
        }

        // Méthode complexe qui gère le calcul de l'image et les autres instances de TransparentGlassPanel qui sont superposé (Bounds en collision)
        Bitmap GetBackgroundImage()
        {
            if (Parent == null)
                return null;
            if (_background != null)
                return _background;
            try
            {
                _drawing = true; // Empeche les autres appels a GetBackgroundImage (recursivite)

                var draw_settings = FindAllGlassLayerToUpdate(this, this.Bounds)
                                    .OrderByDescending(ds => ds.Zindex) // du control le plus bas (zindex grand) au control le plus haut (zindex proche de 0)
                                    .ToList();
                Debug.Assert(draw_settings.All(tgl => tgl.Parent == this.Parent));

                this.SuspendLayout(); // Evite le clignotement
                this.Parent.SuspendDrawing();

                // On supprime les TGL des parent (leur zindex sont sauvegardé dans DrawSettings) puis on les remettra apres
                // Ce petit stratageme permet que le DrawToBitmap fontionne quand "this" est dans un panel avec un BackColor different de sa valeur par defaut
                // Il permet egalement que lorsque 2 ou plusieurs TGL se superposent ils puisse se dessiner sur une form vierge avec les autre TGL dans le bon ordre
                foreach (DrawSettings ds in draw_settings)
                    ds.TGL.Parent.Controls.Remove(ds.TGL);

                int count = draw_settings.Count;
                var results = draw_settings.ForEachIgnoreException(ds =>
                {
                    --count;
                    var bg = new Bitmap(ds.Parent.Size.Width, ds.Parent.Size.Height);
                    ds.Parent.DrawToBitmap(bg, new Rectangle(Point.Empty, ds.Parent.Size));

                    ds.TGL._background = bg;
                    var region_of_buffer_to_draw = new Rectangle(ClientLocationFor(ds.Parent).Add(ds.TGL.Location), ds.TGL.Size);
                    using (var g = Graphics.FromImage(ds.TGL._background))
                        ds.TGL.Draw(g, region_of_buffer_to_draw);

                    ds.Parent.Controls.Add(ds.TGL);
                    ds.Parent.Controls.SetChildIndex(ds.TGL, ds.Zindex - count);
                });
                Debug.Assert(results.All(res => res.Exception == null));

                this.Parent.ResumeDrawing();
                this.Parent.ResumeLayout(); // Evite le clignotement
            }
            catch// (Exception ex)
            {
                DebugTools.Break();
                // Quand Size est de taille 1 x 1, on obtient ici une exception de type 
                //System.ArgumentException: Parameter is not valid
                // Cette erreur viendrait des profondeur de GDI+ ... Aucune idée de pourquoi !
                // Les erreurs GDI sont réputées pour ne pas etre precise
            }
            _drawing = false;
            OnBackgroundRedrawn?.Invoke(this, EventArgs.Empty);
            return _background;
        }

        [Obsolete("Utiliser la methode Draw à la place et dessinez bien dans la region indiqué en argument !", true)]
        public new event PaintEventHandler Paint { add { throw new NotImplementedException(); } remove { throw new NotImplementedException(); } }

        /// <summary>
        /// Methode de base affichant la couleur en transparence sur l'image en cache
        /// </summary>
        /// <param name="clientLocation">Zone du buffer qui represente l'integralité de l'espace du controle. Ne dessinerp as a l'exterieur ca ne sera jamais visible</param>
        protected virtual void Draw(Graphics g, Rectangle region_to_draw)
        {
            if (Opacity > 0)
            { 
                var mode = g.CompositingMode;
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                g.FillRectangle(_opacityBrush, region_to_draw);
                g.CompositingMode = mode;
            }
            // Le premier argument a passer n'est pas sûr son nom est "key"! 
            // D'apres la doc MSDN c'est a nous d'appeller RaisePaintEvent si on appelle pas base.OnPaint
            // L'evenement Paint est bloqué mais pas l'evenement de base
            #if DEBUG
            //this.CheckEventHasNoSubscribers("Paint", string.Format("La specs de la classe {0} n'autorise pas l'evenement Paint", this.GetType().Name));
            #else
            RaisePaintEvent(this, new PaintEventArgs(g, region_to_draw));  // C'est mieux que rien
            #endif
        }
        #endregion Gestion du raffraichissement du background


        #region Deplacement

        void InitMovingMngmt()
        {
            MouseDown += TransparentGlassPanel_MouseDown;
            MouseUp += TransparentGlassPanel_MouseUp;
            MouseMove += TransparentGlassPanel_MouseMove;
        }

        bool _moving;
        Point _MouseClickLocation;
        void TransparentGlassPanel_MouseDown(object sender, MouseEventArgs e)
        {
            _moving = !DesignTimeHelper.IsInDesignMode;
            _MouseClickLocation = e.Location;
        }

        void TransparentGlassPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (_moving)
            {
                Location = Location.Add(e.Location.Substract(_MouseClickLocation));
            }
        }

        void TransparentGlassPanel_MouseUp(object sender, MouseEventArgs e)
        {
            _moving = false;
        }

        #endregion 


        #region 
        // Pour simuler le click : 
        /*
        public const uint WM_LBUTTONDOWN = 0x0201;
        public const uint WM_LBUTTONUP = 0x0202;
        [DllImport("coredll.dll")] // for compact framework.. Desktop will be user32.dll
        public static extern int SendMessage(
        IntPtr hWnd, // handle to destination window
        uint Msg, // message
        Int32 wParam, // first message parameter
        Int32 lParam // second message parameter
        );
        static void SendMouseClick(IntPtr p_iHandle, int p_x, int p_y)
        {
            Int32 l_parm1 = (p_y << 16) | (p_x & 0xffff);
            SendMessage(p_iHandle, WM_LBUTTONDOWN, 0, l_parm);
            SendMessage(p_iHandle, WM_LBUTTONUP, 0, l_parm);
        }
        */
        #endregion
    }
}

