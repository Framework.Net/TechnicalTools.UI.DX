﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX.Forms
{
    public static class NonblockingXtraMessageBox
    {
        public static DialogResult Show(IWin32Window owner, string text, string caption = "")
        {
            Debug.Assert(MessageBoxIconToIcon != null, nameof(MessageBoxIconToIcon) + " != null");
            Debug.Assert(MessageBoxDefaultButtonToInt != null, nameof(MessageBoxDefaultButtonToInt) + " != null");

            var args = new XtraMessageBoxArgs(null, owner, text, caption, new[] { DialogResult.OK },
                                              (Icon)MessageBoxIconToIcon.Invoke(null, new object[] { MessageBoxIcon.None }),
                                              (int)MessageBoxDefaultButtonToInt.Invoke(null, new object[] { MessageBoxDefaultButton.Button1 }));
            return GetMessageBoxForm(args)
                    .ShowMessageBoxDialog(args); // will call DoShowDialog
        }
        static readonly Type XtraMessageHelperType = typeof(XtraMessageBox).Assembly.GetType("DevExpress.XtraEditors.XtraMessageHelper");
        static readonly MethodInfo CanUseNativeForm = XtraMessageHelperType.GetMethod("CanUseNativeForm", BindingFlags.NonPublic | BindingFlags.Static);
        static readonly MethodInfo MessageBoxIconToIcon = XtraMessageHelperType.GetMethod("MessageBoxIconToIcon", BindingFlags.NonPublic | BindingFlags.Static);
        static readonly MethodInfo MessageBoxDefaultButtonToInt = XtraMessageHelperType.GetMethod("MessageBoxDefaultButtonToInt", BindingFlags.NonPublic | BindingFlags.Static);
        static XtraMessageBoxForm GetMessageBoxForm(XtraMessageBoxArgs message)
        {
            Debug.Assert(CanUseNativeForm != null, nameof(CanUseNativeForm) + " != null");
            if ((bool)CanUseNativeForm.Invoke(null, new object[] { message }))
                return new NonBlockingNativeMessageBoxForm();
            return new NonBlockingXtraMessageBoxForm();
        }
    }
    public class NonBlockingNativeMessageBoxForm : NativeMessageBoxForm
    {
        protected override DialogResult DoShowDialog(IWin32Window owner)
        {
            Closed += (_, __) => Dispose();
            Show(owner);
            return DialogResult.OK;
        }
    }
    public class NonBlockingXtraMessageBoxForm : XtraMessageBoxForm
    {
        public NonBlockingXtraMessageBoxForm()
        {
            StartPosition = FormStartPosition.CenterParent; // par default on centre la form
            Load += (_, __) => EnhancedXtraForm.CenterForm(this);
        }
        protected override DialogResult DoShowDialog(IWin32Window owner)
        {
            Closed += (_, __) => Dispose();
            foreach (var btn in Control_Extensions.AllSubControls(this).OfType<SimpleButton>())
                btn.Click += (_, __) => Close();
            Show(owner);
            return DialogResult.OK;
        }
    }
}
