﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data;

using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;


namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    // TODO : Afficher (lire les commentaire) : http://stackoverflow.com/questions/9404946/how-to-measure-memory-usage-with-c-sharp-as-we-can-do-in-java
    //  et relire http://stackoverflow.com/questions/2582967/what-is-the-difference-between-gc-gettotalmemoryfalse-and-gc-gettotalmemorytr
    public partial class FrmObjectTracking : DebugFormBase
    {
        protected class Entry //: INotifyPropertyChanged
        {
            public string    Namespace    { get { return (_type.Namespace ?? string.Empty).Replace(".Generated", string.Empty); } } // On cache "generated" au client car ils n'ont pas besoin de savoir que cela a été généré.
            public string    TypeName     { get { return _type.ToSmartString(); } }
            public uint      MaxCount     { get; private set; }
            public uint      DeltaMaxMin  { get { return MaxCount - MinCount; } }
            public uint      MinCount     { get; private set; }
            public uint?     DeltaNowMin  { get { return NowCount - MinCount; } }
            public uint?     NowCount     { get { return _history.Count == 0 ? null : (uint?)_history.First(); } }
            
            internal IReadOnlyList<uint> Measures { get { return _history; } }

            public Entry(Type type)
            {
                _type = type;
                MinCount = uint.MaxValue;
            }

            readonly Type _type;
            readonly List<uint> _history = new List<uint>();

            public void Add(uint count)
            {
                MaxCount = Math.Max(MaxCount, count);
                MinCount = Math.Min(MinCount, count);

                while (_history.Count > HistoryLength)
                    _history.RemoveAt(_history.Count-1);

                _history.Insert(0, count);

                //if (PropertyChanged != null)
                //{
                //    PropertyChanged(this, new PropertyChangedEventArgs(GetMemberName.For<Entry>(e => e.NowCount)));
                //    PropertyChanged(this, new PropertyChangedEventArgs(GetMemberName.For<Entry>(e => e.MaxCount)));
                //    PropertyChanged(this, new PropertyChangedEventArgs(GetMemberName.For<Entry>(e => e.MinCount)));
                //    PropertyChanged(this, new PropertyChangedEventArgs(GetMemberName.For<Entry>(e => e.Measures)));
                //}
            }

            public void ClearStatistics()
            {
                var last_added = _history.First();
                _history.Clear();
                _history.Add(last_added);
                MaxCount = last_added;
                MinCount = last_added;
            }

            //public event PropertyChangedEventHandler PropertyChanged;
        }

        static int HistoryLength { get; set; } = 120;

        enum eUpdateSpeed
        {
            // Equivalence en milliseconds (reprise des valeurs du Gestionnaire des tâches)
            High = 500, 
            Normal = 1000,
            Low = 4000,
            Paused = 0
        }

        readonly FixedBindingList<Entry> _entries = new FixedBindingList<Entry>();
        readonly Dictionary<Type, Entry> _entriesByType = new Dictionary<Type, Entry>();
        DateTime _LastMeasureTime;
        eUpdateSpeed _lastSpeed = eUpdateSpeed.Paused;

        protected FrmObjectTracking()
        {
            InitializeComponent();
            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            Disposed += (_, __) => tmrRefresh.Dispose();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            colNamespace.FieldName = GetMemberName.For<Entry>(e => e.Namespace);
            colTypeName.FieldName = GetMemberName.For<Entry>(e => e.TypeName);
            colMaxCount.FieldName = GetMemberName.For<Entry>(e => e.MaxCount);
            colDeltaMaxMin.FieldName = GetMemberName.For<Entry>(e => e.DeltaMaxMin);
            colMinCount.FieldName = GetMemberName.For<Entry>(e => e.MinCount);
            colDeltaNowMin.FieldName = GetMemberName.For<Entry>(e => e.DeltaNowMin);
            colNowCount.FieldName = GetMemberName.For<Entry>(e => e.NowCount);
            colValues.FieldName = "a"; // Il faut obligatoirement une chaine non vide qui ne represente pasune propriete existante
            //colValues.FieldName = GetMemberName.For<Entry>(e => e.Values); // car mode unbound, cf CustomDrawCell

            KeyPreview = true;
            KeyDown += FrmObjectTracking_KeyDown;
        }

        /// <summary>
        /// Pour la raison de cette méthode, voir <see cref="DebugFormBase.ShowInOtherGuiThread">base.Show</see>
        /// La fenetre se raffraichit régulièrement en cas de grosse action
        /// </summary>
        /// <returns>La fenêtre créé. Attention !!! Elle est dans un autre thread graphique !</returns>
        public static new Form Show()
        {
            return ShowInOtherGuiThread(() => new FrmObjectTracking(), typeof(FrmObjectTracking).Name);
        }

        void FrmObjectTracking_Load(object sender, EventArgs e)
        {
            lueUpdateSpeed.FillWithEnumValues<eUpdateSpeed>(false);
            lueUpdateSpeed.Properties.Columns[0].SortOrder = ColumnSortOrder.None;
            lueUpdateSpeed.SelectedValueAsEnumSet(eUpdateSpeed.Normal);

            chkTrackObjectInstances.Checked = BaseDTO.TrackInstances;
            gc.DataSource = _entries;
            seHistoryLength.Value = HistoryLength;
        }
        
        private void FrmObjectTracking_Shown(object sender, EventArgs e)
        {
            this.BeginInvoke((Action)btnRefreshNow.PerformClick);
        }

        void lueUpdateSpeed_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (lueUpdateSpeed.EditValue == null)
                return;
            _lastSpeed = lueUpdateSpeed.SelectedValueAsEnum<eUpdateSpeed>();
        }
        
        void lueUpdateSpeed_EditValueChanged(object sender, EventArgs e)
        {
            var speed = lueUpdateSpeed.SelectedValueAsEnum<eUpdateSpeed>();
            if (speed == eUpdateSpeed.Paused)
                tmrRefresh.Enabled = false;
            else
            {
                tmrRefresh.Interval = (int)speed;
                tmrRefresh.Enabled = true;
            }
            btnPause.Text = speed == eUpdateSpeed.Paused ? "Continue" : "Pause";
        }
        
        private void seHistoryLength_EditValueChanged(object sender, EventArgs e)
        {
            HistoryLength = (int)seHistoryLength.Value;
        }

        void btnClean_Click(object sender, EventArgs e)
        {
            GC.Collect(10, GCCollectionMode.Forced, true); // NOSONAR: Because the current view is a technical view. So this is exactly the purpose of the button
            GC.WaitForPendingFinalizers();
            foreach (var counter in ObjectTracker.Instance.Counters)
                counter.Clean();
        }

        private void btnRefreshNow_Click(object sender, EventArgs e)
        {
            CaptureAliveObjects();
            DisplayLastCapture();
        }
        

        private void btnPause_Click(object sender, EventArgs e)
        {
            var speed = lueUpdateSpeed.SelectedValueAsEnum<eUpdateSpeed>();
            if (speed == eUpdateSpeed.Paused)
                lueUpdateSpeed.SelectedValueAsEnumSet(_lastSpeed);
            else
                lueUpdateSpeed.SelectedValueAsEnumSet(eUpdateSpeed.Paused);
        }

        private void FrmObjectTracking_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
                btnRefreshNow.PerformClick();
            else if (e.KeyCode == Keys.Delete)
                btnClean.PerformClick();
        }

        void CaptureAliveObjects()
        {
            _LastMeasureTime = DateTime.Now;
            foreach (var counter in ObjectTracker.Instance.Counters)
            {
                bool isnew = false;
                if (!_entriesByType.TryGetValue(counter.Type, out Entry entry))
                {
                    entry = new Entry(counter.Type);
                    _entriesByType.Add(counter.Type, entry);
                    isnew = true;
                }
                entry.Add(counter.Count);
                if (isnew)
                    _entries.Add(entry);
            }
            _current = _current ?? Process.GetCurrentProcess();

            try { _WorkingSet            = _current.WorkingSet64;            } catch { _WorkingSet = null; }
            try { _WorkingSetPeak        = _current.PeakWorkingSet64;        } catch { _WorkingSetPeak = null; }
            try { _VirtualMemorySize     = _current.VirtualMemorySize64;     } catch { _VirtualMemorySize = null; }
            try { _VirtualMemorySizePeak = _current.PeakVirtualMemorySize64; } catch { _VirtualMemorySizePeak = null; }
            try { _PrivateMemorySize     = _current.PrivateMemorySize64;     } catch { _PrivateMemorySize = null; }
            try { _GCTotalMemory         = GC.GetTotalMemory(false);         } catch { _GCTotalMemory = null; }

        }
        Process _current;
        long? _WorkingSet;         long? _WorkingSetPeak;
        long? _VirtualMemorySize;  long? _VirtualMemorySizePeak;
        long? _PrivateMemorySize;  long? _GCTotalMemory;

        void DisplayLastCapture() // Utiliser DisplayLastCapture (le postponer)
        {
            lblLastRefreshTime.Text = _LastMeasureTime.ToString("HH'h' mm'm' ss's'");
            gv.RefreshData();

            if (_entries.Count <= 0 || _expand_first_time)
                return;
            // Fait en sorte d'ouvrir les groupes
            this.gv.OptionsBehavior.AutoExpandAllGroups = true;
            // Mais par la suite autorise l'utilisateur à les fermer
            // ==> Utile notemment pour debugger les objet d'un namespace en particulier (exemple : cacher le namespace trk et afficher trk.Script*)
            this.BeginInvoke((Action)(() => this.gv.OptionsBehavior.AutoExpandAllGroups = false));
            // Dans cet ecran, et étant donne le tri par defaut, les lignes interessantes sont en haut
            // Cette ligne permet de selectionner la premiere "Group Row" afin d'eviter que la grille ne scrolle automatiquement en bas au fur et a mesure que des lignes apparaissent.
            gv.FocusedRowHandle = -1;

            _expand_first_time = true;
            
            lblWorkingSet.Text = ToSizeString(_WorkingSet);
            lblWorkingSetPeak.Text = ToSizeString(_WorkingSetPeak);
            lblVirtualMemorySize.Text = ToSizeString(_VirtualMemorySize);
            lblVirtualMemorySizePeak.Text = ToSizeString(_VirtualMemorySizePeak);
            lblPrivateMemorySize.Text = ToSizeString(_PrivateMemorySize);
            lblGCTotalMemory.Text = ToSizeString(_GCTotalMemory);
        }
        bool _expand_first_time;
        string ToSizeString(long? value)
        {
            if (value == null)
                return "N.A.";
            decimal limit = 1;
            if (value < limit * 1024)
                return value.ToString() + "B";
            limit *= 1024;
            if (value < limit * 1024)
                return Math.Round(value.Value / limit, 3) + "KB";
            limit *= 1024;
            if (value < limit * 1024)
                return Math.Round(value.Value / limit, 3) + "MB";
            limit *= 1024;
            //if (value < limit * 1024)
                return Math.Round(value.Value / limit, 3) + "GB";
        }
        void tmrRefresh_Tick(object sender, EventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            btnRefreshNow.PerformClick();
        }

        private void btnClearStatistics_Click(object sender, EventArgs e)
        {
            foreach(var entry in _entries)
                entry.ClearStatistics();
            gv.RefreshData();
        }

        private void gv_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column == colValues)
                e.Appearance.BackColor = Color.Black;
        }
        private void gv_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column != colValues)
                return;
            if (!gv.IsDataRow(e.RowHandle))
                return;
            e.Handled = true;
            var entry = (Entry)gv.GetRow(e.RowHandle);
            if (entry.Measures.Count <= 1)
                return;
            
            //e.Appearance.FillRectangle(e.Graphics, e.Bounds);
            //e.DefaultDraw();
            const int dotWidth = 1;
            int width_interval = Math.Max(4, e.Bounds.Width / HistoryLength);
            int i = 0;
            Point before = Point.Empty;
            int h = e.Bounds.Height-1;
            if (entry.MaxCount != 0)
                foreach (uint measure in entry.Measures)
                {
                    var current = new Point(e.Bounds.Left + i * (dotWidth + width_interval),
                                            entry.MinCount == entry.MaxCount 
                                                    ? e.Bounds.Top + e.Bounds.Height/2
                                                    : e.Bounds.Top + (int)(h * (1 - ((double)measure - entry.MinCount) / entry.MaxCount)));
                    if (i != 0)
                        e.Graphics.DrawLine(Pens.Black, before, current);
                    before = current;
                    ++i;
                }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void chkTrackObjectInstances_CheckedChanged(object sender, EventArgs e)
        {
            BaseDTO.TrackInstances = chkTrackObjectInstances.Checked;
            EnhancedXtraUserControl.TrackInstances = chkTrackObjectInstances.Checked;
            EnhancedXtraForm.TrackInstances = chkTrackObjectInstances.Checked;
            EnhancedRibbonForm.TrackInstances = chkTrackObjectInstances.Checked;
        }
    }
}
