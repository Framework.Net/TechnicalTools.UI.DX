﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    public partial class FrmDebugDiagnostics : DebugFormBase
    {
        DefaultAssertionManager _manager;

        public FrmDebugDiagnostics(DefaultAssertionManager manager = null)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            _manager = manager ?? AssertionManager.Instance;
        }

        private void FrmDebugDiagnostics_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            InitTabPageAssertions();
            InitTabPageExceptions();
            InitTabPageSettings();
        }

        #region Exceptions

        protected virtual void InitTabPageExceptions()
        {
            // DefineEnabilitiesAndVisibilities
            btnTestException.Visible = Debugger.IsAttached;

            // CustomizeUI
            colExceptionKind.ConfigureForEnum<UnexpectedExceptionManager.eExceptionKind>();

            // DefineSimpleBindings
            colExceptionWhen      .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.When);
            colExceptionKind      .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.Kind);
            colExceptionMessage   .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.Exception)  + "." + nameof(Exception.Message);
            colExceptionStackTrace.FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.CallStack);
            colExceptionInfo      .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.Info);
            colExceptionFilePath  .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.FilePath);
            colExceptionFileName  .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.FileName);
            colExceptionLine      .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.Line);
            colExceptionColumn    .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.Column);
            colExceptionClassName .FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.ClassName);
            colExceptionMethodName.FieldName = nameof(UnexpectedExceptionManager.ExceptionInfo.MethodName);

            _datasource.Clear();
            lock (UnexpectedExceptionManager.UnexpectedExceptions)
            {
                _datasource.AddRange(UnexpectedExceptionManager.UnexpectedExceptions);
                UnexpectedExceptionManager.UnexpectedExceptions.ListChanged += UnexpectedExceptionsOnListChanged;
                Disposed += (_ ,__) => UnexpectedExceptionManager.UnexpectedExceptions.ListChanged -= UnexpectedExceptionsOnListChanged;
            }
            gcExceptions.DataSource = _datasource;
        }

        void UnexpectedExceptionsOnListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType.In(ListChangedType.ItemAdded, ListChangedType.ItemDeleted, ListChangedType.Reset))
            {
                this.BeginInvoke((Action)(() =>
                    {
                        gcExceptions.DataSource = null;
                        _datasource.Clear();
                        lock (UnexpectedExceptionManager.UnexpectedExceptions)
                            _datasource.AddRange(UnexpectedExceptionManager.UnexpectedExceptions);
                        gcExceptions.DataSource = _datasource;
                    }));
            }
        }

        readonly List<UnexpectedExceptionManager.ExceptionInfo> _datasource = new List<UnexpectedExceptionManager.ExceptionInfo>();

        private void btnTestException_Click(object sender, EventArgs e)
        {
            ExceptionManager.Instance.IgnoreException(() =>
            {
                throw new Exception("[Test] Exception ignored by developper ...");
            });
            throw new Exception("[Test] Unhandled exception in GUI thread...");
        }


        private void btnSendMail_Click(object sender, EventArgs e)
        {
            string filename = Path.Combine(Path.GetTempPath(), "[Bug] Exceptions " + DateTime.Now.ToString("yyyy'-'MM'-'dd' 'hh'.'mm'.'ss") + ".xlsx");
            //if (!ExecuteAndDisplayException("Generating debug file", () => ExportExceptionAndAssertionTo(filename)))
            //    return;

            if (!this.ShowBusyWhileDoingUIWorkInPlace("Preparing mail", pr => 
                {
                    throw new UserUnderstandableException("Not implemented yet!", null);
                    //var mail = new Mail();
                    //mail.AddAttachment(filename);
                    //mail.AddRecipientTo("dev@nexfi.fr");
                    ////mail.AddRecipientCc(System.DirectoryServices.AccountManagement.UserPrincipal.Current.EmailAddress);
                    //mail.SendMailPopup("[Bug] Exceptions " + DateTime.Now.ToString("yyyy'/'MM'/'dd' 'hh':'mm':'ss"), "Please see attached files.");
                }))
                return;
            
            // Si le mail n'a pas réussi à être envoyé
            saveFileDialog.FileName = Path.GetFileName(filename);
            saveFileDialog.Filter = "Open Excel | *.xlsx";
            saveFileDialog.DefaultExt = "xlsx";
            if (DialogResult.OK != saveFileDialog.ShowDialog())
                return;

            if (filename != saveFileDialog.FileName)
                this.ShowBusyWhileDoingUIWorkInPlace("Exporting debug file", pr => File.Move(filename, saveFileDialog.FileName));
        }

        //void ExportExceptionAndAssertionTo(string filename)
        //{
        //    var grids_to_export = new List<IPrintable>();
        //    var grids_names = new List<string>();
        //    if (UnexpectedExceptionManager.UnexpectedExceptions.Count != 0)
        //    {
        //        grids_to_export.Add(gcExceptions);
        //        grids_names.Add("Exceptions");
        //    }
        //    if (AssertionManager.FailedAssertions.Count != 0)
        //    {
        //        grids_to_export.Add(gcAssertions);
        //        grids_names.Add("Assertions");
        //    }
        //    if (grids_to_export.Count == 0)
        //        throw new FunctionalException("There is nothing to send !", null);

        //    ExcelExporter.ExportToExcelAsXlsx(filename, grids_to_export, grids_names, true);
        //}

        #endregion

        #region Assertions

        private void InitTabPageAssertions()
        {
            // DefineEnabilitiesAndVisibilities
            btnTestFailingAssertion.Visible = Debugger.IsAttached;

            // DefineSimpleBindings
            colAssertWhen           .FieldName = nameof(FailedAssertionInfo.When);
            colAssertMessage        .FieldName = nameof(FailedAssertionInfo.Message);
            colAssertDetailedMessage.FieldName = nameof(FailedAssertionInfo.DetailedMessage);
            colAssertStackTrace     .FieldName = nameof(FailedAssertionInfo.Info);
            colAssertFilePath       .FieldName = nameof(FailedAssertionInfo.FilePath);
            colAssertFilename       .FieldName = nameof(FailedAssertionInfo.FileName);
            colAssertLine           .FieldName = nameof(FailedAssertionInfo.Line);
            colAssertColumn         .FieldName = nameof(FailedAssertionInfo.Column);
            colAssertClassName      .FieldName = nameof(FailedAssertionInfo.ClassName);
            colAssertMethodName     .FieldName = nameof(FailedAssertionInfo.MethodName);

            gcAssertions.DataSource = _manager.FailedAssertions;

            //BindingManager.Instance.BindControl(chkAssertionDialogBoxDisabled, this, frm => frm.AssertionDialogBoxDisabled, chk => chk.Checked);

            _StoreFailedAssertions = _manager.StoreFailedAssertions;
            _manager.StoreFailedAssertions = true;
            Disposed += (_, __) => _manager.StoreFailedAssertions = _StoreFailedAssertions;
        }
        bool _StoreFailedAssertions;

        public bool AssertionDialogBoxDisabled
        {
            get { return _manager.MachineNameOnWhichFailedAssertionPopupIsDisabled.Contains(Environment.MachineName); }
            set
            {
                _manager.MachineNameOnWhichFailedAssertionPopupIsDisabled.Remove(Environment.MachineName);
                if (value)
                    _manager.MachineNameOnWhichFailedAssertionPopupIsDisabled.Add(Environment.MachineName);
            }
        }

        private void btnTestFailingAssertion_Click(object sender, EventArgs e)
        {
            Debug.Fail("[Test] A failing assertion...");
        }

        #endregion

        #region Settings
        private void InitTabPageSettings()
        {
            //BindingManager.Instance.BindControl(chkCaptureStackWhenDeferredEvent, this, frm => frm.CaptureStackTraceOnDeferredEvents, chk => chk.Checked);
            //BindingManager.Instance.BindControl(chkRunAsyncCodeSynchronously, this, frm => frm.RunAsyncCodeSynchronously, chk => chk.Checked);
            //BindingManager.Instance.BindControl(chkEnableCacheLogging, this, frm => frm.EnabledCacheLogging, chk => chk.Checked);
        }

        //public bool CaptureStackTraceOnDeferredEvents
        //{
        //    get { return DebugTools.CaptureStackTraceOnDeferredEvents; }
        //    set { DebugTools.CaptureStackTraceOnDeferredEvents = value; }
        //}

        //public bool RunAsyncCodeSynchronously
        //{
        //    get { return DebugTools.RunAsyncCodeSynchronously; }
        //    set { DebugTools.RunAsyncCodeSynchronously = value; }
        //}

        public bool EnabledCacheLogging
        {
            get { return _EnabledCacheLogging; }
            set
            {
                _EnabledCacheLogging = value;
                //CacheManager.Instance.GetDebugLogger().Enabled = _EnabledCacheLogging;
            }
        }
        bool _EnabledCacheLogging;

        private void chkUseSameCultureAsProductPole_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUseSameCultureAsProductPole.Checked)
            {
                CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CurrentCulture;
                CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.CurrentCulture;
            }
            else
            {
                CultureInfo.DefaultThreadCurrentCulture = OriginalDefaultThreadCurrentCulture;
                CultureInfo.DefaultThreadCurrentUICulture = OriginalDefaultThreadCurrentUICulture;
            }
        }

        static readonly CultureInfo OriginalDefaultThreadCurrentCulture = CultureInfo.DefaultThreadCurrentCulture;
        static readonly CultureInfo OriginalDefaultThreadCurrentUICulture = CultureInfo.DefaultThreadCurrentUICulture;

        protected virtual void btnSeeClassHierarchy_Click(object sender, EventArgs e)
        {
            var frm = new FrmDebugClassHierarchy();
            frm.Show(this);
        }

        #endregion
    }
}
