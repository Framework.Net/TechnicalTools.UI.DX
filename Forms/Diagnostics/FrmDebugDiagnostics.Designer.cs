namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    partial class FrmDebugDiagnostics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabs = new DevExpress.XtraTab.XtraTabControl();
            this.TabPageSettings = new DevExpress.XtraTab.XtraTabPage();
            this.chkEnableCacheLogging = new DevExpress.XtraEditors.CheckEdit();
            this.btnSeeClassHierarchy = new DevExpress.XtraEditors.SimpleButton();
            this.chkRunAsyncCodeSynchronously = new DevExpress.XtraEditors.CheckEdit();
            this.chkUseSameCultureAsProductPole = new DevExpress.XtraEditors.CheckEdit();
            this.chkCaptureStackWhenDeferredEvent = new DevExpress.XtraEditors.CheckEdit();
            this.TabPageExceptions = new DevExpress.XtraTab.XtraTabPage();
            this.btnSendMail = new DevExpress.XtraEditors.SimpleButton();
            this.btnTestException = new DevExpress.XtraEditors.SimpleButton();
            this.gcExceptions = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvExceptions = new TechnicalTools.UI.DX.EnhancedGridView();
            this.colExceptionWhen = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionKind = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionMessage = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.repoMemoEditException = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colExceptionDetailedMessage = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionFilePath = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionFileName = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionLine = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionColumn = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionClassName = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionMethodName = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionStackTrace = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colExceptionInfo = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.TabPageAssertions = new DevExpress.XtraTab.XtraTabPage();
            this.chkAssertionDialogBoxDisabled = new System.Windows.Forms.CheckBox();
            this.btnTestFailingAssertion = new DevExpress.XtraEditors.SimpleButton();
            this.gcAssertions = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvAssertions = new TechnicalTools.UI.DX.EnhancedGridView();
            this.colAssertWhen = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertMessage = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.repoMemoEditAssert = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colAssertDetailedMessage = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertFilePath = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertFilename = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertLine = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertColumn = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertClassName = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertMethodName = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertStackTrace = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.colAssertInfo = new TechnicalTools.UI.DX.EnhancedGridColumn();
            this.saveFileDialog = new DevExpress.XtraEditors.XtraSaveFileDialog(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tabs)).BeginInit();
            this.tabs.SuspendLayout();
            this.TabPageSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableCacheLogging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRunAsyncCodeSynchronously.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSameCultureAsProductPole.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCaptureStackWhenDeferredEvent.Properties)).BeginInit();
            this.TabPageExceptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcExceptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExceptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoMemoEditException)).BeginInit();
            this.TabPageAssertions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAssertions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAssertions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoMemoEditAssert)).BeginInit();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedTabPage = this.TabPageSettings;
            this.tabs.Size = new System.Drawing.Size(1264, 627);
            this.tabs.TabIndex = 0;
            this.tabs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TabPageSettings,
            this.TabPageExceptions,
            this.TabPageAssertions});
            // 
            // TabPageSettings
            // 
            this.TabPageSettings.Controls.Add(this.chkEnableCacheLogging);
            this.TabPageSettings.Controls.Add(this.btnSeeClassHierarchy);
            this.TabPageSettings.Controls.Add(this.chkRunAsyncCodeSynchronously);
            this.TabPageSettings.Controls.Add(this.chkUseSameCultureAsProductPole);
            this.TabPageSettings.Controls.Add(this.chkCaptureStackWhenDeferredEvent);
            this.TabPageSettings.Name = "TabPageSettings";
            this.TabPageSettings.Size = new System.Drawing.Size(1258, 599);
            this.TabPageSettings.Text = "Settings (developper helps)";
            // 
            // chkEnableCacheLogging
            // 
            this.chkEnableCacheLogging.Location = new System.Drawing.Point(11, 41);
            this.chkEnableCacheLogging.Name = "chkEnableCacheLogging";
            this.chkEnableCacheLogging.Properties.Caption = "Activer le log des requêtes faîtes au CacheManager dans l\'onglet \"Output\" de Visu" +
    "al Studio";
            this.chkEnableCacheLogging.Size = new System.Drawing.Size(571, 19);
            this.chkEnableCacheLogging.TabIndex = 7;
            // 
            // btnSeeClassHierarchy
            // 
            this.btnSeeClassHierarchy.Location = new System.Drawing.Point(11, 116);
            this.btnSeeClassHierarchy.Name = "btnSeeClassHierarchy";
            this.btnSeeClassHierarchy.Size = new System.Drawing.Size(178, 24);
            this.btnSeeClassHierarchy.TabIndex = 4;
            this.btnSeeClassHierarchy.Text = "Voir la hierarchie des classes";
            this.btnSeeClassHierarchy.Click += new System.EventHandler(this.btnSeeClassHierarchy_Click);
            // 
            // chkRunAsyncCodeSynchronously
            // 
            this.chkRunAsyncCodeSynchronously.Location = new System.Drawing.Point(11, 16);
            this.chkRunAsyncCodeSynchronously.Name = "chkRunAsyncCodeSynchronously";
            this.chkRunAsyncCodeSynchronously.Properties.Caption = "Executer le code multithreadé dans le thread principal (==> debug plus facile)";
            this.chkRunAsyncCodeSynchronously.Size = new System.Drawing.Size(393, 19);
            this.chkRunAsyncCodeSynchronously.TabIndex = 3;
            // 
            // chkUseSameCultureAsProductPole
            // 
            this.chkUseSameCultureAsProductPole.Location = new System.Drawing.Point(11, 91);
            this.chkUseSameCultureAsProductPole.Name = "chkUseSameCultureAsProductPole";
            this.chkUseSameCultureAsProductPole.Properties.Caption = "Utiliser la culture info \"en-US\"";
            this.chkUseSameCultureAsProductPole.Size = new System.Drawing.Size(178, 19);
            this.chkUseSameCultureAsProductPole.TabIndex = 2;
            this.chkUseSameCultureAsProductPole.CheckedChanged += new System.EventHandler(this.chkUseSameCultureAsProductPole_CheckedChanged);
            // 
            // chkCaptureStackWhenDeferredEvent
            // 
            this.chkCaptureStackWhenDeferredEvent.Location = new System.Drawing.Point(11, 66);
            this.chkCaptureStackWhenDeferredEvent.Name = "chkCaptureStackWhenDeferredEvent";
            this.chkCaptureStackWhenDeferredEvent.Properties.Caption = "Capturer la callstack lors des évènement s déférré via Postpone (peut ralentir l\'" +
    "execution)";
            this.chkCaptureStackWhenDeferredEvent.Size = new System.Drawing.Size(473, 19);
            this.chkCaptureStackWhenDeferredEvent.TabIndex = 0;
            // 
            // TabPageExceptions
            // 
            this.TabPageExceptions.Controls.Add(this.btnSendMail);
            this.TabPageExceptions.Controls.Add(this.btnTestException);
            this.TabPageExceptions.Controls.Add(this.gcExceptions);
            this.TabPageExceptions.Name = "TabPageExceptions";
            this.TabPageExceptions.Size = new System.Drawing.Size(1258, 599);
            this.TabPageExceptions.Text = "Exceptions";
            // 
            // btnSendMail
            // 
            this.btnSendMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendMail.Location = new System.Drawing.Point(1079, 5);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(172, 23);
            this.btnSendMail.TabIndex = 12;
            this.btnSendMail.Text = "Send all exceptions to Dev team";
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // btnTestException
            // 
            this.btnTestException.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestException.Location = new System.Drawing.Point(875, 5);
            this.btnTestException.Name = "btnTestException";
            this.btnTestException.Size = new System.Drawing.Size(198, 23);
            this.btnTestException.TabIndex = 11;
            this.btnTestException.Text = "Test : Raise an unhandled exception";
            this.btnTestException.Click += new System.EventHandler(this.btnTestException_Click);
            // 
            // gcExceptions
            // 
            this.gcExceptions.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcExceptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcExceptions.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcExceptions.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcExceptions.EmbeddedNavigator.TextStringFormat = "Line {0}/{1} of {2}";
            this.gcExceptions.Location = new System.Drawing.Point(0, 0);
            this.gcExceptions.MainView = this.gvExceptions;
            this.gcExceptions.Name = "gcExceptions";
            this.gcExceptions.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repoMemoEditException});
            this.gcExceptions.Size = new System.Drawing.Size(1258, 599);
            this.gcExceptions.TabIndex = 10;
            this.gcExceptions.UseEmbeddedNavigator = true;
            this.gcExceptions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvExceptions});
            // 
            // gvExceptions
            // 
            this.gvExceptions.Columns.AddRange(new TechnicalTools.UI.DX.EnhancedGridColumn[] {
            this.colExceptionWhen,
            this.colExceptionKind,
            this.colExceptionMessage,
            this.colExceptionDetailedMessage,
            this.colExceptionFilePath,
            this.colExceptionFileName,
            this.colExceptionLine,
            this.colExceptionColumn,
            this.colExceptionClassName,
            this.colExceptionMethodName,
            this.colExceptionStackTrace,
            this.colExceptionInfo});
            this.gvExceptions.CustomizationFormBounds = new System.Drawing.Rectangle(1497, 560, 210, 172);
            this.gvExceptions.GridControl = this.gcExceptions;
            this.gvExceptions.GroupCount = 1;
            this.gvExceptions.Name = "gvExceptions";
            this.gvExceptions.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvExceptions.OptionsBehavior.ReadOnly = true;
            this.gvExceptions.OptionsView.ColumnAutoWidth = false;
            this.gvExceptions.OptionsView.RowAutoHeight = true;
            this.gvExceptions.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExceptionKind, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExceptionWhen, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colExceptionWhen
            // 
            this.colExceptionWhen.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionWhen.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExceptionWhen.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionWhen.Caption = "When";
            this.colExceptionWhen.DisplayFormat.FormatString = "yyyy\'-\'MM\'-\'dd\' \'hh\':\'mm\':\'ss";
            this.colExceptionWhen.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colExceptionWhen.Name = "colExceptionWhen";
            this.colExceptionWhen.OptionsColumn.FixedWidth = true;
            this.colExceptionWhen.Visible = true;
            this.colExceptionWhen.VisibleIndex = 0;
            this.colExceptionWhen.Width = 111;
            // 
            // colExceptionKind
            // 
            this.colExceptionKind.Caption = "Kind";
            this.colExceptionKind.Name = "colExceptionKind";
            this.colExceptionKind.OptionsColumn.FixedWidth = true;
            this.colExceptionKind.Visible = true;
            this.colExceptionKind.VisibleIndex = 1;
            // 
            // colExceptionMessage
            // 
            this.colExceptionMessage.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionMessage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionMessage.Caption = "Message";
            this.colExceptionMessage.ColumnEdit = this.repoMemoEditException;
            this.colExceptionMessage.Name = "colExceptionMessage";
            this.colExceptionMessage.OptionsColumn.FixedWidth = true;
            this.colExceptionMessage.Visible = true;
            this.colExceptionMessage.VisibleIndex = 6;
            this.colExceptionMessage.Width = 222;
            // 
            // repoMemoEditException
            // 
            this.repoMemoEditException.Name = "repoMemoEditException";
            // 
            // colExceptionDetailedMessage
            // 
            this.colExceptionDetailedMessage.Caption = "Detailed message";
            this.colExceptionDetailedMessage.ColumnEdit = this.repoMemoEditException;
            this.colExceptionDetailedMessage.Name = "colExceptionDetailedMessage";
            this.colExceptionDetailedMessage.OptionsColumn.FixedWidth = true;
            this.colExceptionDetailedMessage.Width = 102;
            // 
            // colExceptionFilePath
            // 
            this.colExceptionFilePath.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionFilePath.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionFilePath.Caption = "Path";
            this.colExceptionFilePath.Name = "colExceptionFilePath";
            this.colExceptionFilePath.OptionsColumn.FixedWidth = true;
            this.colExceptionFilePath.Visible = true;
            this.colExceptionFilePath.VisibleIndex = 1;
            this.colExceptionFilePath.Width = 35;
            // 
            // colExceptionFileName
            // 
            this.colExceptionFileName.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionFileName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionFileName.Caption = "File";
            this.colExceptionFileName.Name = "colExceptionFileName";
            this.colExceptionFileName.OptionsColumn.FixedWidth = true;
            this.colExceptionFileName.Visible = true;
            this.colExceptionFileName.VisibleIndex = 2;
            this.colExceptionFileName.Width = 160;
            // 
            // colExceptionLine
            // 
            this.colExceptionLine.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionLine.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionLine.Caption = "Line";
            this.colExceptionLine.Name = "colExceptionLine";
            this.colExceptionLine.OptionsColumn.FixedWidth = true;
            this.colExceptionLine.Visible = true;
            this.colExceptionLine.VisibleIndex = 3;
            this.colExceptionLine.Width = 31;
            // 
            // colExceptionColumn
            // 
            this.colExceptionColumn.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionColumn.Caption = "Column";
            this.colExceptionColumn.Name = "colExceptionColumn";
            this.colExceptionColumn.OptionsColumn.FixedWidth = true;
            this.colExceptionColumn.Visible = true;
            this.colExceptionColumn.VisibleIndex = 4;
            this.colExceptionColumn.Width = 36;
            // 
            // colExceptionClassName
            // 
            this.colExceptionClassName.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionClassName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionClassName.Caption = "In class";
            this.colExceptionClassName.Name = "colExceptionClassName";
            this.colExceptionClassName.OptionsColumn.FixedWidth = true;
            this.colExceptionClassName.Visible = true;
            this.colExceptionClassName.VisibleIndex = 5;
            this.colExceptionClassName.Width = 115;
            // 
            // colExceptionMethodName
            // 
            this.colExceptionMethodName.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionMethodName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionMethodName.Caption = "Method";
            this.colExceptionMethodName.Name = "colExceptionMethodName";
            this.colExceptionMethodName.OptionsColumn.FixedWidth = true;
            this.colExceptionMethodName.Width = 139;
            // 
            // colExceptionStackTrace
            // 
            this.colExceptionStackTrace.AppearanceCell.Options.UseTextOptions = true;
            this.colExceptionStackTrace.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colExceptionStackTrace.Caption = "Stack trace";
            this.colExceptionStackTrace.ColumnEdit = this.repoMemoEditException;
            this.colExceptionStackTrace.Name = "colExceptionStackTrace";
            this.colExceptionStackTrace.Width = 348;
            // 
            // colExceptionInfo
            // 
            this.colExceptionInfo.Caption = "Info";
            this.colExceptionInfo.ColumnEdit = this.repoMemoEditException;
            this.colExceptionInfo.Name = "colExceptionInfo";
            this.colExceptionInfo.Visible = true;
            this.colExceptionInfo.VisibleIndex = 7;
            this.colExceptionInfo.Width = 530;
            // 
            // TabPageAssertions
            // 
            this.TabPageAssertions.Controls.Add(this.chkAssertionDialogBoxDisabled);
            this.TabPageAssertions.Controls.Add(this.btnTestFailingAssertion);
            this.TabPageAssertions.Controls.Add(this.gcAssertions);
            this.TabPageAssertions.Name = "TabPageAssertions";
            this.TabPageAssertions.Size = new System.Drawing.Size(1258, 599);
            this.TabPageAssertions.Text = "Assertions";
            // 
            // chkAssertionDialogBoxDisabled
            // 
            this.chkAssertionDialogBoxDisabled.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAssertionDialogBoxDisabled.AutoSize = true;
            this.chkAssertionDialogBoxDisabled.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.chkAssertionDialogBoxDisabled.Location = new System.Drawing.Point(893, 9);
            this.chkAssertionDialogBoxDisabled.Name = "chkAssertionDialogBoxDisabled";
            this.chkAssertionDialogBoxDisabled.Size = new System.Drawing.Size(233, 17);
            this.chkAssertionDialogBoxDisabled.TabIndex = 11;
            this.chkAssertionDialogBoxDisabled.Text = "Stop bothering me with popup dialog";
            this.chkAssertionDialogBoxDisabled.UseVisualStyleBackColor = false;
            // 
            // btnTestFailingAssertion
            // 
            this.btnTestFailingAssertion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestFailingAssertion.Location = new System.Drawing.Point(1132, 5);
            this.btnTestFailingAssertion.Name = "btnTestFailingAssertion";
            this.btnTestFailingAssertion.Size = new System.Drawing.Size(119, 23);
            this.btnTestFailingAssertion.TabIndex = 10;
            this.btnTestFailingAssertion.Text = "Raise failing assertion";
            this.btnTestFailingAssertion.Click += new System.EventHandler(this.btnTestFailingAssertion_Click);
            // 
            // gcAssertions
            // 
            this.gcAssertions.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcAssertions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcAssertions.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gcAssertions.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcAssertions.EmbeddedNavigator.TextStringFormat = "Line {0}/{1} of {2}";
            this.gcAssertions.Location = new System.Drawing.Point(0, 0);
            this.gcAssertions.MainView = this.gvAssertions;
            this.gcAssertions.Name = "gcAssertions";
            this.gcAssertions.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repoMemoEditAssert});
            this.gcAssertions.Size = new System.Drawing.Size(1258, 599);
            this.gcAssertions.TabIndex = 9;
            this.gcAssertions.UseEmbeddedNavigator = true;
            this.gcAssertions.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAssertions});
            // 
            // gvAssertions
            // 
            this.gvAssertions.Columns.AddRange(new TechnicalTools.UI.DX.EnhancedGridColumn[] {
            this.colAssertWhen,
            this.colAssertMessage,
            this.colAssertDetailedMessage,
            this.colAssertFilePath,
            this.colAssertFilename,
            this.colAssertLine,
            this.colAssertColumn,
            this.colAssertClassName,
            this.colAssertMethodName,
            this.colAssertStackTrace,
            this.colAssertInfo});
            this.gvAssertions.CustomizationFormBounds = new System.Drawing.Rectangle(1497, 560, 210, 172);
            this.gvAssertions.GridControl = this.gcAssertions;
            this.gvAssertions.Name = "gvAssertions";
            this.gvAssertions.OptionsBehavior.ReadOnly = true;
            this.gvAssertions.OptionsView.ColumnAutoWidth = false;
            this.gvAssertions.OptionsView.RowAutoHeight = true;
            this.gvAssertions.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAssertWhen, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colAssertWhen
            // 
            this.colAssertWhen.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertWhen.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAssertWhen.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertWhen.Caption = "When";
            this.colAssertWhen.DisplayFormat.FormatString = "yyyy\'-\'MM\'-\'dd\' \'hh\':\'mm\':\'ss";
            this.colAssertWhen.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colAssertWhen.Name = "colAssertWhen";
            this.colAssertWhen.Visible = true;
            this.colAssertWhen.VisibleIndex = 0;
            this.colAssertWhen.Width = 111;
            // 
            // colAssertMessage
            // 
            this.colAssertMessage.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertMessage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertMessage.Caption = "Message";
            this.colAssertMessage.ColumnEdit = this.repoMemoEditAssert;
            this.colAssertMessage.Name = "colAssertMessage";
            this.colAssertMessage.OptionsColumn.FixedWidth = true;
            this.colAssertMessage.Visible = true;
            this.colAssertMessage.VisibleIndex = 6;
            this.colAssertMessage.Width = 222;
            // 
            // repoMemoEditAssert
            // 
            this.repoMemoEditAssert.Name = "repoMemoEditAssert";
            // 
            // colAssertDetailedMessage
            // 
            this.colAssertDetailedMessage.Caption = "Detailed message";
            this.colAssertDetailedMessage.ColumnEdit = this.repoMemoEditAssert;
            this.colAssertDetailedMessage.Name = "colAssertDetailedMessage";
            this.colAssertDetailedMessage.OptionsColumn.FixedWidth = true;
            this.colAssertDetailedMessage.Width = 102;
            // 
            // colAssertFilePath
            // 
            this.colAssertFilePath.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertFilePath.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertFilePath.Caption = "Path";
            this.colAssertFilePath.Name = "colAssertFilePath";
            this.colAssertFilePath.OptionsColumn.FixedWidth = true;
            this.colAssertFilePath.Visible = true;
            this.colAssertFilePath.VisibleIndex = 1;
            this.colAssertFilePath.Width = 35;
            // 
            // colAssertFilename
            // 
            this.colAssertFilename.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertFilename.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertFilename.Caption = "File";
            this.colAssertFilename.Name = "colAssertFilename";
            this.colAssertFilename.OptionsColumn.FixedWidth = true;
            this.colAssertFilename.Visible = true;
            this.colAssertFilename.VisibleIndex = 2;
            this.colAssertFilename.Width = 160;
            // 
            // colAssertLine
            // 
            this.colAssertLine.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertLine.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertLine.Caption = "Line";
            this.colAssertLine.Name = "colAssertLine";
            this.colAssertLine.OptionsColumn.FixedWidth = true;
            this.colAssertLine.Visible = true;
            this.colAssertLine.VisibleIndex = 3;
            this.colAssertLine.Width = 31;
            // 
            // colAssertColumn
            // 
            this.colAssertColumn.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertColumn.Caption = "Column";
            this.colAssertColumn.Name = "colAssertColumn";
            this.colAssertColumn.OptionsColumn.FixedWidth = true;
            this.colAssertColumn.Visible = true;
            this.colAssertColumn.VisibleIndex = 4;
            this.colAssertColumn.Width = 36;
            // 
            // colAssertClassName
            // 
            this.colAssertClassName.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertClassName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertClassName.Caption = "In class";
            this.colAssertClassName.Name = "colAssertClassName";
            this.colAssertClassName.OptionsColumn.FixedWidth = true;
            this.colAssertClassName.Visible = true;
            this.colAssertClassName.VisibleIndex = 5;
            this.colAssertClassName.Width = 115;
            // 
            // colAssertMethodName
            // 
            this.colAssertMethodName.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertMethodName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertMethodName.Caption = "Method";
            this.colAssertMethodName.Name = "colAssertMethodName";
            this.colAssertMethodName.OptionsColumn.FixedWidth = true;
            this.colAssertMethodName.Width = 139;
            // 
            // colAssertStackTrace
            // 
            this.colAssertStackTrace.AppearanceCell.Options.UseTextOptions = true;
            this.colAssertStackTrace.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colAssertStackTrace.Caption = "Stack trace";
            this.colAssertStackTrace.ColumnEdit = this.repoMemoEditAssert;
            this.colAssertStackTrace.Name = "colAssertStackTrace";
            this.colAssertStackTrace.Width = 348;
            // 
            // colAssertInfo
            // 
            this.colAssertInfo.Caption = "Info";
            this.colAssertInfo.ColumnEdit = this.repoMemoEditAssert;
            this.colAssertInfo.Name = "colAssertInfo";
            this.colAssertInfo.Visible = true;
            this.colAssertInfo.VisibleIndex = 7;
            this.colAssertInfo.Width = 530;
            // 
            // FrmDebugDiagnostics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 627);
            this.Controls.Add(this.tabs);
            this.Name = "FrmDebugDiagnostics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Debug Diagnostics";
            this.Load += new System.EventHandler(this.FrmDebugDiagnostics_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabs)).EndInit();
            this.tabs.ResumeLayout(false);
            this.TabPageSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableCacheLogging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRunAsyncCodeSynchronously.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSameCultureAsProductPole.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCaptureStackWhenDeferredEvent.Properties)).EndInit();
            this.TabPageExceptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcExceptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExceptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoMemoEditException)).EndInit();
            this.TabPageAssertions.ResumeLayout(false);
            this.TabPageAssertions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAssertions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAssertions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repoMemoEditAssert)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraTab.XtraTabPage TabPageExceptions;
        protected DevExpress.XtraTab.XtraTabPage TabPageAssertions;
        private EnhancedGridControl gcExceptions;
        private EnhancedGridView gvExceptions;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionWhen;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionKind;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionMessage;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repoMemoEditException;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionDetailedMessage;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionFilePath;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionFileName;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionLine;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionColumn;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionClassName;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionMethodName;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionStackTrace;
        private DevExpress.XtraEditors.SimpleButton btnTestException;
        private EnhancedGridControl gcAssertions;
        private EnhancedGridView gvAssertions;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertWhen;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertMessage;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repoMemoEditAssert;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertDetailedMessage;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertFilePath;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertFilename;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertLine;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertColumn;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertClassName;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertMethodName;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertStackTrace;
        private DevExpress.XtraEditors.SimpleButton btnTestFailingAssertion;
        private System.Windows.Forms.CheckBox chkAssertionDialogBoxDisabled;
        private DevExpress.XtraEditors.SimpleButton btnSendMail;
        private TechnicalTools.UI.DX.EnhancedGridColumn colExceptionInfo;
        private TechnicalTools.UI.DX.EnhancedGridColumn colAssertInfo;
        protected DevExpress.XtraTab.XtraTabPage TabPageSettings;
        private DevExpress.XtraEditors.CheckEdit chkCaptureStackWhenDeferredEvent;
        private DevExpress.XtraEditors.CheckEdit chkUseSameCultureAsProductPole;
        private DevExpress.XtraEditors.CheckEdit chkRunAsyncCodeSynchronously;
        private DevExpress.XtraEditors.SimpleButton btnSeeClassHierarchy;
        protected DevExpress.XtraTab.XtraTabControl tabs;
        private DevExpress.XtraEditors.CheckEdit chkEnableCacheLogging;
        private DevExpress.XtraEditors.XtraSaveFileDialog saveFileDialog;

    }
}