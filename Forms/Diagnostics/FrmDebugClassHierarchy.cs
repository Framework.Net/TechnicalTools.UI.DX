﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using TechnicalTools;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX.Forms.Diagnostics
{
    public partial class FrmDebugClassHierarchy : DebugFormBase
    {
        public FrmDebugClassHierarchy()
        {
            InitializeComponent();
            
            treeList.OptionsBehavior.Editable = false;
            treeList.OptionsBehavior.ReadOnly = true;
            treeList.OptionsBehavior.ReadOnly = true;

            treeList.RootValue = null;
            treeList.KeyFieldName = nameof(TypeInfo.MySelf);

            colTypeName.FieldName = nameof(TypeInfo.Name);
            colNamespace.FieldName = nameof(TypeInfo.Namespace);
            colHierarchySize.FieldName = nameof(TypeInfo.HierarchySize);
            colGenericTypeInstanciations.FieldName = nameof(TypeInfo.GenericTypeInstanciations);

            treeList.OptionsFind.AllowFindPanel = true;
            treeList.OptionsFind.ShowClearButton = true;
            treeList.OptionsFind.ShowCloseButton = true;
            treeList.OptionsFind.ShowFindButton = true;


            treeList.OptionsBehavior.EnableFiltering = true;
            treeList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.Extended;

        }

        protected void SetTreeListImages(ImageList imageList)
        {
            treeList.SelectImageList = imageList;
        }

        private void chkDisplayAsHierarchy_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDisplayAsHierarchy.Checked)
                treeList.ParentFieldName = GetMemberName.For<TypeInfo>(ti => ti.Parent);
            else
                treeList.ParentFieldName = " ";
        }

        private void FormDebugClassHierarchy_Load(object sender, EventArgs e)
        {
            chkDisplayAsHierarchy.Checked = true;

            LoadTreeView();
        }
        protected virtual void LoadTreeView()
        {
            try
            {
                SetTreeViewInfos(th_GenerateClassInfo());
            }
            catch (Exception)
            {
                if (!DesignTimeHelper.IsInDesignMode)
                    throw;
            }
        }

        protected void SetTreeViewInfos(List<TypeInfo> typeInfos)
        {
            treeList.DataSource = typeInfos;
            treeList.ExpandAll();
        }


        protected class TypeInfo
        {
            public TypeInfo MySelf { get { return this; } }
            public TypeInfo Parent { get; internal set; }
            public IReadOnlyCollection<TypeInfo> Children { get { return _Children; } }
            internal readonly List<TypeInfo> _Children = new List<TypeInfo>();


            public string Name
            {
                get { return _type.ToSmartString() + 
                            (HierarchySize.HasValue ? " (" + HierarchySize.Value + ")" : "");
                }
            }
            public string Namespace  { get { return _type.Namespace; } }
            public int? HierarchySize { get
            {
                int res = Children.Count + Children.Select(child => child.HierarchySize ?? 0).DefaultIfEmpty(0).Sum();
                if (res == 0)
                    return null;
                return res;
            } }
            public string GenericTypeInstanciations
            {
                get
                {
                    return _type.BaseType == null || !_type.BaseType.IsGenericType 
                        ? ""
                        : _type.BaseType.GetGenericArguments().Select(t => Type_Extensions.ToSmartString(t)).Join();
                }
            }
            

            public TypeInfo(Type type)
            {
                _type = type;
            }
            readonly Type _type;
        }

        protected virtual IEnumerable<Assembly> GetAssembliesToAnalyse()
        {
            return new[] { typeof(object).Assembly };
        }

        protected virtual bool IsInterestingTypeToDisplay(Type t)
        {
            return //.t => t.IsPublic
                !t.IsDelegateType() && // Supprime tous les types delegate
                !t.IsAttributeType() && // Supprime tous les attributs
                !t.IsCompilerGenerated() &&
                //!t.IsExceptionType() && // Supprime toutes les Exceptions
                (t.FullName == null || !t.FullName.Contains("+")) && // Supprime toutes les inner classes
                !t.IsValueType && // Supprime toutes les structures
                !t.IsInterface &&
                !t.IsEnum 
                //&& !t.IsVBModule()
                ;
        }

        protected List<TypeInfo> th_GenerateClassInfo()
        {
            var assemblies_to_analyse = GetAssembliesToAnalyse();

            var types = assemblies_to_analyse.Distinct()
                                             .SelectMany(ass => ass.GetTypes())
                                             .Where(IsInterestingTypeToDisplay)
                                             .ToList();
                                   

            var infos = types.ToDictionary(t => t, t => new TypeInfo(t));
            var ms_infos = new Dictionary<Type, TypeInfo>();
            var dx_infos = new Dictionary<Type, TypeInfo>();

            foreach (var pair in infos)
            {
                var t = pair.Key.BaseType;
                Debug.Assert(t != null);

                if (t == typeof (object))
                    continue;

                if (t.IsGenericType)
                    t = t.GetGenericTypeDefinition();

                if (t.IsDevExpressType())
                {
                    if (!dx_infos.ContainsKey(t))
                        dx_infos.Add(t, new TypeInfo(t));
                    pair.Value.Parent = dx_infos[t];
                }
                else if (t.IsMicrosoftType())
                {
                    if (!ms_infos.ContainsKey(t))
                        ms_infos.Add(t, new TypeInfo(t));
                    pair.Value.Parent = ms_infos[t];
                }
                else if (!IsInterestingTypeToDisplay(t) || !t.IsPublic)
                    continue;
                else
                {
                    Debug.Assert(infos.ContainsKey(t));
                    pair.Value.Parent = infos[t];
                }

                pair.Value.Parent._Children.Add(pair.Value);
            }

            return infos.Values.Concat(ms_infos.Values).Concat(dx_infos.Values)
                        .OrderByDescending(ti => ti.HierarchySize)
                        .ThenByDescending(ti => ti.Namespace)
                        .ThenBy(ti => ti.Name)
                        .ToList();
        }


    }
}
