﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using TechnicalTools.UI.DX.Forms.Internal;


namespace TechnicalTools.UI.DX.Forms
{
    // Inspired and mainly from https://www.devexpress.com/Support/Center/Example/Details/T229906/how-to-create-a-custom-xtramessagebox-with-the-extended-functionality
    public static class EnhancedXtraMessageBox
    {
        public static DialogResult Show(EnhancedXtraMessageArgs options)
        {
            EnhancedXtraMessageBoxForm messageForm = new EnhancedXtraMessageBoxForm(options.Owner ?? EnhancedXtraForm.MainForm);
            DialogResult result = messageForm.ShowForm(options);
            options.ShowMessageNextTime = messageForm.ShowMessageNextTime;
            return result;
        }

        public static DialogResult[] GetDialogResultsFromButtons(MessageBoxButtons buttons)
        {
            MethodInfo xtraMessageBoxInfo = typeof(XtraMessageBox).GetMethod("MessageBoxButtonsToDialogResults", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
            return (DialogResult[])xtraMessageBoxInfo.Invoke(null, new object[] { buttons });
        }

        public static Icon GetIcon(string iconInfo)
        {
            object icon = Enum.Parse(typeof(MessageBoxIcon), iconInfo, true);
            MethodInfo xtraMessageBoxInfo = typeof(XtraMessageBox).GetMethod("MessageBoxIconToIcon", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
            icon = xtraMessageBoxInfo.Invoke(null, new object[] { icon });
            return (Icon)icon;
        }
    }
}
