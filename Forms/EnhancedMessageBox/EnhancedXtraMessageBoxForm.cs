﻿using System;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX.Forms.Internal
{
    // Inspired and mainly from https://www.devexpress.com/Support/Center/Example/Details/T229906/how-to-create-a-custom-xtramessagebox-with-the-extended-functionality
    public class EnhancedXtraMessageBoxForm : DevExpress.XtraEditors.XtraMessageBoxForm
    {
        private Timer CountdownTimer;
        private bool showCountdown;
        private bool autoClose;
        private int delayToUnlockCancelButton;
        private int timeout;
        private DialogResult timeoutResult;
        
        private bool selectNext;
        private bool disablebttns;
        private bool disableCancel;
        private bool center;
        public bool ShowMessageNextTime;

        private CheckEdit cbShowNextTime;
        public EnhancedXtraMessageBoxForm(IWin32Window owner = null)
        {
            Owner = owner as Form; // unfortunately, XtraMessageBox does not handle IWin32Window owner, only Form :(
            CountdownTimer = new Timer
            {
                Tag = this,
                Interval = 1000
            };
            CountdownTimer.Tick += MyTimer_Tick;
            // Prevent memory leak because constructor of timer does not take (sometimes) this.component as argument when created in designer
            Disposed += (_, __) => CountdownTimer.Dispose();
        }

        protected override void OnShown(EventArgs e)
        {
            if (selectNext)
            {
                ShowNextTime();
            }

            IButtonControl btnCancel = null;
            if ((this.CancelButton != null) && (this.CancelButton.DialogResult == DialogResult.Cancel))
                btnCancel = this.CancelButton;
            if (disablebttns)
                DisableAllExceptCancel(btnCancel as SimpleButton);
            if ((disableCancel) && (btnCancel != null))
                (btnCancel as SimpleButton).Enabled = false;
            if (center)
                CenterOnParent();
            if (this.timeout > 0 || delayToUnlockCancelButton > 0)
            {
                this.CountdownTimer.Enabled = true;
                this.CountdownTimer.Start();
            }
            base.OnShown(e);
        }



        void DisableAllExceptCancel(SimpleButton btnCancel)
        {
            foreach (Control cont in this.Controls)
                if ((cont is BaseButton) && (cont != (btnCancel as SimpleButton)))
                    cont.Enabled = false;
        }

        void MyTimer_Tick(object sender, EventArgs e)
        {
            if (Disposing || IsDisposed)
                return;
            if (timeout > 0)
            {
                --timeout;
                if (showCountdown)
                    Text = baseCaption  
                         + (timeout > 0 ? string.Format(countDownPattern, timeout) : "");
                if (timeout <= 0)
                    if (autoClose)
                    {
                        this.DialogResult = timeoutResult;
                        base.Close();
                    }
            }
            var btnCancel = CancelButton as SimpleButton;
            if (delayToUnlockCancelButton > 0 && btnCancel != null)
            {
                --delayToUnlockCancelButton;
                if (baseCancelText == null)
                    baseCancelText = btnCancel.Text;
                if (showCountdown)
                    btnCancel.Text = baseCancelText 
                                   + (delayToUnlockCancelButton > 0 ? " ( " + delayToUnlockCancelButton + "s )" : "");
                if (delayToUnlockCancelButton == 0)
                    btnCancel.Enabled = true;
            }

            if (timeout <= 0 && delayToUnlockCancelButton <= 0)
            {
                Timer messageTimer = (sender as Timer);
                this.Message.Caption = baseCaption;
                messageTimer.Stop();
                messageTimer.Enabled = false;
            }
        }
        string baseCaption;
        string baseCancelText;
        const string countDownPattern = " (Remain {0} sec)";

        public DialogResult ShowForm(EnhancedXtraMessageArgs messageArgs)
        {
            selectNext = messageArgs.ShowNextTime;
            disablebttns = messageArgs.DisableButtons;
            disableCancel = messageArgs.DisableCancel;
            autoClose = messageArgs.AutoClose;
            delayToUnlockCancelButton = messageArgs.DelayToUnlockCancelButton;
            showCountdown = messageArgs.ShowCountdown;
            timeout = messageArgs.Timeout;
            center = messageArgs.Center;
            timeoutResult = messageArgs.AutoCloseResult;

            baseCaption = messageArgs.Caption;
            if (messageArgs.ShowCountdown) // Change the caption so the form has the good size to display timeout
                messageArgs.Caption += string.Format(countDownPattern, messageArgs.Timeout);
            Load += (_, __) => { messageArgs.Caption = baseCaption; }; // restore the original value for developper
            return base.ShowMessageBoxDialog(messageArgs);
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (cbShowNextTime != null)
                this.ShowMessageNextTime = cbShowNextTime.Checked;
            this.CountdownTimer.Stop();
            this.CountdownTimer.Dispose();
            base.OnClosing(e);
        }

        void ShowNextTime()
        {
            cbShowNextTime = new CheckEdit
            {
                Checked = true,
                Text = "Show this dialog again?"
            };
            cbShowNextTime.Properties.AutoWidth = true;
            cbShowNextTime.Properties.AutoHeight = true;
            this.Height += cbShowNextTime.Height;
            cbShowNextTime.Location = new Point(1, this.ClientSize.Height - cbShowNextTime.Height - 1);
            this.Controls.Add(cbShowNextTime);
        }

        void CenterOnParent()
        {
            int y = this.Owner.Height - this.Height;
            int x = this.Owner.Width - this.Width;
            this.Location = new Point(x / 2 + this.Owner.Location.X, y / 2 + this.Owner.Location.Y);
        }
    }
}
