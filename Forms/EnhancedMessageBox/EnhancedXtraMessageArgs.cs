﻿using System;
using System.Windows.Forms;

using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX.Forms
{
    // Inspired and mainly from https://www.devexpress.com/Support/Center/Example/Details/T229906/how-to-create-a-custom-xtramessagebox-with-the-extended-functionality
    public class EnhancedXtraMessageArgs : XtraMessageBoxArgs
    {
        public bool ShowCountdown { get; set; }
        public bool ShowNextTime { get; set; }
        public bool DisableCancel { get; set; }
        public bool DisableButtons { get; set; }
        public bool AutoClose { get; set; }
        public int  DelayToUnlockCancelButton { get; set; }
        public DialogResult AutoCloseResult { get; set; }
        public bool Center { get; set; }
        public int Timeout { get; set; }
        public bool ShowMessageNextTime { get; set; }
        private MessageBoxButtons fButtons;
        public new MessageBoxButtons Buttons
        {
            get
            {
                return this.fButtons;
            }
            set
            {
                base.Buttons = EnhancedXtraMessageBox.GetDialogResultsFromButtons(value);
                this.fButtons = value;
            }
        }



        private string fIcon;
        public new string Icon
        {
            get
            {
                return fIcon;
            }
            set
            {
                fIcon = value;
                base.Icon = EnhancedXtraMessageBox.GetIcon(value);
            }
        }

        public EnhancedXtraMessageArgs()
            : base()
        {
            this.Buttons = MessageBoxButtons.OK;
            this.ShowCountdown = false;
            this.ShowNextTime = false;
            this.DisableButtons = false;
            this.DelayToUnlockCancelButton = 0;
            this.DisableCancel = false;
            this.AutoClose = false;
            this.Center = false;
            this.Timeout = 0;
            this.ShowMessageNextTime = false;
        }
    }
}
