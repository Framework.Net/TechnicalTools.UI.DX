﻿namespace TechnicalTools.UI.DX.Forms.Errors
{
    partial class SoftErrorPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.memoEdit = new TechnicalTools.UI.DX.Controls.MemoEdit();
            this.errorImage = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorImage.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // memoEdit
            // 
            this.memoEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top;
            this.memoEdit.Location = new System.Drawing.Point(65, 31);
            this.memoEdit.Name = "memoEdit";
            this.memoEdit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.memoEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.memoEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.memoEdit.Properties.Appearance.Options.UseBackColor = true;
            this.memoEdit.Properties.Appearance.Options.UseFont = true;
            this.memoEdit.Properties.Appearance.Options.UseForeColor = true;
            this.memoEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.memoEdit.Properties.ReadOnly = true;
            this.memoEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit.ShowScrollbarsOnlyWhenNeeded = true;
            this.memoEdit.Size = new System.Drawing.Size(556, 109);
            this.memoEdit.TabIndex = 4;
            // 
            // errorImage
            // 
            this.errorImage.Cursor = System.Windows.Forms.Cursors.Default;
            this.errorImage.Location = new System.Drawing.Point(0, 0);
            this.errorImage.MinimumSize = new System.Drawing.Size(40, 40);
            this.errorImage.Name = "errorImage";
            this.errorImage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.errorImage.Properties.Appearance.Options.UseBackColor = true;
            this.errorImage.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.errorImage.Properties.NullText = " ";
            this.errorImage.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.errorImage.Size = new System.Drawing.Size(65, 77);
            this.errorImage.TabIndex = 5;
            // 
            // SoftErrorPopup
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 186);
            this.Controls.Add(this.memoEdit);
            this.Controls.Add(this.errorImage);
            this.Name = "SoftErrorPopup";
            this.Load += new System.EventHandler(this.ErrorPopup_Load);
            this.Controls.SetChildIndex(this.errorImage, 0);
            this.Controls.SetChildIndex(this.memoEdit, 0);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorImage.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TechnicalTools.UI.DX.Controls.MemoEdit memoEdit;
        private DevExpress.XtraEditors.PictureEdit errorImage;
    }
}