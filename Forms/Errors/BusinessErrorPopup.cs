﻿using System;
using System.Drawing;


namespace TechnicalTools.UI.DX.Forms.Errors
{
    public partial class BusinessErrorPopup : SoftErrorPopup
    {
        public static Image BusinessErrorImage { get; set; }

        BusinessErrorPopup()
            : this (null, false)
        {
        }

        public BusinessErrorPopup(Exception ex, bool displayTechnicalDetails)
            : base(ex, displayTechnicalDetails)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            Image = BusinessErrorImage;

            // Setter ce skin n'impacte que la form
            // La form ne changera pas de skin tant qu'on ne l'enleve pas.
            // Le but etant de donner une teinte vert a l'erreur car il s'agit 
            // d'une erreur "normale"  car business. Le but est de rassurer l'utilisateur
            LookAndFeel.SetSkinStyle("Office 2007 Green");
        }
    }
}
