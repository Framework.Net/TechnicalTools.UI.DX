﻿namespace TechnicalTools.UI.DX.Forms.Errors
{
    partial class ErrorPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.meIssueReferenceId = new TechnicalTools.UI.DX.Controls.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.meIssueReferenceId.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOk.Location = new System.Drawing.Point(547, 151);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // meIssueReferenceId
            // 
            this.meIssueReferenceId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.meIssueReferenceId.EditValue = "Issue Id: ";
            this.meIssueReferenceId.Location = new System.Drawing.Point(583, 5);
            this.meIssueReferenceId.Name = "meIssueReferenceId";
            this.meIssueReferenceId.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.meIssueReferenceId.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.meIssueReferenceId.Properties.Appearance.Options.UseBackColor = true;
            this.meIssueReferenceId.Properties.Appearance.Options.UseForeColor = true;
            this.meIssueReferenceId.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.meIssueReferenceId.Properties.ReadOnly = true;
            this.meIssueReferenceId.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.meIssueReferenceId.Size = new System.Drawing.Size(50, 16);
            this.meIssueReferenceId.TabIndex = 5;
            // 
            // ErrorPopup
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 186);
            this.ControlBox = false;
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.meIssueReferenceId);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(350, 0);
            this.Name = "ErrorPopup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.ErrorPopup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.meIssueReferenceId.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private Controls.MemoEdit meIssueReferenceId;
    }
}