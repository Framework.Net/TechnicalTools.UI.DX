﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Versioning;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX.Forms.Errors
{
    /// <summary>
    /// Base class for all error understandable by user
    /// </summary>
    public partial class SoftErrorPopup : ErrorPopup
    {
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable because we can need to original object if debugging a dump
        protected readonly Exception _ex;

        protected PictureEdit  Picture  { get { return errorImage; } }
        protected MemoEdit     Memo     { get { return memoEdit; } }

        // Make Memo.Left mutable and allow preview in child class 
        public int MemoLeft { get { return memoEdit.Left; } set { memoEdit.Width -= value -  memoEdit.Left; memoEdit.Left = value; } }

        [DefaultValue(null)]
        [Bindable(false)]
        public Image        Image
        {
            [ResourceExposure(ResourceScope.Machine)]
            get { return Picture.Image; }
            set
            {
                Picture.Visible = value != null;
                if (Image != value)
                {
                    Picture.Height = Picture.Width * value?.Height / value?.Width ?? Picture.Height;
                    Picture.Image = value;
                }
                RefreshLayout();
            }
        }

        protected SoftErrorPopup()
            : this(null, false)
        {
        }

        protected SoftErrorPopup(Exception ex, bool displayTechnicalDetails)
        {
            _ex = ex;
            InitializeComponent();
            _memoEditOriginalLeft = memoEdit.Left;
            _memoEditOriginalWidth = memoEdit.Width;
            if (DesignTimeHelper.IsInDesignMode)
                return;
            
            errorImage.Properties.AllowFocused = true; // to prevent the display of selection rectangle
            errorImage.BackColor = BackColor; // for certain skin
            Text = Application.ProductName + " " + Text;

            string prettyMsg = ExceptionManager.Instance.Format(_ex, !displayTechnicalDetails, false);
            // a lot of arrow here : http://xahlee.info/comp/unicode_arrows.html
            prettyMsg = prettyMsg.Replace("Because of" + Environment.NewLine + "  ", "⤷");
            memoEdit.Text += prettyMsg;
            BugOrTicketId = _ex?.GetUniqueBugId().ToStringInvariant();
        }

        void ErrorPopup_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode) // Prevent following data to be serialiazed
                return;

            var size = CalcMemoHeighRequiredToDisplayError(this, memoEdit);
            var w = ClientSize.Width - memoEdit.Width + size.Width;
            var h = ClientSize.Height - memoEdit.Height + size.Height;
            SetBounds(Left - (w - ClientSize.Width) / 2, Top - (h - ClientSize.Height) / 2, 0, 0, BoundsSpecified.Location);
            SetClientSizeCore(w, h);
        }

        void RefreshLayout()
        {
            if (Image == null)
            {
                var rightMargin = ClientSize.Width - _memoEditOriginalLeft - _memoEditOriginalWidth;
                memoEdit.SetBounds(rightMargin, 0, ClientSize.Width - 2 * rightMargin, 0, BoundsSpecified.X | BoundsSpecified.Width);
            }
            else
                memoEdit.SetBounds(_memoEditOriginalLeft, 0, _memoEditOriginalWidth, 0, BoundsSpecified.X | BoundsSpecified.Width);
        }
        int _memoEditOriginalLeft;
        int _memoEditOriginalWidth;
    }
}
