﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.Versioning;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX.Forms.Errors
{
    public partial class TechnicalErrorPopup : ErrorPopup
    {
        readonly Exception _ex;

        protected SimpleButton ButtonCopy { get { return btnCopy; } }
        protected PictureEdit  Picture    { get { return errorImage; } }
        protected MemoEdit     Memo       { get { return memoEdit; } }

        readonly string _additionalHeaderInfo;

        public static bool ProvideMaximumErrorData { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ForceStopPopupButtonVisible { get; set; }

        public static Image TechnicalErrorImage
        {
            get { return _TechnicalErrorImage ?? DefaultTechnicalErrorImage; }
            set { _TechnicalErrorImage = value; }
        }
        static Image _TechnicalErrorImage;

        public static readonly Image DefaultTechnicalErrorImage = Image.FromStream(typeof(EnhancedGridView_DisplayColumnHint).Assembly.GetManifestResourceStream(typeof(EnhancedGridView_DisplayColumnHint).Namespace + ".Resources.Images.Errors.TechnicalError.png"));

        [DefaultValue(null)]
        [Bindable(false)]
        public Image Image
        {
            [ResourceExposure(ResourceScope.Machine)]
            get { return Picture.Image; }
            set
            {
                Picture.Visible = value != null;
                if (Image != value)
                {
                    Picture.Height = Picture.Width * value?.Height / value?.Width ?? Picture.Height;
                    Picture.Image = value;
                }
                RefreshLayout();
            }
        }

        private TechnicalErrorPopup()
        {
            InitializeComponent();
            _memoEditOriginalLeft = memoEdit.Left;
            _memoEditOriginalWidth = memoEdit.Width;
            if (DesignTimeHelper.IsInDesignMode)
                return;

            errorImage.Properties.AllowFocused = true; // to prevent the display of selection rectangle
            errorImage.BackColor = BackColor; // for certain skin
            Text = Application.ProductName + " " + Text;
            Image = TechnicalErrorImage;

            RefreshExpandButtonText();
        }

        public TechnicalErrorPopup(string message, long? bugId = null)
            : this()
        {
            BugOrTicketId = bugId?.ToStringInvariant();
            memoEdit.Text = message;
        }

        protected TechnicalErrorPopup(Exception ex, string additional_info, bool displayTechnicalDetails)
            : this()
        {
            _ex = ex;
            BugOrTicketId = ex?.GetUniqueBugId().ToStringInvariant();

            var memoText = "";
            if (!string.IsNullOrEmpty(additional_info))
            {
                _additionalHeaderInfo = additional_info;
                memoText += _additionalHeaderInfo + Environment.NewLine;
            }

            if (!displayTechnicalDetails)
                _fullmsg = memoText + ExceptionManager.Instance.Format(_ex);
            memoText += ExceptionManager.Instance.Format(_ex, !displayTechnicalDetails, false);

            if (memoText.Length > 2000)
                memoEdit.Properties.WordWrap = false; // Prevent serious slowness for technical error that are big
            memoEdit.Text = memoText;
        }
        readonly string _fullmsg;

        public TechnicalErrorPopup(Exception ex, string additional_info = null)
            : this (ex, additional_info, ProvideMaximumErrorData)
        {
        }
        
        public TechnicalErrorPopup(UnexpectedExceptionManager.ExceptionInfo info)
            : this (info.Exception, null, ProvideMaximumErrorData || info.ForDevOnly)
        {
            txtForDevOnly.Visible = info.ForDevOnly;
            if (!string.IsNullOrWhiteSpace(info.ThreadName))
                Text += "    [Thread: " + info.ThreadName + " ]";
        }

        public static Func<Version> GetVersioningInfo;

        void TechnicalErrorPopup_Load(object sender, EventArgs e)
        {
            if (DesignTimeHelper.IsInDesignMode) // Prevent following data to be serialiazed
                return;

            if (GetVersioningInfo != null)
                ExceptionManager.Instance.IgnoreExceptionAndBreak(() =>
                {
                    BugOrTicketId = BugOrTicketId + " (app version: " + GetVersioningInfo() + ")";
                });

            chkStopPopup.Visible = ForceStopPopupButtonVisible && _ex != null;

            var size = CalcMemoHeighRequiredToDisplayError(this, memoEdit);
            var w = ClientSize.Width - memoEdit.Width + size.Width;
            var h = ClientSize.Height - memoEdit.Height + size.Height;
            var l = Left - (w - ClientSize.Width) / 2;
            if (l < 0)
            {
                w += l;
                l = 0;
            }
            var t = Top - (h - ClientSize.Height) / 2;
            if (t < 0)
            {
                h += t;
                t = 0;
            }
            SetBounds(l, t, 0, 0, BoundsSpecified.Location);
            SetClientSizeCore(w, h);
        }

        void RefreshLayout()
        {
            if (Image == null)
            {
                var rightMargin = ClientSize.Width - _memoEditOriginalLeft - _memoEditOriginalWidth;
                memoEdit.SetBounds(rightMargin, 0, ClientSize.Width - 2 * rightMargin, 0, BoundsSpecified.X | BoundsSpecified.Width);
            }
            else
                memoEdit.SetBounds(_memoEditOriginalLeft, 0, _memoEditOriginalWidth, 0, BoundsSpecified.X | BoundsSpecified.Width);
        }
        int _memoEditOriginalLeft;
        int _memoEditOriginalWidth;

        void btnCopy_Click(object sender, EventArgs e)
        {
            // Ligne pour copier a utiliser d'apres : 
            // http://social.msdn.microsoft.com/Forums/en-US/a23c31df-e645-4500-ab4a-e6178510a9b1/clipboard-error?forum=Vsexpressvb
            // via
            // http://stackoverflow.com/questions/5707990/requested-clipboard-operation-did-not-succeed
            // Cela semble marcher une fois dans VS, apres ca VS semble ne jamais delocker le clipboard donc ca foire tout le temps... :(
            this.ShowBusyWhileDoingUIWorkInPlace("Copying error in clipboard", pr => Clipboard.SetDataObject(ErrorReport, true, 10, 100)); // Essaye 10 fois toutes les 100ms.
        }

        string ErrorReport
        {
            get
            {
                return "Bug Id: " + BugOrTicketId + $@"
- Here is what I did on {Application.ProductName} at {DateTime.Now:yyyy'/'MM'/'dd' 'HH':'mm':'ss}:


 - And here is the error I get" + Environment.NewLine
                       + (string.IsNullOrEmpty(_additionalHeaderInfo) ? "" : _additionalHeaderInfo + Environment.NewLine)
                       + (_fullmsg ?? memoEdit.Text);
            }
        }

        void chkStopPopup_CheckedChanged(object sender, EventArgs e)
        {
            if (chkStopPopup.Checked)
                UnexpectedExceptionManager.IgnoreThisException(_ex);
            else
                UnexpectedExceptionManager.DoNotIgnoreThisException(_ex);
        }

        void lblCollapseExpand_Click(object sender, EventArgs e)
        {
            _isExpanded = !_isExpanded;
            RefreshExpandButtonText();
            if (_isExpanded)
            {
                var size = CalcMemoHeighRequiredToDisplayError(this, memoEdit);

                // Fait en sorte que la taille expanded ne soit pas plus petite que la taille non etendu (actuel)
                size = new Size(Math.Max(size.Width, ClientSize.Width), Math.Max(size.Height, ClientSize.Height));

                Debug.Assert(size.Width >= ClientSize.Width);
                Debug.Assert(size.Height >= ClientSize.Height);
                _boundsBeforeExpanded = Bounds;
                SetBounds(Left - (size.Width - ClientSize.Width) / 2, Top - (size.Height - ClientSize.Height) / 2, 0, 0, BoundsSpecified.Location);
                SetClientSizeCore(size.Width, size.Height);
            }
            else if (_boundsBeforeExpanded.HasValue)
            {
                Bounds = _boundsBeforeExpanded.Value;
            }
        }
        bool _isExpanded;
        Rectangle? _boundsBeforeExpanded;
        void RefreshExpandButtonText()
        {
            lblCollapseExpand2.Text = lblCollapseExpand1.Text = _isExpanded ? "▲" : "▼";
        }
    }
}
