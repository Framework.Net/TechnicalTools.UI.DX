﻿namespace TechnicalTools.UI.DX.Forms.Errors
{
    partial class CriticalErrorPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerLockButton = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timerLockButton
            // 
            this.timerLockButton.Interval = 1000;
            this.timerLockButton.Tick += new System.EventHandler(this.timerLockButton_Tick);
            // 
            // CriticalErrorPopup
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 561);
            this.Name = "CriticalErrorPopup";
            this.Text = "Critical Error !     ***  CONTACT   IT   DEPARTMENT  ***";
            this.Load += new System.EventHandler(this.CriticalErrorPopup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerLockButton;
    }
}