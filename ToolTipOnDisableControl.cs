﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.Utils;
using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX
{
    // Thanks to http://stackoverflow.com/questions/2063974/how-do-i-capture-the-mouse-move-event
    public static class ToolTipOnDisableControl
    {
        static ToolTipOnDisableControl()
        {
            GlobalMouseHandler.MouseMovedEvent += GlobalMouseHandler_MouseMovedEvent;
            Application.AddMessageFilter(new GlobalMouseHandler());
        }

        public static void EnableFeatureOn(Control ctl)
        {
            if (_ControlHandled.ContainsKey(ctl))
                return;
            var fComponents = ctl.GetType().GetField("components", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var container = (IContainer)fComponents.GetValue(ctl);
            _ControlHandled.TryAdd(ctl, new ToolTipController(container ?? new Container()));
            ctl.Disposed += (_, __) =>
            {
                _ControlHandled.TryRemove(ctl, out ToolTipController dummy);
            };
        }
        static readonly ConcurrentDictionary<Control, ToolTipController> _ControlHandled = new ConcurrentDictionary<Control, ToolTipController>();

        [DebuggerStepThrough]
        static void GlobalMouseHandler_MouseMovedEvent(object sender, MouseEventArgs e)
        {
            foreach (var kvp in _ControlHandled)
            {
                var pos = e.Location;
                if (!kvp.Key.Bounds.Contains(pos))
                    continue;
                if (PointToHandleByControl.TryGetValue(kvp.Key, out Point oldPos)) // if a pos must be treated by Control
                {
                    if (PointToHandleByControl.TryUpdate(kvp.Key, pos, oldPos)) // and we success to update it before it is actually treated by control in the begininvoke below
                        continue; // then we dont need to add a new event to handle (avoid message queue overloading)
                }
                else
                    PointToHandleByControl.TryAdd(kvp.Key, pos);
                // Always invoke because between testing IsDisposed below and treating pos someone could dispose the control (for example Application.Run)
                try
                {
                    kvp.Key.BeginInvoke((Action<KeyValuePair<Control, ToolTipController>>)CheckIfMouseIsOverDisabledControl, kvp);
                }
                catch (InvalidOperationException ex)
                {
                    Debug.Assert(!kvp.Key.IsHandleCreated);
                    if (ex.HResult != -2146233079) // ignore exception that occurs when BeginInvoke is done while handle is not yet created or does not exist anymore
                        throw;
                }
            }
        }
        static readonly ConcurrentDictionary<Control, Point> PointToHandleByControl = new ConcurrentDictionary<Control, Point>();

        [DebuggerStepThrough]
        static void CheckIfMouseIsOverDisabledControl(KeyValuePair<Control, ToolTipController> kvp)
        {
            var success = PointToHandleByControl.TryRemove(kvp.Key, out Point posToHandle);
            Debug.Assert(success);
            if (kvp.Key.IsDisposed)
                return;
            // We consider the control cannot be disposed until the end of this code
            var ctlPos = kvp.Key.PointToClient(posToHandle);
            var ctl = kvp.Key.GetChildAtPoint(ctlPos) as Control;
            var ctlInner = ctl;
            if (ctl != null) // Ne devrait jamais arriver mais on sait jamais
                do
                {
                    ctlPos.Offset(-ctl.Left, -ctl.Top);
                    ctl = ctlInner;
                    ctlInner = ctl.GetChildAtPoint(ctlPos, GetChildAtPointSkip.Invisible);
                }
                while (ctlInner != null && ctlInner != ctl);
            if (ctl is BaseControl bctl && !bctl.Enabled && !string.IsNullOrWhiteSpace(bctl.ToolTip))
                kvp.Value.ShowHint(bctl.ToolTip, bctl.ToolTipTitle);
            else
                kvp.Value.HideHint();
        }

        public static Control FindControlAtPoint(Control container, Point pos)
        {
            Control child;
            foreach (Control c in container.Controls)
            {
                if (c.Visible && c.Bounds.Contains(pos))
                {
                    child = FindControlAtPoint(c, new Point(pos.X - c.Left, pos.Y - c.Top));
                    if (child == null) return c;
                    else return child;
                }
            }
            return null;
        }

        public static Control FindControlAtCursor(Control ctl)
        {
            Point pos = Cursor.Position;
            if (ctl.Bounds.Contains(pos))
                return FindControlAtPoint(ctl, ctl.PointToClient(pos));
            return null;
        }
        

        public class GlobalMouseHandler : IMessageFilter
        {
            private const int WM_MOUSEMOVE = 0x0200;
            private Point? previousMousePosition = null;
            public static event EventHandler<MouseEventArgs> MouseMovedEvent = DummyHandler;
            [DebuggerHidden, DebuggerStepThrough] static void DummyHandler(object _, EventArgs __) { /* To make null-check useless on MouseMovedEvent */}

            #region IMessageFilter Members

            [DebuggerStepThrough]
            public bool PreFilterMessage(ref Message m)
            {
                if (m.Msg == WM_MOUSEMOVE)
                {
                    System.Drawing.Point currentMousePoint = Control.MousePosition;
                    if (previousMousePosition != currentMousePoint)
                    { 
                        if (previousMousePosition.HasValue)
                        {
                            previousMousePosition = currentMousePoint;
                            MouseMovedEvent(this, new MouseEventArgs(MouseButtons.None, 0, currentMousePoint.X, currentMousePoint.Y, 0));
                        }
                        else
                            previousMousePosition = currentMousePoint;
                    }
                }
                // Always allow message to continue to the next filter control
                return false;
            }

            #endregion
        }
    }
}
