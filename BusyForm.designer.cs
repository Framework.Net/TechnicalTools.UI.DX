﻿namespace TechnicalTools.UI.DX
{
	partial class BusyForm
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.BusyFormlayoutControl1ConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.btnStop = new DevExpress.XtraEditors.SimpleButton();
            this.marqueeProgressBarControl = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.marqueeProgressBarControl_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnStop_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblProgress = new DevExpress.XtraLayout.SimpleLabelItem();
            this.spaceBetweenprogressAndButton = new DevExpress.XtraLayout.EmptySpaceItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barlblNonBlockingDevelopper = new DevExpress.XtraBars.BarStaticItem();
            this.barElapsedTimeStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barlblPleaseWait = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.timerElapsedTime = new System.Windows.Forms.Timer(this.components);
            this.designTimeSkinApplicator1 = new TechnicalTools.UI.DX.BaseClasses.DesignTimeSkinApplicator();
            ((System.ComponentModel.ISupportInitialize)(this.BusyFormlayoutControl1ConvertedLayout)).BeginInit();
            this.BusyFormlayoutControl1ConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStop_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spaceBetweenprogressAndButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // BusyFormlayoutControl1ConvertedLayout
            // 
            this.BusyFormlayoutControl1ConvertedLayout.Controls.Add(this.btnStop);
            this.BusyFormlayoutControl1ConvertedLayout.Controls.Add(this.marqueeProgressBarControl);
            this.BusyFormlayoutControl1ConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BusyFormlayoutControl1ConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.BusyFormlayoutControl1ConvertedLayout.Name = "BusyFormlayoutControl1ConvertedLayout";
            this.BusyFormlayoutControl1ConvertedLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(792, 147, 383, 350);
            this.BusyFormlayoutControl1ConvertedLayout.Root = this.layoutControlGroup1;
            this.BusyFormlayoutControl1ConvertedLayout.Size = new System.Drawing.Size(383, 105);
            this.BusyFormlayoutControl1ConvertedLayout.TabIndex = 3;
            // 
            // btnStop
            // 
            this.btnStop.Appearance.Options.UseTextOptions = true;
            this.btnStop.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnStop.Location = new System.Drawing.Point(12, 60);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(359, 33);
            this.btnStop.StyleController = this.BusyFormlayoutControl1ConvertedLayout;
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop / Cancel";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // marqueeProgressBarControl
            // 
            this.marqueeProgressBarControl.EditValue = 0;
            this.marqueeProgressBarControl.Location = new System.Drawing.Point(12, 42);
            this.marqueeProgressBarControl.Name = "marqueeProgressBarControl";
            this.marqueeProgressBarControl.Size = new System.Drawing.Size(359, 14);
            this.marqueeProgressBarControl.StyleController = this.BusyFormlayoutControl1ConvertedLayout;
            this.marqueeProgressBarControl.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.marqueeProgressBarControl_LayoutItem,
            this.btnStop_LayoutItem,
            this.lblProgress,
            this.spaceBetweenprogressAndButton});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(383, 105);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // marqueeProgressBarControl_LayoutItem
            // 
            this.marqueeProgressBarControl_LayoutItem.Control = this.marqueeProgressBarControl;
            this.marqueeProgressBarControl_LayoutItem.Location = new System.Drawing.Point(0, 30);
            this.marqueeProgressBarControl_LayoutItem.Name = "marqueeProgressBarControl_LayoutItem";
            this.marqueeProgressBarControl_LayoutItem.Size = new System.Drawing.Size(363, 18);
            this.marqueeProgressBarControl_LayoutItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.marqueeProgressBarControl_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.marqueeProgressBarControl_LayoutItem.TextVisible = false;
            // 
            // btnStop_LayoutItem
            // 
            this.btnStop_LayoutItem.Control = this.btnStop;
            this.btnStop_LayoutItem.Location = new System.Drawing.Point(0, 48);
            this.btnStop_LayoutItem.MaxSize = new System.Drawing.Size(0, 37);
            this.btnStop_LayoutItem.MinSize = new System.Drawing.Size(261, 37);
            this.btnStop_LayoutItem.Name = "btnStop_LayoutItem";
            this.btnStop_LayoutItem.Size = new System.Drawing.Size(363, 37);
            this.btnStop_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnStop_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnStop_LayoutItem.TextVisible = false;
            // 
            // lblProgress
            // 
            this.lblProgress.AllowHotTrack = false;
            this.lblProgress.Location = new System.Drawing.Point(0, 0);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(363, 17);
            this.lblProgress.Text = "Processing...";
            this.lblProgress.TextSize = new System.Drawing.Size(63, 13);
            // 
            // spaceBetweenprogressAndButton
            // 
            this.spaceBetweenprogressAndButton.AllowHotTrack = false;
            this.spaceBetweenprogressAndButton.Location = new System.Drawing.Point(0, 17);
            this.spaceBetweenprogressAndButton.Name = "spaceBetweenprogressAndButton";
            this.spaceBetweenprogressAndButton.Size = new System.Drawing.Size(363, 13);
            this.spaceBetweenprogressAndButton.TextSize = new System.Drawing.Size(0, 0);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barElapsedTimeStatus,
            this.barlblPleaseWait,
            this.barlblNonBlockingDevelopper});
            this.barManager1.MaxItemId = 3;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barlblNonBlockingDevelopper),
            new DevExpress.XtraBars.LinkPersistInfo(this.barElapsedTimeStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this.barlblPleaseWait)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barlblNonBlockingDevelopper
            // 
            this.barlblNonBlockingDevelopper.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barlblNonBlockingDevelopper.Caption = "! Warning !";
            this.barlblNonBlockingDevelopper.Id = 2;
            this.barlblNonBlockingDevelopper.Name = "barlblNonBlockingDevelopper";
            this.barlblNonBlockingDevelopper.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barElapsedTimeStatus
            // 
            this.barElapsedTimeStatus.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barElapsedTimeStatus.Caption = "Elapsed time: 0s";
            this.barElapsedTimeStatus.Id = 0;
            this.barElapsedTimeStatus.Name = "barElapsedTimeStatus";
            // 
            // barlblPleaseWait
            // 
            this.barlblPleaseWait.Caption = "Please wait...";
            this.barlblPleaseWait.Id = 1;
            this.barlblPleaseWait.Name = "barlblPleaseWait";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(383, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 105);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(383, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 105);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(383, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 105);
            // 
            // timerElapsedTime
            // 
            this.timerElapsedTime.Interval = 900;
            this.timerElapsedTime.Tick += new System.EventHandler(this.timerElapsedTime_Tick);
            // 
            // designTimeSkinApplicator1
            // 
            this.designTimeSkinApplicator1.ContainerControl = this;
            // 
            // BusyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 128);
            this.ControlBox = false;
            this.Controls.Add(this.BusyFormlayoutControl1ConvertedLayout);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.IconOptions.ShowIcon = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BusyForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = " ";
            ((System.ComponentModel.ISupportInitialize)(this.BusyFormlayoutControl1ConvertedLayout)).EndInit();
            this.BusyFormlayoutControl1ConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStop_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spaceBetweenprogressAndButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private DevExpress.XtraLayout.LayoutControl BusyFormlayoutControl1ConvertedLayout;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.EmptySpaceItem spaceBetweenprogressAndButton;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl;
        private DevExpress.XtraLayout.LayoutControlItem marqueeProgressBarControl_LayoutItem;
        private DevExpress.XtraEditors.SimpleButton btnStop;
        private DevExpress.XtraLayout.LayoutControlItem btnStop_LayoutItem;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barElapsedTimeStatus;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.Timer timerElapsedTime;
        private DevExpress.XtraBars.BarStaticItem barlblPleaseWait;
        private DevExpress.XtraBars.BarStaticItem barlblNonBlockingDevelopper;
        private BaseClasses.DesignTimeSkinApplicator designTimeSkinApplicator1;
        private DevExpress.XtraLayout.SimpleLabelItem lblProgress;
    }
}