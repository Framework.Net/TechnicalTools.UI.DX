﻿using System;

using DevExpress.XtraWaitForm;


namespace TechnicalTools.UI.DX
{
    // http://www.devexpress.com/example=E3575
    public partial class WaitFormWithProgress : WaitForm
    {
        public WaitFormWithProgress()
        {
            InitializeComponent();
            progressPanel1.AutoSize = true;
        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            progressPanel1.Description = description;
        }
        public override void ProcessCommand(Enum cmd, object arg)
        {
            var command = (WaitFormCommand)cmd;
            if (command == WaitFormCommand.SetProgress)
            {
                var value = arg is int || arg is decimal || arg is double ? Math.Round(Convert.ToDecimal(arg), 2)
                          : arg is Tuple<int, int> progressAndTarget ? (int)(100.0 * progressAndTarget.Item1 / progressAndTarget.Item2)
                          : arg;
                progressBarControl1.EditValue = value;
                progressBarControl1.ShowProgressInTaskBar = true;
            }
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum WaitFormCommand
        {
            SetProgress
        }
    }
}
