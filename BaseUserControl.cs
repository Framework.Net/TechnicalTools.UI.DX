﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TechnicalTools;

namespace TechnicalTools.UI.DX
{
    // Voir http://msdn.microsoft.com/en-us/library/ms171823.aspx pour plus d'info sur comment integrer dans le designer
    [DesignTimeVisible(false)]
    public partial class BaseUserControl : UserControl
    {
        public BaseUserControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Renvoie la taille de la bordure droite (ou gauche qui est considéré comme égale) 
        /// et la taille de la bordure haute (ou bas qui est consideré comme égale aussi).
        /// </summary>
        public Size BordersSize { get { return BordersSizeFor(Parent); } }
        public static Size BordersSizeFor(Control parent)
        {
            if (parent == null)
                return new Size();
            int side_width = (parent.Size.Width - parent.ClientRectangle.Width) / 2; // assume borders are the same for each side
            return new Size(side_width, side_width);
            // A étudier : Mais ne marche pas forcement pour les fenetre de type non sizable, ToolBox etc
            // SystemInformation.FrameBorderSize.Width
            // SystemInformation.CaptionHeight;
            // SystemInformation.BorderSize;
        }

        /// <summary>
        /// Hauteur de la barre de la fenetre (sans les bordure, pour cela voir BordersSize)
        /// </summary>
        public int TitleBarHeight { get { return TitleBarHeightFor(Parent); } }
        public static int TitleBarHeightFor(Control parent)
        {
            if (parent == null) return 0;
            return parent.Size.Substract(parent.ClientRectangle).Substract(BordersSizeFor(parent).Multiply(2)).Height;
        }

        /// <summary>
        /// Renvoie la position de ClientSize dans le rectangle du control (Size). Utile surtout pour les Form
        /// </summary>
        public Point ClientLocation { get { return ClientLocationFor(Parent); } }
        public static Point ClientLocationFor(Control parent)
        {
            return BordersSizeFor(parent).Add(0, TitleBarHeightFor(parent)).ToPoint();
        }

        /// <summary>
        /// Renvoie, lors des évènements Resizing et Moving, la valeur de Bounds précédente
        /// Cette valeur est mise à jour des que ces évènements sont terminés.
        /// </summary>
        protected Rectangle OldBounds { get { return new Rectangle(OldLocation, OldSize); } }

        #region Meilleur evenement Move : Moving
        public event MovingEventHandler Moving;

        protected void RaiseMoving(Point old_location)
        {
            Moving?.Invoke(this, new MovingEventArgs(old_location));
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);
            RaiseMoving(OldLocation);
            OldLocation = Location;
        }
        protected Point OldLocation { get; private set; }
        #endregion

        #region Meilleur evenement Resize : Resizing
        public event ResizingEventHandler Resizing;

        protected void RaiseResizing(Size old_size)
        {
            Resizing?.Invoke(this, new ResizingEventArgs(old_size));
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e); // Declenche les autres events (le userControl GlassLayer a besoin de s'executer apres les autres handler)
            RaiseResizing(OldSize);
            OldSize = Size;
        }
        protected Size OldSize { get; private set; }
        #endregion

        #region Meilleur evenement ParentChanged : ParentChanging

        public event ParentChangingEventHandler ParentChanging;

        protected void RaiseParentChanging(Control old_parent)
        {
            ParentChanging?.Invoke(this, new ParentChangingEventArgs(old_parent));
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e); // Declenche les autres events (le userControl GlassLayer a besoin de s'executer apres les autres handler)
            if (OldParent != Parent) // TODO : Vraiment utile ?
            {
                RaiseParentChanging(OldParent);
                OldParent = Parent;
            }
        }
        protected Control OldParent { get; private set; }

        #endregion Meilleur evenement ParentChanged

        #region Meilleur evenement Parent Resize : ParentResizing
        public event ResizingEventHandler ParentResizing;

        protected void RaiseParentResizing(Size old_parent_size)
        {
            ParentResizing?.Invoke(this, new ResizingEventArgs(old_parent_size));
        }

        private void InitOnParentResizing()
        {
            ParentChanging += BaseUserControl_ParentChanging;
        }

        void BaseUserControl_ParentChanging(object sender, ParentChangingEventArgs e)
        {
            if (e.OldParent != null)
                e.OldParent.Resize -= Parent_Resize;
            if (Parent != null)
                Parent.Resize += Parent_Resize;
            Parent_Resize(sender, e);
        }

        void Parent_Resize(object sender, EventArgs e)
        {
            RaiseResizing(OldParentSize);
            OldParentSize = Parent.Size;
        }
        protected Size OldParentSize { get; private set; }
        #endregion

        /// <summary>Permet d'appeler du code après tous les évènement en cours.
        ///          Cela permet par exemple d'attendre qu'une form soit dessinée avant de faire quelquechose.
        /// </summary>
        protected internal static void PostPone(MethodInvoker sub)
        {
            System.Threading.SynchronizationContext.Current.Post((_) =>
            {
                // Permet souvent à l'interface de s'afficher si PostPone est utilisé dans Form_Load
                Application.DoEvents();
                sub();
            }, null);
        }
    }


    public delegate void ParentChangingEventHandler(object sender, ParentChangingEventArgs e);
    public class ParentChangingEventArgs : EventArgs
    {
        public Control OldParent { get; private set; }

        public ParentChangingEventArgs(Control oldParent)
        {
            OldParent = oldParent;
        }
    }

    public delegate void ResizingEventHandler(object sender, ResizingEventArgs e);
    public class ResizingEventArgs : EventArgs
    {
        public Size OldSize { get; private set; }

        public ResizingEventArgs(Size oldSize)
        {
            OldSize = oldSize;
        }
    }

    public delegate void MovingEventHandler(object sender, MovingEventArgs e);
    public class MovingEventArgs : EventArgs
    {
        public Point OldLocation { get; private set; }

        public MovingEventArgs(Point oldLocation)
        {
            OldLocation = oldLocation;
        }
    }
}
