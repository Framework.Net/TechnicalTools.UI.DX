﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using DevExpress.Diagram.Core;
using DevExpress.Diagram.Core.Native;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraDiagram;
using DevExpress.XtraDiagram.Native;
using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX
{
    [ToolboxItem(true)]
    public partial class EnhancedDiagramControl : DiagramControl
    {
        [DXCategory("Behavior")]
        [DefaultValue(true)]
        public bool SelectOnRightClick { get; set; } = true; // Default behavior of DX

        public EnhancedDiagramControl()
        {
            EnablePanToolOnDragWithRightClick_Init();
            ContextMenu_Init();
            SmartMouseActionBasedOnMouseLocation_Init();
        }

        #region Disable selection on right click

        [AskToDevExpress]
        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (PanWithRightClick_MouseUp(e))
                return;
            if (e.Button != MouseButtons.Right || SelectOnRightClick)
            {
                base.OnMouseUp(e);
                return;
            }
            // After looking for clean way to disable selection on right click (SelectOnRightClick = false)
            // There is no "SelectOnRightClick" like DX WFP in OptionsBehavior
            // There is no "void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)" method to overide like WPF either :(
            // and there is no CreateContextMenu that allow us to ignore the mouseup event 
            // (anyway... developpers using MouseUp event would undergo a counter intuitive behavior)

            // So we do it in the hard way (assuming that in base.OnMouseUp there is nothing more than selection handling...

            // Call the method Control.OnMouseUp
            // This allows us to skip the behavior in DiagramControl 
            // but keep the behavior of Control class (which could be complex to mimic)
            // this allow also the even MouseUp to be raised
            var method = typeof(Control).GetMethod(nameof(OnMouseUp), BindingFlags.Instance | BindingFlags.NonPublic);
            var ftn = method.MethodHandle.GetFunctionPointer();
            var Control_OnMouseUp = (Action<MouseEventArgs>)Activator.CreateInstance(typeof(Action<MouseEventArgs>), this, ftn);
            Control_OnMouseUp(e);
        }

        #endregion Disable selection on right click

        #region Add Context menu

        [DXCategory("Behavior")]
        public event EventHandler<MouseContextMenuClick> PopupMenuShowing;

        void ContextMenu_Init()
        {
            MouseUp += EnhancedDiagramControl_MouseUp;
        }

        void EnhancedDiagramControl_MouseUp(object sender, MouseEventArgs e)
        {
            DiagramItem item = CalcHitItem(e.Location);
            if (e.Button == MouseButtons.Right)
            {
                var menu = new DXPopupMenu();
                var ee = new MouseContextMenuClick(e.Button, e.Clicks, e.X, e.Y, e.Delta,
                                                   menu, item, PointToDocument(new PointFloat(e.X, e.Y)));
                try
                {
                    PopupMenuShowing?.Invoke(this, ee);
                }
                finally
                {
                    if (menu.Items.Count > 0)
                        menu.ShowPopup(this, e.Location);
                }
            }
        }

        public class MouseContextMenuClick : MouseEventArgs
        {
            public DXPopupMenu Menu { get; }
            public DiagramItem Item { get; }
            /// <summary>
            /// DocumentLocation gives the location in document coordinate, while "Location" gives the location in canvas coordinate
            /// DocumentLocation is the property to use if you want to add an item where the mouse is.
            /// </summary>
            public PointFloat DocumentLocation { get; }

            public MouseContextMenuClick(MouseButtons button, int clicks, int x, int y, int delta,
                                         DXPopupMenu menu, DiagramItem item, PointFloat documentLocation)
                 : base(button, clicks, x, y, delta)
            {
                Menu = menu;
                Item = item;
                DocumentLocation = documentLocation;
            }
        }

        #endregion Add Context menu


        #region Enable moving canvas on right click

        [AskToDevExpress]
        [DXCategory("Behavior")]
        [DefaultValue(true)]
        public bool EnablePanToolOnDragWithRightClick { get; set; } = true;

        void EnablePanToolOnDragWithRightClick_Init()
        {
            MouseDown += EnhancedDiagramControl_MouseDown;
        }

        void EnhancedDiagramControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (EnablePanToolOnDragWithRightClick)
                if (e.Button == MouseButtons.Right)
                {
                    var shape = CalcHitItem(e.Location);
                    if (shape == null)
                    {
                        _potentialPan = true;
                        _activeToolBackup = OptionsBehavior.ActiveTool;
                        _panToolStartLocation = e.Location;
                    }
                }
        }
        bool _potentialPan;
        Point _panToolStartLocation;

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (EnablePanToolOnDragWithRightClick)
                if (_potentialPan && e.Button == MouseButtons.Right)
                {
                    if (OptionsBehavior.ActiveTool != OptionsBehavior.PanTool)
                    {
                        OptionsBehavior.ActiveTool = OptionsBehavior.PanTool;
                        base.OnMouseDown(new MouseEventArgs(MouseButtons.Left, e.Clicks, e.X, e.Y, e.Delta));
                    }
                    base.OnMouseMove(new MouseEventArgs(MouseButtons.Left, e.Clicks, e.X, e.Y, e.Delta));
                    return;
                }
            base.OnMouseMove(e);
        }
        DiagramTool _activeToolBackup;

        bool PanWithRightClick_MouseUp(MouseEventArgs e)
        {
            if (EnablePanToolOnDragWithRightClick)
                if (_potentialPan)
                {
                    var panToolsUsed = e.Location.SquareDistance(_panToolStartLocation) > 2;
                    if (panToolsUsed)
                        base.OnMouseUp(new MouseEventArgs(MouseButtons.Left, e.Clicks, e.X, e.Y, e.Delta));
                    OptionsBehavior.ActiveTool = _activeToolBackup;
                    _potentialPan = false;
                    return panToolsUsed;
                }
            return false;
        }

        #endregion

        #region Make mouse action move shape or add connector based on mouse location

        [DXCategory("Behavior")]
        [DefaultValue(DefaultBoolean.Default)]
        public DefaultBoolean SmartMouseAction { get; set; } = DefaultBoolean.Default;
        [DXCategory("Behavior")]
        [DefaultValue(5)]
        public int SwitchToConnectorEffectMarginPercent { get; set; } = 5; // Default behavior of DX

        [DXCategory("Behavior")]
        [DefaultValue(5)]
        [Description("Prevent connector to be created with null begin and/or null end item")]
        public bool PreventOrphanConnector { get; set; } = true;
        public bool MoreBeautifulConnectorByDefault { get; set; } = true;

        // Thanks to https://supportcenter.devexpress.com/ticket/details/t650459
        void SmartMouseActionBasedOnMouseLocation_Init()
        {
            MouseMove += SmartMouseActionBasedOnMouseLocation_MouseMove;
            OptionsBehavior.ConnectorTool = new EnhancedConnectorTool(this);
        }

        protected override DiagramConnector CreateConnector()
        {
            var connector = base.CreateConnector();
            if (PreventOrphanConnector)
            {
                // To make this restrictions dynamic, use event QueryConnectionPoints 
                connector.BeginPointRestrictions = ConnectorPointRestrictions.KeepConnected;
                connector.EndPointRestrictions = ConnectorPointRestrictions.KeepConnected;
            }
            if (MoreBeautifulConnectorByDefault)
            {
                connector.Type = ConnectorType.Curved;
                connector.Appearance.BorderSize = 3;
            }
            return connector;
        }

        private void SmartMouseActionBasedOnMouseLocation_MouseMove(object sender, MouseEventArgs e)
        {
            if (SmartMouseAction != DefaultBoolean.True)
                return;
            if (e.Button != MouseButtons.None)
                return;
            var shape = CalcHitItem(e.Location);
            if (shape == null || shape.IsSelected)
            {
                OptionsBehavior.ActiveTool = OptionsBehavior.PointerTool;
                return;
            }
            var locationInShape = PointToDocument(new PointFloat(e.Location));
            var parent = shape;
            while (parent != Page)
            {
                locationInShape.Offset(-parent.X, -parent.Y);
                parent = parent.ParentItem;
            }

            var margin = SwitchToConnectorEffectMarginPercent / 100f;
            var size = 1 - margin * 2;
            var dragBounds = new RectangleF(shape.Width * margin, shape.Height * margin, 
                                            shape.Width * size, shape.Height * size);
            if (dragBounds.Contains(locationInShape))
                OptionsBehavior.ActiveTool = OptionsBehavior.PointerTool;
            else
                OptionsBehavior.ActiveTool = OptionsBehavior.ConnectorTool;
        }

        // The factory connector tool seems to be used to show the connector when it build by user
        // So we cannot inherit directly from the superbase ConnectorTool :(
        public class EnhancedConnectorTool : ConnectorTool
        {
            public EnhancedConnectorTool(EnhancedDiagramControl owner)
                //: base(new ConnectorTool().ToolId, () => new ConnectorTool().ToolName, 
                //       // Redispatch to the same factorized creation method, diagram is expected to be == owner
                //       diagram => ((EnhancedDiagramControl)diagram).CreateAndInitializeDiagramConnector())
            {
                _owner = owner;
            }
            protected readonly EnhancedDiagramControl _owner;

            public override InputState CreateActiveInputState(IDiagramControl diagram, IInputElement item, IMouseButtonArgs mouseArgs, IDiagramItem previouslyCreatedItem)
            {
                if (_owner.PreventOrphanConnector)
                {
                    // from https://supportcenter.devexpress.com/Ticket/Details/T616327/diagram-connector-no-connector-without-parent-child
                    var connector = (IDiagramConnector)previouslyCreatedItem;
                    if (!connector.IsInDiagram())
                        connector.BeginPoint = mouseArgs.Position;

                    var res = !diagram.FindItemsAtPoint(mouseArgs.Position, diagram.GlueToConnectionPointDistance).Any()
                         ? item.CreatePointerToolMousePressedState(diagram, mouseArgs)
                         : item.CreateConnectorToolMousePressedState(diagram, mouseArgs, connector);
                    return res;
                }
                return base.CreateActiveInputState(diagram, item, mouseArgs, previouslyCreatedItem);
            }
        }

        #endregion

        #region Make zoom the default action for mouse wheel

        [AskToDevExpress]
        [DXCategory("Behavior")]
        [DefaultValue(true)]
        public bool ReverseZoomScrollBehavior { get; set; } = true;

        // Code from reflection, we do this because DiagramHandler.OnMouseWheel is not overridable
        protected override void OnMouseWheelCore(MouseEventArgs ev)
        {
            if (!ReverseZoomScrollBehavior)
            {
                base.OnMouseWheelCore(ev);
                return;
            }
            bool userHandled = false;
            void preventDefaultBehavior(object sender, MouseEventArgs e)
            {
                var ee = (DXMouseEventArgs)e;
                userHandled = ee.Handled; // backup if user handled event
                ee.Handled = true; // Prevent the default behavior
            }
            MouseWheel += preventDefaultBehavior; // assume this handler will be the last to be executed
            try
            {
                base.OnMouseWheelCore(ev); // Let a chance to user to handle the event
            }
            finally
            {
                MouseWheel -= preventDefaultBehavior;
            }
            if (userHandled)
                return;
            var diagramHandler = (EnhancedDiagramControlHandler)DiagramHandler;
            DXMouseEventArgs mouseArgs = DXMouseEventArgs.GetMouseArgs(ev);
            try
            {
                // Simulation of call to base.OnMouseWheel has already been done
                mouseArgs.Handled = true;
                diagramHandler.OnMouseWheel(mouseArgs);
            }
            finally
            {
                mouseArgs.Sync();
            }
        }

        protected override DiagramControlHandler CreateDiagramHandler()
        {
            return new EnhancedDiagramControlHandler(this);
        }

        class EnhancedDiagramControlHandler : DiagramControlHandler
        {
            public EnhancedDiagramControlHandler(EnhancedDiagramControl diagram)
                 : base(diagram)
            {
            }

            // Code from reflection, we just reverse the check on Keys.Control check to make zoom the default behavior
            internal void OnMouseWheel(DXMouseEventArgs e)
            {
                if ((ModifierKeysHelper.ModifierKeys & Keys.Control) == 0)
                {
                    base.DoZoom(e);
                    return;
                }
                Orientation orientation = ((ModifierKeysHelper.ModifierKeys & Keys.Shift) == 0 &&
                                          !e.IsHMouseWheel) 
                                        ? Orientation.Vertical 
                                        : Orientation.Horizontal;
                base_DoMouseWheel.Invoke(this, new object[] { e, orientation });
            }
            static readonly MethodInfo base_DoMouseWheel = typeof(DiagramControlHandler).GetMethod("DoMouseWheel", BindingFlags.Instance | BindingFlags.NonPublic).ThrowIfNull();
        }

        #endregion Make zoom the default action for mouse wheel
    }
}
