﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.Versioning;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX.Controls
{
    [DesignTimeVisible(true)]
    [DesignerCategory("TechnicalTools.UI.DX")]
    public partial class GIFPlayer : UserControl
    {
        Image _animatedImage;
        bool _currentlyAnimating;
        int _frameCount;
        
        bool _animationEnabled;

        [Localizable(true)]
        [Bindable(true)]
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Image GIF
        {
            [ResourceExposure(ResourceScope.Machine)]
            get { return _animatedImage; }
            set { SetGIF(value); }
        }

        [DefaultValue(0)]
        public int CurrentFrame
        {
            get { return _currentFrame; }
            set { _currentFrame = value; Invalidate(); }
        }
        int _currentFrame;

        [DefaultValue(false)]
        public bool Loop { get; set; } = false;

        public GIFPlayer()
        {
            InitializeComponent();
            if (DesignMode)
                return;
            DoubleBuffered = true; // Mandatory for large GIF, otherwise there is blank blinking sometimes
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (DesignMode)
            {
                base.OnPaint(e);
                return;
            }

            // Get the next frame ready for rendering.
            if (_animationEnabled)
            {
                if (!_currentlyAnimating)
                {
                    //Begin the animation only once.
                    ImageAnimator.Animate(_animatedImage, OnFrameChanged);
                    _currentlyAnimating = true;
                }

                // We don't use these line because ImageAnimator will internally call SelectActiveFrame
                // using its own "frame" value that continued to progress over time even if we called StopAnimate.
                //ImageAnimator.UpdateFrames();
                // So ...
            }

            if (_animatedImage != null)
            {
                //... we push ourselve the "frame" index we think we have to push
                // Finally, we only use ImageAnimator to delegate the threading / incrementing stuff and maangement of delay to wait between frame
                // This work of course only if all delays between any two successive frames are globally the same
                _animatedImage.SelectActiveFrame(FrameDimension.Time, _currentFrame);
                // Draw the image at index "frame" in the animation.
                e.Graphics.DrawImage(_animatedImage, new Point(0, 0));
            }
            int w = _frameCount == 0 ? 0 : bar.Width * (_currentFrame+1) / _frameCount;
            if (progress.Width != w)
                progress.Width = w;
        }

        void SetGIF(Image value)
        {
            _frameCount = value.GetFrameCount(FrameDimension.Time); // should make an exception if not a gif (?)
            var b = _animationEnabled;
            Reset();
            _animatedImage = value;
            MinimumSize = new Size(_animatedImage.Width, _animatedImage.Height + bar.Height);
            MaximumSize = MinimumSize;
            Invalidate();
            if (b)
                Play();
        }

        public void Play()
        {
            if (_animationEnabled)
                return;
            _animationEnabled = true;
            Invalidate();
            BeginInvoke((Action)(() =>
            {
                lblPause.Visible = true;
                lblPlay.Visible = false;
            }));
        }

        public void Pause()
        {
            if (!_animationEnabled)
                return;
            _animationEnabled = false;
            _currentlyAnimating = false;
            ImageAnimator.StopAnimate(_animatedImage, OnFrameChanged);
            Invalidate();
            if (!IsDisposed)
                BeginInvoke((Action)(() =>
                {
                    lblPause.Visible = false;
                    lblPlay.Visible = true;
                }));
        }
        public void Reset()
        {
            Pause();
            _currentFrame = 0;
            Invalidate();
        }

        void OnFrameChanged(object sender, EventArgs e)
        {
            if (_animationEnabled)
            {
                _currentFrame = (_currentFrame + 1) % _frameCount;
                if (_currentFrame == _frameCount - 1 && !Loop)
                    Pause();
                //UpdateProgressWidth();
                Invalidate();
            }
        }

        private void progress_MouseClick(object sender, MouseEventArgs e)
        {
            _currentFrame = _frameCount * e.X / bar.Width;
            Invalidate();
        }
        void GIFPlayer_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        void lblStop_Click(object sender, EventArgs e)
        {
            Reset();
        }
        void lblPlay_Click(object sender, EventArgs e)
        {
            Play();
        }
        void lblPause_Click(object sender, EventArgs e)
        {
            Pause();
        }
    }
}
