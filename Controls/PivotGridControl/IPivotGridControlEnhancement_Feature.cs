﻿using System;

namespace TechnicalTools.UI.DX
{
    public interface IPivotGridControlEnhancement_Feature
    {
        void Install(PivotGridControlEnhancement pgce);
        void Uninstall();
    }
}
