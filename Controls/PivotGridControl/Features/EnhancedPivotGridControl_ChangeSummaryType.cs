﻿using System;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data.PivotGrid;
using DevExpress.Utils.Menu;
using DevExpress.XtraPivotGrid;

using TechnicalTools.Logs;


namespace TechnicalTools.UI.DX
{
    public class EnhancedPivotGridControl_ChangeSummaryType : IPivotGridControlEnhancement_Feature
    {
        public EnhancedPivotGridControl_ChangeSummaryType(PivotGridControlEnhancement pgce)
        {
            Install(pgce);
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EnhancedPivotGridControl_ShowDynamicChart));
        PivotGridControlEnhancement _pgce;
        

        public void Install(PivotGridControlEnhancement pgce)
        {
            if (pgce == _pgce)
                return;
            Uninstall();
            _pgce = pgce;
            _pgce.PivotGrid.MouseDown += pgc_MouseDown;
            _pgce.PivotGrid.PopupMenuShowing += PivotGrid_PopupMenuShowing;
        }

        public void Uninstall()
        {
            if (_pgce == null)
                return;
            _pgce.PivotGrid.MouseDown -= pgc_MouseDown;
            _pgce.PivotGrid.PopupMenuShowing -= PivotGrid_PopupMenuShowing;
            _pgce = null;
        }

        void pgc_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;
            var hi = _pgce.PivotGrid.CalcHitInfo(e.Location);
            if (hi.HitTest == PivotGridHitTest.HeadersArea &&
                hi.HeaderField != null &&
                hi.HeaderField.Area.In(PivotArea.DataArea))
            {
                _lastFieldClicked = hi.HeaderField;
            }
        }
        PivotGridField _lastFieldClicked;

        void PivotGrid_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.MenuType == PivotGridMenuType.HeaderSummaries)
                return;
            if (_lastFieldClicked == null)
                return;
            var pgc = sender as PivotGridControl;

            var mnuGrouping = new DXSubMenuItem("Change Summary type to...");
            foreach (var type in Enum.GetValues(typeof(PivotSummaryType)).Cast<PivotSummaryType>())
            {
                var mnu = new DXMenuItem(type.ToString());
                mnu.Click += (_, __) => _lastFieldClicked.SummaryType = type;
                mnu.Enabled = _lastFieldClicked.SummaryType != type;
                mnuGrouping.Items.Add(mnu);
            }
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuGrouping);
        }
    }
}
