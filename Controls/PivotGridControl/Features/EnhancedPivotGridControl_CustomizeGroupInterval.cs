﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Utils.Menu;
using DevExpress.XtraPivotGrid;

using TechnicalTools.Logs;


namespace TechnicalTools.UI.DX
{
    public class EnhancedPivotGridControl_CustomizeGroupInterval : IPivotGridControlEnhancement_Feature
    {
        public EnhancedPivotGridControl_CustomizeGroupInterval(PivotGridControlEnhancement pgce)
        {
            Install(pgce);
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EnhancedPivotGridControl_ShowDynamicChart));
        PivotGridControlEnhancement _pgce;
        

        public void Install(PivotGridControlEnhancement pgce)
        {
            if (pgce == _pgce)
                return;
            Uninstall();
            _pgce = pgce;
            _pgce.PivotGrid.MouseDown += pgc_MouseDown;
            _pgce.PivotGrid.PopupMenuShowing += PivotGrid_PopupMenuShowing;
        }

        public void Uninstall()
        {
            if (_pgce == null)
                return;
            _pgce.PivotGrid.MouseDown -= pgc_MouseDown;
            _pgce.PivotGrid.PopupMenuShowing -= PivotGrid_PopupMenuShowing;
            _pgce = null;
        }

        void pgc_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;
            var hi = _pgce.PivotGrid.CalcHitInfo(e.Location);
            if (hi.HitTest == PivotGridHitTest.Value &&
                hi.ValueInfo.Field != null &&
                hi.ValueInfo.Field.Area.In(PivotArea.ColumnArea, PivotArea.RowArea))
            {
                _lastFieldClicked = hi.ValueInfo.Field as EnhancedPivotGridField;
            }
        }
        EnhancedPivotGridField _lastFieldClicked;

        void PivotGrid_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.MenuType == PivotGridMenuType.HeaderSummaries)
                return;
            if (_lastFieldClicked == null)
                return;
            var pgc = sender as PivotGridControl;

            var mnuGrouping = new DXSubMenuItem("Change grouping...");
            if (_lastFieldClicked.BoundProperty.PropertyType.RemoveNullability() == typeof(DateTime))
            {
                foreach (var interval in Enum.GetValues(typeof(PivotGroupInterval)).Cast<PivotGroupInterval>())
                {
                    var mnu = new DXMenuItem("Set grouping interval to " + interval);
                    mnu.Click += (_, __) => _lastFieldClicked.GroupInterval = interval;
                    mnu.Enabled = _lastFieldClicked.GroupInterval != interval;
                    mnuGrouping.Items.Add(mnu);
                }
            }
            else if (_lastFieldClicked.BoundProperty.PropertyType.RemoveNullability().IsNumericType())
            {
                if (_lastFieldClicked is EnhancedPivotGridField epgf)
                    foreach (var cgi in Enum.GetValues(typeof(eCustomGroupInterval)).Cast<eCustomGroupInterval>())
                    {
                        var mnu = new DXMenuItem("Set grouping interval to " + cgi);
                        mnu.Click += (_, __) => SetLogInterval(epgf, eCustomGroupInterval.Log10);
                        mnu.Enabled = epgf.GroupInterval != PivotGroupInterval.Custom
                                   || epgf.CustomGroupIntervalType != cgi;
                        mnuGrouping.Items.Add(mnu);
                    }
            }
            if (mnuGrouping.Items.Count > 0)
            {
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuGrouping);
                if (e.Menu.Items.Count > 2)
                    mnuGrouping.BeginGroup = true;
            }
        }

        public void SetLogInterval(EnhancedPivotGridField field, eCustomGroupInterval logType)
        {
            var t = field.BoundProperty.PropertyType.RemoveNullability();
            Debug.Assert(t.IsNumericType());
            field.CustomGroupIntervalType = logType;
            field.PivotGrid.CustomGroupInterval -= pgc_CustomGroupInterval;
            field.PivotGrid.CustomGroupInterval += pgc_CustomGroupInterval;
        }

        void pgc_CustomGroupInterval(object sender, PivotCustomGroupIntervalEventArgs e)
        {
            if (!(e.Field is EnhancedPivotGridField epgf) || 
                epgf.CustomGroupIntervalType == eCustomGroupInterval.None)
                return;
            if (e.Field.Area != PivotArea.ColumnArea &&
                e.Field.Area != PivotArea.RowArea)
                return;
            var t = epgf.BoundProperty.PropertyType.RemoveNullability();
            if (e.Value == null)
            {
                e.GroupValue = "No Value";
                return;
            }
            var power = getPower[t][epgf.CustomGroupIntervalType](e.Value);
            if (power == null)
                e.GroupValue = "0";
            else
            {
                var groupValues = _groupValues[epgf.CustomGroupIntervalType];
                if (!groupValues.TryGetValue(power.Value, out string group))
                {
                    switch (epgf.CustomGroupIntervalType)
                    {
                        case eCustomGroupInterval.Log2: group = "2^" + (power.Value - 1) + " < ... <= 2^" + power.Value; break;
                        case eCustomGroupInterval.Ln: group = "e^" + (power.Value - 1) + " < ... <= e^" + power.Value; break;
                        case eCustomGroupInterval.Log10: group = Math.Pow(10, power.Value - 1).ToString("#,##0.##########") + " < ... <= " + Math.Pow(10, power.Value).ToString("#,##0.##########"); break;
                        case eCustomGroupInterval.Log20: group = "20^" + (power.Value - 1) + " < ... <= 20^" + power.Value; break;
                    }
                    groupValues.Add(power.Value, group);
                }
                e.GroupValue = group;
            }
        }
        readonly Dictionary<eCustomGroupInterval, Dictionary<double, string>> _groupValues = Enum.GetValues(typeof(eCustomGroupInterval))
                                                                                                 .Cast<eCustomGroupInterval>()
                                                                                                 .ToDictionary(cgi => cgi, _ => new Dictionary<double, string>());

        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForDecimal = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (double)(decimal)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(2) ) : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (double)(decimal)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : v < 0 ? (int)Math.Floor(Math.Log  (-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (double)(decimal)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : v < 0 ? (int)Math.Floor(Math.Log10(-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (double)(decimal)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForDouble = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (double)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(2) ) : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (double)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : v < 0 ? (int)Math.Floor(Math.Log  (-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (double)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : v < 0 ? (int)Math.Floor(Math.Log10(-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (double)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForFloat = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (float)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(2) ) : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (float)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : v < 0 ? (int)Math.Floor(Math.Log  (-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (float)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : v < 0 ? (int)Math.Floor(Math.Log10(-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (float)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForInt = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (int)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(2) ) : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (int)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : v < 0 ? (int)Math.Floor(Math.Log  (-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (int)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : v < 0 ? (int)Math.Floor(Math.Log10(-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (int)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForUInt = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (uint)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (uint)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (uint)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (uint)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForLong = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (long)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(2) ) : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (long)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : v < 0 ? (int)Math.Floor(Math.Log  (-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (long)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : v < 0 ? (int)Math.Floor(Math.Log10(-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (long)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForULong = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (ulong)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (ulong)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (ulong)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (ulong)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForShort = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (short)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(2) ) : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (short)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : v < 0 ? (int)Math.Floor(Math.Log  (-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (short)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : v < 0 ? (int)Math.Floor(Math.Log10(-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (short)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForUShort = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (ushort)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (ushort)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (ushort)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (ushort)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForSByte = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (sbyte)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(2) ) : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (sbyte)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : v < 0 ? (int)Math.Floor(Math.Log  (-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (sbyte)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : v < 0 ? (int)Math.Floor(Math.Log10(-v)               ) : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (sbyte)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : v < 0 ? (int)Math.Floor(Math.Log  (-v) / Math.Log(20)) : (int?)null; } }
        };
        static readonly Dictionary<eCustomGroupInterval, Func<object, int?>> groupFuncsForByte = new Dictionary<eCustomGroupInterval, Func<object, int?>>()
        {
            { eCustomGroupInterval.Log2,  (object o) => { var v = (byte)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(2))  : (int?)null; } },
            { eCustomGroupInterval.Ln,    (object o) => { var v = (byte)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v))                : (int?)null; } },
            { eCustomGroupInterval.Log10, (object o) => { var v = (byte)o; return v > 0 ? -(int)Math.Floor(-Math.Log10(v))                : (int?)null; } },
            { eCustomGroupInterval.Log20, (object o) => { var v = (byte)o; return v > 0 ? -(int)Math.Floor(-Math.Log  (v) / Math.Log(20)) : (int?)null; } }
        };

        static readonly Dictionary<Type, Dictionary<eCustomGroupInterval, Func<object, int?>>> getPower = new Dictionary<Type, Dictionary<eCustomGroupInterval, Func<object, int?>>>()
        {
            { typeof(decimal), groupFuncsForDecimal },
            { typeof(double), groupFuncsForDouble },
            { typeof(float), groupFuncsForFloat },
            { typeof(int), groupFuncsForInt },
            { typeof(uint), groupFuncsForUInt },
            { typeof(long), groupFuncsForLong },
            { typeof(ulong), groupFuncsForULong },
            { typeof(short), groupFuncsForShort },
            { typeof(ushort), groupFuncsForUShort },
            { typeof(sbyte), groupFuncsForSByte },
            { typeof(byte), groupFuncsForByte },
        };
    }

    public enum eCustomGroupInterval
    {
        None = 0,
        Log2,
        Ln,
        Log10,
        Log20
    }

    public partial class EnhancedPivotGridField
    {
        internal eCustomGroupInterval CustomGroupIntervalType { get; set; }
    }

}
