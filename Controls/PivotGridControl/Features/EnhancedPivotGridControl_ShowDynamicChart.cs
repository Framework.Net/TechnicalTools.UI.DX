﻿using System;

using DevExpress.Utils.Menu;
using DevExpress.XtraPivotGrid;

using TechnicalTools.Logs;


namespace TechnicalTools.UI.DX
{
    public class EnhancedPivotGridControl_ShowDynamicChart : IPivotGridControlEnhancement_Feature
    {
        public EnhancedPivotGridControl_ShowDynamicChart(PivotGridControlEnhancement pgce)
        {
            Install(pgce);
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EnhancedPivotGridControl_ShowDynamicChart));
        PivotGridControlEnhancement _pgce;
        

        public void Install(PivotGridControlEnhancement pgce)
        {
            if (pgce == _pgce)
                return;
            Uninstall();
            _pgce = pgce;
            _pgce.PivotGrid.PopupMenuShowing += PivotGrid_PopupMenuShowing;
        }

        public void Uninstall()
        {
            if (_pgce == null)
                return;
            _pgce.PivotGrid.PopupMenuShowing -= PivotGrid_PopupMenuShowing;
            _pgce = null;
        }

        void PivotGrid_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.MenuType == PivotGridMenuType.HeaderSummaries)
                return;
            var pgc = sender as PivotGridControl;
            var mnuShowChart = new DXMenuItem("Show Dynamic Chart", (_, __) => ShowChart());
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuShowChart);
        }

        private void ShowChart()
        {
            var view = new EnhancedXtraUserControl();
            var ctl = new Controls.EnhancedChartControl();
            ctl.Dock = System.Windows.Forms.DockStyle.Fill;
            ctl.DataSource = _pgce.PivotGrid;
            view.Controls.Add(ctl);
            var parentForm = _pgce.PivotGrid.FindForm() ?? EnhancedXtraForm.MainForm;
            view.Size = new System.Drawing.Size(parentForm.Width * 80 / 100, parentForm.Height * 80 / 100);
            var satelliteFrm = EnhancedXtraForm.CreateWithInner(view, parentForm);
            satelliteFrm.Show(parentForm);
        }
    }
}
