﻿using System;
using System.Linq;
using DevExpress.Utils.Menu;
using DevExpress.XtraPivotGrid;

using TechnicalTools.Logs;


namespace TechnicalTools.UI.DX
{
    // Make behavior of field list form more intuitive
    public class EnhancedPivotGridControl_FieldListFormBehavior : IPivotGridControlEnhancement_Feature
    {
        public EnhancedPivotGridControl_FieldListFormBehavior(PivotGridControlEnhancement pgce)
        {
            Install(pgce);
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EnhancedPivotGridControl_ShowDynamicChart));
        PivotGridControlEnhancement _pgce;
        

        public void Install(PivotGridControlEnhancement pgce)
        {
            if (pgce == _pgce)
                return;
            Uninstall();
            _pgce = pgce;

            // A defaut static location in case a developper want to show the ofrm before user use menu
            // By default the location is screen's bottom right  :/
            _pgce.PivotGrid.CustomizationFormBounds = new System.Drawing.Rectangle(150, 150, 0, 0);

            _pgce.PivotGrid.MouseDown += pgc_MouseDown;
            _pgce.PivotGrid.PopupMenuShowing += pgc_PopupMenuShowing;
        }

        public void Uninstall()
        {
            if (_pgce == null)
                return;
            _pgce.PivotGrid.MouseDown -= pgc_MouseDown;
            _pgce.PivotGrid.PopupMenuShowing -= pgc_PopupMenuShowing;
            _pgce = null;
        }

        // To dynamically update the position where the user click
        void pgc_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                var p = _pgce.PivotGrid.PointToScreen(e.Location);
                _pgce.PivotGrid.CustomizationFormBounds = new System.Drawing.Rectangle(p.X, p.Y, 0, 0);
            }
        }

        // By Default DX created this menu but not always (when user click on data for example
        // This is an nnoying behavior for user that doesn ot understand why the menu does always exists
        void pgc_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            // By Default DX created this menu but not always (when user click on data for example
            // This is an nnoying behavior for user that doesn ot understand why the menu does always exists
            if (e.MenuType == PivotGridMenuType.HeaderSummaries)
                return;
            if (e.Menu.Items.Cast<DXMenuItem>().All(mnu => mnu.Caption != "Hide Field List"
                                                        && mnu.Caption != "Show Field List"))
            {
                var mnu = _pgce.PivotGrid.CustomizationForm?.Visible ?? false
                        ? new DXMenuItem("Hide Field List", (_, __) => _pgce.PivotGrid.HideCustomization())
                        : new DXMenuItem("Show Field List", (_, __) => _pgce.PivotGrid.FieldsCustomization());
                if (e.Menu.Items.Count > 0)
                    mnu.BeginGroup = true;
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnu);
            }
        }
    }
}
