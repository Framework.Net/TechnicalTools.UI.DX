﻿using System;
using System.Collections.Generic;
using System.Drawing;

using DevExpress.Data;
using DevExpress.Data.Filtering.Helpers;
using DevExpress.XtraEditors.Filtering;
using DevExpress.XtraEditors.Repository;


namespace TechnicalTools.UI.DX.Controls
{
    public class MappedFilterColumn : UnboundFilterColumn
    {
        // For Children, see https://www.devexpress.com/Support/Center/Question/Details/T367680/how-to-add-relation-based-filter-column-to-filtercontrol
        public override bool                 HasChildren { get { return base.HasChildren; } }
        public override List<IBoundProperty>    Children { get { return base.Children; } }
        
        public MappedFilterColumn(string columnCaption, string fieldName, Type columnType, RepositoryItem columnEdit, FilterColumnClauseClass clauseClass)
            : base(columnCaption, fieldName, columnType, columnEdit, clauseClass)
        {
        }

        public override void SetColumnCaption(string caption) { base.SetColumnCaption(caption); }

        public override RepositoryItem CreateItemCollectionEditor() { return base.CreateItemCollectionEditor(); }
        public override void SetColumnEditor(RepositoryItem item)   { base.SetColumnEditor(item); }
        public override void SetImage(Image image)                  { base.SetImage(image); }
        public override void SetParent(IBoundProperty parent)       { base.SetParent(parent); }


        //public override bool IsValidClause(DevExpress.Data.Filtering.Helpers.ClauseType clause)
        //{
        //    return MappedFilterControl.GetTypeRelatedClauseTypeSet(ColumnType).Contains(clause);
        //    //return clause == DevExpress.Data.Filtering.Helpers.ClauseType.Between ||
        //    //clause == DevExpress.Data.Filtering.Helpers.ClauseType.Greater ||
        //    //clause == DevExpress.Data.Filtering.Helpers.ClauseType.Less ||
        //    //clause == DevExpress.Data.Filtering.Helpers.ClauseType.LessOrEqual ||
        //    //clause == DevExpress.Data.Filtering.Helpers.ClauseType.GreaterOrEqual ||
        //    //clause == DevExpress.Data.Filtering.Helpers.ClauseType.Equals;
        //}
    }
}
