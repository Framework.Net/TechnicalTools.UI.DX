﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using DevExpress.Data.Filtering.Helpers;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Filtering;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX.Helpers;


namespace TechnicalTools.UI.DX.Controls
{
    // INTERESTING FOR QUERY Builder : https://www.devexpress.com/Support/Center/Question/Details/T574308/filtercontrol-default-operand-value
    // TO SEE : https://www.devexpress.com/Support/Center/Question/Details/Q334725/how-could-i-add-a-new-clausetype-like-not-begins-with-to-filtercontrol
    // https://www.devexpress.com/Support/Center/Question/Details/T282799/best-way-to-have-selection-list-in-filtercontrol
    // https://www.devexpress.com/Support/Center/Example/Details/E3457/how-to-implement-a-custom-menu-for-filtercontrol
    // https://www.devexpress.com/Support/Center/Question/Details/T463103/filtercontrol-is-it-possible-to-force-the-criteria-part-to-always-be-a-check-list
    // https://documentation.devexpress.com/CoreLibraries/5206/DevExpress-ORM-Tool/Examples/How-to-Implement-a-Custom-Criteria-Language-Operator
    // https://www.devexpress.com/Support/Center/Question/Details/Q498502/updating-value-in-filtercontrol
    // https://www.devexpress.com/Support/Center/Question/Details/Q488581/filtercontrol-customclausetype-with-n-of-additionaloperands-similar-to-clausetype-anyof
    // https://www.devexpress.com/Support/Center/Question/Details/Q264421/filtercontrol-customizing-individual-node-colors
    [DesignTimeVisible(true)]
    [ToolboxItem(true)]
    [DesignerCategory("TechnicalTools.UI.DX")]
    public partial class MappedFilterControl : FilterControl //FilterEditorControl
    {
        public MappedFilterControl()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            // Make incremental searching supported on column selection. 
            // We can select values by typing text within the edit box.
            UseMenuForOperandsAndOperators = true;

            ClauseTypeManagement();

            PopupMenuShowing += MappedFilterControl_PopupMenuShowing;

            // Only if base type is FilterEditor
            ReadOnly_Management(); 
        }

        public void Init(Type type, string filterCriteria, IEnumerable<FilterColumn> columns = null, bool allowInterface = false)
        {
            _type = type;
            _originalFilterCriteria = filterCriteria;
            var key = Tuple.Create(type, allowInterface);
            if (columns == null)
                columns = _FilterColumns.TryGetValueClass(key);
            if (columns == null)
            {
                if (type != null)
                {
                    var lst = CriteriaParserInitializer.Instance.BuildFilterColumns(type, allowInterface);
                    _FilterColumns.Add(key, lst);
                    columns = lst;
                }
            }

            FilterColumns.Clear();
            if (columns != null)
                foreach (var col in columns)
                    FilterColumns.Add(col);

            FilterString = filterCriteria; // Must be set after column are created so DX can map field to column and use Caption instead of display name (see https://www.devexpress.com/Support/Center/Question/Details/Q529094/filtercontrol-set-the-columncaption-via-the-operandproperty-criteriaoperator)
        }
        Type _type;
        string _originalFilterCriteria;
        static readonly Dictionary<Tuple<Type, bool>, List<FilterColumn>> _FilterColumns = new Dictionary<Tuple<Type, bool>, List<FilterColumn>>();

        private void MappedFilterControl_PopupMenuShowing(object sender, DevExpress.XtraEditors.Filtering.PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            e.Menu.Items.Add(new DXMenuHeaderItem() { Caption = "About this condition" });
            e.Menu.Items.Add(new DXMenuItem("Move Up", MoveUp));
            e.Menu.Items.Add(new DXMenuItem("Move Down", MoveDown));
            e.Menu.Items.Add(new DXMenuItem("Move to Top", MoveToTop));
            e.Menu.Items.Add(new DXMenuItem("Move to bottom", MoveToBottom));
            e.Menu.Items.Add(new DXMenuItem("Creater an outer group", CreateOuterGroup));
        }


        void MoveUp(object sender, EventArgs e)
        {
            var selectedNode = FocusInfo.Node as Node;
            GroupNode parentNode = selectedNode.ParentNode as GroupNode;
            if (parentNode == null)
                return;
            var childrens = parentNode.GetChildren(); // return reference, not a copy
            var index = childrens.IndexOf(selectedNode); // capture the index of selected node by user 
            if (index == 0)
                return;
            childrens.RemoveAt(index);
            --index;
            childrens.Insert(index, selectedNode);
        }
        void MoveDown(object sender, EventArgs e)
        {
            var selectedNode = FocusInfo.Node as Node;
            GroupNode parentNode = selectedNode.ParentNode as GroupNode;
            if (parentNode == null)
                return;
            var childrens = parentNode.GetChildren(); // return reference, not a copy
            var index = childrens.IndexOf(selectedNode); // capture the index of selected node by user 
            if (index == childrens.Count - 1)
                return;
            childrens.RemoveAt(index);
            ++index;
            childrens.Insert(index, selectedNode);
        }
        void MoveToTop(object sender, EventArgs e)
        {            
            var selectedNode = FocusInfo.Node as Node;
            GroupNode parentNode = selectedNode.ParentNode as GroupNode;
            if (parentNode == null)
                return;
            var childrens = parentNode.GetChildren(); // return reference, not a copy
            var index = childrens.IndexOf(selectedNode); // capture the index of selected node by user 
            if (index == 0)
                return;
            childrens.RemoveAt(index);
            childrens.Insert(0, selectedNode);
        }
        void MoveToBottom(object sender, EventArgs e)
        {
            var selectedNode = FocusInfo.Node as Node;
            GroupNode parentNode = selectedNode.ParentNode as GroupNode;
            if (parentNode == null)
                return;
            var childrens = parentNode.GetChildren(); // return reference, not a copy
            var index = childrens.IndexOf(selectedNode); // capture the index of selected node by user 
            if (index == childrens.Count - 1)
                return;
            childrens.RemoveAt(index);
            childrens.Add(selectedNode);
        }

        void CreateOuterGroup(object sender, EventArgs e)
        {
            // Note : we have to do node remove before the add back for the same node
            // Otherwise it throw an exception whil dealing with ParentNode property that seems to be reseted to null
            var selectedNode = FocusInfo.Node as Node;
            GroupNode parentNode = selectedNode.ParentNode as GroupNode;
            var newNodeType = ((selectedNode as GroupNode)?.NodeType ?? GroupType.And).In(GroupType.And, GroupType.NotAnd)
                            ? GroupType.Or
                            : GroupType.And;
            if (parentNode == null)
            {
                Model.BeginUpdate();
                GroupNode newGroupNode = Model.CreateGroupNode(null);
                newGroupNode.NodeType = newNodeType;
                Model.RootNode = newGroupNode;
                newGroupNode.AddNode(selectedNode);
                FocusInfo = new FilterControlFocusInfo(newGroupNode, 0);
                Model.EndUpdate(FilterChangedAction.RebuildWholeTree);
            }
            else
            {
                var childrens = parentNode.GetChildren(); // return reference, not a copy
                Model.BeginUpdate();
                GroupNode newGroupNode = Model.CreateGroupNode(parentNode); // add node at the end
                newGroupNode.NodeType = newNodeType;
                childrens.RemoveAt(childrens.Count - 1); // remove it
                var index = childrens.IndexOf(selectedNode); // capture the index of selected node by user 
                selectedNode.DeleteElement(); // remove the selected node by user, from where it was
                childrens.Insert(index, newGroupNode); // put the gnew group node instead
                newGroupNode.AddNode(selectedNode); // add back the node selected node by user in new group
                FocusInfo = new FilterControlFocusInfo((Node)parentNode.GetChildren()[index], 0);
                Model.EndUpdate(FilterChangedAction.RebuildWholeTree);
            }
        }

        // Only if base type is FilterEditor
        #region Prevent user to change left operand and operator (from https://www.devexpress.com/Support/Center/Question/Details/Q528079/creating-a-derived-filtercontrol-that-prevents-changes-to-anything-other-than-property)
        // MODULE A TESTER !

        public bool PreventUserToChangeOperand { get; set; }

        void ReadOnly_Management()
        {
            PopupMenuShowing += ReadOnly_PopupMenuShowing;
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            if (PreventUserToChangeOperand)
            {
                DebugTools.Assert(false, "A TESTER");
                FilterControlLabelInfo li = Model.GetLabelInfoByCoordinates(e.X, e.Y);
                if (li == null || li.Text == null)
                    return;
                if (li.Text.Contains("@"))
                    return;
            }
            base.OnMouseDown(e);
        }
        // Avoid delete nodes by pressing the Delete key.
        private void ReadOnly_PopupMenuShowing(object sender, DevExpress.XtraEditors.Filtering.PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (PreventUserToChangeOperand)
            {
                DebugTools.Assert(false, "A TESTER");
                if (e.MenuType == FilterControlMenuType.Group)
                    e.Cancel = true;
            }
        }

        #endregion

        // Only if base type is FilterEditor
        #region from https://www.devexpress.com/Support/Center/Question/Details/T554706/filtereditorcontrol-how-to-use-searchlookupedit-to-select-the-property-operand


        protected override void ShowElementMenu(ElementType type)
        {
            if (type == ElementType.Property)
            {
                if (edit == null)
                {
                    edit = new SearchLookUpEdit { Parent = Parent.FindForm() };
                    edit.Size = new Size(300, 300);
                    var focusedItem = FocusedItem;
                    edit.Location = new Point(focusedItem.ItemBounds.X, focusedItem.ItemBounds.Bottom);
                    edit.Visible = false;
                    GridView gv = edit.Properties.View;
                    gv.OptionsView.ShowColumnHeaders = false;
                    gv.Columns.AddVisible("FieldName");
                    edit.Properties.DataSource = FilterColumns;
                }
                // unsubscribe and subscribe again to prevent false event and avoid null check in our code and (maybe) DX' code  
                edit.EditValueChanged -= edit_EditValueChanged;
                // we have to put a value that wont be selected by user 
                // otherwise the EditValueChanged is not raised and the filter criteria is not updated
                // See progress from here : http://devexpress.com/Support/Center/Question/Details/T554706/filtereditorcontrol-how-to-use-searchlookupedit-to-select-the-property-operand
                edit.EditValue = null; 
                edit.EditValueChanged += edit_EditValueChanged;
                edit.ShowPopup();
            }
            else
            {
                base.ShowElementMenu(type);
                if (type == ElementType.Group)
                {

                }
            }
        }
        SearchLookUpEdit edit = null;

        internal void edit_EditValueChanged(object sender, EventArgs e)
        {
            SearchLookUpEdit edit = sender as SearchLookUpEdit;
            MenuItemPropertyClick(edit.EditValue.ToString());
        }


        FilterLabelInfoTextViewInfo FocusedItem
        {
            get
            {
                if (FocusInfo.Node == null || !Model.IsLabelRegistered(FocusInfo.Node)) return null;
                for (int i = 0; i < Model[FocusInfo.Node].ViewInfo.Count; i++)
                {
                    var element = Model[FocusInfo.Node].ViewInfo[i] as FilterLabelInfoTextViewInfo;
                    if (element.InfoText.Tag is NodeEditableElement nodeElement && 
                        nodeElement.CreateFocusInfo() == FocusInfo)
                        return element;
                }
                return null;
            }
        }

        #endregion
    }

}
