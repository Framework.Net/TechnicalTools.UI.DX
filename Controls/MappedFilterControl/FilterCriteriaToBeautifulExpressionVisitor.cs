﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.Data.Filtering;

using TechnicalTools;
using TechnicalTools.Model;

using DataMapper;


namespace TechnicalTools.UI.DX.Controls
{
    // TODO : when DX version >= 18.1, read : https://www.devexpress.com/Support/Center/Question/Details/T320172/how-to-traverse-through-and-modify-the-criteriaoperator-instances
    // https://documentation.devexpress.com/CoreLibraries/4928/DevExpress-Data-Library/Criteria-Language-Syntax
    public class FilterCriteriaToBeautifulExpressionVisitor : ICriteriaVisitor<string>, IClientCriteriaVisitor<string>
    {
        public static string Convert(CriteriaOperator op, Type type)
        {
            var transformer = new FilterCriteriaToBeautifulExpressionVisitor(type);
            var code = op.Accept(transformer);
            return code;
        }

        private FilterCriteriaToBeautifulExpressionVisitor(Type type)
        {
            _type = type;
        }
        readonly Type _type;

        #region ICriteriaVisitor Members

        string ICriteriaVisitor<string>.Visit(FunctionOperator theOperator)
        {
            List<string> parameters = new List<string>();
            foreach (CriteriaOperator operand in theOperator.Operands)
                parameters.Add(operand.Accept(this));
            return string.Format("{0}({1})", GetFunctionName(theOperator.OperatorType),
                string.Join(", ", parameters.ToArray()));
        }

        string ICriteriaVisitor<string>.Visit(GroupOperator theOperator)
        {
            var op = GetGroupOperatorAsCode(theOperator.OperatorType);
            var space = new string(' ', op.Length);
            List<string> operands = new List<string>();
            foreach (CriteriaOperator operand in theOperator.Operands)
                operands.Add(operand.Accept(this).Replace(Environment.NewLine, Environment.NewLine + space));
            if (theOperator.OperatorType == GroupOperatorType.Or)
                space = space.Truncate(space.Length - 1);
            return (theOperator.OperatorType == GroupOperatorType.Or ? "(" : "")
                 + space
                 + operands.Join(Environment.NewLine + op)
                 + (theOperator.OperatorType == GroupOperatorType.Or ? ")" : "");
        }

        string ICriteriaVisitor<string>.Visit(InOperator theOperator)
        {
            List<string> operands = new List<string>();
            foreach (CriteriaOperator operand in theOperator.Operands)
                operands.Add(operand.Accept(this));
            var left = theOperator.LeftOperand.Accept(this);
            var lst = string.Join(", ", operands.ToArray());
            if (lst.Length > 50)
                lst = string.Join("," + Environment.NewLine, operands.ToArray());
            var res = left + " In (";
            return res + lst.Replace(Environment.NewLine, Environment.NewLine + new string(' ', res.Length));
        }

        string ICriteriaVisitor<string>.Visit(UnaryOperator theOperator)
        {
            var opStr = GetUnaryOperationName(theOperator.OperatorType);
            var right = theOperator.Operand.Accept(this);
            var space = new string(' ', opStr.Length + 1);
            return opStr + " " + right.Replace(Environment.NewLine, Environment.NewLine + space);
        }

        string ICriteriaVisitor<string>.Visit(BinaryOperator theOperator)
        {
            string left = theOperator.LeftOperand.Accept(this);
            string op = GetBinaryOperationName(theOperator.OperatorType);
            var rightOpValue = theOperator.RightOperand as OperandValue;
            if (!ReferenceEquals(rightOpValue, null) && rightOpValue.Value == null)
                op = "is";
            string right = theOperator.RightOperand.Accept(this);
            return string.Format("{0} {1} {2}", left, op, right);
        }

        string ICriteriaVisitor<string>.Visit(BetweenOperator theOperator)
        {
            var test = theOperator.TestExpression.Accept(this);
            var begin = theOperator.BeginExpression.Accept(this);
            var end = theOperator.EndExpression.Accept(this);

            return string.Format("{0} between {1} and {2}", test, begin, end);
        }


        string ICriteriaVisitor<string>.Visit(OperandValue theOperand)
        {
            if (theOperand.Value == null)
                return "Empty";
            if (theOperand.Value is bool)
                return ((bool)theOperand.Value).ToString().ToLowerInvariant();
            if (theOperand.Value.GetType().IsNumericType())
                return theOperand.Value.ToStringInvariant();
            if (theOperand.Value is TimeSpan)
                return ((TimeSpan)theOperand.Value).ToHumanReadableShortNotation();
            if (theOperand.Value is DateTime dt)
            {
                if (dt.Date == dt)
                    return dt.ToString("d");
                if (dt.Second == 0 && dt.Millisecond == 0)
                    return dt.ToString("dd/MM/yyy HH:mm");
                return dt.ToString();
            }
            
            if (theOperand.Value is Enum)
            {
                return (theOperand.Value as Enum).GetDescription();
            }
            if (theOperand.Value is string)
            {
                return "\""  // Should be more complex but we will never need other cases.
                     + ((string)theOperand.Value).Replace(@"\", @"\\").Replace("\"", "\\\"")
                     + "\"";
            }
            if (theOperand.Value is char)
            {
                return "'" + theOperand.Value + "'";
            }
            if (theOperand.Value is DynamicEnum de)
                return "\"" + de.Caption + "\"";
            if (theOperand.Value is IStaticEnum se)
                return "\"" + se.ToString() + "\"";
            return null;
        }

        #endregion

        #region IClientCriteriaVisitor Members

        string IClientCriteriaVisitor<string>.Visit(OperandProperty theOperand)
        {
            return theOperand.PropertyName;
        }

        string IClientCriteriaVisitor<string>.Visit(AggregateOperand theOperand)
        {
            throw new NotImplementedException();
        }

        string IClientCriteriaVisitor<string>.Visit(JoinOperand theOperand)
        {
            throw new NotImplementedException();
        }

        #endregion

        string GetGroupOperatorAsCode(GroupOperatorType operatorType)
        {
            switch (operatorType)
            {
                case GroupOperatorType.And: return " AND ";
                case GroupOperatorType.Or: return " OR ";
                default: return operatorType.ToString();
            }
        }

        
        string GetFunctionName(FunctionOperatorType operatorType)
        {
            switch (operatorType)
            {
                case FunctionOperatorType.Contains: return "Contains";
                case FunctionOperatorType.Concat: return "Concat";
                case FunctionOperatorType.Iif: return "Iif";
                case FunctionOperatorType.IsNull: return "Is Empty";
                case FunctionOperatorType.StartsWith: return " starts with ";
                case FunctionOperatorType.EndsWith: return " ends with ";
                default:
                    Debug.Fail("operatorType " + operatorType + " to handle!"); // TODO !
                    return operatorType.ToString();
            }
        }

        string GetUnaryOperationName(UnaryOperatorType operatorType)
        {
            switch (operatorType)
            {
                case UnaryOperatorType.BitwiseNot: return "~";
                case UnaryOperatorType.IsNull: return "Empty == ";
                case UnaryOperatorType.Minus: return "-";
                case UnaryOperatorType.Not: return "Not";
                case UnaryOperatorType.Plus: return "+";
                default: return string.Empty;
            }
        }

        string GetBinaryOperationName(BinaryOperatorType operatorType)
        {
            switch (operatorType)
            {
                case BinaryOperatorType.Equal: return "=";
                case BinaryOperatorType.NotEqual: return "!=";
                case BinaryOperatorType.Greater: return ">";
                case BinaryOperatorType.Less: return "<";
                case BinaryOperatorType.LessOrEqual: return "<=";
                case BinaryOperatorType.GreaterOrEqual: return ">=";
                case BinaryOperatorType.BitwiseAnd: return "&";
                case BinaryOperatorType.BitwiseOr: return "|";
                case BinaryOperatorType.BitwiseXor: return "^";
                case BinaryOperatorType.Divide: return "/";
                case BinaryOperatorType.Modulo: return "%";
                case BinaryOperatorType.Multiply: return "*";
                case BinaryOperatorType.Plus: return "+";
                case BinaryOperatorType.Minus: return "-";
                default:                       return operatorType.ToString();
            }
        }

    }

}
