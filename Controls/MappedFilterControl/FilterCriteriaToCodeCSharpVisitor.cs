﻿using System;
using System.Collections.Generic;
using System.Linq;

using DevExpress.Data.Filtering;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace TechnicalTools.UI.DX.Controls
{
    // TODO : when DX version >= 18.1, read : https://www.devexpress.com/Support/Center/Question/Details/T320172/how-to-traverse-through-and-modify-the-criteriaoperator-instances
    // https://documentation.devexpress.com/CoreLibraries/4928/DevExpress-Data-Library/Criteria-Language-Syntax
    public class FilterCriteriaToCodeCSharpVisitor : ICriteriaVisitor<string>, IClientCriteriaVisitor<string>
    {
        public static Tuple<string, List<string>> ConvertToCSharp(CriteriaOperator op, Type type, string objVarName)
        {
            var transformer = new FilterCriteriaToCodeCSharpVisitor(type, objVarName);
            var code = op.Accept(transformer);
            return Tuple.Create(code, transformer._Errors.ToList());
        }

        private FilterCriteriaToCodeCSharpVisitor(Type type, string objVarName)
        {
            _type = type;
            _objVarName = objVarName;
        }
        readonly string _objVarName;
        readonly Type _type;
        readonly List<string> _Errors = new List<string>();
        readonly Stack<CriteriaOperator> _nodes = new Stack<CriteriaOperator>();

        #region ICriteriaVisitor Members

        string ICriteriaVisitor<string>.Visit(FunctionOperator theOperator)
        {
            _nodes.Push(theOperator);
            List<string> parameters = new List<string>();
            foreach (CriteriaOperator operand in theOperator.Operands)
                parameters.Add(operand.Accept(this));
            _nodes.Pop();
            if (theOperator.OperatorType == FunctionOperatorType.StartsWith ||
                theOperator.OperatorType == FunctionOperatorType.EndsWith ||
                theOperator.OperatorType == FunctionOperatorType.Contains)
                return string.Format("({1} ?? string.Empty).{0}({2})", GetFunctionName(theOperator.OperatorType), parameters[0], parameters[1]);
            if (theOperator.OperatorType == FunctionOperatorType.IsNull)
                return string.Format("{0}({1})", GetFunctionName(theOperator.OperatorType), string.Join(", ", parameters.ToArray()));

            throw new TechnicalException("Dont know how to group operator and operand for this operator!", null);
        }

        string ICriteriaVisitor<string>.Visit(GroupOperator theOperator)
        {
            _nodes.Push(theOperator);
            List<string> operands = new List<string>();
            foreach (CriteriaOperator operand in theOperator.Operands)
                operands.Add(string.Format("({0})", operand.Accept(this)));
            _nodes.Pop();
            var op = GetGroupOperatorAsCode(theOperator.OperatorType);
            return new string(' ', op.Length) 
                 + operands.Join(Environment.NewLine + op);
        }

        string ICriteriaVisitor<string>.Visit(InOperator theOperator)
        {
            _nodes.Push(theOperator);
            List<string> operands = new List<string>();
            foreach (CriteriaOperator operand in theOperator.Operands)
                operands.Add(operand.Accept(this));
            var left = theOperator.LeftOperand.Accept(this);
            _nodes.Pop();
            return string.Format("{0}.In({1})", left, string.Join(", ", operands.ToArray()));
        }

        string ICriteriaVisitor<string>.Visit(UnaryOperator theOperator)
        {
            _nodes.Push(theOperator);
            var right = theOperator.Operand.Accept(this);
            _nodes.Pop();
            return string.Format("{0} ({1})", GetUnaryOperationName(theOperator.OperatorType), right);
        }

        string ICriteriaVisitor<string>.Visit(BinaryOperator theOperator)
        {
            _nodes.Push(theOperator);
            string left = theOperator.LeftOperand.Accept(this);
            string op = GetBinaryOperationName(theOperator.OperatorType);
            string right = theOperator.RightOperand.Accept(this);
            _nodes.Pop();
            return string.Format("({0} {1} {2})", left, op, right);
        }

        string ICriteriaVisitor<string>.Visit(BetweenOperator theOperator)
        {
            _nodes.Push(theOperator);
            var test = theOperator.TestExpression.Accept(this);
            var begin = theOperator.BeginExpression.Accept(this);
            var end = theOperator.EndExpression.Accept(this);
            _nodes.Pop();

            return string.Format("(({0}) >= ({1})) && (({0}) <= ({2}))", test, begin, end);
        }


        string ICriteriaVisitor<string>.Visit(OperandValue theOperand)
        {
            if (theOperand.Value == null)
                return "null";
            if (theOperand.Value is bool)
                return ((bool)theOperand.Value).ToString().ToLowerInvariant();
            if (theOperand.Value.GetType().IsNumericType())
                return (theOperand.Value).ToStringInvariant()
                    + (theOperand.Value is decimal ? "m" : "")
                    + (theOperand.Value is float ? "f" : "")
                    + (theOperand.Value is uint ? "u" : "")
                    + (theOperand.Value is long ? "L" : "")
                    + (theOperand.Value is ulong ? "UL" : "");
            if (theOperand.Value is TimeSpan)
                return "TimeSpan.FromTicks(" + ((TimeSpan)theOperand.Value).Ticks.ToStringInvariant() + ")";
            if (theOperand.Value is DateTime dt)
            {
                if (dt.Date == dt)
                    return $"new DateTime({dt.Year}, {dt.Month}, {dt.Day})";
                if (dt == new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second))
                    return $"new DateTime({dt.Year}, {dt.Month}, {dt.Day}, {dt.Hour}, {dt.Minute}, {dt.Second})";
                return "new DateTime(" + dt.Ticks.ToStringInvariant() + ")";
            }
            
            if (theOperand.Value is Enum)
            {
                var t = theOperand.Value.GetType();
                return t.ToSmartString(true) + "." + theOperand.Value;
            }
            if (theOperand.Value is string)
            {
                return "\""  // Should be more complex but we will never need other cases.
                     + ((string)theOperand.Value).Replace(@"\", @"\\").Replace("\"", "\\\"")
                     + "\"";
            }
            if (theOperand.Value is char)
            {
                return "'" + theOperand.Value + "'";
            }
            if (theOperand.Value is DynamicEnum de)
            {
                if (!(de is IHasTypedIdReadable))
                    throw new TechnicalException($"Cannot convert {de.GetType().FullName} to C# Code. Make this type inherits from {typeof(DynamicEnum<,>).ToSmartString()}!", null);

                return de.GetType().FullName + "." + nameof(DynamicEnum.DummyEnumWithId.GetValueFor) + "(" + (de as IHasTypedIdReadable).Id.Keys[0].ToStringInvariant() + ", null)";
            }
            if (theOperand.Value is IStaticEnum se)
            {
                if (!(se is IHasTypedIdReadable))
                    throw new TechnicalException($"Cannot convert {se.GetType().FullName} to C# Code. Make this type implements {typeof(IHasTypedIdReadable).Name}!", null);
                var valueInfo = IStaticEnum_Extensions.GetValueSet(se.GetType());
                return se.GetType().FullName + "." + valueInfo[se.Id.ToStringInvariant()].Field.Name;
            }

            _Errors.Add($"Don't know how to Serialize OperandValue '{{{theOperand.Value}}}'!");
            return null;
        }
        
        private Type GetOperandType(OperandProperty leftOperand)
        {
            var parts = leftOperand.PropertyName.Split('.');
            var t = _type;
            foreach (var p in parts)
            {
                var prop = t.GetProperty(p);
                t = prop.PropertyType;
            }
            return t;
        }


        #endregion

        #region IClientCriteriaVisitor Members

        string IClientCriteriaVisitor<string>.Visit(OperandProperty theOperand)
        {
            return _objVarName + "." + theOperand.PropertyName;
        }

        string IClientCriteriaVisitor<string>.Visit(AggregateOperand theOperand)
        {
            throw new NotImplementedException();
        }

        string IClientCriteriaVisitor<string>.Visit(JoinOperand theOperand)
        {
            throw new NotImplementedException();
        }

        #endregion

        string GetGroupOperatorAsCode(GroupOperatorType operatorType)
        {
            switch (operatorType)
            {
                case GroupOperatorType.And: return " && ";
                case GroupOperatorType.Or: return " || ";
                default:
                    _Errors.Add("Don't know how to handle group operator " + operatorType.ToString() + "!");
                    return operatorType.ToString();
            }
        }

        
        string GetFunctionName(FunctionOperatorType operatorType)
        {
            switch (operatorType)
            {
                case FunctionOperatorType.Concat: return "Concat";
                case FunctionOperatorType.Contains: return "Contains";
                case FunctionOperatorType.Iif: return "Iif";
                case FunctionOperatorType.IsNull: return "Is null";
                case FunctionOperatorType.StartsWith: return nameof(string.StartsWith); // caller have to handle the serialization of node in the good order
                case FunctionOperatorType.EndsWith: return nameof(string.EndsWith); // Idem
                default: // TODO ! 
                    _Errors.Add("Don't know how to handle group operator " + operatorType.ToString() + "!");
                    return operatorType.ToString();
            }
        }

        string GetUnaryOperationName(UnaryOperatorType operatorType)
        {
            switch (operatorType)
            {
                case UnaryOperatorType.BitwiseNot: return "~";
                case UnaryOperatorType.IsNull: return "null == ";
                case UnaryOperatorType.Minus: return "-";
                case UnaryOperatorType.Not: return "!";
                case UnaryOperatorType.Plus: return "+";
                default: return string.Empty;
            }
        }

        string GetBinaryOperationName(BinaryOperatorType operatorType)
        {
            switch (operatorType)
            {
                case BinaryOperatorType.Equal: return "==";
                case BinaryOperatorType.NotEqual: return "!=";
                case BinaryOperatorType.Greater: return ">";
                case BinaryOperatorType.Less: return "<";
                case BinaryOperatorType.LessOrEqual: return "<=";
                case BinaryOperatorType.GreaterOrEqual: return ">=";
                case BinaryOperatorType.BitwiseAnd: return "&";
                case BinaryOperatorType.BitwiseOr: return "|";
                case BinaryOperatorType.BitwiseXor: return "^";
                case BinaryOperatorType.Divide: return "/";
                case BinaryOperatorType.Modulo: return "%";
                case BinaryOperatorType.Multiply: return "*";
                case BinaryOperatorType.Plus: return "+";
                case BinaryOperatorType.Minus: return "-";
                default:
                    if (operatorType.ToString() == "Like") //BinaryOperatorType.Like // because is marked obsolete (dont remember the pragma...)
                        _Errors.Add("Like Operator not handled!");
                    else
                        _Errors.Add("Unknown operator " + operatorType.ToString() + "!");
                    return " ";
            }
        }

    }

}
