﻿using System;
using System.ComponentModel;

using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public partial class EnhancedSearchLookUpEdit : SearchLookUpEdit
    {
        // Note : 
        // These attribute are important!
        // When we drop an instance of EnhancedSearchlookUpEdit in a control designer, 
        // it is serialized in designer.cs file.
        // The property Properties below is also serialized with its own properties.
        // Without these attributes designer would remove / ignore all the values and BeginInit and Endinit woul not be called.
        // This is probably due to the new keyword
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [DXCategory("Properties")]
        public new EnhancedSearchLookUpEditRepositoryItem Properties { get { return (EnhancedSearchLookUpEditRepositoryItem)base.Properties; } }

        public EnhancedSearchLookUpEdit()
        {
        }
    }
}
