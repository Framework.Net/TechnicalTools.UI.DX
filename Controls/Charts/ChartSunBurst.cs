﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using DevExpress.XtraTreeMap;


namespace TechnicalTools.UI.DX.Controls
{
    /// <summary>
    /// Dessine un graph en forme de camembert automatiquement en faisant : 
    /// var graph = new TrkPieChart()
    /// graph.AddSerie("title", [dictionaire contenant nom et valeur à afficher])
    /// </summary>
    [ToolboxItem(true)]
    public partial class ChartSunBurst : EnhancedXtraUserControl//, IVisitableTrackerControl, IControlAccessibility
    {
        public ChartSunBurst()
        {
            InitializeComponent();
        }

        public void DisplayHierarchy<T>(string title, List<T> firstChildren, Func<T, string> getLabel, Func<T, decimal> getValue, Func<T, IEnumerable<T>> getChildren)
        {
            var data = ConvertData(firstChildren, getLabel, getValue, getChildren);
            Display(data, title);
        }
        void Display(SunburstItemStorage storage, string title)
        {
            Chart.StartAngle = 90D;
            Chart.ToolTipTextPattern = "{L}: {V:F2} Mt";
            Chart.CenterLabel.TextPattern = "{TV}";
            Chart.CenterLabel.TextPattern = title;
            Chart.Colorizer = new SunburstGradientColorizer()
            {
                Min = 0.6,// Specifies the color transparency of an item with the minimum value(should be in the[0, 1] range),
                Max = 1,// Specifies the color transparency of an item with the maximum value(should be in the[0, 1] range),
                Mode = GradientColorizerMode.ByGroupLevel,
                GradientColor = Color.Black
            };
            Chart.DataAdapter = storage;
        }
        SunburstItemStorage ConvertData<T>(List<T> firstChildren, Func<T, string> getLabel, Func<T, decimal> getValue, Func<T, IEnumerable<T>> getChildren)
        {
            var storage = new SunburstItemStorage();
            var queue = new Queue<Tuple<T, SunburstItem>>();
            foreach (var child in firstChildren)
            {
                var item = Tuple.Create(child, new SunburstItem((double)getValue(child), getLabel(child)));
                queue.Enqueue(item);
                storage.Items.Add(item.Item2);
            }
            while (queue.Count > 0)
            {
                var item = queue.Dequeue();
                foreach (var child in getChildren(item.Item1))
                {
                    var subItem = Tuple.Create(child, new SunburstItem((double)getValue(child), getLabel(child)));
                    queue.Enqueue(subItem);
                    item.Item2.Children.Add(subItem.Item2);
                }
            }
            return storage;
        }
    }
}
