﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

using DevExpress.Utils.Menu;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Designer;
using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX.Controls
{
    [ToolboxItem(true)]
    public class EnhancedChartControl : ChartControl
    {
        public bool AllowDesignerAccessToUser { get; set; } = true;

        public EnhancedChartControl()
        {
            MouseUp += ctlChart_MouseUp;
        }

        [DXCategory("Behavior")]
        public event EventHandler<DXPopupMenu> PopupMenuShowing;
        
        private void ctlChart_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var menu = new DXPopupMenu();
                //var menu = new PopupMenu(new BarManager()
                //{
                //    // important to make the menu disappearing when user click elsewhere 
                //    Form = FindForm()
                //});
                if (AllowDesignerAccessToUser)
                {
                    var mnu = new DXMenuItem("Open Chart Designer...", (_, ee) =>
                    {
                        var designer = new ChartDesigner(this);
                        designer.ShowDialog(true);
                    });
                    //var mnu = new BarButtonItem() { Caption = "Design Chart" }
                    //mnu.ItemClick += (_, ee) =>
                    //{
                    //    var designer = new ChartDesigner(ctlChart)
                    //    designer.ShowDialog()
                    //};
                    menu.Items.Add(mnu);
                    //menu.AddItem(mnu)
                }
                try
                {
                    PopupMenuShowing?.Invoke(this, menu);
                }
                finally
                {
                    if (menu.Items.Count > 0)
                        menu.ShowPopup(this, /*this.PointToScreen(*/e.Location/*)*/);
                }
            }
        }
    }
}
