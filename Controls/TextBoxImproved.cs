﻿using System;
using System.ComponentModel;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX.Controls
{
    // http://stackoverflow.com/questions/2653153/c-sharp-winforms-vertical-alignment-for-textbox-etc
    [DesignTimeVisible(true)]
    [DesignerCategory("TechnicalTools.UI")]
    public class TextBoxImproved : TextBox
    {
        [Browsable(true)]
        [DefaultValue(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Localizable(true)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public new bool AutoSize { get { return base.AutoSize; } set { base.AutoSize = value; } }

        public TextBoxImproved()
        {
            //base.SetAutoSizeMode(AutoSizeMode.GrowOnly);
        }
    }
}
