﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Registrator;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.BandedGrid.Drawing;
using DevExpress.XtraGrid.Views.BandedGrid.Handler;
using DevExpress.XtraGrid.Views.BandedGrid.ViewInfo;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Base.Handler;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


// Registration for Visual Studio Design Time
namespace TechnicalTools.UI.DX
{
    // from https://www.devexpress.com/Support/Center/Example/Details/E900
    public partial class EnhancedGridControl
    {
        protected override BaseView CreateDefaultView()
        {
            return CreateView(typeof(EnhancedGridView).Name);
        }
        protected override void RegisterAvailableViewsCore(InfoCollection collection)
        {
            base.RegisterAvailableViewsCore(collection);
            collection.Add(new EnhancedGridViewInfoRegistrator());
            collection.Add(new EnhancedBandedGridViewInfoRegistrator());
        }
        
        public override BaseView CreateView(string name)
        {
            // TODO : From https://www.devexpress.com/support/center/Question/Details/T263326
            //BaseView view = base.CreateView(name);
            //view.Assign(MainView, true);
            //return view;
            return base.CreateView(name);
        }
    }

    public partial class EnhancedGridView
    {
        public EnhancedGridView()
            : this(null)
        {
        }
        public EnhancedGridView(GridControl gridControl)
            : base(gridControl)
        {
            Initialize();
        }

        protected override string ViewName { get { return typeof(EnhancedGridView).Name; } }
    }
    public partial class EnhancedBandedGridView
    {
        public EnhancedBandedGridView()
            : this(null)
        {
        }
        public EnhancedBandedGridView(GridControl gridControl)
            : base(gridControl)
        {
            Initialize();
        }

        protected override string ViewName { get { return typeof(EnhancedBandedGridView).Name; } }
    }

    public class EnhancedGridViewInfoRegistrator : GridInfoRegistrator
    {
        public override string ViewName { get { return typeof(EnhancedGridView).Name; } }
        public override BaseView CreateView(GridControl grid) { return new EnhancedGridView(grid as EnhancedGridControl); }
        //public override BaseViewPainter CreatePainter(BaseView view)
        //{
        //    return new MyGridViewPainter((MyGridView)view);
        //}
        public override DevExpress.XtraGrid.Views.Base.ViewInfo.BaseViewInfo CreateViewInfo(BaseView view) { return new EnhancedGridViewInfo(view as EnhancedGridView); }
        public override BaseViewHandler CreateHandler(BaseView view) { return new EnhancedGridHandler(view as EnhancedGridView); }
    }
    public class EnhancedBandedGridViewInfoRegistrator : BandedGridInfoRegistrator
    {
        public override string ViewName { get { return typeof(EnhancedBandedGridView).Name; } }
        public override BaseView CreateView(GridControl grid) { return new EnhancedBandedGridView(grid as EnhancedGridControl); }
        public override BaseViewPainter CreatePainter(BaseView view)
        {
            return new EnhancedBandedGridViewPainter((EnhancedBandedGridView)view);
        }
        public override DevExpress.XtraGrid.Views.Base.ViewInfo.BaseViewInfo CreateViewInfo(BaseView view) { return new EnhancedBandedGridViewInfo(view as EnhancedBandedGridView); }
        public override BaseViewHandler CreateHandler(BaseView view) { return new EnhancedBandedGridHandler(view as EnhancedBandedGridView); }
    }
    public class EnhancedGridViewInfo : GridViewInfo
    {
        public EnhancedGridViewInfo(EnhancedGridView gridView) : base(gridView) { }

        public override int CalcRowHeight(Graphics graphics, int rowHandle, int rowVisibleIndex, int min, int level, bool useCache, GridColumnsInfo columns)
        {
            return base.CalcRowHeight(graphics, rowHandle, rowVisibleIndex, min, level, useCache, columns);
        }
        public override int CalcColumnBestWidth(GridColumn column)
        {
            return ((EnhancedGridView)View)._DisplayColumnHint.OnCalcColumnBestWidth(base.CalcColumnBestWidth, column);
        }
        //public override int MinRowHeight { get { return base.MinRowHeight - 2; } }
    }
    public class EnhancedBandedGridViewInfo : BandedGridViewInfo
    {
        public EnhancedBandedGridViewInfo(EnhancedBandedGridView gridView) : base(gridView) { }

        public override int CalcRowHeight(Graphics graphics, int rowHandle, int rowVisibleIndex, int min, int level, bool useCache, GridColumnsInfo columns)
        {
            return base.CalcRowHeight(graphics, rowHandle, rowVisibleIndex, min, level, useCache, columns);
        }
        public override int CalcColumnBestWidth(GridColumn column)
        {
            return ((EnhancedBandedGridView)View)._DisplayColumnHint.OnCalcColumnBestWidth(base.CalcColumnBestWidth, column);
        }
        //public override int MinRowHeight { get { return base.MinRowHeight - 2; } }
    }

    public class EnhancedGridHandler : DevExpress.XtraGrid.Views.Grid.Handler.GridHandler
    {
        public EnhancedGridHandler(EnhancedGridView gridView) : base(gridView) { }

        [DebuggerHidden, DebuggerStepThrough]
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            // Code désactivé car il faut tester View.OptionsBehavior.AllowDeleteRows, puis si a default, Editable etc ...
            //if (e.KeyData == Keys.Delete && View.State == GridState.Normal)
            //    View.DeleteRow(View.FocusedRowHandle);
        }
    }
    public class EnhancedBandedGridHandler : BandedGridHandler
    {
        public EnhancedBandedGridHandler(EnhancedBandedGridView gridView) : base(gridView) { }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            // Code désactivé car il faut tester View.OptionsBehavior.AllowDeleteRows, puis si a default, Editable etc ...
            //if (e.KeyData == Keys.Delete && View.State == BandedGridState.Normal)
            //    View.DeleteRow(View.FocusedRowHandle);
        }
    }

    // From https://www.devexpress.com/Support/Center/Example/Details/E2290
    // "How to emulate a TreeList using the master-detail GridView"
    public class EnhancedBandedGridViewPainter : BandedGridPainter
    {
        public EnhancedBandedGridViewPainter(EnhancedBandedGridView view) : base(view) { }

        protected override void DrawRegularRow(GridViewDrawArgs e, GridDataRowInfo ri)
        {
            base.DrawRegularRow(e, ri);
            return; // TODO : a voir ...
            //if (ri.IsMasterRow && ri.MasterRowExpanded)
            //{
            //    GridView detailView = View.GetDetailView(ri.RowHandle, 0) as GridView;
            //    if (detailView == null)
            //        return;

            //    GridCellInfo cell = ri.Cells[View.VisibleColumns[0]];
            //    int center;
            //    if (cell != null) center = cell.CellButtonRect.Left + cell.CellButtonRect.Width / 2;
            //    else center = ri.DetailIndentBounds.Left + ri.DetailIndentBounds.Width / 2;

            //    GridViewInfo detailViewInfo = (GridViewInfo)detailView.GetViewInfo();
            //    int level = 0;
            //    Point p1;
            //    Point p2;
            //    foreach (GridRowInfo rowInfo in detailViewInfo.RowsInfo)
            //    {
            //        if (detailView.IsRowVisible(rowInfo.RowHandle) != RowVisibleState.Visible) continue;
            //        level = rowInfo.Bounds.Top + rowInfo.Bounds.Height / 2;
            //        p1 = new Point(center, level);
            //        p2 = new Point(ri.DetailIndentBounds.Right, level);
            //        e.Graphics.DrawLine(Pens.Black, p1, p2);
            //    }

            //    p1 = new Point(center, ri.DetailIndentBounds.Top);
            //    p2 = new Point(center, level);
            //    e.Graphics.DrawLine(Pens.Black, p1, p2);
            //}
        }
    }
}
