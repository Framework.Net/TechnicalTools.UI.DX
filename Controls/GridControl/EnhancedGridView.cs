﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Data.Filtering;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace TechnicalTools.UI.DX
{
    public partial class EnhancedGridView : GridView, IEnhancedGridView
    {
        #region Fonctionalité affichage des summary dans les group row  (identique EnhancedGridView et EnhancedBandedGridView)

        [DefaultValue(false)]
        public bool DrawSummaryAlignedWithColumn
        {
            get { return _GroupDrawerHelper != null && GroupDrawerHelper.AlignGroupSummary; }
            set { GroupDrawerHelper.AlignGroupSummary = value; }
        }

        [DefaultValue(0d)]
        public double CornerRoundedness
        {
            get { return _GroupDrawerHelper == null ? 0d : GroupDrawerHelper.CornerRoundedness; }
            set { GroupDrawerHelper.CornerRoundedness = value; }
        }

        [DefaultValue(false)]
        public bool UseColorGradientForSubGroup
        {
            get { return _GroupDrawerHelper != null && GroupDrawerHelper.UseColorGradientForSubGroup; }
            set { GroupDrawerHelper.UseColorGradientForSubGroup = value; }
        }

        [DefaultValue(null)]
        public Func<AppearanceObject, GridGroupSummaryItem, AppearanceObject> CustomizeSummaryDraw
        {
            get { return _GroupDrawerHelper == null ? null : GroupDrawerHelper.CustomizeAppearance; }
            set { GroupDrawerHelper.CustomizeAppearance = value; }
        }

        GridView_GroupDrawerHelper GroupDrawerHelper
        {
            get
            {
                if (_GroupDrawerHelper == null)
                {
                    _GroupDrawerHelper = new GridView_GroupDrawerHelper(this);
                    _GroupDrawerHelper.Install();
                }
                return _GroupDrawerHelper;
            }
            set
            {
                Debug.Assert(value == null);
                _GroupDrawerHelper.Uninstall();
                _GroupDrawerHelper = null;
            }
        }
        GridView_GroupDrawerHelper _GroupDrawerHelper;

        #endregion

        void Initialize()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;

            OptionsView.ShowGroupedColumns = false;
            OptionsMenu.EnableGroupRowMenu = true; // Starting from DX 19.2, group menus are not display by default (!?)
            OptionsClipboard.CopyColumnHeaders = DefaultBoolean.False;

            CopyPaste_Configure();
            DoubleClickRowItem_Initialize();
            BetterFilterEditor_Initialize();
            UnboundColumnOptimization();

            CustomDrawGroupRow += OnCustomDrawGroupRow;
            ShowCustomizationForm += OnShowCustomizationForm;

            _ExcelExportUI = new EnhancedGridView_ExportExcel(this);
            TransformData = new EnhancedGridView_TransformData(this);
            _MoveColumnTo = new EnhancedGridView_MoveColumnTo(this);
            _FixedColumns = new EnhancedGridView_FixedColumns(this);
            _SmartSort = new EnhancedGridView_SmartSort(this);
            BrowsableNestedProperties = new EnhancedGridView_BrowsableNestedProperties(this);
            CustomCalculatedColumn = new EnhancedGridView_CustomCalculatedColumn(this);
            _FindColumn = new EnhancedGridView_FindColumn(this);
            _TripleClickSelectAll = new EnhancedGridView_TripleClickSelectAll(this);
            _FreeColumn = new EnhancedGridView_FreeColumn(this);
            _CollapseAllGroupRow = new EnhancedGridView_CollapseAllGroupRow(this);
            _DisplayGroupRowCount = new EnhancedGridView_DisplayGroupRowCount<EnhancedGridView>(this);
            _GroupByRange = new EnhancedGridView_GroupByRange(this);
            _GroupByTuple = new EnhancedGridView_GroupByTuple(this);
            _CtrlAndWheelToZoom = new EnhancedGridView_CtrlAndWheelToZoom(this);
            _RotateColumnHeaders = new EnhancedGridView_RotateColumnHeaders(this);
            _ShowChart = new EnhancedGridView_ShowChart(this);
            SummariesAndFooterHandler = new EnhancedGridView_ShowSummariesAndFooter(this);
            //SortByGroupSummaryValue = new EnhancedGridView_SortByGroupSummaryValue(this);
            ColumnFormatChanger = new EnhancedGridView_ColumnFormatChanger(this);
            _ExperimentalFeatures = new EnhancedGridView_ExperimentalFeatures(this);
            _CustomErrorMessages = new EnhancedGridView_CustomErrorMessages(this);
            _MultipleCellEditor = new EnhancedGridView_MultipleCellEditor(this);
            LayoutManager = new EnhancedGridView_LayoutManager(this);
            _DisplayColumnHint = new EnhancedGridView_DisplayColumnHint(this);
            _EditableMergedCell = new EnhancedGridView_EditableMergedCell(this);
            _CopyValue = new EnhancedGridView_CopyValue(this);
            ResizeRow = new EnhancedGridView_ResizeRow(this);

            // On pourrait remplacer cette ligne par les suivantes
            DrawSummaryAlignedWithColumn = true;
            //OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.True; 
            //// Mais cela a pour effet de bord, le fait que les colonnes groupé sont affiché (meme si OptionsView.ShowGroupedColumns = false)
            // Solution à essayer ici : https://www.devexpress.com/Support/Center/Question/Details/T409238
            // L'interêt de cette solution est que les valeurs groupées se retrouve dans l'export excel !

            // TODO : Chercher dans le code EvenRow et mettre les lignes dans TrkBandedGridView par default
        }

        public GridView_DetailViewManager                         DetailViewManager { get { return _DetailViewManager ?? (_DetailViewManager = new GridView_DetailViewManager(this)); } } GridView_DetailViewManager _DetailViewManager;

        EnhancedGridView_ExportExcel                              _ExcelExportUI;
        public EnhancedGridView_TransformData                     TransformData { get; private set; }
        EnhancedGridView_MoveColumnTo                             _MoveColumnTo;
        EnhancedGridView_FixedColumns                             _FixedColumns;
        EnhancedGridView_SmartSort                                _SmartSort;
        public EnhancedGridView_BrowsableNestedProperties         BrowsableNestedProperties { get; private set; }
        public EnhancedGridView_CustomCalculatedColumn            CustomCalculatedColumn { get; private set; }
        internal EnhancedGridView_FindColumn                      _FindColumn;
        EnhancedGridView_TripleClickSelectAll                     _TripleClickSelectAll;
        public EnhancedGridView_FreeColumn                        _FreeColumn;
        public EnhancedGridView_RotateColumnHeaders               _RotateColumnHeaders { get; private set; }
        EnhancedGridView_ShowChart                                _ShowChart { get; set; }
        public EnhancedGridView_ShowSummariesAndFooter            SummariesAndFooterHandler { get; private set; }
        //public EnhancedGridView_SortByGroupSummaryValue           SortByGroupSummaryValue { get; private set; }
        public EnhancedGridView_ColumnFormatChanger               ColumnFormatChanger { get; private set; }
        EnhancedGridView_CollapseAllGroupRow                      _CollapseAllGroupRow;
        EnhancedGridView_DisplayGroupRowCount<EnhancedGridView>   _DisplayGroupRowCount;
        EnhancedGridView_GroupByRange                             _GroupByRange;
        EnhancedGridView_GroupByTuple                             _GroupByTuple;
        EnhancedGridView_CtrlAndWheelToZoom                       _CtrlAndWheelToZoom;
        EnhancedGridView_ExperimentalFeatures                     _ExperimentalFeatures;
        EnhancedGridView_CustomErrorMessages                      _CustomErrorMessages;
        EnhancedGridView_MultipleCellEditor                       _MultipleCellEditor;
        public EnhancedGridView_LayoutManager                     LayoutManager;
        internal EnhancedGridView_DisplayColumnHint               _DisplayColumnHint;
        EnhancedGridView_EditableMergedCell                       _EditableMergedCell;
        EnhancedGridView_CopyValue                                _CopyValue;
        public EnhancedGridView_ResizeRow                         ResizeRow { get; private set; }

        public bool IsExporting { get; internal set; }
        
        public override void Assign(BaseView v, bool copyEvents)
        {
            base.Assign(v, copyEvents);
            // Quand DevExpress cree une vue (grille) de details, il copie L'integraltié de la grille "model" 
            // qu'on voit et edite dans le designer et dont une instance est crée aun runtime
            // Le probleme c'est que lors de la copie (ou Assignation) du model a l'instance crée dynamiquement il copie aussi les evenements
            // Il faut donc désinscrire les evenements des objets associé a la vue du model (ci dessous)
            // et garder les objets associés à la copie
            if (v is EnhancedGridView egv)
            {
                DetailViewManager.Assign(egv.DetailViewManager, copyEvents);
                _ExcelExportUI.Assign(egv._ExcelExportUI, copyEvents);
                TransformData.Assign(egv.TransformData, copyEvents);
                _MoveColumnTo.Assign(egv._MoveColumnTo, copyEvents);
                _FixedColumns.Assign(egv._FixedColumns, copyEvents);
                _SmartSort.Assign(egv._SmartSort, copyEvents);
                BrowsableNestedProperties.Assign(egv.BrowsableNestedProperties, copyEvents);
                CustomCalculatedColumn.Assign(egv.CustomCalculatedColumn, copyEvents);
                _FindColumn.Assign(egv._FindColumn, copyEvents);
                _TripleClickSelectAll.Assign(egv._TripleClickSelectAll, copyEvents);
                _FreeColumn.Assign(egv._FreeColumn, copyEvents);
                _RotateColumnHeaders.Assign(egv._RotateColumnHeaders, copyEvents);
                _ShowChart.Assign(egv._ShowChart, copyEvents);
                SummariesAndFooterHandler.Assign(egv.SummariesAndFooterHandler, copyEvents);
                //SortByGroupSummaryValue.Assign(egv.SortByGroupSummaryValue, copyEvents);
                ColumnFormatChanger.Assign(egv.ColumnFormatChanger, copyEvents);
                _CollapseAllGroupRow.Assign(egv._CollapseAllGroupRow, copyEvents);
                _DisplayGroupRowCount.Assign(egv._DisplayGroupRowCount, copyEvents);
                _GroupByRange.Assign(egv._GroupByRange, copyEvents);
                _GroupByTuple.Assign(egv._GroupByTuple, copyEvents);
                _CtrlAndWheelToZoom.Assign(egv._CtrlAndWheelToZoom, copyEvents);
                _ExperimentalFeatures.Assign(egv._ExperimentalFeatures, copyEvents);
                _CustomErrorMessages.Assign(egv._CustomErrorMessages, copyEvents);
                _MultipleCellEditor.Assign(egv._MultipleCellEditor, copyEvents);
                LayoutManager.Assign(egv.LayoutManager, copyEvents);
                _DisplayColumnHint.Assign(egv._DisplayColumnHint, copyEvents);
                _EditableMergedCell.Assign(egv._EditableMergedCell, copyEvents);
                _CopyValue.Assign(egv._CopyValue, copyEvents);
            }

            if (v is EnhancedGridView ev)
            {
                foreach (GridColumn col in ev.Columns)
                {
                    if (col is IUnboundColumnOptimizationRequirement ucol)
                    {
                        var myucol = Columns[col.FieldName];
                        Debug.Assert(myucol is IUnboundColumnOptimizationRequirement);
                        (myucol as IUnboundColumnOptimizationRequirement).Assign(ucol, copyEvents);
                    }
                }

                if (copyEvents)
                {
                    BeforeCustomDrawGroupRow += ev.BeforeCustomDrawGroupRow;
                    RowItemDoubleClick += ev.RowItemDoubleClick;
                }
            }
        }

        protected override void OnGridControlChanged(GridControl prevControl)
        {
            base.OnGridControlChanged(prevControl);
            GridControlChanged?.Invoke(this, prevControl);
        }
        public event Action<IEnhancedGridView, GridControl> GridControlChanged;

        public event RowObjectCustomDrawEventHandler BeforeCustomDrawGroupRow;
        private void OnCustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
            BeforeCustomDrawGroupRow?.Invoke(sender, e);
            Debug.Assert(!e.Handled, "BeforeCustomDrawGroupRow is not intended for handler that handle drawing, but for subscriber that only change settings of group drawing!");
        }
        private void OnShowCustomizationForm(object sender, EventArgs e)
        {
            var view = sender as GridView;
            DefaultShowCustomizationFormImpl(view, e);
        }
        internal static void DefaultShowCustomizationFormImpl(GridView view, EventArgs e)
        {
            var frm = view.CustomizationForm;
            var screen = Screen.FromPoint(Cursor.Position);
            int x = Cursor.Position.X + 1;
            int y = Cursor.Position.Y;
            var h = Math.Min(screen.WorkingArea.Height, 76 + view.Columns.Count(c => !c.Visible) * 20);
            if (y + h > screen.WorkingArea.Bottom)
                y = screen.WorkingArea.Bottom - h;
            frm.SetBounds(x, y, frm.Width, h);
        }

        #region Better Filter Editor

        void BetterFilterEditor_Initialize()
        {
            OptionsFilter.UseNewCustomFilterDialog = true; // N'a pas l'air de changer grand chose
            FilterEditorCreated += EnhancedGridView_FilterEditorCreated;
        }

        private void EnhancedGridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            e.FilterControl.ShowOperandTypeIcon = true;
        }

        #endregion

        #region Fonctionalité RowItemDoubleClick

        public event EventHandler<RowItemDoubleClickEventArgs> RowItemDoubleClick;

        void DoubleClickRowItem_Initialize()
        {
            // 1er cas simple : IsEditable = false
            // Cela marche aussi pour le 2ieme cas quand l'utilisateur clique proche des ligne inter row 
            // (en dehors de la où apparait le control permettant d'editer la cellule). Mais c'est improbable.
            DoubleClick += DoubleClickRowItem_OnRowDoubleClick;

            // 2ieme cas complexe : IsEditable = true & ReadOnly = true/false
            ShownEditor += DoubleClickRowItem_OnShownEditor;
        }

        private void DoubleClickRowItem_OnRowDoubleClick(object _, EventArgs __)
        {
            this.DoWithRowObjectUnderMouse((object item, GridColumn col, int rowHandle) =>
                {
                    RowItemDoubleClick?.Invoke(this, new RowItemDoubleClickEventArgs(item, col, rowHandle));
                });
        }
   
        // et le deuxième click intercepté par le control
        void DoubleClickRowItem_OnShownEditor(object sender, EventArgs __)
        {
            var view = sender as GridView;
            Debug.Assert(view != null, "view != null");
        
            view.ActiveEditor.DoubleClick += DoubleClickRowItem_ActiveEditor_DoubleClick;
        }

        private void DoubleClickRowItem_ActiveEditor_DoubleClick(object _, EventArgs __)
        {
            // TODO : Si l'editeur est plus grand que la cellule, le code qui suit va retourner un mauvais item
            this.DoWithRowObjectUnderMouse((object item, GridColumn col, int rowHandle) =>
            {
                RowItemDoubleClick?.Invoke(this, new RowItemDoubleClickEventArgs(item, col, rowHandle));
            });
        }

        protected override bool CanShowColumn(GridColumn column)
        {
            return (ChangeColumnVisibility == null ? null : ChangeColumnVisibility(column)) ?? base.CanShowColumn(column);
        }
        public event Func<GridColumn, bool?> ChangeColumnVisibility;

        #endregion

        protected override CriteriaOperator CreateAutoFilterCriterion(GridColumn column, AutoFilterCondition condition, object _value, string strVal)
        {
            if (column.ColumnType.IsNumericType() &&
                System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencyDecimalSeparator == ",")
            {
                strVal = strVal.Replace(".", ",");
                _value = (object)strVal;
            }

            var res = base.CreateAutoFilterCriterion(column, condition, _value, strVal);
            if (column.ColumnType.IsNumericType() && strVal.TrimStart().StartsWith("~="))
            {
                strVal = strVal.Trim();
                _value = strVal;

                if (decimal.TryParse(strVal.Substring(2).Trim(), out decimal decimalValue))
                {
                    var index = strVal.IndexOf(System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencyDecimalSeparator);
                    decimal range = 1;
                    if (index >= 0)
                        range = (decimal)Math.Pow(10, -(strVal.Length - index - 1));
                    res = new BetweenOperator(column.FieldName, decimalValue + (decimalValue > 0 ? 0 : -range), decimalValue + (decimalValue > 0 ? range : 0));
                }
            }
            else if (strVal.TrimStart().StartsWith("contains(") && strVal.TrimEnd().EndsWith(")"))
            {
                strVal = strVal.Trim();
                strVal = strVal.Remove(strVal.Length - 1).Substring("contains(".Length).Trim();
                if (strVal.Length > 0)
                {
                    res = new FunctionOperator(FunctionOperatorType.Contains, new OperandProperty(column.FieldName), strVal);
                }
            }
            else if (strVal.TrimStart().StartsWith("containsnot(") && strVal.TrimEnd().EndsWith(")"))
            {
                strVal = strVal.Trim();
                strVal = strVal.Remove(strVal.Length - 1).Substring("containsnot(".Length).Trim();
                if (strVal.Length > 0)
                {
                    res = new UnaryOperator(UnaryOperatorType.Not, new FunctionOperator(FunctionOperatorType.Contains, new OperandProperty(column.FieldName), strVal));
                }
            }

            if (CreatingAutoFilterCriterion == null)
                return res;

            var e = new CreatingAutoFilterCriterionEventArgs()
            {
                Column = column,
                Condition = condition,
                Value = _value,
                StringValue = strVal,
                Result = res
            };
            CreatingAutoFilterCriterion(this, e);
            return e.Result;
        }
        public event EventHandler<CreatingAutoFilterCriterionEventArgs> CreatingAutoFilterCriterion;

        protected override void ActivateEditor(GridCellInfo cell)
        {
            if (BeforeActivateEditor?.Invoke(this, cell) ?? false)
                return;
            base.ActivateEditor(cell);
        }
        public event AfterActivateEditorHandler BeforeActivateEditor;
        protected override bool PostEditor(bool causeValidation)
        {
            // Same check than base class to prevent writing in model.
            // This case happens when gridview is readonly = true, IsEditable = true
            if (ActiveEditor == null || !EditingValueModified || fEditingCell == null)
                return base.PostEditor(causeValidation);
            IsPostingEditorValueToModel = true;
            try
            {
                var res = BeforePostEditor?.Invoke(this, causeValidation) ?? true;
                return res && base.PostEditor(causeValidation);
            }
            finally
            {
                IsPostingEditorValueToModel = false;
            }
        }
        public bool IsPostingEditorValueToModel { get; protected set; }

        protected override void SetRowCellValueCore(int rowHandle, GridColumn column, object _value, bool fromEditor)
        {
            base.SetRowCellValueCore(rowHandle, column, _value, fromEditor);
        }
        public event BeforePostEditorHandler BeforePostEditor;
    }

    public class RowItemDoubleClickEventArgs : EventArgs
    {
        public object     Item      { get; private set; }
        public GridColumn Column    { get; private set; }
        public int        RowHandle { get; private set; }

        public RowItemDoubleClickEventArgs(object item, GridColumn col, int rowHandle)
        {
            Item = item;
            Column = col;
            RowHandle = rowHandle;
        }
    }
}
