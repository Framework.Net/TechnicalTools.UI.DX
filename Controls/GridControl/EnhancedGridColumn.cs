﻿using System;
using DevExpress.Utils.Serializing;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;


namespace TechnicalTools.UI.DX
{
    public partial class EnhancedGridColumn : GridColumn, IEnhancedGridColumn
    {
        //[XtraSerializableProperty] // Make SaveLayoutToXml to ignore this property
        //public string CustomData { get; set; }

        //protected override void Assign(GridColumn column)
        //{
        //    base.Assign(column);

        //    var gridColumn = column as EnhancedGridColumn;
        //    if (gridColumn != null)
        //        this.CustomData = gridColumn.CustomData;
        //}
        public EnhancedGridColumn()
        {
        }
    }

    public class EnhancedGridBand : GridBand
    {
        public EnhancedGridBand()
        {
        }

    }

    public partial class BandedEnhancedGridColumn : BandedGridColumn, IEnhancedGridColumn
    {
        //[XtraSerializableProperty] // Make SaveLayoutToXml to ignore this property
        //public string CustomData { get; set; }

        //protected override void Assign(GridColumn column)
        //{
        //    base.Assign(column);

        //    var gridColumn = column as EnhancedGridColumn;
        //    if (gridColumn != null)
        //        this.CustomData = gridColumn.CustomData;
        //}
        public BandedEnhancedGridColumn()
        {
        }
    }

    public partial class EnhancedGridView
    {
        protected override GridColumnCollection CreateColumnCollection()
        {
            return new EnhancedGridColumnCollection(this);
        }
    }

    public class EnhancedGridColumnCollection : GridColumnCollection
    {
        public EnhancedGridColumnCollection(ColumnView view) : base(view) { }

        protected override GridColumn CreateColumn()
        {
            return new EnhancedGridColumn();
        }

    }



    public partial class EnhancedBandedGridView
    {
        protected override GridBandCollection CreateBands()
        {
            return new EnhancedGridBandCollection(this);
        }
        protected override GridColumnCollection CreateColumnCollection()
        {
            return new EnhancedBandedGridColumnCollection(this);
        }
    }

    public class EnhancedGridBandCollection : GridBandCollection
    {
        public EnhancedGridBandCollection(BandedGridView view) : base(view, null) { }

        public override GridBand CreateBand()
        {
            return new EnhancedGridBand();
        }
    }

    public class EnhancedBandedGridColumnCollection : BandedGridColumnCollection
    {
        public EnhancedBandedGridColumnCollection(ColumnView view) : base(view) { }

        protected override GridColumn CreateColumn()
        {
            return new BandedEnhancedGridColumn();
        }

    }
}
