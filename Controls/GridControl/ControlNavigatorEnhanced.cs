﻿using System;
using System.ComponentModel;
using System.Globalization;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.NavigatorButtons;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// ControlNavigator pour GridControl mettant à disposition dans TextStringFormat l'argument "{2}" 
    /// Pour attacher ce Control Navigator, il faut mettre sa propriete NavigatableControl = YourGridControl;
    /// Rappel : 
    ///    Fait Par ControlNavigator : "{0}" affiche la position de la ligne courante
    ///    Fait Par ControlNavigator : "{1}" affiche le nombre total d'item (= row) visible
    ///    NOUVEAU :                   "{2}" affiche le nombre total d'item dans la datasource
    /// </summary>
    //[ToolboxItem(true)]
    public class ControlNavigatorEnhanced : GridControlNavigator
    {
        public ControlNavigatorEnhanced(GridControl control)
            : base(control)
        {
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TotalRecordsCount
        {
            get
            {
                try
                {
                    return ((GridControl)NavigatableControl).FocusedView.DataController.ListSourceRowCount;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        protected override NavigatorButtonsBase CreateButtons()
        {
            //return base.CreateButtons();
            return new ControlNavigatorEnhancedButtons(this); // désactive car fait disparaitre les bouttons par defaut
        }
    }

    public class ControlNavigatorEnhancedButtons : ControlNavigatorButtons
    {
        public ControlNavigatorEnhancedButtons(INavigatorOwner owner)
            : base(owner)
        {
        }

        protected override NavigatorButtonsViewInfo CreateViewInfo()
        {
            return new ControlNavigatorEnhancedViewInfo(this);
        }

        protected override NavigatorButtonCollectionBase CreateNavigatorButtonCollection()
        {
            return new ControlNavigatorEnhancedButtonCollection(this);
        }
    }

    public class ControlNavigatorEnhancedViewInfo : NavigatorButtonsViewInfo
    {
        public ControlNavigatorEnhancedViewInfo(NavigatorButtonsBase buttons)
            : base(buttons)
        {
        }

        protected override NavigatorTextViewInfo CreateTextViewInfo()
        {
            return new ControlNavigatorEnhancedTextViewInfo(this);
        }
    }


    public class ControlNavigatorEnhancedTextViewInfo : NavigatorTextViewInfo
    {
        public ControlNavigatorEnhancedTextViewInfo(NavigatorButtonsViewInfo viewInfo)
            : base(viewInfo)
        {
        }

        private int TotalRecordsCount {get { return ((ControlNavigatorEnhanced) Buttons.Owner).TotalRecordsCount; } }

        protected override string GetText(int currentRecord, int count)
        {
            _useCustomFormat = true;
            var res = string.Format(TextStringFormat, (currentRecord < 0 ? 0 : currentRecord), count, TotalRecordsCount);
            _useCustomFormat = false;
            return res;
        }
        bool _useCustomFormat;


        // Permet d'optimiser de maniere significative la vitesse d'affichage des gridview 
        // Le seul code qu'on souhaite reelement overrider c'est GetWidestText, car celui ci throw des exceptions syst"matiquement provoquant un ralentissement conséquent
        // La cause des exception provient du fait que pour calculer la plus grande chaine potentiellement affichable, on utilise TextStringFormat qui contient "{2}"
        // string.Format plante dessus car aucun troisieme argument n'est donne dans le code de DevExpress
        // On substitue donc le format dans tous les cas sauf qu'on on en a reelement besoin
        protected override string TextStringFormat
        {
            get
            {
                if (_useCustomFormat)
                    return base.TextStringFormat;

                return base.TextStringFormat.Replace("{2}", TotalRecordsCount.ToString(CultureInfo.InvariantCulture));
            }
        }
    }


    public class ControlNavigatorEnhancedButtonCollection : ControlNavigatorButtonCollection
    {
        public ControlNavigatorEnhancedButtonCollection(NavigatorButtonsBase buttons) : base(buttons) { }
        protected override void CreateButtons(NavigatorButtonsBase buttons)
        {
            base.CreateButtons(buttons);
            //AddButton(new ControlNavigatorEnhancedCustomButtonHelper(buttons));
        }
    }

    public class ControlNavigatorEnhancedCustomButtonHelper : ControlNavigatorButtonHelperBase
    {
        public ControlNavigatorEnhancedCustomButtonHelper(NavigatorButtonsBase buttons) : base(buttons) { }
        public override NavigatorButtonType ButtonType
        {
            get
            {
                return NavigatorButtonType.Custom;
            }
        }
        public override bool Enabled
        {
            get
            {
                return true;
            }
        }
    }
}
