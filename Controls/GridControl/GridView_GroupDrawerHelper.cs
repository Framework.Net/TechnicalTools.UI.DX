﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Text;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Base.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace TechnicalTools.UI.DX
{
    internal class GridView_GroupDrawerHelper
    {
        readonly GridView _gridView;

        public Func<AppearanceObject, GridGroupSummaryItem, AppearanceObject> CustomizeAppearance { get; set; }

        public GridView_GroupDrawerHelper(GridView gridView)
        {
            _gridView = gridView;
            if (DesignTimeHelper.IsInDesignMode)
                return;
            SummaryAlignedWithColumn_Init();
        }
        public void Install()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            // Subscribe after user events
            SynchronizationContext.Current.Post(_ =>
            {
                _gridView.CustomDrawGroupRow += gridView_CustomDrawGroupRow;
                _gridView.GroupLevelStyle += gridView_GroupLevelStyle;

                _gridView.Disposed += (__, ___) => Uninstall();
            }, null);
            // TODO : creer un override de CalcColumnBestWidth dans les EnhancedGridView et EnhancedBandedGridView et appeler un evenement declaré dans IEnhancedGridView
            // S'abonner ici a cet evenement afin de renseigner la taille de la summary value afin que BestFit fonctionne
            _gridView.GroupFormat = _gridView.GroupFormat.Replace("{2}", ""); // Remove summary display slot
        }

        public void Uninstall()
        {
            _gridView.CustomDrawGroupRow -= gridView_CustomDrawGroupRow;
            _gridView.GroupLevelStyle -= gridView_GroupLevelStyle;
        }

        #region Dispatch des évènements aux fonctionalités

        void gridView_GroupLevelStyle(object _, GroupLevelStyleEventArgs e)
        {
            //e.LevelAppearance.BackColor = Color.White;
        }

        void gridView_CustomDrawGroupRow(object _, RowObjectCustomDrawEventArgs e)
        {
            e.DefaultDraw();
            GroupRowDrawing_DrawGroupRow(e);
             
            DrawGroupCorner(e);
            e.Handled = true;
        }

        #endregion Dispatch des evenements aux fonctionalités

        #region Fonctionalité Arrondi des angles pour les groupRow

        /// <summary>
        /// roundedness: Allowed range is [0, 1] (0 = square corner, 1 = very roundish corner)
        /// </summary>
        [DefaultValue(0)]
        public double CornerRoundedness
        {
            get { return _CornerRoundedness; }
            set
            {
                if (value < 0 || value > 1)
                    throw new Exception("Invalid value. Allowed range is [0, 1] !");
                _CornerRoundedness = value;
            }
        }
        double _CornerRoundedness;

        void DrawGroupCorner(RowObjectCustomDrawEventArgs e)
        {
            if (CornerRoundedness == 0)
                return;
            int level = _gridView.GetRowLevel(e.RowHandle);
            var info = (GridGroupRowInfo)e.Info;

            using (var br = new SolidBrush(e.Appearance.BackColor))
            {
                const int corner_width = 19; // TODO : trouver la constante :( 
                int indent = level * corner_width;
                //e.Graphics.FillRectangle(br, info.Bounds.MoveLeftBy(indent));
                var cornerDrawingArea = new Rectangle(info.Bounds.X + indent, info.Bounds.Y, corner_width, info.Bounds.Height);
                var cornerArea = new Rectangle(cornerDrawingArea.X, cornerDrawingArea.Y - cornerDrawingArea.Height, cornerDrawingArea.Width * 2, cornerDrawingArea.Height * 2);
                int dh = (int)(cornerArea.Height * CornerRoundedness);
                int dw = (int)(cornerArea.Width * CornerRoundedness);
                cornerArea = new Rectangle(cornerArea.X, cornerArea.Y - dh + cornerArea.Height, dw, dh);
                dw = (cornerArea.Width + 1) / 2;
                dh = (cornerArea.Height + 1) / 2;
                var cornerFinalArea = new Rectangle(cornerArea.X, cornerArea.Y + dw, dw, dh);
                // e.Graphics.FillRectangle(new SolidBrush(Color.White), cornerDrawingArea);
                e.Graphics.FillRectangle(new SolidBrush(Color.White), cornerFinalArea);
                var mode = e.Graphics.SmoothingMode;
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                e.Graphics.FillPie(br, cornerArea, 90, 90);
                e.Graphics.SmoothingMode = mode;
            }
        }

        #endregion Fonctionalité Arrondi des angle pour les groupRow


        #region Alignement des group summary avec les colonnes
        // Fonctionalité pompé sur https://www.devexpress.com/Support/Center/Example/Details/E773
        // AVEC quelques modif importantes
        // A voir également un jour pour comparer / etudier : https://www.devexpress.com/Support/Center/Example/Details/E2084


        [DefaultValue(false)]
        public bool AlignGroupSummary { get; set; }

        [DefaultValue(false)]
        public bool UseColorGradientForSubGroup { get; set; }

        public Color ColorGradientForSubGroup { get; set; }
        public readonly Color ColorGradientForSubGroupDefault = Color.White;


        void SummaryAlignedWithColumn_Init()
        {
            ColorGradientForSubGroup = ColorGradientForSubGroupDefault;

            //var gridControl = _gridView.GridControl;
            //gridControl.ForceInitialize();
        }

        void GroupRowDrawing_DrawGroupRow(RowObjectCustomDrawEventArgs e)
        {
            if (!AlignGroupSummary)
                return;
            DrawBackground(e);
            var groupSummaries = ExtractSummaryItems();
            if (groupSummaries.Count == 0)
                return;

            DrawSummaryValues(e, groupSummaries);
            e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
        }


        List<GridGroupSummaryItem> ExtractSummaryItems()
        {
            return _gridView.GroupSummary
                            .OfType<GridGroupSummaryItem>()
                            .Where(si => si.SummaryType != SummaryItemType.None)
                            .ToList();
        }

        void DrawBackground(RowObjectCustomDrawEventArgs e)
        {
            var painter = (GridGroupRowPainter)e.Painter;
            var info = (GridGroupRowInfo)e.Info;
            int level = _gridView.GetRowLevel(e.RowHandle);
            int row = _gridView.GetDataRowHandleByGroupRowHandle(e.RowHandle);
            // Ne modifie pas info.GroupText car il est deja modifié par EnhancedGridView_DisplayGroupRowCount
            //info.GroupText = string.Format("{0}", _gridView.GetRowCellDisplayText(row, _gridView.GroupedColumns[level]));
            //info.GroupText = string.Format("{0}: {1}", view.GroupedColumns[level].Caption, view.GetRowCellDisplayText(row, view.GroupedColumns[level]));

            if (UseColorGradientForSubGroup)
                e.Appearance.BackColor = e.Appearance.BackColor.InterpolateTo(ColorGradientForSubGroup, Math.Min(1, level * 1.0 / MaxGroupRowGradient));

            e.Appearance.DrawBackground(e.Cache, info.Bounds);

            // Ajout d'une boucle pour afficher les indentations, grâce à https://www.devexpress.com/Support/Center/Question/Details/CQ52454
            //using (var pen = new Pen(Color.Gray))
            //{
            //    pen.DashPattern = new float[] { 1, 1 };

                foreach (IndentInfo indent in info.Indents)
                {
                    indent.Appearance.FillRectangle(e.Cache, indent.Bounds);

            //        e.Graphics.DrawLine(pen,
            //                            new Point(indent.Bounds.Left + indent.Bounds.Width / 2, indent.Bounds.Top),
            //                            new Point(indent.Bounds.Left + indent.Bounds.Width / 2, indent.Bounds.Bottom));
            //        e.Graphics.DrawLine(pen,
            //                            new Point(indent.Bounds.Left + indent.Bounds.Width / 2, indent.Bounds.Top + indent.Bounds.Height / 2),
            //                            new Point(indent.Bounds.Right, indent.Bounds.Top + indent.Bounds.Height / 2));
                }
            //} 
            painter.ElementsPainter.GroupRow.DrawObject(info);
        }
        public static uint MaxGroupRowGradient = 3;

        void DrawSummaryValues(RowObjectCustomDrawEventArgs e, List<GridGroupSummaryItem> groupSummaries)
        {
            Hashtable values = _gridView.GetGroupSummaryValues(e.RowHandle);
            if (values == null)
                return;
            foreach (var item in groupSummaries)
            {
                Rectangle rect = GetColumnBounds(item);
                if (rect.IsEmpty)
                    continue;
                string text = item.GetDisplayText(values[item], false);
                var col = _gridView.Columns[item.FieldName];
                if (values[item] is DateTime && values[item].ToString() == text) // Seems to be the default text for date
                { 
                    // Let's see if there is a better way to display date
                    // this code is related to how EnhancedGridView_ColumnFormatChanger.FillMenu code works
                    if ((col.ColumnEdit as RepositoryItemDateEdit)?.Mask?.UseMaskAsDisplayFormat ?? false)
                        text = ((DateTime)values[item]).ToString((col.ColumnEdit as RepositoryItemDateEdit).Mask.EditMask);
                }

                rect = CalcSummaryRect(text, e, col);

                if (col is BandedGridColumn bcol)
                {
                    if (bcol.OwnerBand == null)
                        continue; // pas visible de toute facon
                    // Si la colonne n'est pas fixée elle est donc affiché en dessous des colonnes fixés (si elles existent). Une partie nde la zone n'est donc pas visible car recouverte
                    // On fait en sorte que le rectangle d'affichage soit le rectangle normal - les zone ou les colonne sont fixées
                    if (bcol.OwnerBand.Fixed == FixedStyle.None)
                    {
                        if (_gridView.GetViewInfo() is GridViewInfo vi)
                        {
                            var r = rect;
                            r.Intersect(vi.ViewRects.FixedLeft);
                            if (r.Width > 0)
                            { 
                                // Supprime la partie du rectangle sur laquelle on souhaite dessiner qui fait aussi partie de la zone figée à gauche (bandes / colonnes)
                                rect.X += r.Width;
                                rect.Width -= r.Width;
                            }
                            r = rect;
                            r.Intersect(vi.ViewRects.FixedRight);
                            if (r.Width > 0)
                            { // Supprime la partie du rectangle sur laquelle on souhaite dessiner qui fait aussi partie de la zone figée à droite (bandes / colonnes)
                                rect.Width -= r.Width;
                            }
                        }
                    }
                }
                var appearance = CustomizeAppearance == null ? e.Appearance : CustomizeAppearance(e.Appearance, item);
                appearance.DrawString(e.Cache, text, rect);
            }
        }

        Rectangle GetColumnBounds(GridGroupSummaryItem item)
        {
            var rec = new Rectangle();
            GridColumn column = _gridView.Columns[item.FieldName];
            if (column != null) // ajout 
                rec = GetColumnBounds(column);
            return rec;
        }

        static Rectangle GetColumnBounds(GridColumn column)
        {
            var gridInfo = (GridViewInfo)column.View.GetViewInfo();
            GridColumnInfoArgs colInfo = gridInfo.ColumnsInfo[column];

            if (colInfo != null)
                return colInfo.Bounds;
            return Rectangle.Empty;
        }

        static Rectangle CalcSummaryRect(string text, RowObjectCustomDrawEventArgs e, GridColumn column)
        {
            SizeF sz = TextUtils.GetStringSize(e.Graphics, text, e.Appearance.Font);
            int width = Convert.ToInt32(sz.Width) + 1;
            Rectangle result = GetColumnBounds(column);
            // gridInfo est la meme variable que celle de GetColumnBounds normalement
            //if (!gridInfo.ViewRects.FixedLeft.IsEmpty)
            //{
            //    int fixedLeftRight = gridInfo.ViewRects.FixedLeft.Right;
            //    int marginLeft = result.Right - width - fixedLeftRight;
            //    if (marginLeft < 0 && column.Fixed == FixedStyle.None)
            //        return Rectangle.Empty;
            //}
            //if (!gridInfo.ViewRects.FixedRight.IsEmpty)
            //{
            //    int fixedRightLeft = gridInfo.ViewRects.FixedRight.Left;
            //    if (fixedRightLeft <= result.Right && column.Fixed == FixedStyle.None)
            //        return Rectangle.Empty;
            //}
            result = FixLeftEdge(width, result);
            result.Y = e.Bounds.Y;
            result.Height = e.Bounds.Height - 2;

            return PreventSummaryTextOverlapping(e, result);
        }

        static Rectangle PreventSummaryTextOverlapping(RowObjectCustomDrawEventArgs e, Rectangle rect)
        {
            var gInfo = (GridGroupRowInfo)e.Info;
            int groupTextLocation = gInfo.ButtonBounds.Right + 10;
            int groupTextWidth = TextUtils.GetStringSize(e.Graphics, gInfo.GroupText, e.Appearance.Font).Width;

            //from https://www.devexpress.com/Support/Center/Question/Details/Q490939
            // Mais  throw une exception
            //Graphics graphics = gInfo.ViewInfo.GInfo.Graphics;
            //var groupTextBounds = StringPainter.Default.Calculate(graphics, gInfo.View.Appearance.GroupRow, gInfo.GroupText, gInfo.ViewInfo.ViewRects.Rows.Width).Bounds;


            var r = new Rectangle(groupTextLocation, 0, groupTextWidth, e.Info.Bounds.Height);
            if (r.Right > rect.X)
            {
                if (r.Right > rect.Right)
                    rect.Width = 0;
                else
                {
                    rect.Width -= r.Right - rect.X;
                    rect.X = r.Right;
                }
            }
            return rect;
        }

        static Rectangle FixLeftEdge(int width, Rectangle result)
        {
            int delta = result.Width - width - 2;
            if (delta > 0)
            {
                result.X += delta;
                result.Width -= delta;
            }
            return result;
        }

        #endregion
    }
}
