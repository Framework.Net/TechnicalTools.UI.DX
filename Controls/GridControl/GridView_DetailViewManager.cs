﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// This class is used to improve cooperation between a lot of classes that would like to handle master/detail relations in Devexpress's gridviews
    /// Currently the event associated to master row detail system (ie the event beginning with MasterRow) does not allow cooperation.
    /// So only one class (or directly the UI form/view) can handle these events and their expecting value.
    /// For example the event GridView.MasterRowEmpty as an event argument using a boolean to indicate if the master row has or has not details view.
    /// If two class subscribe to this event, none of them can clearly state that the master row has no details views.
    /// Because each handler does not know each other, no handler can take responsability to set "false" to Event argument "isEmpty".
    /// The idea of this class is just to wrap MasterRowEvent to make cooperation better. By letting code to subscribe to this class instead of directly to gridview
    /// This class will aggregate response to make behavior consistent for both of class and make them independant.
    /// This improve the overall quality of code design.
    /// </summary>
    public class GridView_DetailViewManager
    {
        readonly GridView _view;

        public abstract class DetailView
        {
            protected DetailView()
            {
            }
            readonly Dictionary<object, BaseView> _RememberedPatternDetailViews = new Dictionary<object, BaseView>();
            protected void RememberPatternDetailViewsFor(object obj, BaseView patternDetailView)
            {
                _RememberedPatternDetailViews.Add(obj, patternDetailView);
            }

            public virtual Type BaseTypeOfDetailObject { get { return typeof(object); } }

            /// <summary>
            /// Indicate if the view has a meaning for object represented by GetRow(view, rowHandle).
            /// This method should be constant for a each object. For example : if (obj is Foo && (obj as Foo).ImmutableValue == 42)
            /// If the result of this method change for example ImmutableValue is actually updted by an event from DB (pushing)
            /// you have to do this update in gui thread to prevent gridview to mess silently with detail view
            /// </summary>
            public abstract   bool   HasMeaningAndIsEnabledFor(GridView masterView, int rowHandle);

            /// <summary>
            /// Just get a name for the relation. For example "All his/her objects".
            /// </summary>
            public abstract string   GetName(GridView masterView, int rowHandle);
            /// <summary>
            /// Just in case GetName must return a non user friendly name.
            /// Return the user friendly name here.
            /// </summary>
            public virtual  string   GetDisplayCaption(GridView masterView, int rowHandle) { return GetName(masterView, rowHandle); }
            /// <summary>
            /// Indicate if the detail view is empty. It helps gridview to know if we haveto display + sign at left of master row.
            /// </summary>
            /// <returns>
            /// null means : maybe... we dont know yet (see GetChildList to know how to handle that)
            /// true means : yes it is "definitively empty"
            /// false means : no means "definitively NOT empty"
            /// </returns>
            public abstract bool?    IsEmpty(GridView masterView, int rowHandle);

            /// <summary>
            /// Tells how to get business object of masterview from a masterRowHandle
            /// Default implementation is masterView.GetRow(rowHandle)
            /// </summary>
            protected virtual object GetRow(GridView masterView, int rowHandle) { return masterView.GetRow(rowHandle); }

            /// <summary>
            /// Create pattern view instance that gridview must clone to display its detail view.
            /// You must do the maximum of configuration here to speed up UI for user.
            /// See also <seealso cref="RememberPatternDetailViewsFor"/> to know more.
            /// </summary>
            public virtual  BaseView GetDefaultView(GridView masterView, int rowHandle, BaseView proposedDefaultPatternView)
            {
                if (rowHandle < 0)
                    return proposedDefaultPatternView;
                var bo = GetRow(masterView, rowHandle);
                if (ReferenceEquals(bo, null))
                    return proposedDefaultPatternView;
                BaseView gvPatternDetailView;
                if (_RememberedPatternDetailViews.TryGetValue(bo, out gvPatternDetailView))
                    return gvPatternDetailView;
                var egv = new EnhancedGridView(masterView.GridControl);
                SetupDefaultLayoutOnPatternDetailViewExpandedForBusinessObject?.Invoke(masterView, egv, bo);
                return egv;
            }
            /// <summary>
            /// Default pattern gridview configuration applied in GetDefaultView. Perfect injection for global UI framework decision.
            /// </summary>
            public static Action<GridView, BaseView, object> SetupDefaultLayoutOnPatternDetailViewExpandedForBusinessObject = (masterView, proposedDefaultPatternView, rowObject) =>
            {
            };

            /// <summary>
            /// Indicate if user can expand.
            /// The first details view expandable is the one that is shown by default
            /// </summary>
            public virtual  bool     CanExpand(GridView masterView, int rowHandle)
            {
                var loadTask = GetChildList(masterView, rowHandle);
                return loadTask != null && (loadTask.Status != TaskStatus.RanToCompletion || loadTask.Result.Count > 0); 
            }

            /// <summary>
            /// Event when pattern detail view is cloned to detailView. and this clone has a datasource assigned.
            /// You can configure again the detail view here but it is advised do maximum configuration in <see cref="GetDefaultView(GridView, int, BaseView)"/> method.
            /// When overriding this method, call base method and if it returns true, it means <see cref="RememberDetailViewInstance"/> is true and a detailview already exist.
            /// In that case you can directly return true. Else, configure detailView and return false.
            /// </summary>
            public virtual void Expanded(GridView masterView, int rowHandle, BaseView clonedDetailView)
            {
                SetupDefaultLayoutOnDetailViewExpandedForBusinessObject?.Invoke(masterView, clonedDetailView, GetRow(masterView, rowHandle));
            }

            /// <summary>
            /// Default gridview configuration applied in Expanded. Perfect injection for global UI framework decision.
            /// </summary>
            public static Action<GridView, BaseView, object> SetupDefaultLayoutOnDetailViewExpandedForBusinessObject = (masterView, clonedDetailView, rowObject) =>
            {
                var gv = clonedDetailView as GridView;
                if (gv != null)
                    AssignDefaultConfiguration(gv);
            };

            /// <summary>
            /// Just load the details view and return a task that load the details object list to display
            /// If developper does not want to load asynchronous, he can use <see cref="Task.FromResult{TResult}(TResult)" />
            /// </summary>
            public abstract Task<IReadOnlyCollection<object>> GetChildList(GridView masterView, int rowHandle);

            /// <summary>
            /// Indicate if view can collapse. Probably to prevent user for leaving if data is editable and not good
            /// </summary>
            public virtual  bool     CanCollapse(GridView masterView, int rowHandle) { return true; }

            public virtual  void     Collapsed(GridView masterView, int rowHandle) { }
        }


        internal static void AssignDefaultConfiguration(GridView gv)
        {
            gv.BeginUpdate();
            // Readonly behavior is the default to make developpers' code explicit about when user can change data
            gv.OptionsBehavior.ReadOnly = true;
            gv.OptionsBehavior.Editable = true;

            gv.OptionsBehavior.Editable = false;
            gv.OptionsView.AllowCellMerge = true;
            gv.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CellSelect;
            gv.OptionsSelection.MultiSelect = true; // sinon la ligne au dessus ne marche pas de maniere intuitive (ie : selection une seule cellule)
            gv.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown; // Sinon il faut trois click pour editer une cellule !
            gv.OptionsView.RowAutoHeight = true; // Pour afficher les memoedit sur plusieurs lignes
            gv.OptionsView.ColumnAutoWidth = false;

            gv.OptionsView.ShowGroupPanel = gv.ParentView == null;

            gv.RowCellStyle += (_, e) =>
            {
                e.Appearance.BackColor = e.GetCellFocusedBackgroundColorOrCurrentOne(gv);
            };

            var backup = gv.BestFitMaxRowCount;
            gv.BestFitMaxRowCount = 5;
            gv.BestFitColumns();
            gv.BestFitMaxRowCount = backup;
            gv.EndUpdate();
        }
        public IReadOnlyList<DetailView> DetailViews { get { return _detailViews; } }
        readonly List<DetailView> _detailViews = new List<DetailView>();
        

        public GridView_DetailViewManager(GridView view)
        {
            _view = view;
        }

        internal void Assign(GridView_DetailViewManager f, bool copyEvents)
        {
            // We use loop for using behavior of Add / Remove
            while (_detailViews.Count > 0) 
                Remove(_detailViews.Last());
            foreach (var dv in f._detailViews)
                Add(dv);
        }

        public void Add(DetailView rel)
        {
            _detailViews.Add(rel);
            if (_detailViews.Count == 1)
                Install();
        }
        public bool Remove(DetailView rel)
        {
            var b = _detailViews.Remove(rel);
            if (_detailViews.Count == 0)
                Uninstall();
            return b;
        }

        public void Install()
        {
            Uninstall(); // security (and it is better if our handler are in last for consistency check)
            _view.MasterRowGetRelationCount += view_MasterRowGetRelationCount;
            _view.MasterRowGetRelationName += view_MasterRowGetRelationName;
            _view.MasterRowGetRelationDisplayCaption += view_MasterRowGetRelationDisplayCaption;
            _view.MasterRowEmpty += view_MasterRowEmpty;
            _view.MasterRowGetLevelDefaultView += view_MasterRowGetLevelDefaultView;
            _view.MasterRowExpanding += view_MasterRowExpanding;
            _view.MasterRowExpanded += view_MasterRowExpanded;
            _view.MasterRowGetChildList += view_MasterRowGetChildList;
            _view.MasterRowCollapsed  += view_MasterRowCollapsed;
            _view.MasterRowCollapsing += view_MasterRowCollapsing;
            _view.CustomDrawCell += view_CustomDrawCell;

            // make AllowExpandEmptyDetails in effect
            _view.OptionsDetail.EnableMasterViewMode = true;
            // allow user to show an empty detail view boudn to a bindinlist
            // while a background worker is loading it
            _view.OptionsDetail.AllowExpandEmptyDetails = true;
            _view.OptionsDetail.SmartDetailExpandButtonMode = DetailExpandButtonMode.AlwaysEnabled;
        }
        public void Uninstall()
        {
            _view.MasterRowGetRelationCount -= view_MasterRowGetRelationCount;
            _view.MasterRowGetRelationName -= view_MasterRowGetRelationName;
            _view.MasterRowGetRelationDisplayCaption -= view_MasterRowGetRelationDisplayCaption;
            _view.MasterRowEmpty -= view_MasterRowEmpty;
            _view.MasterRowGetLevelDefaultView -= view_MasterRowGetLevelDefaultView;
            _view.MasterRowExpanding -= view_MasterRowExpanding;
            _view.MasterRowExpanded -= view_MasterRowExpanded;
            _view.MasterRowGetChildList -= view_MasterRowGetChildList;
            _view.MasterRowCollapsed -= view_MasterRowCollapsed;
            _view.MasterRowCollapsing -= view_MasterRowCollapsing;
            _view.CustomDrawCell -= view_CustomDrawCell;
        }


        void view_MasterRowGetRelationCount(object sender, MasterRowGetRelationCountEventArgs e)
        {
            if (e.RelationCount > 0)
                throw new TechnicalException("Some other code is using \"MasterRow*\" event manually." + Environment.NewLine +
                                             "Convert this code so it uses " + _view.GetType().Name + "." + nameof(EnhancedGridView.DetailViewManager) + " or stop using " + nameof(EnhancedGridView.DetailViewManager) + " youself :/", null);
            var view = (GridView)sender;
            if (e.RowHandle == GridControl.InvalidRowHandle) // init case
            {
                e.RelationCount = 1; //_currentDataSource == _originalRootDataSource ? 1 : 0; // Say to the gridview that master / detail mode is enabled for this view
                                     // Permet que l'event MasterRowGetChildList ne soit pas appelé deux fois pour un click de l'utilisateur
                                     // cf https://www.devexpress.com/Support/Center/Question/Details/Q223489/masterrowgetchildlist-is-called-twice
                                     // Ou alors il faudrait gerer un cache ici ...
                                     // De plus cela permet le chargement asynchrone des vue de detail.
                                     // cf https://www.devexpress.com/Support/Center/Example/Details/E2745/master-detail-how-to-load-detail-view-data-asynchronously
                return;
            }

            var line = view.GetRow(e.RowHandle);
            if (line == null)
            {
                // BUG : Devexpress (to submit to them) See  https://www.devexpress.com/Support/Center/Question/Details/Q517939/xtragrid-gridview-getrow-0-returns-null
                // To reproduce (i think) : load a datasource with 10 items in grid
                // apply a filter so that the rowhandle 0 points to item in position 9 in datasource
                // Now assign a new datasource with only 5 item.
                // If the item in datasource where the rowhandle was pointing is not in datasource anymore
                // MasterRowGetRelationCount is still raised but in the handler we get rowhandle = 0
                // and when we call (sender as GridView).GetRow(e.RowHandle) we get null.
                return;
            }

            int cnt = 0;
            foreach (var rel in _detailViews)
                cnt += rel.HasMeaningAndIsEnabledFor(sender as GridView, e.RowHandle) ? 1 : 0;
            e.RelationCount = cnt;
        }
        private DetailView GetRelationDefinitionHavingMeaning(object sender, int rowHandle, int relationIndex)
        {
            int cnt = relationIndex;
            foreach (var rel in _detailViews)
            {
                cnt -= rel.HasMeaningAndIsEnabledFor(sender as GridView, rowHandle) ? 1 : 0;
                if (cnt < 0)
                    return rel;
            }
            return null;
        }

        void view_MasterRowGetRelationName(object sender, MasterRowGetRelationNameEventArgs e)
        {
            if (e.RowHandle == GridControl.InvalidRowHandle)
                return;
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            e.RelationName = rel.GetName(sender as GridView, e.RowHandle);
        }

        void view_MasterRowGetRelationDisplayCaption(object sender, MasterRowGetRelationNameEventArgs e)
        {
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            e.RelationName = rel.GetDisplayCaption(sender as GridView, e.RowHandle);
        }

        void view_MasterRowEmpty(object sender, MasterRowEmptyEventArgs e)
        {
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            e.IsEmpty = rel.IsEmpty(sender as GridView, e.RowHandle) ?? false;
        }


        void view_MasterRowGetLevelDefaultView(object sender, MasterRowGetLevelDefaultViewEventArgs e)
        {
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            e.DefaultView = rel.GetDefaultView(sender as GridView, e.RowHandle, e.DefaultView);
        }

        void view_MasterRowExpanding(object sender, MasterRowCanExpandEventArgs e)
        {
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            var view = sender as GridView;
            e.Allow = rel.CanExpand(view, e.RowHandle);
        }

        void view_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
        {
            var view = sender as GridView;
            var subView = view.GetDetailView(e.RowHandle, e.RelationIndex);
            if (subView == null) // Happens sometimes (don't know wy...)
                return;
            var rel = GetRelationDefinitionHavingMeaning(view, e.RowHandle, e.RelationIndex);
            rel.Expanded(view, e.RowHandle, subView);
        }

        void view_MasterRowGetChildList(object sender, MasterRowGetChildListEventArgs e)
        {
            var start = DateTime.UtcNow;
            var view = sender as GridView;
            var gc = view.GridControl;
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            var loadTask = rel.GetChildList(view, e.RowHandle);
            if (loadTask == null)
                return;
            // Wait 1s before showing waitform. Note that GetChildList call may have already wait 1s to see if it need to retunr a task or a list
            var timetoWait = 500 - (int)(DateTime.UtcNow - start).TotalMilliseconds;
            // If task has taken 1s to work (and is still not done) we need to make user wait
            if (Task.WaitAll(loadTask.WrapInArray(), Math.Max(0, timetoWait)))
            {
                e.ChildList = loadTask.Result is IList lst ? lst : loadTask.Result.ToList();
                if ((e.ChildList?.Count ?? 0) == 0)
                    ShowNothingToDisplayMessage(gc);
            }
            else
            {
                var fblst = new object[0].ToTypedFixedBindingList(rel.BaseTypeOfDetailObject);
                e.ChildList = fblst;
                // So, we let user to know a process is running in background and UI has not frozen
                DefaultShowWaitingLayerOnSubviewUntilTaskIsDone(loadTask, view, e.RowHandle, e.RelationIndex);
                loadTask.ContinueWith(_ =>
                {
                    if (loadTask.Status != TaskStatus.RanToCompletion) // In case of bug from the loader
                        return;
                    var loadedItems = loadTask.Result;
                    if ((loadedItems?.Count ?? 0) == 0)
                        ShowNothingToDisplayMessage(gc);
                    else
                    {
                        var detailView = (GridView)view.GetDetailView(e.RowHandle, e.RelationIndex);
                        detailView.BeginDataUpdate();
                        // WithListChangedEventsDisabled 
                        // prevent a lot of event to happens for each items,
                        // It just makes one at the end (reset) that would set the focusrowhandle to the end
                        // but thanks to EndDataUpdate, it does not happens
                        fblst.WithListChangedEventsDisabled(() => 
                        {
                            foreach (var item in loadedItems)
                                fblst.Add(item);
                        });
                        detailView.EndDataUpdate();
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }
        public static Action<Task /*loadTask*/, GridView /*masterView*/, int /*rowHandle*/, int /*relationIndex*/> ShowWaitingLayerOnSubviewUntilTaskIsDone { get; set; } = DefaultShowWaitingLayerOnSubviewUntilTaskIsDone;
        // TODO : Replace this default method by https://supportcenter.devexpress.com/ticket/details/t496636/gridview-how-to-display-an-indicator-in-the-detail-view-to-indicate-that-detail-data-is
        public static void DefaultShowWaitingLayerOnSubviewUntilTaskIsDone(Task loadTask, GridView masterView, int rowHandle, int relationIndex)
        {
            WaitControlGeneric.ShowOnControl(masterView.GridControl, "Loading in progress...", () =>
            {
                // At least one another second to avoid ugly blinking if task need 1.01 s...
                Thread.Sleep(900);
                ExceptionManager.Instance.IgnoreException(loadTask.Wait);
            }, null);
        }

        public static Action<GridControl> DefaultShowNothingToDisplayMessage = gc => gc.BeginInvokeSafely(() => XtraMessageBox.Show(gc, "There is nothing to display on this tab!"));

        protected virtual void ShowNothingToDisplayMessage(GridControl gc)
        {
            DefaultShowNothingToDisplayMessage?.Invoke(gc);
        }

        void view_MasterRowCollapsing(object sender, MasterRowCanExpandEventArgs e)
        {
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            e.Allow = rel.CanCollapse(sender as GridView, e.RowHandle);
        }

        void view_MasterRowCollapsed(object sender, CustomMasterRowEventArgs e)
        {
            var rel = GetRelationDefinitionHavingMeaning(sender, e.RowHandle, e.RelationIndex);
            rel.Collapsed(sender as GridView, e.RowHandle);
        }

        void view_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (e.RowHandle == GridControl.AutoFilterRowHandle)
                return;
            if (e.Column.VisibleIndex == 0)
            {
                bool? isAllEmpty = null;
                foreach (var rel in _detailViews)
                    if (rel.HasMeaningAndIsEnabledFor(sender as GridView, e.RowHandle))
                    {
                        var empty = rel.IsEmpty(sender as GridView, e.RowHandle);
                        if (isAllEmpty == null)
                            isAllEmpty = empty;
                        else if (empty == false)
                            isAllEmpty = false;
                        else if (empty == null && isAllEmpty == true)
                            isAllEmpty = null;
                    }
                if (isAllEmpty == true)
                {
                    var view = (GridView)sender;
                    // Idea from https://www.devexpress.com/Support/Center/Question/Details/T115118/gridcontrol-add-an-option-to-hide-expand-buttons-when-detail-views-do-not-have-data
                    ((GridCellInfo)e.Cell).CellButtonRect = Rectangle.Empty;
                }
                else if (isAllEmpty == null)
                {
                    // TODO: Draw a loading icon ?
                }
            }
        }
    }
}
