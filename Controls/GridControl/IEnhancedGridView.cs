using System;

using DevExpress.Data.Filtering;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace TechnicalTools.UI.DX
{
    public partial interface IEnhancedGridView : IEnhancedGridView_Inherited
    {
        GridView_DetailViewManager DetailViewManager { get; }
        EnhancedGridView_ResizeRow ResizeRow         { get; }

        event Func<GridColumn, bool?> ChangeColumnVisibility;
        event RowObjectCustomDrawEventHandler BeforeCustomDrawGroupRow;
        event Action<IEnhancedGridView, GridControl> GridControlChanged;

        Func<AppearanceObject, GridGroupSummaryItem, AppearanceObject> CustomizeSummaryDraw { get; set; }

        EnhancedGridView_ShowSummariesAndFooter SummariesAndFooterHandler { get; }
        EnhancedGridView_ColumnFormatChanger    ColumnFormatChanger { get; }

        event EventHandler<CreatingAutoFilterCriterionEventArgs> CreatingAutoFilterCriterion;
        event AfterActivateEditorHandler   BeforeActivateEditor;
        event BeforePostEditorHandler BeforePostEditor;

        event EventHandler<RowItemDoubleClickEventArgs> RowItemDoubleClick;
    }
    public delegate bool AfterActivateEditorHandler(object sender, GridCellInfo cell);
    public delegate bool BeforePostEditorHandler(object sender, bool causeValidation);
    internal interface IEnhancedGridView_AccesForViewFeature
    {
        RepositoryItem GetCellEditor(GridCellInfo cell);
    }


    public class CreatingAutoFilterCriterionEventArgs : EventArgs
    {
        public GridColumn           Column      { get; internal set; }
        public AutoFilterCondition  Condition   { get; internal set; }
        public object               Value       { get; internal set; }
        public string               StringValue { get; internal set; }
        public CriteriaOperator     Result      { get; set; }
    }
}
