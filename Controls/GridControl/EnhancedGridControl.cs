﻿using System;
using System.ComponentModel;

using DevExpress.XtraEditors;
using DevExpress.XtraGrid;


namespace TechnicalTools.UI.DX
{
    [ToolboxItem(true)]
    public partial class EnhancedGridControl : GridControl
    {
        protected override ControlNavigator CreateEmbeddedNavigator()
        {
            var navigator = new ControlNavigatorEnhanced(this);
            navigator.SizeChanged += OnEmbeddedNavigator_SizeChanged; // like the base method
            return navigator;
        }

        public override object DataSource
        {
            get { return base.DataSource; }
            set
            {
                DataSourceChanging?.Invoke(this, new Model.PropertyChangingEventArgs(nameof(DataSource), base.DataSource, value));
                base.DataSource = value;
                DataSourceRefreshed?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler<Model.PropertyChangingEventArgs> DataSourceChanging;

        // When datasource is IList and does not implementing BindingList
        // User need to call RefreshDataSource() to refresh datasource if he does not want to change the datasource
        public override void RefreshDataSource()
        {
            base.RefreshDataSource();
            DataSourceRefreshed?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler DataSourceRefreshed;
    }
}
