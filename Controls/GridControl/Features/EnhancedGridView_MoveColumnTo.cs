﻿using System;
using System.Diagnostics;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_MoveColumnTo
    {
        readonly GridView _view;

        public EnhancedGridView_MoveColumnTo(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing        -= View_PopupMenuShowing;        view.PopupMenuShowing        += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing        -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_MoveColumnTo f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
        }


        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            var col = e.HitInfo.Column;
            if (e.HitInfo.InColumn)
            {
                var mnuMain = new DXSubMenuItem("Move to...");
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuMain);
                mnuMain.Items.Add(new DXMenuItem("Leftmost", (_, __) =>
                {
                    if (col is BandedGridColumn bcol && bcol.OwnerBand != null)
                        bcol.OwnerBand.Columns.MoveTo(0, bcol);
                    else
                        col.VisibleIndex = 0;
                }));
                mnuMain.Items.Add(new DXMenuItem("Rightmost", (_, __) =>
                {
                    if (col is BandedGridColumn bcol && bcol.OwnerBand != null)
                        bcol.OwnerBand.Columns.MoveTo(0, bcol);
                    else
                        col.VisibleIndex = view.VisibleColumns.Count;
                }));
            }
        }
    }
}
