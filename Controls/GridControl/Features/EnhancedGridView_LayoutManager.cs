﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Utils.Drawing;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

using TechnicalTools;


namespace TechnicalTools.UI.DX
{
    // A voir https://www.devexpress.com/Support/Center/Example/Details/E1951/how-to-implement-your-own-serializable-property-for-the-gridcolumn
    public partial class EnhancedGridView_LayoutManager
    {
        readonly GridView          _view;
                 ILayoutRepository _layoutRepository;

        public static bool AllEnabled { get; set; } = true;
        public bool ShowLayoutMenu { get; set; } = true;

        public static Func<ILayoutRepository> CreateLayoutRepository { get; set; } = () => new DefaultLayoutRepository();

        public EnhancedGridView_LayoutManager(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            Uninstall();
            _layoutRepository = CreateLayoutRepository?.Invoke();
            view.PopupMenuShowing     += View_PopupMenuShowing;
            view.CustomDrawGroupPanel += View_CustomDrawGroupPanel;
            view.MouseUp += View_MouseUp;
        }

        public void Uninstall()
        {
            _layoutRepository = null;
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing     -= View_PopupMenuShowing;
            view.CustomDrawGroupPanel -= View_CustomDrawGroupPanel;
            view.MouseUp -= View_MouseUp;
        }

        internal void Assign(EnhancedGridView_LayoutManager f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
            ShowLayoutMenu = f.ShowLayoutMenu;
        }

        void View_CustomDrawGroupPanel(object sender, CustomDrawEventArgs e)
        {
            if (_layoutRepository == null || !ShowLayoutMenu || !AllEnabled)
                return;

            var view = (GridView)sender;
            // Test si on est dans une detailView et pas dans la masterview. 
            // On gere pas pour le moment
            if (view.ParentView != null)
                return;

            var viewInfo = (GridViewInfo)view.GetViewInfo();
            GridElementsPainter elementsPainter = viewInfo.Painter.ElementsPainter;
            StyleObjectInfoArgs groupArgs = new StyleObjectInfoArgs(e.Cache, e.Bounds, e.Appearance, ObjectState.Normal);
            elementsPainter.GroupPanel.DrawObject(groupArgs);
            if (view.OptionsView.ShowGroupPanel)
            {
                string text = "Layouts"; // text to display in button

                using (var font = GetButtonFont(view))
                using (var layoutButtonBrush = new SolidBrush(LayoutButtonColor))
                {
                    // Is groupArgs.Bounds == viewInfo.ViewRects.GroupPanel ? It seems it is.
                    var rect = RefreshButtonBounds(view, e.Graphics, text, font, groupArgs.Bounds);
                    e.Graphics.FillRectangle(layoutButtonBrush, rect);
                    var rectTextCentered = new RectangleF(rect.X + TextMargin, rect.Y + TextMargin, rect.Width, rect.Height);
                    e.Graphics.DrawString(text, font, SystemBrushes.ControlText, rectTextCentered);
                    e.Graphics.DrawRectangle(SystemPens.ControlDark, rect);
                    _lastButtonBounds = rect;
                }
            }
            e.Handled = true;
        }
        Rectangle _lastButtonBounds;
        Font GetButtonFont(GridView view)
        {
            return new Font(view.Appearance.GroupPanel.Font, FontStyle.Bold);
        }
        /// <summary>
        /// Determine where the Layout button should be drawn (in gridview coordinate)
        /// </summary>
        /// <returns></returns>
        Rectangle RefreshButtonBounds(GridView view, Graphics g, string text, Font font, Rectangle groupPanelBounds)
        {
            int rightDistance = 20; // Distance between right border of button and right border of gridview (or left side of "find" area)
            
            // Get information about "Find" Area.
            // For a better code wait for answer here : https://www.devexpress.com/Support/Center/Question/Details/T848902
            var viewInfo = (GridViewInfo)view.GetViewInfo();
            var findRect = viewInfo.ViewRects.GroupPanelFindPanelButton;
            Control findControl = null;
            if (findRect.IsEmpty) // Zoom Glass button not display ? It means we user is currently looking for something
            { 
                findControl = view.GridControl.Controls.OfType<DevExpress.XtraGrid.Controls.FindControl>().FirstOrDefault();
                if (findControl != null) // defensive code but I _guess_ ut should never be null
                    if (view.OptionsFind.FindPanelLocation != GridFindPanelLocation.Panel)
                    {
                        findRect = findControl.Bounds;
                        rightDistance /= 2;
                    }
                    else
                        findControl = null;
            }

            SizeF textSize = g.MeasureString(text, font);
            var left = (int)(groupPanelBounds.Right - rightDistance - findRect.Width - textSize.Width - 2 * TextMargin);
            var w = (int)Math.Ceiling(textSize.Width) + 2 * TextMargin;
            var h = (int)Math.Ceiling(textSize.Height) + 2 * TextMargin;
            var top = findControl == null ? findRect.Top : (groupPanelBounds.Height - h) / 2;
            var rect = new Rectangle(left, top, w, h);

            return rect;
        }
        const int TextMargin = 5; // distance between text enclosing box and border of button (value 0 is ugly)

        public static Color LayoutButtonColor { get; set; } = Color.MediumPurple;
        // Not static anymore, because it caused it cause an exception to be raised 
        // from System.Drawing.Graphics.CheckErrorStatus with 
        // message = object is currently in use elsewhere 
        // HResult = -2146233079
        // When used in different UI thread (run by Application class)
        readonly SolidBrush _LayoutButtonBrush = new SolidBrush(Color.FromArgb(128, LayoutButtonColor));
        
        void View_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (!ShowLayoutMenu || !AllEnabled)
                return;
            if (_lastButtonBounds.Contains(e.Point))
                e.Menu.Items.Clear(); // Remove standard menu because...
        }
        void View_MouseUp(object sender, MouseEventArgs e)
        {
            if (!ShowLayoutMenu || !AllEnabled)
                return;
            if (_lastButtonBounds.Width == 0) // not processed yet
                return;
            if (!_lastButtonBounds.Contains(e.Location))
                return;
            var view = (GridView)sender;

            if (_layoutRepository.UIKeyPath == null) // First time ? We initialize unique key
            {
                var keyUI = view.Name.Yield()
                                .Concat(view.GridControl.Concat(
                                        view.GridControl.AllParentControls()
                                            .Where(ctl => ctl is EnhancedXtraUserControl && !(ctl is XtraUserControlComposite)
                                                       || ctl is DockPanel
                                                       || ctl is Form)
                                            .TakeUntil(ctl => ctl is Form))
                                            .Select(ctl => ctl.Name))
                                .NotBlank()
                                .Reverse()
                                .Join(".");
                _layoutRepository.UIKeyPath = keyUI;
            }

            //... because we display this one here (on mouse click right or left)
            var menu = BuildMenu(view); 
            menu.Show(e.Location);
        }

        #region Menus 

        GridViewMenu BuildMenu(GridView view)
        {
            var menu = new GridViewMenu(view);

            BuildMenu(view, false, menu.Items, menu.HidePopup);
            if (_layoutRepository.HandleSharingLayout)
            {
                var mnu = new DXSubMenuItem("*** Shared Layouts ***");
                menu.Items.Add(mnu);
                mnu.BeginGroup = true;
                BuildMenu(view, true, mnu.Items, menu.HidePopup);
            }

            return menu;
        }

        void BuildMenu(GridView view, bool forShared, DXMenuItemCollection items, Action hidePopup)
        {
            var defaultLayoutInfo = _layoutRepository.DefaultLayoutInfo;
            foreach (var layoutName in _layoutRepository.GetLayoutNames(forShared).OrderBy(lytName => forShared == defaultLayoutInfo.Item2 && lytName == defaultLayoutInfo.Item1 ? 0 : 1))
            {
                bool isDefaultLayout = forShared == defaultLayoutInfo.Item2 && layoutName == defaultLayoutInfo.Item1;
                var caption = layoutName.Replace("&", "&&") + (isDefaultLayout ? " [Default]" : "");
                var mnuLayout = new DXSubMenuItem(caption);
                mnuLayout.Click += (_, __) =>
                {
                    hidePopup();
                    ApplyLayout(view, layoutName, forShared);
                };
                
                mnuLayout.Items.Add(new DXMenuItem("Apply", (_, __) => ApplyLayout(view, layoutName, forShared)));
                mnuLayout.Items.Add(new DXMenuItem("Set as default", (_, __) => SetLayoutAsDefault(view, layoutName, forShared)) { BeginGroup = true });
                mnuLayout.Items.Add(new DXMenuItem("Rename...", (_, __) => RenameLayout(view, layoutName, forShared)) { BeginGroup = true });
                mnuLayout.Items.Add(new DXMenuItem("Save (overwrite!)", (_, __) => SaveCurrentLayout(view, layoutName, forShared)) { BeginGroup = true });
                mnuLayout.Items.Add(new DXMenuItem("Delete!", (_, __) => DeleteLayout(view, layoutName, forShared)) { BeginGroup = true });

                items.Add(mnuLayout);
            }
            var mnuSave = new DXMenuItem("Save current layout...", (_, __) => SaveCurrentLayout(view, null, forShared)) { BeginGroup = true };
            items.Add(mnuSave);
        }

        void SetLayoutAsDefault(GridView view, string layoutName, bool shared)
        {
            _layoutRepository.DefaultLayoutInfo = Tuple.Create(layoutName, shared);
        }

        void SaveCurrentLayout(GridView view, string layoutName, bool shared)
        {
            string newLayoutName = layoutName ?? AskUniqueLayoutNameToUser(view, shared);
            if (newLayoutName == null)
                return;

            var mStream = CaptureLayoutNative(view);
            mStream.Position = 0;
            _layoutRepository.SaveLayout(newLayoutName, shared, mStream);
        }

        void RenameLayout(GridView view, string layoutName, bool shared)
        {
            var newLayoutName = AskUniqueLayoutNameToUser(view, shared, layoutName);
            if (newLayoutName == null || layoutName == newLayoutName)
                return;
            var stream = _layoutRepository.RetrieveLayout(layoutName, shared);
            stream.Position = 0;
            _layoutRepository.SaveLayout(newLayoutName, shared, stream);
            _layoutRepository.DeleteLayout(layoutName, shared);
        }

        void ApplyLayout(GridView view, string layoutName, bool shared)
        {
            view.GridControl.ShowBusyWhileDoingUIWorkInPlace(pr =>
            {
                var stream = _layoutRepository.RetrieveLayout(layoutName, shared);
                stream.Position = 0;
                ApplyLayoutNative(view, stream);
            });
        }

        void DeleteLayout(GridView view, string layoutName, bool shared)
        {
            var user = XtraMessageBox.Show(view.GridControl.FindForm(), $"Are you sure you want to delete layout \"{layoutName}\"?", "Please confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (user == DialogResult.No)
                return;
            _layoutRepository.DeleteLayout(layoutName, shared);
        }

        string AskUniqueLayoutNameToUser(GridView view, bool shared, string proposedLayoutName = null)
        {
            string newLayoutName = proposedLayoutName ?? _layoutRepository.GetLayoutNames(shared).CreateUnique(shared ? "Our Layout" : "My layout");
            string uniquenessConstraint = "";
            do
            {
                do
                {
                    var answer = InputBox.ShowDialog(view.GridControl.FindForm(), ref newLayoutName,
                        $"Please enter a name for your new awsome {(shared ? "shared " : "")}layout" +
                        uniquenessConstraint, validateOnEnter: true);
                    if (answer == DialogResult.Cancel)
                        return null;
                } while (string.IsNullOrWhiteSpace(newLayoutName));
                newLayoutName = newLayoutName.Trim();
                uniquenessConstraint = Environment.NewLine + "(Sorry this name is already taken!)";
            } while (_layoutRepository.GetLayoutNames(shared).Any(layoutName => layoutName.ToLowerInvariant() == newLayoutName.ToLowerInvariant()));

            return newLayoutName;
        }

        #endregion Menus 


        #region Core Feature

        void ApplyLayoutNative(GridView view, Stream stream)
        {
            // From https://www.devexpress.com/Support/Center/Example/Details/E1759/how-to-restore-the-auto-filter-row-from-an-active-filter-after-restoring-the-layout
            // via https://www.devexpress.com/Support/Center/Question/Details/T505076/gridcontrol-how-to-restore-the-filter-text-in-auto-filter-row
            var b = GridView.GuessAutoFilterRowValuesFromFilterAfterRestoreLayout;
            GridView.GuessAutoFilterRowValuesFromFilterAfterRestoreLayout = true;
            try
            {
                view.RestoreLayoutFromStream(stream, DevExpress.Utils.OptionsLayoutBase.FullLayout);
            }
            finally
            {
                GridView.GuessAutoFilterRowValuesFromFilterAfterRestoreLayout = b;
            }
        }

        MemoryStream CaptureLayoutNative(GridView view)
        {
            var mStream = new MemoryStream();

            // From https://www.devexpress.com/Support/Center/Example/Details/E1759/how-to-restore-the-auto-filter-row-from-an-active-filter-after-restoring-the-layout
            // via https://www.devexpress.com/Support/Center/Question/Details/T505076/gridcontrol-how-to-restore-the-filter-text-in-auto-filter-row
            var b = GridView.GuessAutoFilterRowValuesFromFilterAfterRestoreLayout;
            GridView.GuessAutoFilterRowValuesFromFilterAfterRestoreLayout = true;
            try
            {
                view.SaveLayoutToStream(mStream, DevExpress.Utils.OptionsLayoutBase.FullLayout);
            }
            finally
            {
                GridView.GuessAutoFilterRowValuesFromFilterAfterRestoreLayout = b;
            }
            return mStream;
        }

        #endregion
    }
}
