﻿using System;
using System.Diagnostics;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_ExperimentalFeatures
    {
        readonly GridView _view;

        public EnhancedGridView_ExperimentalFeatures(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing;  view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_ExperimentalFeatures f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        static void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;

            var view = sender as GridView;
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, new DXMenuItem("Advanced gridview configuration...", mnuAdvancedGridViewConfiguration_Click) { Tag = view });
        }

        static void mnuAdvancedGridViewConfiguration_Click(object sender, EventArgs e)
        {
            var mnu = sender as DXMenuItem;
            var view = mnu.Tag as GridView;

            var panel = new EnhancedGridView_ExperimentalFeaturesPanel(view);
            panel.ShowInForm(view.GridControl.FindForm());
        }

    }
}
