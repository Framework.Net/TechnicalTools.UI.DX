﻿using System;
using System.Diagnostics;

using DevExpress.Data;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    // TODO : How to add Tooltips for a GridColumns SummaryItem : https://www.devexpress.com/Support/Center/Example/Details/E1979
    public class EnhancedGridView_ShowSummariesAndFooter
    {
        readonly GridView _view;

        public EnhancedGridView_ShowSummariesAndFooter(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            Uninstall(view);
            view.PopupMenuShowing += View_PopupMenuShowing;
            _view.CustomSummaryCalculate += gv_CustomSummaryCalculate;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
            _view.CustomSummaryCalculate -= gv_CustomSummaryCalculate;
        }

        internal void Assign(EnhancedGridView_ShowSummariesAndFooter f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;

            var view = sender as GridView;
            //Debug.Assert(view == _view); // faux dans le cas de sous vues

            if (e.HitInfo.InGroupRow)
            {
                //Debug.Assert((e.HitInfo.Column == null) == (view.OptionsBehavior.AlignGroupSummaryInGroupRow != DevExpress.Utils.DefaultBoolean.True)); // Si un jour ca change, plus besoin du code ci dessous
                GridColumn columnTargeted = view.FindColumnUnderMouse(e.HitInfo);
                if (columnTargeted == null)
                    return;

                var mnuSummary = new DXSubMenuItem("Show summary here...");

                if ((columnTargeted.ColumnType.TryGetNullableType() ?? columnTargeted.ColumnType).IsNumericType())
                {
                    var mnuSum = new DXMenuCheckItem("Sum");
                    var sumFormat = (columnTargeted.ColumnType.TryGetNullableType() ?? columnTargeted.ColumnType).IsIntegerType() ? "{0:n0}" : "{0:#,###.00;-#,###.00;0}";
                    mnuSum.Click += (_, __) => ShowSumInGroupRow(columnTargeted, sumFormat);
                    mnuSummary.Items.Add(mnuSum);

                    var mnuAverage = new DXMenuCheckItem("Average");
                    mnuAverage.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Average,
                                                                                               columnTargeted.FieldName, columnTargeted,
                                                                                               "{0:#,###.00;-#,###.00;0}") { ShowInGroupColumnFooter = null });
                    mnuSummary.Items.Add(mnuAverage);

                    var mnuMin = new DXMenuCheckItem("Min");
                    mnuMin.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Min,
                                                                                          columnTargeted.FieldName, columnTargeted,
                                                                                           "{0:#,###.00;-#,###.00;0}") { ShowInGroupColumnFooter = null });
                    mnuSummary.Items.Add(mnuMin);

                    var mnuMax = new DXMenuCheckItem("Max");
                    mnuMax.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Max,
                                                                                           columnTargeted.FieldName, columnTargeted,
                                                                                          "{0:#,###.00;-#,###.00;0}") { ShowInGroupColumnFooter = null });
                    mnuSummary.Items.Add(mnuMax);
                }
                else if (columnTargeted.ColumnType == typeof(string))
                {
                    var mnuShortestValue = new DXMenuCheckItem("Shortest text (not empty)");
                    mnuShortestValue.Click += (_, __) => AddOrReplace((GridView)columnTargeted.View,
                                                            new GridGroupSummaryItem(SummaryItemType.Custom, columnTargeted.FieldName, columnTargeted, "")
                                                            {
                                                                ShowInGroupColumnFooter = null,
                                                                Tag = StringShortestNotEmptyMenuKey
                                                            });
                    mnuSummary.Items.Add(mnuShortestValue);

                    var mnuLongestValue = new DXMenuCheckItem("Longest text");
                    mnuLongestValue.Click += (_, __) => AddOrReplace((GridView)columnTargeted.View,
                                                            new GridGroupSummaryItem(SummaryItemType.Custom, columnTargeted.FieldName, columnTargeted, "")
                                                            {
                                                                ShowInGroupColumnFooter = null,
                                                                Tag = StringLongestMenuKey
                                                            });
                    mnuSummary.Items.Add(mnuLongestValue);
                }
                else if ((columnTargeted.ColumnType.TryGetNullableType() ?? columnTargeted.ColumnType) == typeof(TimeSpan) ||
                         (columnTargeted.ColumnType.TryGetNullableType() ?? columnTargeted.ColumnType) == typeof(DateTime))
                {
                    var mnuSum = new DXMenuCheckItem("Sum");
                    mnuSum.Click += (_, __) => ShowSumInGroupRow(columnTargeted);
                    mnuSummary.Items.Add(mnuSum);

                    //var mnuAverage = new DXMenuCheckItem("Average");
                    //mnuAverage.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Average,
                    //                                                                           columnTargeted.FieldName, columnTargeted,
                    //                                                                           "") { ShowInGroupColumnFooter = null });
                    //mnuSummary.Items.Add(mnuAverage);

                    var mnuMin = new DXMenuCheckItem("Min");
                    mnuMin.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Min,
                                                                                           columnTargeted.FieldName, columnTargeted,
                                                                                           "") { ShowInGroupColumnFooter = null });
                    mnuSummary.Items.Add(mnuMin);

                    var mnuMax = new DXMenuCheckItem("Max");
                    mnuMax.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Max,
                                                                                           columnTargeted.FieldName, columnTargeted, 
                                                                                           "") { ShowInGroupColumnFooter = null });
                    mnuSummary.Items.Add(mnuMax);
                }
                else if ((columnTargeted.ColumnType.TryGetNullableType() ?? columnTargeted.ColumnType) == typeof(bool))
                {
                    var mnuSum = new DXMenuCheckItem("Sum of checked");
                    mnuSum.Click += (_, __) => ShowSumInGroupRow(columnTargeted, "{0:n0}");
                    mnuSummary.Items.Add(mnuSum);

                    //var mnuAverage = new DXMenuCheckItem("Average");
                    //mnuAverage.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Average,
                    //                                                                           columnTargeted.FieldName, columnTargeted,
                    //                                                                           "") { ShowInGroupColumnFooter = null });
                    //mnuSummary.Items.Add(mnuAverage);

                    var mnuMin = new DXMenuCheckItem("All is true ?");
                    mnuMin.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Min,
                                                                                           columnTargeted.FieldName, columnTargeted,
                                                                                           "")
                    { ShowInGroupColumnFooter = null });
                    mnuSummary.Items.Add(mnuMin);

                    var mnuMax = new DXMenuCheckItem("At least one true ?");
                    mnuMax.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Max,
                                                                                           columnTargeted.FieldName, columnTargeted,
                                                                                           "")
                    { ShowInGroupColumnFooter = null });
                    mnuSummary.Items.Add(mnuMax);
                }
                else if (columnTargeted.ColumnType == typeof(Model.CurrencyValue) ||
                         columnTargeted.ColumnType == typeof(Model.CurrencyValueList))
                {
                    var isList = columnTargeted.ColumnType == typeof(Model.CurrencyValueList);
                    var mnuSum = new DXMenuCheckItem("Sum");
                    mnuSum.Click += (_, __) => AddOrReplace((GridView)columnTargeted.View,
                                                            new GridGroupSummaryItem(SummaryItemType.Custom, columnTargeted.FieldName, columnTargeted, "")
                                                            {
                                                              ShowInGroupColumnFooter = null,
                                                              // Setting Tag allows us to delegate the job to CurrencyDisplayHelper class
                                                              Tag = isList ? CurrencyDisplayHelper.CurrencyValueListSumMenuKey : CurrencyDisplayHelper.CurrencyValueSumMenuKey
                                                            });

                    mnuSummary.Items.Add(mnuSum);

                    //var mnuAverage = new DXMenuCheckItem("Average");
                    //mnuAverage.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Average,
                    //                                                                           columnTargeted.FieldName, columnTargeted,
                    //                                                                            "{0:#,###.00;-#,###.00;0}")
                    //{ ShowInGroupColumnFooter = null });
                    //mnuSummary.Items.Add(mnuAverage);

                    //var mnuMin = new DXMenuCheckItem("Min");
                    //mnuMin.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Min,
                    //                                                                      columnTargeted.FieldName, columnTargeted,
                    //                                                                       "{0:#,###.00;-#,###.00;0}")
                    //{ ShowInGroupColumnFooter = null });
                    //mnuSummary.Items.Add(mnuMin);

                    //var mnuMax = new DXMenuCheckItem("Max");
                    //mnuMax.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Max,
                    //                                                                       columnTargeted.FieldName, columnTargeted,
                    //                                                                      "{0:#,###.00}")
                    //{ ShowInGroupColumnFooter = null });
                    //mnuSummary.Items.Add(mnuMax);
                }

                var mnuFirstGroupValue = new DXMenuCheckItem("First Group Value");
                mnuFirstGroupValue.Click += (_, __) => AddOrReplace((GridView)columnTargeted.View,
                                                        new GridGroupSummaryItem(SummaryItemType.Custom, columnTargeted.FieldName, columnTargeted, "")
                                                        {
                                                            ShowInGroupColumnFooter = null,
                                                            Tag = FirstGroupValueNotEmptyMenuKey
                                                        });
                mnuSummary.Items.Add(mnuFirstGroupValue);

                var mnuLastGroupValue = new DXMenuCheckItem("Last Group Value");
                mnuLastGroupValue.Click += (_, __) => AddOrReplace((GridView)columnTargeted.View,
                                                        new GridGroupSummaryItem(SummaryItemType.Custom, columnTargeted.FieldName, columnTargeted, "")
                                                        {
                                                            ShowInGroupColumnFooter = null,
                                                            Tag = LastGroupValueNotEmptyMenuKey
                                                        });
                mnuSummary.Items.Add(mnuLastGroupValue);

                var mnuCount = new DXMenuCheckItem("Count");
                mnuCount.Click += (_, __) => AddOrReplace(view, new GridGroupSummaryItem(SummaryItemType.Count,
                                                                                         columnTargeted.FieldName, columnTargeted,
                                                                                         "{0:n0}")
                { ShowInGroupColumnFooter = null });
                mnuSummary.Items.Add(mnuCount);


                if (mnuSummary.Items.Count > 0)
                {
                    var alreadyPresentSummary = view.FindSummary(columnTargeted);
                    if (alreadyPresentSummary != null)
                    {
                        var mnuNone = new DXMenuCheckItem("None");
                        mnuNone.Click += (_, __) => view.GroupSummary.Remove(alreadyPresentSummary);
                        mnuSummary.Items.Add(mnuNone);
                    }
                    e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuSummary);
                }
                // Ca marche mais je vois pas bien l'interêt etant donne qu'on affiche dans les group row !
                //var mnuShowGroupFooter = new DXMenuCheckItem("Show Group Footer", view.GroupFooterShowMode == GroupFooterShowMode.VisibleIfExpanded); //view.GroupSummary.Cast<GridGroupSummaryItem>().Any(item => item.ShowInGroupColumnFooter != null)
                //mnuShowGroupFooter.Click += (_, __) =>
                //{
                //    view.GroupFooterShowMode = view.GroupFooterShowMode == GroupFooterShowMode.VisibleIfExpanded ? GroupFooterShowMode.Hidden : GroupFooterShowMode.VisibleIfExpanded;
                //};
                //e.Menu.AddToGroup(GridViewMenuHelper.eHeaderType.Standard, mnuShowGroupFooter);
            }

            var mnu = new DXMenuCheckItem("Show Footer", view.OptionsView.ShowFooter);
            mnu.Click += (_, __) =>
            {
                view.OptionsView.ShowFooter = !view.OptionsView.ShowFooter;
            };
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnu);
        }
        public static readonly string StringShortestNotEmptyMenuKey = "StringShortestNotEmpty";
        public static readonly string StringLongestMenuKey = "StringLongest";
        public static readonly string FirstGroupValueNotEmptyMenuKey = "FirstGroupValueNotEmpty";
        public static readonly string LastGroupValueNotEmptyMenuKey = "LastGroupValueNotEmpty";

        static void AddOrReplace(GridView view, GridGroupSummaryItem grpSumItem)
        {
            var alreadyPresentSummary = view.FindSummary(view.Columns[grpSumItem.FieldName]);
            view.GroupSummary.Remove(alreadyPresentSummary);
            view.GroupSummary.Add(grpSumItem);
        }

        public void ShowSumInGroupRow(GridColumn columnTargeted, string format = null)
        {
            AddOrReplace((GridView)columnTargeted.View, 
                         new GridGroupSummaryItem(SummaryItemType.Sum, columnTargeted.FieldName, columnTargeted, format ?? "{0:#,###.00;-#,###.00;0}")
                         { ShowInGroupColumnFooter = null });
        }

        private void gv_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e)
        {
            var item = e.Item as GridSummaryItem; // classe de base pour GridColumnSummaryItem (afficher dnas le footer) et GridGroupSummaryItem affiché dans les group row
            GridView view = sender as GridView;
            if (Equals(StringShortestNotEmptyMenuKey, item.Tag))
            {
                if (e.SummaryProcess == CustomSummaryProcess.Start)
                    e.TotalValue = null;
                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                    if (!string.IsNullOrWhiteSpace(e.FieldValue as string) && 
                        (e.TotalValue == null || (e.FieldValue as string).Length < (e.TotalValue as string).Length))
                        e.TotalValue =  e.FieldValue as string;
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    // Nothing
                }
            }
            else if (Equals(StringLongestMenuKey, item.Tag))
            {
                if (e.SummaryProcess == CustomSummaryProcess.Start)
                    e.TotalValue = null;
                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                    if (e.FieldValue != null && (e.TotalValue == null || (e.FieldValue as string).Length > (e.TotalValue as string).Length))
                        e.TotalValue = e.FieldValue as string;
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    // Nothing
                }
            }
            else if (Equals(FirstGroupValueNotEmptyMenuKey, item.Tag))
            {
                if (e.SummaryProcess == CustomSummaryProcess.Start)
                    e.TotalValue = null;
                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                    if (e.TotalValue == null)
                        e.TotalValue = !(e.FieldValue is string) || string.IsNullOrWhiteSpace(e.FieldValue as string)
                                     ? e.FieldValue
                                     : null;
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    // Nothing
                }
            }
            else if (Equals(LastGroupValueNotEmptyMenuKey, item.Tag))
            {
                if (e.SummaryProcess == CustomSummaryProcess.Start)
                    e.TotalValue = null;
                if (e.SummaryProcess == CustomSummaryProcess.Calculate)
                    e.TotalValue = !(e.FieldValue is string) || !string.IsNullOrWhiteSpace(e.FieldValue as string)
                                    ? e.FieldValue
                                    : e.TotalValue;
                if (e.SummaryProcess == CustomSummaryProcess.Finalize)
                {
                    // Nothing
                }
            }
        }
    }
}
