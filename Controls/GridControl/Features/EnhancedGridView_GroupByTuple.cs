﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.Data;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;
using TechnicalTools.UI.DX.Helpers;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_GroupByTuple
    {
        readonly GridView _view;

        public EnhancedGridView_GroupByTuple(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing        -= View_PopupMenuShowing;        view.PopupMenuShowing        += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing        -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_GroupByTuple f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
        }



        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            if (GridViewHelper.DxVersion.Major > 16 || 
                GridViewHelper.DxVersion.Major == 16 && GridViewHelper.DxVersion.Minor >= 2)
                return; // Now done natively by DevExpress by pressing Ctrl when grouping columns
            if (e.HitInfo.InColumn)
            {
                var column = e.HitInfo.Column;
                var mnuGroupBy = e.Menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == GroupByAdvancedCaption);
                bool addMnuGroupBy = mnuGroupBy == null;
                mnuGroupBy  = mnuGroupBy ?? new DXSubMenuItem(GroupByAdvancedCaption);


                var mnuGroupByTupleFirst = new DXMenuItem("Group by using this column and ...", (_, __) => 
                {
                    _columnsForGroup.Clear();
                    _columnsForGroup.Add(column);
                });
                mnuGroupBy.Items.Add(mnuGroupByTupleFirst);

                // Check it is always the same view 
                // Use Case : on a subdetail view we want to group by two columns.
                // We choose the first column, then we collapse and expand the sub detail view
                // "view" would be different than _columnsForGroup[0] because view is recreated by devexpress
                bool canContinueGroupByBuilding = _columnsForGroup.Count > 0 && _columnsForGroup.All(col => col.View == view);

                var mnuGroupByTupleSecondContinue = new DXMenuItem("... and this column and ...", (_, __) => _columnsForGroup.Add(column))
                {
                    Enabled = canContinueGroupByBuilding
                };
                mnuGroupBy.Items.Add(mnuGroupByTupleSecondContinue);


                var mnuGroupByTupleSecondDone = new DXMenuItem("... and finaly this column", (_, __) => { _columnsForGroup.Add(column); DoGroupBy(view); })
                {
                    Enabled = canContinueGroupByBuilding
                };
                mnuGroupBy.Items.Add(mnuGroupByTupleSecondDone);

                var mnuGroupByClear = new DXMenuItem("Clear group by", (_, __) => ClearGroupBy());
                mnuGroupBy.Items.Add(mnuGroupByClear);
                mnuGroupByClear.Enabled = _groupByColumn != null;

                if (addMnuGroupBy)
                    e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuGroupBy);
            }
        }
        readonly List<GridColumn> _columnsForGroup = new List<GridColumn>();
        GridColumn _groupByColumn;
        public static readonly string GroupByAdvancedCaption = "Group by... (advanced)";

        private void ClearGroupBy()
        {
            _groupByColumn.View.Columns.Remove(_groupByColumn);
            _groupByColumn.Dispose();
            _groupByColumn = null;
        }

        private void DoGroupBy(GridView view)
        {
            Debug.Assert(_columnsForGroup.Count >= 2);
            if (_groupByColumn != null)
                ClearGroupBy();

            view.BeginUpdate();
            var groupFieldName = _columnsForGroup.Select(col => col.FieldName + "|").Join();
            _groupByColumn = view.Columns.AddVisible(groupFieldName, "<" + _columnsForGroup.Select(col => col.GetTextCaption().IfBlankUse(col.FieldName?.Uncamelify())).Join(" | ") + "> ");
            _groupByColumn.UnboundType = UnboundColumnType.String;
            _groupByColumn.UnboundExpression = "concat(" + _columnsForGroup.Select(col => "[" + col.FieldName + "]").Join(", ', ' , ") + ")";
            _groupByColumn.GroupIndex = view.GroupCount;
            view.EndUpdate();
        }
    }
}
