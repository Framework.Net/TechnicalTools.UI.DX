﻿using System;
using System.Diagnostics;
using System.Linq;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_CollapseAllGroupRow
    {
        readonly GridView _view;

        public EnhancedGridView_CollapseAllGroupRow(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_CollapseAllGroupRow f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }


        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.HitInfo.InGroupRow)
            {
                var view = sender as GridView;

                var mnuGroup = e.Menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == GroupRowMenuCaption);
                if (mnuGroup == null)
                {
                    mnuGroup = new DXSubMenuItem(GroupRowMenuCaption);
                    e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuGroup);
                }

                var mnuExpandAll = new DXMenuItem("Expand all group rows", (_, __) => view.ExpandAllGroups());
                var mnuCollaseAll = new DXMenuItem("Collapse all group rows", (_, __) => view.CollapseAllGroups());

                mnuGroup.Items.Add(mnuExpandAll);
                mnuGroup.Items.Add(mnuCollaseAll);
            }
        }

        public const string GroupRowMenuCaption = "Group...";
    }
}
