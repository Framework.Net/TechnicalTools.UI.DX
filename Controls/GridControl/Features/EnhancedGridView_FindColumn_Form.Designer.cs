﻿namespace TechnicalTools.UI.DX
{
    partial class EnhancedGridView_FindColumn_Form
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtColumnCaptionToFind = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnFind = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnFind_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnClose_LayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumnCaptionToFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFind_LayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose_LayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // txtColumnCaptionToFind
            // 
            this.txtColumnCaptionToFind.Location = new System.Drawing.Point(12, 28);
            this.txtColumnCaptionToFind.Name = "txtColumnCaptionToFind";
            this.txtColumnCaptionToFind.Size = new System.Drawing.Size(394, 20);
            this.txtColumnCaptionToFind.StyleController = this.layoutControl1;
            this.txtColumnCaptionToFind.TabIndex = 0;
            this.txtColumnCaptionToFind.EditValueChanged += new System.EventHandler(this.txtColumnCaptionToFind_EditValueChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.btnFind);
            this.layoutControl1.Controls.Add(this.txtColumnCaptionToFind);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(639, 223, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(418, 109);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(12, 75);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(310, 75);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(96, 22);
            this.btnFind.StyleController = this.layoutControl1;
            this.btnFind.TabIndex = 1;
            this.btnFind.Text = "Find !";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.btnFind_LayoutItem,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.btnClose_LayoutItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(418, 109);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtColumnCaptionToFind;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(398, 40);
            this.layoutControlItem1.Text = "Type the caption of column to find";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(164, 13);
            // 
            // btnFind_LayoutItem
            // 
            this.btnFind_LayoutItem.Control = this.btnFind;
            this.btnFind_LayoutItem.Location = new System.Drawing.Point(298, 63);
            this.btnFind_LayoutItem.MaxSize = new System.Drawing.Size(100, 26);
            this.btnFind_LayoutItem.MinSize = new System.Drawing.Size(100, 26);
            this.btnFind_LayoutItem.Name = "btnFind_LayoutItem";
            this.btnFind_LayoutItem.Size = new System.Drawing.Size(100, 26);
            this.btnFind_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnFind_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnFind_LayoutItem.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(100, 63);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(198, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 40);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(398, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnClose_LayoutItem
            // 
            this.btnClose_LayoutItem.Control = this.btnCancel;
            this.btnClose_LayoutItem.Location = new System.Drawing.Point(0, 63);
            this.btnClose_LayoutItem.MaxSize = new System.Drawing.Size(100, 26);
            this.btnClose_LayoutItem.MinSize = new System.Drawing.Size(100, 26);
            this.btnClose_LayoutItem.Name = "btnClose_LayoutItem";
            this.btnClose_LayoutItem.Size = new System.Drawing.Size(100, 26);
            this.btnClose_LayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.btnClose_LayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.btnClose_LayoutItem.TextVisible = false;
            // 
            // EnhancedGridView_FindColumn_Form
            // 
            this.Controls.Add(this.layoutControl1);
            this.Name = "EnhancedGridView_FindColumn_Form";
            this.Size = new System.Drawing.Size(418, 109);
            ((System.ComponentModel.ISupportInitialize)(this.txtColumnCaptionToFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnFind_LayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose_LayoutItem)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private DevExpress.XtraEditors.TextEdit txtColumnCaptionToFind;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnFind;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem btnFind_LayoutItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem btnClose_LayoutItem;
    }
}
