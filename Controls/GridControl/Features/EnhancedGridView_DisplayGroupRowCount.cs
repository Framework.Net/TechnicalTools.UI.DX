﻿using System;
using System.Diagnostics;
using System.Linq;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_DisplayGroupRowCount<TView>
        where TView : GridView, IEnhancedGridView
    {
        readonly TView _view;
        bool _display_is_enabled;

        public EnhancedGridView_DisplayGroupRowCount(TView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(TView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(TView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_DisplayGroupRowCount<TView> f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            _display_is_enabled = f._display_is_enabled;
        }
        



        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if (e.HitInfo.InGroupRow)
            {
                var view = sender as GridView;

                var mnuGroup = e.Menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == EnhancedGridView_CollapseAllGroupRow.GroupRowMenuCaption);
                if (mnuGroup == null)
                {
                    mnuGroup = new DXSubMenuItem(EnhancedGridView_CollapseAllGroupRow.GroupRowMenuCaption);
                    e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuGroup);
                }

                DXMenuItem mnuToggleDisplay;
                if (_display_is_enabled)
                    mnuToggleDisplay = new DXMenuItem("Hide group row count", (_, __) => SetDisplay(false));
                else
                    mnuToggleDisplay = new DXMenuItem("Display group row count", (_, __) => SetDisplay(true)); 

                mnuGroup.Items.Add(mnuToggleDisplay);
            }
        }

        public void SetDisplay(bool enabled)
        {
            _display_is_enabled = enabled;
            // We subscribe again each time to make sure we are the last on the event handler list
            // I know this is not a guaranted behavior but it works like this since more than 10 years so it wont change any time soon.
            if (_display_is_enabled)
                _view.BeforeCustomDrawGroupRow += View_BeforeCustomDrawGroupRow;
            else 
                _view.BeforeCustomDrawGroupRow -= View_BeforeCustomDrawGroupRow;

            int groupRowHandle = -1;
            while (_view.IsValidRowHandle(groupRowHandle))
            {
                _view.RefreshRow(groupRowHandle);
                --groupRowHandle;
            }
        }
        

        private void View_BeforeCustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
            Debug.Assert(!e.Handled);
            //e.DefaultDraw();
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            // get the text : string.IsNullOrEmpty(info.Column.Caption) ? info.Column.ToString() : info.Column.Caption
            // get the value : info.GroupValueText
            // We respect the format set by developper in designer and we append the count 

            // Take a rowhandle and return the number of item in grid and the number of groups
            Tuple<long, long> getCount(int rowHandle)
            {
                long cnt = 0;
                long grpCnt = 0;
                int childsCount = _view.GetChildRowCount(rowHandle);
                for (int i = 0; i < childsCount; ++i)
                {
                    var h = _view.GetChildRowHandle(rowHandle, i);
                    if (h >= 0)
                        ++cnt;
                    else
                    {
                        ++grpCnt;
                        var tuple = getCount(h); // another group (recursive call) or not (real row) 
                        cnt += tuple.Item1;
                    }
                }
                return Tuple.Create(cnt, grpCnt);
            }
            
            var count = getCount(e.RowHandle);  

            string toAppend = string.Format(" ({0}{1})", count.Item1, count.Item2 == 0 ? "" : " in " + count.Item2 + " group" + (count.Item2 == 1 ? "" : "s"));

            if (!info.GroupText.EndsWith(toAppend)) // Devexpress ne reset pas toujours GroupText
                info.GroupText += toAppend;
        }


    }
}
