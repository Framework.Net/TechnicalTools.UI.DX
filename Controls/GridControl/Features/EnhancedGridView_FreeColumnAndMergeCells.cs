﻿using System;
using System.Diagnostics;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_FreeColumn
    {
        readonly GridView _view;
        public bool AllowChangeMergeCell { get; set; }

        public EnhancedGridView_FreeColumn(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            AllowChangeMergeCell = true;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_FreeColumn f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            AllowChangeMergeCell = f.AllowChangeMergeCell;
        }

       
        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;

            DXMenuCheckItem mnu;
            if (e.HitInfo.InColumnPanel)
            {
                mnu = new DXMenuCheckItem("Keep all columns in visible grid area", view.OptionsView.ColumnAutoWidth);
                mnu.Click += (_, __) =>
                {
                    view.OptionsView.ColumnAutoWidth = !view.OptionsView.ColumnAutoWidth;
                    if (view.OptionsView.ColumnAutoWidth)
                        return;
                    var v = view.BestFitMaxRowCount;
                    view.BestFitMaxRowCount = 10;
                    try
                    {
                        view.BestFitColumns();
                    }
                    finally
                    {
                        view.BestFitMaxRowCount = v;
                    }
                };
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnu);
            }

            if (AllowChangeMergeCell)
            {
                mnu = new DXMenuCheckItem("Merge cells", view.OptionsView.AllowCellMerge);
                mnu.Click += (_, __) =>
                {
                    view.OptionsView.AllowCellMerge = !view.OptionsView.AllowCellMerge;
                };
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnu);
            }

        }
    }
}
