﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;

using DevExpress.Data;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.UI.DX.Controls;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_ShowChart
    {
        readonly GridView _view;

        public EnhancedGridView_ShowChart(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing        -= View_PopupMenuShowing;        view.PopupMenuShowing        += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing        -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_ShowChart f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
        }


        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            var colY = e.HitInfo.Column;
            if (e.HitInfo.InColumn && view.RowCount > 0)
            {
                Type typeY = colY.ColumnType.IsNumericType() ? typeof(decimal)
                           : colY.ColumnType.RemoveNullability() == typeof(TimeSpan) ? typeof(TimeSpan)
                           : colY.ColumnType.RemoveNullability() == typeof(DateTime) ? typeof(DateTime)
                           : null;
                if (typeY != null)
                {
                    var mnuMain = new DXSubMenuItem("Show these values on chart...");
                    foreach (GridColumn colX in view.Columns.Except(colY))
                    {
                        Type typeX = colX.ColumnType.IsNumericType() ? typeof(decimal)
                                   : colX.ColumnType.RemoveNullability() == typeof(TimeSpan) ? typeof(TimeSpan)
                                   : colX.ColumnType.RemoveNullability() == typeof(DateTime) ? typeof(DateTime)
                                   : null;
                        if (typeX != null)
                        {
                            mnuMain.Items.Add(new DXMenuItem("From values on " + colX.GetCaption(), (_, __) =>
                            {
                                _view.GridControl.ShowBusyWhileDoingUIWork(pr =>
                                {
                                    MethodInfo method = GetType().GetMethod(nameof(CreateAndConfigureDiagram), BindingFlags.Instance | BindingFlags.NonPublic);
                                    MethodInfo generic = method.MakeGenericMethod(typeX, typeY);
                                    var ctl = (ChartCurves)generic.InvokeAndCleanException(this, new object[] { colX, colY });
                                    var owner = view.GridControl.FindForm() ?? EnhancedXtraForm.MainForm;
                                    var frm = EnhancedXtraForm.CreateWithInner(ctl, owner);
                                    frm.Text = colY.GetCaption() + " by " + colX.GetCaption();
                                    frm.Size = new Size(owner.Size.Width * 80 / 100, owner.Size.Height * 80 / 100);
                                    frm.Show(owner);
                                });
                            }));
                        }
                    }
                    if (mnuMain.Items.Count > 0)
                        e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuMain);
                }
            }

            GridColumn columnTargeted;
            GridSummaryItem summaryTargeted;
            if (e.HitInfo.InGroupRow &&
                e.HitInfo.RowHandle < 0 && 
                view.IsValidRowHandle(e.HitInfo.RowHandle) &&
                null != (columnTargeted = view.FindColumnUnderMouse(e.HitInfo)) &&
                null != (summaryTargeted = view.GetGroupSummaryValues(e.HitInfo.RowHandle)
                                              ?.Cast<DictionaryEntry>()
                                               .Select(sv => (GridSummaryItem)sv.Key)
                                               .FirstOrDefault(gsi => gsi.FieldName == columnTargeted.FieldName)))
            {
                Debug.Assert(summaryTargeted.SummaryType != SummaryItemType.None);
                string title = summaryTargeted.SummaryType + (summaryTargeted.SummaryType == SummaryItemType.Count ? "" : " of " + columnTargeted.GetCaption())
                             + " by " + view.GroupedColumns.Select(col => col.GetCaption()).Join(" / ");
                e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, new DXMenuItem("Show " + title + " in a sunburst chart", (_, __) =>
                {
                    var series = view.GroupedColumns.Select(grp => Tuple.Create(new List<string>(), new List<decimal>())).ToList();

                    IEnumerable<int> getChildren(int groupRowId)
                    {
                        return Enumerable.Range(-groupRowId + 1, int.MaxValue + groupRowId - 1).Select(grh => -grh)
                                         .TakeWhile(grh => view.IsValidRowHandle(grh)
                                                        && view.GetRowLevel(grh) > (groupRowId == 0 ? -1 : view.GetRowLevel(groupRowId)))
                                         .Where(grh => view.GetRowLevel(grh) == (groupRowId == 0 ? -1 : view.GetRowLevel(groupRowId)) + 1);
                    }
                    string getLabel(int groupRowId)
                    {
                        return view.GetGroupRowValue(groupRowId)?.ToString();
                    }
                    decimal getValue(int groupRowId)
                    {
                        return Convert.ToDecimal(view.GetGroupSummaryValues(groupRowId)[summaryTargeted]);
                    }

                    var ctl = new ChartSunBurst();
                    ctl.DisplayHierarchy("", getChildren(0).ToList(), getLabel, getValue, getChildren);
                    var owner = view.GridControl.FindForm() ?? EnhancedXtraForm.MainForm;
                    var frm = EnhancedXtraForm.CreateWithInner(ctl, owner);
                    frm.Text = title;
                    frm.Show(owner);
                }));
            }
        }

        ChartCurves CreateAndConfigureDiagram<TX, TY>(GridColumn colX, GridColumn colY)
            where TX : struct
            where TY : struct
        {
            var view = colX.View;
            var ctl = new ChartCurves();
            var getX = GetDecimalGetterOnColumn<TX>(colX);
            var getY = GetDecimalGetterOnColumn<TY>(colY);
            var serie = new Dictionary<TX, TY>();
            // Obtain the number of data rows.
            int dataRowCount = view.DataRowCount;
            // Traverse data rows and change the Price field values.  
            int count = 0;
            for (int i = 0; i < dataRowCount; i++)
            {
                var row = view.GetRow(i);
                TX? x = getX(row, i);
                if (x == null)
                    continue;
                TY? y = getY(row, i);
                if (y == null)
                    continue;
                serie[x.Value] = y.Value;
                if (count == serie.Count)
                {
                    var msg = $"Cannot draw curve, value {x} on X Axis (column {colX.GetTextCaption()}) is present more than once!" + Environment.NewLine +
                              $"You can workaround this issue this way:" + Environment.NewLine +
                              $"1) Group data row by column {colX.GetTextCaption()} (ie: just drag and drop column {colX.GetTextCaption()} at the top left part of the gridview)" + Environment.NewLine +
                              $"2) Apply a summary of type mean/average (ie: Right click at any interception of a group row and column {colY.GetTextCaption()}" + Environment.NewLine +
                              $"3) Extract grouped values by righ clickin on any row (grouped or not) and chosing menu Export => Extract the most inner summary value" + Environment.NewLine +
                              $"4) A windows open, reusing all the groups you did except the most inner one. Ungroup all the columns by doing the reverse of step 1" + Environment.NewLine +
                              $"5) Try again to show values on a chart";
                    throw new Model.UserUnderstandableException(msg, null);
                }
                ++count;
            }
            ctl.AddSeries("", new Dictionary<string, Dictionary<TX, TY>>() { { colX.GetCaption(), serie } });
            return ctl;
        }
        static Func<object, int, T?> GetDecimalGetterOnColumn<T>(GridColumn col)
            where T : struct
        {
            var view = col.View;
            Func<object, int, object> getCellValue;
            if (col is IUnboundColumnOptimizationRequirement uCol && uCol.UnboundValueGetter != null)
                getCellValue = (row, listSourceRowIndex) => uCol.UnboundValueGetter(row, listSourceRowIndex);
            else
                getCellValue = (row, listSourceRowIndex) => view.GetListSourceRowCellValue(listSourceRowIndex, col);
            if (typeof(T) == typeof(decimal))
                return (row, listSourceRowIndex) =>
                {
                    var v = getCellValue(row, listSourceRowIndex);
                    if (v == null)
                        return null;
                    return (T)(object)Convert.ToDecimal(v);
                };
            else
                return (row, listSourceRowIndex) =>
                {
                    var v = getCellValue(row, listSourceRowIndex);
                    if (v == null)
                        return null;
                    return (T)v;
                };
        }
    }
}
