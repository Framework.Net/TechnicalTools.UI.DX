﻿using System;

using TechnicalTools.Model;

using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_CustomErrorMessages
    {
        GridView _view;

        public EnhancedGridView_CustomErrorMessages(GridView gv)
        {
            Install(gv);
        }

        public void Install(GridView gv)
        {
            _view = gv;
            gv.InvalidValueException += gv_InvalidValueException;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.InvalidValueException += gv_InvalidValueException;
        }

        internal void Assign(EnhancedGridView_CustomErrorMessages f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        void gv_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            var gv = sender as GridView;
            var exception = e.Exception;
            if (exception is DevExpress.XtraEditors.Controls.EditorValueException)
                exception = (exception as DevExpress.XtraEditors.Controls.EditorValueException).SourceException;
            if (exception is TechnicalDbConstraintMinimalValue)
            {
                var ex = exception as TechnicalDbConstraintMinimalValue;
                e.ErrorText = $"Value {ex.CurrentValue} is too small (minimal value allowed is {ex.MinimalValue})";
            }
            else if (exception is TechnicalDbConstraintMaximalValue)
            {
                var ex = exception as TechnicalDbConstraintMaximalValue;
                e.ErrorText = $"Value {ex.CurrentValue} is too big (maximal value allowed is {ex.MaximalValue})";
            }
            else if (exception is TechnicalDbConstraintMinimumLength)
            {
                var ex = exception as TechnicalDbConstraintMinimumLength;
                e.ErrorText = $"This value must be at least {ex.MinimumLength} characters long! (current length is {ex.CurrentLength})";
            }
            else if (exception is TechnicalDbConstraintMaximumLength)
            {
                var ex = exception as TechnicalDbConstraintMaximumLength;
                e.ErrorText = $"This value is limited to a maximum of {ex.MaximalLength} characters! (current length is {ex.CurrentLength})";
            }
            else if (exception is TechnicalDbConstraintNotNullable)
            {
                //var ex = exception as TechnicalDbConstraintNotNullable;
                e.ErrorText = "Value cannot be empty !";
            }
            else if (exception is TechnicalDbConstraintDateTimeHaveToMuchPrecision)
            {
                var ex = exception as TechnicalDbConstraintDateTimeHaveToMuchPrecision;
                e.ErrorText = ex.Message; // do better ?
            }
        }
    }
}
