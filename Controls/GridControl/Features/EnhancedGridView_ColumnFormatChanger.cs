﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using DevExpress.Utils.Menu;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;


namespace TechnicalTools.UI.DX
{
    // TODO : How to add Tooltips for a GridColumns SummaryItem : https://www.devexpress.com/Support/Center/Example/Details/E1979
    public class EnhancedGridView_ColumnFormatChanger
    {
        readonly GridView _view;

        public EnhancedGridView_ColumnFormatChanger(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing; view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_ColumnFormatChanger f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        public GridViewMenu BuildMenu(GridColumn col, Type colType = null, GridView view = null)
        {
            view = _view;
            var menu = new GridViewMenu(view);
            FillMenu(menu, col, colType, view);
            return menu;
        }
        void FillMenu(GridViewMenu menu, GridColumn col, Type colType, GridView view)
        {
            Debug.Assert(col != null);

            var info = _infos.GetValue(col, c => new ColumnCustomDisplayInfo(c));

            var mnus = new List<DXSubMenuItem>();
            if ((colType ?? col.ColumnType?.RemoveNullability()) == typeof(DateTime))
            {
                void mnuClick(object mnuAsSender, EventArgs __)
                {
                    var mnu = mnuAsSender as DXMenuItem;
                    var tuple = (Tuple<DXSubMenuItem, string>)mnu.Tag;
                    Debug.Assert(info.CanCustomizeEditor, "Menu should have been disabled if we can't customize the column editor!" + Environment.NewLine +
                                                          "Ne sait pas comment changer uniquement l'affichage sans changer le mask d'edition et le Column Editor ce qui est un effet de bord important");
                    var mnuParent = tuple.Item1;

                    // Se contenter de changer seulement col.DisplayFormat, ne fonctionne pas
                    // Si ce code est modifier, changer le code aussi dans GridView_GroupDrawerHelper.DrawSummaryValues
                    var ce = new RepositoryItemDateEdit();
                    ce.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                    ce.Mask.EditMask = new string[] { (string)mnuParent.Tag, tuple.Item2 }.NotBlank().Join(" ");
                    ce.Mask.UseMaskAsDisplayFormat = true;
                    col.ColumnEdit = info.Editor = ce;
                }


                mnus.Add(new DXSubMenuItem("to      dd/mm/yyyy")          { Tag = "dd/MM/yyyy",      Enabled = info.CanCustomizeEditor });
                mnus.Add(new DXSubMenuItem("to  day dd/mm/yyyy")          { Tag = "dddd dd/MM/yyyy", Enabled = info.CanCustomizeEditor });
                mnus.Add(new DXSubMenuItem("to      yyyy/mm/dd")          { Tag = "yyyy/MM/dd",      Enabled = info.CanCustomizeEditor });
                mnus.Add(new DXSubMenuItem("to      yyyy/mm/dd day")      { Tag = "yyyy/MM/dd dddd", Enabled = info.CanCustomizeEditor });
                mnus.Add(new DXSubMenuItem("to      yyyy/mm")             { Tag = "yyyy/MM",         Enabled = info.CanCustomizeEditor });
                mnus.Add(new DXSubMenuItem("to  date part not displayed") {                          Enabled = info.CanCustomizeEditor });
                foreach (var mnuParent in mnus)
                {
                    mnuParent.Items.Add(new DXMenuItem("and  hh:mm:ss",     mnuClick) { Tag = Tuple.Create(mnuParent, "HH:mm:ss"),     Enabled = info.CanCustomizeEditor });
                    mnuParent.Items.Add(new DXMenuItem("and  hh:mm",        mnuClick) { Tag = Tuple.Create(mnuParent, "HH:mm"),        Enabled = info.CanCustomizeEditor });
                    mnuParent.Items.Add(new DXMenuItem("and  hh:mm:ss.fff", mnuClick) { Tag = Tuple.Create(mnuParent, "HH:mm:ss.fff"), Enabled = info.CanCustomizeEditor });
                    if (mnuParent.Tag != null)
                        mnuParent.Items.Add(new DXMenuItem("and  time part not displayed", mnuClick) { Tag = Tuple.Create(mnuParent, (string)null), Enabled = info.CanCustomizeEditor });
                }
            }

            if (mnus.Count > 0)
            {
                var mnuChangeFormat = new DXSubMenuItem("Change display format...");
                foreach (var mnu in mnus)
                    mnuChangeFormat.Items.Add(mnu);
                menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuChangeFormat);
            }
        }


        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            // Se produit quand on clique en bas de la zone du footer (1 pixel au dessus de la limite du bas) + une autre condition inconnu encore
            // ou bien que  GridView.OptionsMenu.EnableGroupRowMenu est a false (defaukt) a partir de DX 19.2
            if (e.Menu == null) 
                return;

            if (e.HitInfo.InColumn || e.HitInfo.InGroupColumn)
            {
                var view = sender as GridView;
                var col = e.HitInfo.Column;
                FillMenu(e.Menu, col, null, view);
            }
        }
        readonly ConditionalWeakTable<GridColumn, ColumnCustomDisplayInfo> _infos = new ConditionalWeakTable<GridColumn, ColumnCustomDisplayInfo>();
        class ColumnCustomDisplayInfo
        {
            public readonly GridColumn For;
            public string              OriginalFormat { get; set; }
            // Specific Repository if not repository was already set by a developper
            public RepositoryItem      Editor         { get; set; }

            public ColumnCustomDisplayInfo(GridColumn col)
            {
                For = col;
            }
            // Check that a any developper did set its own custom editor
            public bool CanCustomizeEditor
            {
                get { return For.ColumnEdit == null || For.ColumnEdit == Editor; }
            }
        }

        public void DisplayDateAndTimeFor(GridColumn col)
        {
            var menu = BuildMenu(col, typeof(DateTime), (GridView)col.View);
            var mnuFormat = menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == "Change display format...")
                               ?.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == "to      dd/mm/yyyy")
                               ?.Items.OfType<DXMenuItem>().FirstOrDefault(mnu => mnu.Caption == "and  hh:mm:ss.fff");
            if (mnuFormat != null && mnuFormat.Enabled)
                mnuFormat.GenerateClickEvent();
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        }

        public void DisplayDateOnlyFor(GridColumn col)
        {
            var menu = BuildMenu(col, typeof(DateTime), (GridView)col.View);
            var mnuFormat = menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == "Change display format...")
                               ?.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == "to      dd/mm/yyyy")
                               ?.Items.OfType<DXMenuItem>().FirstOrDefault(mnu => mnu.Caption == "and  time part not displayed");
            if (mnuFormat != null && mnuFormat.Enabled)
                mnuFormat.GenerateClickEvent();
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        }
        public void DisplayTimeOnlyFor(GridColumn col)
        {
            var menu = BuildMenu(col, typeof(DateTime), (GridView)col.View);
            var mnuFormat = menu.Items.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == "Change display format...")
                               ?.Items?.OfType<DXSubMenuItem>().FirstOrDefault(mnu => mnu.Caption == "to  date part not displayed")
                               ?.Items?.OfType<DXMenuItem>().FirstOrDefault(mnu => mnu.Caption == "and  hh:mm:ss");
            if (mnuFormat != null && mnuFormat.Enabled)
                mnuFormat.GenerateClickEvent();
            col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            col.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
        }
    }
}
