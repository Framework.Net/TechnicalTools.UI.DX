﻿using System;
using System.Data;
using System.Diagnostics;

using DevExpress.Utils.Serializing;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;


namespace TechnicalTools.UI.DX
{
    public class UnboundColumnOptimizer : GridView_Feature<UnboundColumnOptimizer>
    {
        public UnboundColumnOptimizer(GridView view) : base(view)
        {

        }

        protected override void Install(GridView view)
        {
            view.CustomUnboundColumnData += UnboundColumnOptimization_CustomUnboundColumnData;
        }

        protected override void Uninstall(GridView view)
        {
            view.CustomUnboundColumnData -= UnboundColumnOptimization_CustomUnboundColumnData;
        }

        protected internal override void Assign(UnboundColumnOptimizer f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        public void SetUnboundDataAccessorsFor(GridColumn col, Func<object, int, object> getter, Action<object, object> setter)
        {
            var unboundCol = col as IUnboundColumnOptimizationRequirement;
            Debug.Assert(unboundCol != null);
            unboundCol.UnboundValueGetter = getter;
            unboundCol.UnboundValueSetter = setter;
        }

        void UnboundColumnOptimization_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            var unboundCol = e.Column as IUnboundColumnOptimizationRequirement;
            if (e.IsGetData)
            {
                var getter = unboundCol?.UnboundValueGetter;
                if (getter != null)
                    e.Value = getter((e.Row as DataRowView)?.Row ?? e.Row, e.ListSourceRowIndex);
            }
            else
                unboundCol?.UnboundValueSetter?.Invoke((e.Row as DataRowView)?.Row ?? e.Row, e.Value);
        }
    }

    // Define what column should have 
    public interface IUnboundColumnOptimizationRequirement
    {
        // First object is a row object, second is listSourceRowIndex (index in the underlying datasource)
        Func<object, int, object> UnboundValueGetter { get; set; }
        // First object is a row object, second is the value to assign
        Action<object, object> UnboundValueSetter { get; set; }

        void Assign(IUnboundColumnOptimizationRequirement col, bool copyEvents);
    }

    #region Plumbing

    public partial interface IEnhancedGridColumn : IUnboundColumnOptimizationRequirement
    {
    }

    public partial class EnhancedGridColumn : IUnboundColumnOptimizationRequirement
    {
        [XtraSerializableProperty] // Make SaveLayoutToXml to ignore this property
        Func<object, int, object> IUnboundColumnOptimizationRequirement.UnboundValueGetter { get; set; }
        [XtraSerializableProperty] // Make SaveLayoutToXml to ignore this property
        Action<object, object> IUnboundColumnOptimizationRequirement.UnboundValueSetter { get; set; }

        void IUnboundColumnOptimizationRequirement.Assign(IUnboundColumnOptimizationRequirement col, bool copyEvents)
        {
            if (copyEvents)
            {
                (this as IUnboundColumnOptimizationRequirement).UnboundValueGetter = col.UnboundValueGetter;
                (this as IUnboundColumnOptimizationRequirement).UnboundValueSetter = col.UnboundValueSetter;
            }
        }
    }

    public partial class BandedEnhancedGridColumn : IUnboundColumnOptimizationRequirement
    {
        [XtraSerializableProperty] // Make SaveLayoutToXml to ignore this property
        Func<object, int, object> IUnboundColumnOptimizationRequirement.UnboundValueGetter { get; set; }
        [XtraSerializableProperty] // Make SaveLayoutToXml to ignore this property
        Action<object, object> IUnboundColumnOptimizationRequirement.UnboundValueSetter { get; set; }

        void IUnboundColumnOptimizationRequirement.Assign(IUnboundColumnOptimizationRequirement col, bool copyEvents)
        {
            if (copyEvents)
            {
                (this as IUnboundColumnOptimizationRequirement).UnboundValueGetter = col.UnboundValueGetter;
                (this as IUnboundColumnOptimizationRequirement).UnboundValueSetter = col.UnboundValueSetter;
            }
        }
    }

    public interface IEnhancedGridView_UnboundColumnOptimizer
    {
        UnboundColumnOptimizer UnboundColumnOptimizer { get; }
        void SetUnboundDataAccessorsFor(GridColumn col, Func<object, int, object> getter, Action<object, object> setter);
    }
    public partial interface IEnhancedGridView : IEnhancedGridView_UnboundColumnOptimizer
    {
    }
    
    public partial class EnhancedGridView : IEnhancedGridView_UnboundColumnOptimizer
    {
        public UnboundColumnOptimizer UnboundColumnOptimizer { get; private set; }
        void UnboundColumnOptimization() { UnboundColumnOptimizer = new UnboundColumnOptimizer(this); }

        public void SetUnboundDataAccessorsFor(GridColumn col, Func<object, int, object> getter, Action<object, object> setter)
        {
            UnboundColumnOptimizer.SetUnboundDataAccessorsFor(col, getter, setter);
        }
    }
    public partial class EnhancedBandedGridView : IEnhancedGridView_UnboundColumnOptimizer
    {
        public UnboundColumnOptimizer UnboundColumnOptimizer { get; private set; }
        void UnboundColumnOptimization() { UnboundColumnOptimizer = new UnboundColumnOptimizer(this); }
        public void SetUnboundDataAccessorsFor(GridColumn col, Func<object, int, object> getter, Action<object, object> setter)
        {
            UnboundColumnOptimizer.SetUnboundDataAccessorsFor(col, getter, setter);
        }
    }

    #endregion Plumbing
}
