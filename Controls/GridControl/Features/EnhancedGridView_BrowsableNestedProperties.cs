﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Xml.Linq;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Algorithm.Graph;
using TechnicalTools.Tools;
using TechnicalTools.Logs;
using TechnicalTools.Annotations;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.UI.DX.Helpers;

using DataMapper;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_BrowsableNestedProperties
    {
        readonly IEnhancedGridView _view;

        /// <summary>
        /// By default the complete menu is built in an asynchronous way. 
        /// A first, incomplete, menu is displayed immediately to user with a hourglass on submenu this class generate.
        /// This is important to make UI responsive from user's perspective.
        /// Later, while the first menu is already displayed, the full menu becomes available.
        /// So we want to replace the old one by the new one.
        /// However sometimes the replacement does not work. For example when the menu is on a gridview inside a PopupContainerForm.
        /// PopupContainerForm is designed to disappear when losing focu be closed if user do some action.
        /// By setting this property to true, the first menu is the definitive menu but user may have to wait some ms / s.
        /// </summary>
        public bool BuildMenuSynchronously { get; set; }

        /// <summary>
        /// Indicate the depth in object model to display to user
        /// 0 means the feature is disabled. Recommended values are 3, 4, 5
        /// With a value of 3 the user can still add a property at level 3 
        /// then right click again starting from this new column so he can reach level 6
        /// But the algorithm did not browse all 6 levels from the original column the user clicked on.
        /// </summary>
        public int MenuBuildingMaxRecursion { get; set; } = DefaultMenuBuildingMaxRecursion; // 0 disables the feature
        public static int DefaultMenuBuildingMaxRecursion { get; set; } = 5;

        /// <summary>
        /// Allow different point of view of columns cells values to be explored in different menu instead only one
        /// </summary>
        public bool AllowExploringColumnValueAbstractTypes { get; set; } = DefaultAllowExploringColumnValueAbstractTypes;
        public static bool DefaultAllowExploringColumnValueAbstractTypes { get; set; }

        // Allow you to filter the type which are allowed to be reachable and browsable by menu
        // By default it allow all "native" type (number, datetime, timespan, string, char, bool) 
        // then for the remaining type deny all other Microsoft type and DevExpress types. 
        // So only your type (DAL / Business etc) should be browsed.. but also any third party type too !
        public static Func<Type, bool?> ShowInstanceOfType { get; set; } = ShowInstanceOfTypeDefault;
        
        public EnhancedGridView_BrowsableNestedProperties(IEnhancedGridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }
        static readonly ILogger _log  = LogManager.Default.CreateLogger(typeof(EnhancedGridView_BrowsableNestedProperties));

        public void Install()
        {
            Install(_view);
        }
        void Install(IEnhancedGridView view)
        {
            var gv = (GridView)view;
            Debug.Assert(gv != null);
            gv.PopupMenuShowing -= View_PopupMenuShowing; gv.PopupMenuShowing += View_PopupMenuShowing;
            view.SaveLayoutData -= View_SaveLayoutData; view.SaveLayoutData += View_SaveLayoutData;
            view.RestoreLayoutData -= View_RestoreLayoutData; view.RestoreLayoutData += View_RestoreLayoutData;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(IEnhancedGridView view)
        {
            var gv = (GridView)view;
            gv.PopupMenuShowing -= View_PopupMenuShowing;
            view.SaveLayoutData -= View_SaveLayoutData; view.SaveLayoutData += View_SaveLayoutData;
            view.RestoreLayoutData -= View_RestoreLayoutData; view.RestoreLayoutData += View_RestoreLayoutData;
        }

        internal void Assign(EnhancedGridView_BrowsableNestedProperties f, bool copyEvents)
        {
            BuildMenuSynchronously = f.BuildMenuSynchronously;
            MenuBuildingMaxRecursion = f.MenuBuildingMaxRecursion;
            f.Uninstall(_view); // See caller comments to understand this not intuitive code
            Install(_view);
        }



        private void View_SaveLayoutData(object sender, SaveLayoutDataEventArgs e)
        {
            var gv = (GridView)_view;
            var displayedBci = gv.Columns
                                 .SelectNotNull(col => (col as IEnhancedGridColumn_IsRelatedPropertyColumn)?.RelatedPropertyInfo)
                                 .ToList();
            // Get all BrowsingColumnInfo instances
            var allBcis = new HashSet<BrowsingColumnInfo>();
            foreach (var bci in displayedBci)
            {
                var p = bci;
                do
                {
                    allBcis.Add(p);
                    p = p.Source;
                }
                while (p != null);
            }
            // Sort them in a topological order...
            var tri = TopologicalOrderSimple.DoTopologicalSort(allBcis, bci => (bci.Dependencies ?? Enumerable.Empty<BrowsingColumnInfo>()).Concat(bci.Source).NotNull(), true)
                                            .AsEnumerable()
                                            .Reverse()
                                            .ToList();
            var node = new XElement(GetType().Name);
            var refs = new Dictionary<BrowsingColumnInfo, int>();
            int id = 0;
            // ...So now each bci can reference only bcis previously handled in loop (so the reference has an id)
            foreach (var bci in tri)
            {
                var nCol = new XElement(bci.GetType().Name);
                if (bci.DisplayColumn != null)
                {
                    // Provide a reference to user's mind in case of problem on restoring a column
                    nCol.Add(new XAttribute("DisplayColumnCaption", bci.DisplayColumn.GetCaption()));
                }
                bci.SaveToXml(nCol, bciRef => { Debug.Assert(bciRef == bci.Source || (bci.Dependencies?.Contains(bciRef) ?? false)); return refs[bciRef]; });
                node.Add(nCol);
                refs.Add(bci, id++);
            }
            if (!node.IsEmpty)
                e.Data = node;
        }
        private void View_RestoreLayoutData(object sender, RestoreLayoutDataEventArgs e)
        {
            if (e.AfterDefaultRestore)
                return;
            var node = e.Data(GetType().Name);
            if (node == null)
                return;
            var gv = (GridView)_view;
            var refs = new List<BrowsingColumnInfo>();
            BrowsingColumnInfo idToRef(int id) => refs[id];
            foreach (var nCol in node.Elements())
            {
                var displayColumnCaption = nCol.Attribute("DisplayColumnCaption")?.Value;
                var colKind = nCol.Name.LocalName;
                BrowsingColumnInfo bci;
                try
                {
                    bci = colKind == nameof(RelatedPropertyInfo) ? new RelatedPropertyInfo()
                        : colKind == nameof(StringAnalysisColumnInfoPattern) ? new StringAnalysisColumnInfoPattern()
                        : colKind == nameof(StringAnalysisColumnInfoPatternId) ? new StringAnalysisColumnInfoPatternId()
                        : (BrowsingColumnInfo)null;
                    if (bci == null)
                        throw new TechnicalException($"Unknown column kind \"{colKind}\"!", null);
                    bci.RestoreFromToXml(nCol, gv, idToRef);
                    if (bci.DisplayColumn == null)
                    {
                        // Don't care where we add, real layout applyment will be done by DevExpress
                        GetOrCreateAndShowColumnOnNestedProperty(gv, bci);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(ex, LogTags.Ignored);
                    if (displayColumnCaption != null) // this was mapped to a displayed column, so we warn user
                        BusEvents.Instance.RaiseUserUnderstandableMessage($"Column \"{displayColumnCaption}\" has not been restored. This can happens after a code refactoring." + Environment.NewLine +
                                                                          $"Add it again using menu \"{MenuCaption}\" and save/overwrite layout." + Environment.NewLine +
                                                                          "Contact IT only if this bug persists.", _log);
                    bci = new BrokenBrowsingColumnInfo(nCol, ex);
                    try { bci.RestoreFromToXml(nCol, gv, idToRef); }
                    catch { /* we did best effort */}
                }
                refs.Add(bci);
            }
        }
        public static bool IsColumnNestedProperty(GridColumn col)
        {
            return (col as IEnhancedGridColumn_IsRelatedPropertyColumn)?.RelatedPropertyInfo != null;
        }

        public static bool? ShowInstanceOfTypeDefault(Type t)
        {
            if (IsNativeType(t))
                return true;
            if (t.RemoveNullability().IsMicrosoftType() && !t.IsEnum)
                return false;
            if (t.RemoveNullability().IsDevExpressType() && !t.IsEnum)
                return false;
            return null;
        }

        public static bool IsNativeType(Type t)
        {
            return t.ToDevExpressGridViewUnboundType() != UnboundColumnType.Object;
        }

        static string ToOriginalBoundFieldname(string fd)
        {
            return new string(fd.Replace(BrowsingColumnInfo.FieldSeparator, ".").Where(c => c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_' || c == '.').ToArray());
        }
        void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            if ((!e.HitInfo.InColumn && !e.HitInfo.InGroupColumn) || e.HitInfo.Column == null)
                return;

            var view = (GridView)sender;
            var enumerable = view.DataSource as IReadOnlyCollection<object>
                          ?? (view.DataSource as DataView)?.Cast<DataRowView>().ToList();

            if (enumerable == null || enumerable.Count == 0)
                return;
            var originColumn = e.HitInfo.Column;

            if (MenuBuildingMaxRecursion == 0)
                return;

            var mnuAddNestedProperty = new DXSubMenuItem(UnicodeChars.HOURGLASS + " " + MenuCaption);
            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuAddNestedProperty);

            var currentShowPoint = e.Point;
            // Look up fieldnames (normalized name, without punctation etc )
            // => boolean telling if fieldname is intended to be displayed with the normal binding
            var fieldNameDisplayHandled = view.Columns.ToLookup(col => ToOriginalBoundFieldname(col.FieldName),
                                                                  col => col.UnboundType == UnboundColumnType.Bound 
                                                                      // Not already displayed by current class
                                                                      || ToOriginalBoundFieldname(col.FieldName) != col.FieldName);
            
            // Make sure thread is not executing anymore (from previous execution of current method)
            // This should not take long because we canceled it in previous execution (see below)
            _thBuildMenu?.Join();
            // So now nobody is using _menuDoneBeforeFirstShow again ..

            if (_cancellationTokenSource != null)
                _cancellationTokenSource.Dispose();
            _cancellationTokenSource = new CancellationTokenSource();
            // First Execution ? 
            if (_menuDoneBeforeFirstShow == null)
            {
                _menuDoneBeforeFirstShow = new AutoResetEvent(false);
                
                view.Disposed += (_, __) =>
                {
                    _menuDoneBeforeFirstShow.Dispose();
                    _cancellationTokenSource.Dispose();
                };
            }
            _menuDoneBeforeFirstShow.Set();

            void action()
            {
                // Thread.Sleep(100);  (to test the problem : uncomment this line)
                var subMenus = GetMenuContent(_cancellationTokenSource.Token, enumerable, fieldNameDisplayHandled, originColumn); // this can be really low sometiems so wee need to guarantee to not execute this in main gui thread (so .. no Task )
                if (_cancellationTokenSource.Token.IsCancellationRequested)
                    return;
                void commitToMenu()
                {
                    foreach (var subMenu in subMenus)
                        mnuAddNestedProperty.Items.Add(subMenu);
                    mnuAddNestedProperty.Caption = mnuAddNestedProperty.Caption.Replace(UnicodeChars.HOURGLASS + " ", "");
                    mnuAddNestedProperty.Enabled = subMenus.Count > 0;
                }
                if (_menuDoneBeforeFirstShow.WaitOne(0)) // finish to build menu in time allowed by main thread
                {
                    try
                    {
                        commitToMenu();
                    }
                    finally
                    {
                        _menuDoneBeforeFirstShow.Set(); // unlock main thread that will show menu
                    }
                }
                else // maingui already show the menu, so we redisplay it (make the menu blink but we dont have another choice)
                    originColumn.View.GridControl.BeginInvoke((Action)(() =>
                    {
                        if (!_cancellationTokenSource.Token.IsCancellationRequested)
                        {
                            commitToMenu();
                            // Trying to change menu when displaying... not possible see here
                            // https://www.devexpress.com/Support/Center/Question/Details/T410889/how-to-add-items-to-the-popup-menu-after-it-was-shown
                            // https://www.devexpress.com/Support/Center/Question/Details/S92262/dxsubmenuitem-beforepopup-event-for-submenus-should-fire-just-before-menu-is-shown
                            e.Menu.Show(currentShowPoint);
                        }
                    }));
            }
            if (BuildMenuSynchronously)
                action();
            else
            {
                _thBuildMenu = new Thread(action);
                _thBuildMenu.Start();
            }
            Thread.Sleep(200); // give a little time (not really noticeable to user) but may avoid blinking of menu (to test the problem : comment this line)
            _menuDoneBeforeFirstShow.WaitOne();
            if (_menuDoneBeforeFirstShow == null)
            e.Menu.CloseUp += (_, __) =>  ExceptionManager.Instance.IgnoreException<ObjectDisposedException>(_cancellationTokenSource.Cancel);
        }
        AutoResetEvent _menuDoneBeforeFirstShow;
        CancellationTokenSource _cancellationTokenSource;
        Thread _thBuildMenu;
        const string MenuCaption = "Add related information...";
        List<SubMenuItem> GetMenuContent(CancellationToken cancellationToken, IReadOnlyCollection<object> enumerable, ILookup<string, bool> fieldNameDisplayHandled, GridColumn originColumn)
        {
            var mnuTypesToFill = GetBrowsableTypes(cancellationToken, enumerable, originColumn);
            return mnuTypesToFill.SelectNotNull(mnuTypeToFill => FillSubMenu(mnuTypeToFill, originColumn, (originColumn as IEnhancedGridColumn)?.RelatedPropertyInfo, fieldNameDisplayHandled, cancellationToken)).ToList();
        }

        List<SubMenuItem> GetBrowsableTypes(CancellationToken cancellationToken, IReadOnlyCollection<object> enumerable, GridColumn col)
        {
            var result = new List<SubMenuItem>();
            var view = col.View;

            // Allow user to create new column based on type of datasource' items
            var itemType = enumerable.GetType().GetOneImplementationOf(typeof(IEnumerable<>))?.GetGenericArguments().First();
            if (itemType != null && itemType != typeof(object) && (itemType.IsClass || itemType.IsInterface))
                result.Add(new SubMenuItem(itemType, null, "Add a column about \"" + GetSmartTypeName(itemType) + "\" (= row)"));

            // Allow user to create new column based on cell values of current column 
            // if column is an optimized unbound column
            if ((col as IUnboundColumnOptimizationRequirement)?.UnboundValueGetter != null ||
                 col.UnboundType == UnboundColumnType.Bound 
                 && IsBrowsableType(col.ColumnType)
                 && enumerable.Count > 0)
            {
                var depthOfTypes = GetCellValueTypes(cancellationToken, enumerable, col, view);
                if (depthOfTypes != null)
                {
                    depthOfTypes = OrderByInheritanceDepth(depthOfTypes);
                    foreach (var cellType in depthOfTypes)
                        if (cellType != typeof(object) && (!cellType.IsAbstract || AllowExploringColumnValueAbstractTypes))
                        {
                            var name = BusinessNameAttribute.GetNameFor(cellType) ?? cellType.ToSmartString().Uncamelify();
                            if (name.Length > 0)
                                if (char.ToLowerInvariant(name.Last()) == 's')
                                    name += "' ";
                                else
                                    name += "'s ";
                            result.Add(new SubMenuItem(null, cellType, "Explore " + name + "hidden values from this column") { Tag = cellType });
                        }
                }
            }

            return result;
        }
        internal static string GetSmartTypeName(Type t)
        {
            var name = BusinessNameAttribute.GetNameFor(t);
            if (name != null)
                return name;
            if (typeof(GridViewDataSourceFlattenizer.Join).IsAssignableFrom(t))
                return GetSmartTypeName(t.GetGenericArguments()[0]);
            return t.ToSmartString().Uncamelify();
        }
        static bool IsBrowsableType(Type type)
        {
            return type.IsClass ||
                   type.IsInterface ||
                   type.RemoveNullability() == typeof(DateTime) ||
                   type.RemoveNullability() == typeof(TimeSpan);
        }

        class SubMenuItem : DXSubMenuItem
        {
            public Type RowType { get; private set; }
            public Type CellType { get; private set; }

            public SubMenuItem(Type rowType, Type cellType, string caption) : base(caption)
            {
                RowType = rowType;
                CellType = cellType;
            }
        }


        static IEnumerable<Type> GetCellValueTypes(CancellationToken cancellationToken, IEnumerable datasource, GridColumn col, ColumnView view)
        {
            var cellValueTypes = new HashSet<Type>();
            var getter = col.UnboundType == UnboundColumnType.Bound ? null : (col as IUnboundColumnOptimizationRequirement)?.UnboundValueGetter;
            int listSourceRowIndex = 0;
            foreach (var item in datasource)
            {
                if (cancellationToken.IsCancellationRequested)
                    return null;
                // GetRowCellValue is really slow when object is directly bound
                // So you better create Unbound Column and set its function (col as IEnhancedGridColumn).UnboundValueGetter
                var cellValue = getter == null ? view.GetListSourceRowCellValue(listSourceRowIndex, col) : getter((item as DataRowView)?.Row ?? item, listSourceRowIndex);
                if (cellValue != null)
                {
                    var type = cellValue.GetType();
                    if (!cellValueTypes.Contains(type))
                        cellValueTypes.Add(type);
                }
                ++listSourceRowIndex;
            }
            return cellValueTypes;
        }
        static IEnumerable<Type> OrderByInheritanceDepth(IEnumerable<Type> types)
        {
            Dictionary<Type, int> depthOfTypes = new Dictionary<Type, int>();
            foreach (var type in types)
            {
                if (type == typeof(object) || !IsBrowsableType(type))
                    continue;

                Type parent = type;
                int depth = 0;
                while (parent != null) // parent of interface type are not typeof(object) but null
                {
                    ++depth;
                    parent = parent.GetBaseType();
                }

                parent = type;
                while (parent != null && !depthOfTypes.ContainsKey(parent))
                {
                    depthOfTypes.Add(parent, depth);
                    --depth;
                    parent = parent.GetBaseType();
                }
            }

            return depthOfTypes.OrderBy(kvp => kvp.Value).Select(kvp => kvp.Key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mnuTypeToFill">Main menu to fill</param>
        /// <param name="originColumn">On which column user clicked to get menu</param>
        /// <param name="parentBci">For recursivity, give the handler of parent column</param>
        /// <param name="fieldNames"></param>
        /// <param name="cancellationToken">For recursivity and background building. allow to cancel menu creation</param>
        /// <returns></returns>
        SubMenuItem FillSubMenu(SubMenuItem mnuTypeToFill, GridColumn originColumn, BrowsingColumnInfo parentBci,
                                ILookup<string, bool> fieldNameDisplayHandled, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
                return null;
            if (parentBci?.ParentSources.Count() >= MenuBuildingMaxRecursion)
                return null;

            var browsableType = mnuTypeToFill.RowType ?? mnuTypeToFill.CellType;
            Debug.Assert(browsableType != null);
            var props = browsableType.GetPublicFlattenizedProperties().Reverse().ToArray();
            if (props.Length == 0)
                return null;
            var navigatingProperties = new List<DXSubMenuItem>();
            foreach (var p in props)
            {
                if (cancellationToken.IsCancellationRequested)
                    break;
                if (!p.CanRead || p.IsTechnicalPropertyHidden())
                    continue;
                if (p.IsStatic())
                    continue;
                if (!(ShowInstanceOfType(p.PropertyType) ?? true))
                    continue;
                if (p.GetIndexParameters().Length > 0) // Indexed property
                    continue;
                var rpi = new RelatedPropertyInfo(mnuTypeToFill.RowType != null ? null : parentBci,
                                                  mnuTypeToFill.RowType != null || parentBci?.OriginColumn == originColumn ? null : originColumn,
                                                  p);
                bool isRpiExplorable = rpi.AsDevExpressType() == UnboundColumnType.Object
                                    && !rpi.GetValueType().RemoveNullability().IsEnum;
                var fieldNameStd = ToOriginalBoundFieldname(rpi.UniqueFieldName);
                // if normalized fieldname is already used and not mapped in an usual way
                var developperProbablyWantsToHideIt = fieldNameDisplayHandled[fieldNameStd].Any(v => !v);
                if (isRpiExplorable)
                {
                    var subMnu = new SubMenuItem(null, p.PropertyType, rpi.BrowsingPathTarget);
                    FillSubMenu(subMnu, originColumn, rpi, fieldNameDisplayHandled, cancellationToken);
                    subMnu.Items.Insert(0, new DXMenuItem(developperProbablyWantsToHideIt ? "<Display customized by developper!>" : "<itself as text>",
                            (_, __) => { GetOrCreateAndShowColumnOnNestedProperty((GridView)originColumn.View, rpi, originColumn); })
                    {
                        Enabled = !developperProbablyWantsToHideIt
                    });
                    navigatingProperties.Add(subMnu);
                }
                else
                {
                    if (fieldNameDisplayHandled[fieldNameStd].Any(v => v))
                        continue; // already displayed
                    // Now if the exploration path is already displayed
                    var mnuAddProperty = new DXMenuItem(rpi.BrowsingPathTarget,
                        (_, __) => { GetOrCreateAndShowColumnOnNestedProperty((GridView)originColumn.View, rpi, originColumn); })
                    {
                        Enabled = !developperProbablyWantsToHideIt
                    };
                    mnuTypeToFill.Items.Add(mnuAddProperty);
                }
            }

            if (browsableType == typeof(string))
            {
                var cbci = new StringAnalysisColumnInfoPattern(mnuTypeToFill.RowType != null ? null : parentBci,
                                                               mnuTypeToFill.RowType != null || parentBci?.OriginColumn == originColumn ? null : originColumn);
                var cbci2 = new StringAnalysisColumnInfoPatternId(mnuTypeToFill.RowType != null ? null : parentBci,
                                                                  mnuTypeToFill.RowType != null || parentBci?.OriginColumn == originColumn ? null : originColumn,
                                                                  cbci);
                var mnuAddProperty = new DXMenuItem(cbci.BrowsingPathTarget + " && " + cbci2.BrowsingPathTarget,
                       (_, __) => {
                           GetOrCreateAndShowColumnOnNestedProperty((GridView)originColumn.View, cbci2, originColumn);
                           GetOrCreateAndShowColumnOnNestedProperty((GridView)originColumn.View, cbci, originColumn);
                       });
                mnuTypeToFill.Items.Add(mnuAddProperty);
            }


            if (navigatingProperties.Count > 0)
            {
                if (mnuTypeToFill.Items.Count > 0)
                    mnuTypeToFill.Items.Insert(0, new DXMenuHeaderItem { Caption = "Properties" });
                foreach (var mnu in navigatingProperties.AsEnumerable().Reverse())
                    mnuTypeToFill.Items.Insert(0, mnu);
                mnuTypeToFill.Items.Insert(0, new DXMenuHeaderItem { Caption = "Neighboring Related Data" });
            }

            return mnuTypeToFill.Items.Count == 0 ? null : mnuTypeToFill;
        }

        void GetOrCreateAndShowColumnOnNestedProperty(GridView gv, BrowsingColumnInfo bci, GridColumn afterCol = null)
        {
            var col = gv.Columns[bci.UniqueFieldName];
            if (col != null) // Maybe already mapped by user or already displayed as a BrowsingColumnInfo...
            {
                EnhancedGridView_FindColumn_Form.MoveScrollBarTo(gv, col);
                return;
            }

            col = gv.Columns.AddField(bci.UniqueFieldName);
            var rCol = col as IEnhancedGridColumn;
            Debug.Assert(rCol != null);

            Debug.Assert(!string.IsNullOrWhiteSpace(col.Name), "Important to be taken in account for layout");
            col.Caption = bci.BrowsingPath;
            col.OptionsColumn.ReadOnly = true;
            col.OptionsColumn.AllowSort = DefaultBoolean.True;
            col.OptionsColumn.AllowGroup = DefaultBoolean.True;
            col.UnboundType = bci.AsDevExpressType();

            if (bci.GetValueType().IsEnum)
            {
                var mConfigureForEnum = typeof(GridColumn_ExtensionsForEnums).GetMethods(BindingFlags.Static | BindingFlags.Public)
                                                                             .Single(m => m.Name == nameof(GridColumn_ExtensionsForEnums.ConfigureForEnum) 
                                                                                       && m.GetGenericArguments().Length == 1
                                                                                       && m.GetParameters().Length == 1 && m.GetParameters()[0].ParameterType == typeof(GridColumn))
                                                                             .MakeGenericMethod(bci.GetValueType());
                mConfigureForEnum.Invoke(null, new object[] {col});
            }
            else if (typeof(IStaticEnum).IsAssignableFrom(bci.GetValueType()))
            {
                var set = IStaticEnum_Extensions.GetValueSet(bci.GetValueType())
                                                .Select(kvp => kvp.Value.EnumValue)
                                                .ToList();
                col.ConfigureForObjects(set, v => v.Label, true);
            }
            else if (typeof(DynamicEnum).IsAssignableFrom(bci.GetValueType()))
            {
                var spAll = bci.GetValueType().GetProperty(nameof(DynamicEnum.DummyEnum.All), BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                Debug.Assert(spAll != null, nameof(spAll) + " != null");
                var enumValuesUntyped = ((IEnumerable)spAll.GetValue(null)).Cast<DynamicEnum>().ToList();
                var gmCast = typeof(Enumerable).GetMethod(nameof(Enumerable.Cast));
                Debug.Assert(gmCast != null, nameof(gmCast) + " != null");
                var mCast = gmCast.MakeGenericMethod(bci.GetValueType());
                var enumValues = (IEnumerable<DynamicEnum>)mCast.Invoke(null, new object[] { enumValuesUntyped });
                col.ConfigureForObjects(enumValues, v => v.Caption, true);
            }

            rCol.UnboundValueGetter = bci.GetValue;

            col.View.ColumnPositionChanged -= OnColumnPositionChanged;
            col.View.ColumnPositionChanged += OnColumnPositionChanged;

            col.InsertAfter(afterCol);
            bci.DisplayColumn = col;
        }

        void OnColumnPositionChanged(object sender, EventArgs e)
        {
            var view = (GridView)((GridColumn)sender).View;
            foreach (var col in view.Columns.ToList())
                if (IsColumnNestedProperty(col) && !col.Visible)
                {
                    view.Columns.Remove(col);
                    (col as IEnhancedGridColumn).RelatedPropertyInfo.DisplayColumn = null;
                    (col as IEnhancedGridColumn).RelatedPropertyInfo = null;
                }
        }
    }

    public interface IEnhancedGridColumn_IsRelatedPropertyColumn
    {
        BrowsingColumnInfo RelatedPropertyInfo { get; set; }
    }

    public partial interface IEnhancedGridColumn : IEnhancedGridColumn_IsRelatedPropertyColumn
    {
    }
    public partial class EnhancedGridColumn
    {
        BrowsingColumnInfo IEnhancedGridColumn_IsRelatedPropertyColumn.RelatedPropertyInfo { get; set; }
    }
    public partial class BandedEnhancedGridColumn
    {
        BrowsingColumnInfo IEnhancedGridColumn_IsRelatedPropertyColumn.RelatedPropertyInfo { get; set; }
    }
}
