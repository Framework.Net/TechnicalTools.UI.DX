﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

using DevExpress.Data.Filtering;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;

using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX
{
    public class EnhancedGridView_ExportExcel
    {
        readonly GridView _view;

        public EnhancedGridView_ExportExcel(GridView view)
        {
            Debug.Assert(view != null);
            _view = view;
            Install();
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(EnhancedGridView_ExportExcel));

        public void Install()
        {
            Install(_view);
        }
        void Install(GridView view)
        {
            Debug.Assert(view != null);
            view.PopupMenuShowing -= View_PopupMenuShowing;  view.PopupMenuShowing += View_PopupMenuShowing;
        }

        public void Uninstall()
        {
            Uninstall(_view);
        }
        void Uninstall(GridView view)
        {
            view.PopupMenuShowing -= View_PopupMenuShowing;
        }

        internal void Assign(EnhancedGridView_ExportExcel f, bool copyEvents)
        {
            f.Uninstall(_view); // See caller comments to understand this not intuitive code

            // Copy other arguments from f to this
            //     Nothing to do for now
        }

        static void View_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;
            var view = sender as GridView;
            var mnuExport = new DXSubMenuItem(ExportMenuCaption);

            Action toExcelFile = () => DefaultExportToExcel(view, tryOpenExportFile: true);
            Action toCSVFile = () => DefaultExportToCSV(view, tryOpenExportFile: true);
            Action toCSVClipBoard = () => DefaultExportToCSVClipBoard(tempFile => DefaultExportToCSV(view, targetFilename: tempFile, tryOpenExportFile: false));

            if (view.IsDetailView)
            {
                // c'est ce qui fait que ca marche !
                toExcelFile    = toExcelFile.WrapLambda(view.ZoomView, view.NormalView);
                toCSVFile      = toCSVFile.WrapLambda(view.ZoomView, view.NormalView);
                toCSVClipBoard = toCSVClipBoard.WrapLambda(view.ZoomView, view.NormalView);
            }

            mnuExport.Items.Add(new DXMenuItem("Export data to Excel file (*.xlsx)",    (_, __) => toExcelFile()));
            mnuExport.Items.Add(new DXMenuItem("Export data to CSV file",       (_, __) => toCSVFile()));
            mnuExport.Items.Add(new DXMenuItem("Copy data to clipboard (CSV format)", (_, __) => toCSVClipBoard()));

            e.Menu.AddToGroup(ePopupMenuHeaderType.Standard, mnuExport);
        }
        public static string ExportMenuCaption { get; } = "Export...";

        public static string DefaultExportToExcel(GridView view, bool withCurrentFilter = true, bool tryOpenExportFile = false, string targetFilename = null)
        {
            try
            {
                var eview = view as EnhancedGridView;
                var ebview = view as EnhancedBandedGridView;

                string filename = targetFilename ?? ChooseFileDestination();
                if (filename == null)
                    return null;

                var options = new XlsxExportOptionsEx(TextExportMode.Value)
                {
                    ExportType = DevExpress.Export.ExportType.DataAware
                };
                var bakPrintDetails     = view.OptionsPrint.PrintDetails;     //view.OptionsPrint.PrintDetails     = true; // permet d'exporter les sous grilles
                var bakExpandAllDetails = view.OptionsPrint.ExpandAllDetails; //view.OptionsPrint.ExpandAllDetails = true; // permet d'exporter les sous grilles

                var isExporting = eview?.IsExporting ?? ebview?.IsExporting ?? false;
                try
                {
                    if      (eview != null)  eview .IsExporting = true;
                    else if (ebview != null) ebview.IsExporting = true;

                    void export() => view.ExportToXlsx(filename, options);
                    if (withCurrentFilter)
                        export();
                    else
                        RemoveAllFilterTemporary(view, export);
                }
                finally
                {
                    if      (eview != null)  eview .IsExporting = isExporting;
                    else if (ebview != null) ebview.IsExporting = isExporting;
                    view.OptionsPrint.PrintDetails     = bakPrintDetails;
                    view.OptionsPrint.ExpandAllDetails = bakExpandAllDetails;
                }
                if (tryOpenExportFile)
                    TryOpenFile(filename);
                return filename;
            }
            catch (Exception ex)
            {
                BusEvents.Instance.RaiseUserUnderstandableMessage("Error while exporting to Excel!" + Environment.NewLine +
                                                                  "Because" + ExceptionManager.Instance.Format(ex, true), _log);
                return null;
            }
        }

        public static string DefaultExportToCSV(GridView view, bool withCurrentFilter = true, bool tryOpenExportFile = false, string targetFilename = null)
        {
            try
            {
                var eview = view as EnhancedGridView;
                var ebview = view as EnhancedBandedGridView;

                string filename = targetFilename ?? ChooseFileDestination("csv", "CSV File");
                if (filename == null)
                    return null;

                var options = new CsvExportOptionsEx()
                {
                    ExportType = DevExpress.Export.ExportType.WYSIWYG
                };
                var bakPrintDetails = view.OptionsPrint.PrintDetails; view.OptionsPrint.PrintDetails = true; // permet d'exporter les sous grilles
                var bakExpandAllDetails = view.OptionsPrint.ExpandAllDetails; view.OptionsPrint.ExpandAllDetails = true; // permet d'exporter les sous grilles

                var isExporting = eview?.IsExporting ?? ebview?.IsExporting ?? false;

                void h(object sender, MasterRowGetRelationNameEventArgs e)
                {
                    // Prevent ExportToCsv call below to throw a NullReferenceException
                    // (See https://www.devexpress.com/Support/Center/Question/Details/Q426126/exception-when-expand-master-detail-view)
                    // This happens when a filter (AutoFilterRow) is currently applied on gridview
                    if (e.RelationName == null) // often true when GridControl.InvalidRowHandle == e.RowHandle
                        e.RelationName = string.Empty;
                }
                view.MasterRowGetRelationDisplayCaption += h;
                try
                {
                    if (eview != null) eview.IsExporting = true;
                    else if (ebview != null) ebview.IsExporting = true;

                    void export() => view.ExportToCsv(filename, options);
                    if (withCurrentFilter)
                        export();
                    else
                        RemoveAllFilterTemporary(view, export);
                }
                finally
                {
                    view.MasterRowGetRelationDisplayCaption -= h;
                    if (eview != null) eview.IsExporting = isExporting;
                    else if (ebview != null) ebview.IsExporting = isExporting;
                    view.OptionsPrint.PrintDetails = bakPrintDetails;
                    view.OptionsPrint.ExpandAllDetails = bakExpandAllDetails;
                }
                if (tryOpenExportFile)
                    TryOpenFile(filename);
                return filename;
            }
            catch (Exception ex)
            {
                BusEvents.Instance.RaiseUserUnderstandableMessage("Error while exporting to Excel!" + Environment.NewLine +
                                                                  "Because" + ExceptionManager.Instance.Format(ex, true), _log);
                return null;
            }
        }

        internal static void DefaultExportToCSVClipBoard(Action<string> exportToCsvFile)
        {
            var tempFile = Path.GetTempFileName();
            exportToCsvFile(tempFile);
            string csvText = File.ReadAllText(tempFile);
            ExceptionManager.Instance.IgnoreExceptionAndBreak(() =>
            {
                if (File.Exists(tempFile)) // file may not exist if user cancelled
                {
                    File.Delete(tempFile);
                    if (!string.IsNullOrEmpty(csvText)) // empty string (not null) make SetText to throw
                        Clipboard.SetText(csvText);
                }
            });
        }


        internal static string ChooseFileDestination(string ext = "xlsx", string filetypename = "Excel")
        {
            var dlg = new XtraSaveFileDialog()
            {
                DefaultExt = $".{ext}", // utilisé seulement quand "All files" est choisi dans le filtre est qu'aucune extension n'a été choisie par l'utilisateur.
                Title = "Please choose where to export the lines",
                ValidateNames = true,
                Filter = $"{filetypename} (*.{ext})|*.{ext}",
                CheckPathExists = true
            };
            if (DialogResult.OK != dlg.ShowDialog())
                return null;
            return dlg.FileName;
        }

        public static bool TryOpenFile(string filePath)
        {
            try
            {
                Path_Extensions.OpenFile(filePath);
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ExceptionManager.Instance.Format(ex, true));
                return false;
            }
        }



        public static void RemoveAllFilterTemporary(GridView gv, Action action)
        {
            // DevExpress fonctionne de tel sorte qu'il exporte ce qu'on voit, on est donc obligé de modifier temporairement la datasource, c'est bien plus simple
            CriteriaOperator filter = gv.ActiveFilterCriteria;
            gv.ClearColumnsFilter();
            void noFilter(object ___, RowFilterEventArgs ee)
            {
                ee.Handled = true;
                ee.Visible = true;
            }
            gv.CustomRowFilter += noFilter; // idea from https://www.devexpress.com/Support/Center/Example/Details/E2801
            try
            {
                action();
            }
            finally
            {
                gv.CustomRowFilter -= noFilter;
                gv.ActiveFilterCriteria = filter; // TODO :L'autofiltr row n'affiche pas les donnes rentré, mais le filtre fonctionne toujours
            }
        }
    }
}
