﻿using System;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraTab;


namespace TechnicalTools.UI.DX.Tools
{
    public static class TabControl_Extensions
    {
        public static void ForceControlCreations(this TabControl control)
        {
            ForceControlCreations((Control)control);
            foreach (Control subcontrol in control.Controls)
            {
                ForceControlCreations(subcontrol);
            }
        }
        public static void ForceControlCreations(this XtraTabControl tabs, params XtraTabPage[] exceptThesePages)
        {
            ForceControlCreations((Control)tabs);
            foreach (Control subcontrol in tabs.TabPages.Except(exceptThesePages).Reverse())
                ForceControlCreations(subcontrol);
        }
        static void ForceControlCreations(Control control)
        {
            var method = control.GetType().GetMethod("CreateControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var parameters = method.GetParameters();
            method.Invoke(control, new object[] { true });
        }

    }
}
