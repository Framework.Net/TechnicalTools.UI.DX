﻿using System;
using System.Threading;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX
{
    public static class Form_Extensions
    {
        public static Tuple<SynchronizationContext, Form> ShowInOtherGuiThread(Func<Form> createForm)
        {
            Form form = null;
            SynchronizationContext ctx = null;

            var evt = new ManualResetEvent(false);
            var th = new Thread(() =>
            {
                var frm = createForm();
                form = frm;
                frm.HandleCreated += (_, __) =>
                {
                    ctx = SynchronizationContext.Current;
                    evt.Set();
                };
                Application.Run(frm);
            });
            th.SetApartmentState(ApartmentState.STA); // Sinon certain controle graphique plante (exemple http://stackoverflow.com/questions/135803/dragdrop-registration-did-not-succeed)
            th.IsBackground = true;
            th.Start();
            evt.WaitOne();
            evt.Dispose();
            return Tuple.Create(ctx, form);
        }

        private static void Frm_Shown(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
