﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using TechnicalTools.Diagnostics;
using TechnicalTools.UI.DX.Tools;


namespace TechnicalTools.UI.DX
{
	public static class Control_Extensions
	{
        /// <summary>
        /// Just to make developper' life easy by preventing him to write unecessary complex code like this:
        /// <code>
        /// var sender = ...;
        /// var e = ...;
        /// BeginInvoke((Action)(() => something(sender, e)));
        /// </code>
        /// Now he can write:
        /// <code>
        /// var sender = ...;
        /// var e = ...;
        /// BeginInvoke(() => something(sender, e));
        /// </code>
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="action">MethodInvoker (or Action in recent framework) </param>
        [DebuggerHidden, DebuggerStepThrough]
        public static void BeginInvoke(this Control ctl, MethodInvoker action)
        {
            ctl.BeginInvoke(action);
        }

        /// <summary>
        /// Same as <see cref="BeginInvoke(Control, MethodInvoker)"/>
        /// but discard usual exceptions when calling BeginInvoke
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        public static void BeginInvokeSafely(this Control ctl, MethodInvoker action)
        {
            try
            {
                BeginInvoke(ctl, action);
            }
            catch (Exception)
            {
                if (!ctl.IsDisposed && !ctl.Disposing && ctl.IsHandleCreated)
                    DebugTools.Break(); // Unknown error, interesting for developper
            }
        }

        public static IDisposable LockTemporary(this Control ctl)
        {
            return new TemporaryControlLocker(ctl);
        }
        public static IDisposable LockTemporary<T>(this T[] ctls)
            where T : Control
        {
            return new TemporaryControlGroupLocker(ctls);
        }
        public static IDisposable LockTemporary(params Control[] ctls)
        {
            return new TemporaryControlGroupLocker(ctls);
        }

        public static IDisposable ChangeTextTemporary(this Control ctl, string newText)
        {
            return new TemporaryChangeTextHandler(ctl, newText);
        }

        [Obsolete("Normalement le code doit avoir la connaissance du fait qu'un invoke est nécessaire ou non." +
                  " Avoir du code qui teste si y'ne a peut etre besoin rend le code complexe à suivre", true)]
		public static void InvokeIfRequired<T>(this T c, Action<T> action) 
            where T : Control
		{
			if (c.InvokeRequired)
			{
				c.Invoke(new Action(() => action?.Invoke(c)));
			}
			else
			{
				action?.Invoke(c);
			}
		}

 
        public static Control FindFocusedControl(this Control control)
        {
            var container = control as IContainerControl;
            while (container != null)
            {
                control = container.ActiveControl;
                container = control as IContainerControl;
            }
            return control;
        }

        public static IEnumerable<Control> AllSubControls(this Control ctl)
        {
            var q = new Queue<Control>();
            q.Enqueue(ctl);
            while (q.Count > 0)
            {
                var c = q.Dequeue();
                foreach (Control subc in c.Controls)
                {
                    q.Enqueue(subc);
                    yield return subc;
                }
            }
        }

	    public static IEnumerable<Control> AllParentControls(this Control ctl)
	    {
	        while (ctl.Parent != null)
	        {
	            yield return ctl.Parent;
	            ctl = ctl.Parent;
	        }
	    }

        public static void AllowCrossThreadCallsTemporary(this Control @this, Action action)
        {
            lock(_crossThreadAllowingToken)
            {
                if (++_crossThreadAllowingTokens == 1)
                    Control.CheckForIllegalCrossThreadCalls = false;
            }
            try
            {
                action();
            }
            finally
            {
                lock (_crossThreadAllowingToken)
                {
                    if (--_crossThreadAllowingTokens == 0)
                        Control.CheckForIllegalCrossThreadCalls = true;
                }
            }
        }
        static readonly object _crossThreadAllowingToken = new object();
        static int _crossThreadAllowingTokens = 0;

        // from http://stackoverflow.com/questions/778095/windows-forms-using-backgroundimage-slows-down-drawing-of-the-forms-controls
        #region Redraw Suspend/Resume

        [DllImport("user32.dll", EntryPoint = "SendMessageA", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        private const int WM_SETREDRAW = 0xB;

        /// <summary>
        /// Permet de suspendre l'afichage d'un controle.
        /// Très utile si, en fonction du choix de l'utilisateur (combobox), on doit modifier l'interface.
        /// En general on l'interface agit de facon très moche pendant ce temps là.
        /// Il s'agit en fait de l'équivalent d'un SuspendLayout mais qui fonctionne.
        /// Attention a bien appeler ResumeDrawing après (en utilisant un try/finally si besoin)
        /// </summary>
        public static void SuspendDrawing(this Control target)
        {
            if (target.Visible)
                SendMessage(target.Handle, WM_SETREDRAW, 0, 0);
            // Methode 2
            //Message msgSuspendUpdate = Message.Create(target (ou target.Parent?).Handle, WM_SETREDRAW, IntPtr.Zero, IntPtr.Zero);
            //NativeWindow window = NativeWindow.FromHandle(target (ou target.Parent?).Handle);
            //window.DefWndProc(ref msgSuspendUpdate);
        }

        public static void ResumeDrawing(this Control target, bool redraw = true)
        {
            // Test important sinon peut rendre visible des form qui etait invisible sans qu'elle declenche d'evenet Shown Load VisibleChanged (et visible reste a false !)
            if (target.Visible)
                SendMessage(target.Handle, WM_SETREDRAW, 1, 0);

            // Methode 2
            // {
            // Create a C "true" boolean as an IntPtr
            //IntPtr wparam = new IntPtr(1);
            //Message msgResumeUpdate = Message.Create(target (ou target.Parent?).Handle, WM_SETREDRAW, wparam,
            //    IntPtr.Zero);

            //NativeWindow window = NativeWindow.FromHandle(target (ou target.Parent?).Handle);
            //window.DefWndProc(ref msgResumeUpdate);
            // }

            if (redraw)
                target.Refresh();
        }
        #endregion
    }
}
