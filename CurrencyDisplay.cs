﻿using System;
using System.Linq;

using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Model;


namespace TechnicalTools.UI.DX
{
    public class CurrencyDisplayHelper
    {
        readonly GridView _view;

        public CurrencyDisplayHelper(GridView view)
        {
            _view = view;
            if (_view.DataSource != null)
                Install();
            _view.DataSourceChanged += (_, __) =>
            {
                Uninstall();
                if (_view.DataSource != null)
                    Install();
            };
        }
        public void Install()
        {
            _view.PopupMenuShowing += gv_PopupMenuShowing;
            _view.CustomSummaryCalculate += gv_CustomSummaryCalculate;
        }
        public void Uninstall()
        {
            _view.PopupMenuShowing -= gv_PopupMenuShowing;
            _view.CustomSummaryCalculate -= gv_CustomSummaryCalculate;
        }

        void gv_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu == null)
                return;

            var view = sender as GridView;
            if (e.MenuType != GridMenuType.Summary)
                return;
            if (e.HitInfo.Column == null) // click on indicator
                return;
            if (e.HitInfo.Column.ColumnType == typeof(CurrencyValue) ||
                e.HitInfo.Column.ColumnType == typeof(CurrencyValueList))
            {
                DevExpress.XtraGrid.Menu.GridViewFooterMenu footerMenu = e.Menu as DevExpress.XtraGrid.Menu.GridViewFooterMenu;
                bool check = e.HitInfo.Column.SummaryItem.SummaryType == DevExpress.Data.SummaryItemType.Custom &&
                             Equals("Sum", e.HitInfo.Column.SummaryItem.Tag);

                var menuItemSum = footerMenu.Items.Cast<DXMenuItem>().FirstOrDefault(mnu => mnu.Caption == "Sum");
                if (menuItemSum == null)
                    return;
                DXMenuItem menuItem = new DXMenuCheckItem("Sum", check, menuItemSum.Image, (_, __) =>
                {
                    e.HitInfo.Column.SummaryItem.Tag = e.HitInfo.Column.ColumnType == typeof(CurrencyValue) ? CurrencyValueSumMenuKey : CurrencyValueListSumMenuKey;
                    e.HitInfo.Column.SummaryItem.SetSummary(DevExpress.Data.SummaryItemType.Custom, string.Empty);
                });
                footerMenu.Items.Insert(footerMenu.Items.Cast<DXMenuItem>().ToList().IndexOf(menuItemSum), menuItem);
                footerMenu.Items.Remove(menuItemSum);

                var menuItemNone = footerMenu.Items.Cast<DXMenuItem>().FirstOrDefault(mnu => mnu.Caption == "None");
                if (menuItemNone != null)
                    menuItemNone.Enabled = true;
            }
        }
        public static readonly string CurrencyValueSumMenuKey = "CurrencyValueSum";
        public static readonly string CurrencyValueListSumMenuKey = "CurrencyValueListSum";


        private void gv_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
        {
            var item = e.Item as GridSummaryItem; // classe de base pour GridColumnSummaryItem (afficher dnas le footer) et GridGroupSummaryItem affiché dans les group row
            GridView view = sender as GridView;
            if (Equals(CurrencyValueSumMenuKey, item.Tag))
            {
                if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
                    e.TotalValue = new CurrencyValueList(new CurrencyValue[] { }, true);
                if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
                    if (e.FieldValue != null)
                        (e.TotalValue as CurrencyValueList).SumInPlace((CurrencyValue)e.FieldValue);
                if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
                    (e.TotalValue as CurrencyValueList).SortByAmount();
            }
            else if (Equals(CurrencyValueListSumMenuKey, item.Tag))
            {
                if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
                    e.TotalValue = new CurrencyValueList(new CurrencyValue[] { }, true);
                if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
                    if (e.FieldValue != null)
                        (e.TotalValue as CurrencyValueList).SumInPlace(e.FieldValue as CurrencyValueList);
                if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
                    (e.TotalValue as CurrencyValueList).SortByAmount();
            }
        }
    }
}
