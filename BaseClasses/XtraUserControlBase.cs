﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// Classe de base pour afficher une vue d'un objet (et un seul !)
    /// </summary>
    public partial class XtraUserControlBase : EnhancedXtraUserControl, IBaseControl
    {
        /// <summary> See IBaseControl.UserCanSee </summary>
        public virtual bool UserCanSee  { get; } = true;
        /// <summary> See IBaseControl.UserCanEdit </summary>
        public virtual bool UserCanEdit { get; } = true;

        protected XtraUserControlBase()
        {
            InitializeComponent();

            FormChanging_Init();
            FormClosing_Init();
        }

        public void RefreshAccesses()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (InvokeRequired)
                BeginInvoke((Action)RefreshEnabilitiesAndVisibilities);
            else
                RefreshEnabilitiesAndVisibilities();
        }

        /// <summary>
        /// Allow developper to update controls's accessibilities to reflect any interesting changes.
        /// For exemple impersonation triger this method.
        /// </summary>
        protected internal virtual void RefreshEnabilitiesAndVisibilities()
        {
        }


        #region Property and method to handle this as a DialogBox

        public DialogResult DialogResult { get; private set; } = DialogResult.Cancel;

        protected void AcceptAndClose()
        {
            DialogResult = DialogResult.OK;
            CloseTopFormOrPanel();
        }

        protected void CancelAndClose()
        {
            DialogResult = DialogResult.Cancel;
            CloseTopFormOrPanel();
        }
        void CloseTopFormOrPanel()
        {
            if (DialogResult == DialogResult.None)
                DialogResult = DialogResult.Cancel;
            if (TopFormOrPanel is DockPanel)
                (TopFormOrPanel as DockPanel).Close();
            if (TopFormOrPanel is FloatForm)
                ((TopFormOrPanel as FloatForm).Controls[0] as DockPanel).Close();
            if (TopFormOrPanel is Form)
            {
                (TopFormOrPanel as Form).DialogResult = DialogResult;
                (TopFormOrPanel as Form).Close();
            }
        }

        #endregion 

        #region Better event ParentChanged & ParentChanging

        public event ParentChangingEventHandler ParentChanging;

        protected void RaiseParentChanging(Control old_parent)
        {
            ParentChanging?.Invoke(this, new ParentChangingEventArgs(old_parent));
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e); // Declenche les autres events (le userControl GlassLayer a besoin de s'executer apres les autres handler)
            if (OldParent != Parent) // TODO : Vraiment utile ?
            {
                RaiseParentChanging(OldParent);
                OldParent = Parent;
            }
        }
        protected Control OldParent { get; private set; }

        #endregion


        #region Event that triggers when the owner container (form / panel) is changing

        public event ParentChangingEventHandler TopFormOrPanelChanging;

        private readonly List<Control> allAncestorToTopForm = new List<Control>();

        void FormChanging_Init()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            ParentChanged += AnyParentChanged;
            allAncestorToTopForm.Add(this);
            if (Parent != null)
                AnyParentChanged(this, EventArgs.Empty);
        }

        void AnyParentChanged(object sender, EventArgs e)
        {
            //Debug.WriteLine("-----------------");
            //var p2 = sender as Control;
            //while (p2 != null)
            //{
            //    Debug.WriteLine("Parent : " + p2.GetType().Name + " " + p2.Name);
            //    p2 = p2.Parent;
            //}

            var index = allAncestorToTopForm.IndexOf(sender as Control);
            for (int i = allAncestorToTopForm.Count-1; i >= index; --i)
                allAncestorToTopForm[i].ParentChanged -= AnyParentChanged;
            allAncestorToTopForm.RemoveRange(index, allAncestorToTopForm.Count - index);
            var parent = sender as Control;
            do
            {
                allAncestorToTopForm.Add(parent);
                if (parent is DockPanel &&
                    parent.Parent is DocumentContainer) // DockPanel is about to be docked (DocumentContainer parent will change soon or parent is already set)
                {
                    // When Dock panel is detached, this is DockPanel.Parent that is set to null
                    parent.ParentChanged += AnyParentChanged;
                    TopFormOrPanel = (DockPanel)parent;
                    return;
                }
                if (parent is Form) // normal form or FloatForm
                {
                    TopFormOrPanel = (Form)parent;
                    return;
                }
                parent.ParentChanged += AnyParentChanged;
                parent = parent.Parent;
            }
            while (parent != null);
            TopFormOrPanel = null;
        }
        public ContainerControl TopFormOrPanel
        {
            get { return _TopFormOrPanel; }
            private set
            {
                if (_TopFormOrPanel != value)
                {
                    var old = _TopFormOrPanel;
                    _TopFormOrPanel = value;
                    TopFormOrPanelChanging?.Invoke(this, new ParentChangingEventArgs(old));
                }
            }
        }
        ContainerControl _TopFormOrPanel;

        #endregion

        #region Event that triggers when the owner container (form / panel) is closing

        public event TopFormOrPanelCancelEventHandler TopFormOrPanelClosing;
        public event TopFormOrPanelClosedEventHandler TopFormOrPanelClosed;

        void FormClosing_Init()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            TopFormOrPanelChanging += OnTopFormChanging;
        }

        protected virtual void OnTopFormChanging(object sender, ParentChangingEventArgs e)
        {
            //Debug.WriteLine("TopForm is now " + (TopFormOrPanel?.GetType()?.Name ?? "") + "  " + (TopFormOrPanel?.Name ?? "null") +
            //                " instead of " + (e.OldParent?.GetType()?.Name ?? "") + "  " +( e.OldParent?.Name ?? "null"));
            UnsubscribeParentEvent(e.OldParent);
            SubscribeParentEvent(TopFormOrPanel);
        }
        void SubscribeParentEvent(Control newParent)
        {
            if (newParent is DockPanel dp)
            {
                dp.DockManager.ClosingPanel += TopForm_ClosingPanel;
                dp.DockManager.ClosedPanel += TopForm_ClosedPanel;
            }
            else if (newParent is Form f)
            {
                f.FormClosing += TopForm_FormClosing;
                f.FormClosed += TopForm_FormClosed;

                if (newParent is FloatForm ff && ff.Controls.Count > 0) // == 0 quand on dock un panel à coté d'un autre
                {
                    if (ff.Controls[0] is DockPanel ffc)
                    {
                        ffc.ClosingPanel += TopForm_ClosingPanel;
                        ffc.ClosedPanel += TopForm_ClosedPanel;
                    }
                }
            }
            else
                DebugTools.Should(newParent == null, "Don't Know how to handle this hierarchy of controls!");
        }
        void UnsubscribeParentEvent(Control oldParent)
        {
            if (oldParent is DockPanel dp)
            {
                dp.DockManager.ClosingPanel -= TopForm_ClosingPanel;
                dp.DockManager.ClosedPanel -= TopForm_ClosedPanel;
            }
            else if (oldParent is Form f)
            {
                f.FormClosing -= TopForm_FormClosing;
                f.FormClosed -= TopForm_FormClosed;

                if (f is FloatForm ff && ff.Controls.Count > 0) // == 0 quand on dock un panel à coté d'un autre
                {
                    if (ff.Controls[0] is DockPanel ffc)
                    {
                        ffc.ClosingPanel -= TopForm_ClosingPanel;
                        ffc.ClosedPanel -= TopForm_ClosedPanel;
                    }
                }
            }
        }

        void TopForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (TopFormOrPanelClosing != null)
            {
                var ee = new TopFormOrPanelCancelEventArgs(sender as Control, e.Cancel);
                TopFormOrPanelClosing(this, ee);
                e.Cancel = ee.Cancel;
            }
        }
        void TopForm_ClosingPanel(object sender, DockPanelCancelEventArgs e)
        {
            if (TopFormOrPanelClosing != null &&
                ReferenceEquals(e.Panel.Controls.Cast<Control>().FirstOrDefault()?.Controls.Cast<Control>().FirstOrDefault(), this))
            {
                var ee = new TopFormOrPanelCancelEventArgs(sender as Control, e.Cancel);
                TopFormOrPanelClosing(this, ee);
                e.Cancel = ee.Cancel;
            }
        }
        void TopForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            TopFormOrPanelClosed?.Invoke(this, new TopFormOrPanelClosedEventArgs(sender as Control, e.CloseReason));
        }
        void TopForm_ClosedPanel(object sender, DockPanelEventArgs e)
        {
            TopFormOrPanelClosed?.Invoke(this, new TopFormOrPanelClosedEventArgs(sender as Control, CloseReason.None));
        }

        public delegate void TopFormOrPanelCancelEventHandler(XtraUserControlBase sender, TopFormOrPanelCancelEventArgs e);
        public class TopFormOrPanelCancelEventArgs
        {
            public Control ContainerControl { get; set; }
            public bool    Cancel           { get; set; }

            public TopFormOrPanelCancelEventArgs(Control containerControl, bool cancel)
            {
                ContainerControl = containerControl;
                Cancel = cancel;
            }
        }

        public delegate void TopFormOrPanelClosedEventHandler(XtraUserControlBase sender, TopFormOrPanelClosedEventArgs e);
        public class TopFormOrPanelClosedEventArgs
        {
            public Control     ContainerControl { get; set; }
            public CloseReason Reason           { get; set; }

            public TopFormOrPanelClosedEventArgs(Control containerControl, CloseReason reason)
            {
                ContainerControl = containerControl;
                Reason = reason;
            }
        }

        #endregion
    }
}
