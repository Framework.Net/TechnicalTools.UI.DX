﻿using System;


namespace TechnicalTools.UI.DX
{
    // Classe de base de toutes les forms. 
    // Etant donné que les vues (composant graphique) doivent heriter  de XtraUserControlBase,
    // A part la fenêtre principale (et encore..) et les fenetre de type dialobox (et encore ...)
    // personne ne devrait hériter de cette fenêtre.
    public partial class FormBase : EnhancedXtraForm, IBaseControl
    {        
        /// <summary> See IBaseControl.UserCanSee </summary>
        public virtual bool UserCanSee { get; } = true;
        /// <summary> See IBaseControl.UserCanEdit </summary>
        public virtual bool UserCanEdit { get; } = true;

        protected FormBase()
        {
            InitializeComponent();
        }
    }
}