﻿using System;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;



namespace TechnicalTools.UI.DX
{
    // Classe de base pour les fenêtres pouvant acceuillir d'autres fenetres internes (en tant qu'onglet, ou tuile)
    public partial class FormComposite : FormBase, IHasDockUserControl
    {
        public FormComposite()
        {
            InitializeComponent();
        }
        DockPanel IHasDockUserControl.DockAsDefault(XtraUserControlBase ctl, string caption) { return _DockAsDefault(ctl, caption); }
        DockPanel IHasDockUserControl.DockAsDefault(BaseEdit            be,  string caption) { return _DockAsDefault(be,  caption); }

        //void _DockAsDefault(Control ctl, string caption)
        //{
        //    var dockpanel = tabbedView1.AddDocument(ctl);
        //    dockpanel.Caption = string.IsNullOrWhiteSpace(caption) ? ctl.Text : caption;
        //    //return dockpanel;
        //}
        DockPanel _DockAsDefault(Control ctl, string caption)
        {
            var dockPanel = dockManager1.AddPanel(DockingStyle.Float);

            dockPanel.DockedAsTabbedDocument = true;
            dockPanel.Controls.Add(ctl);
            ctl.Dock = DockStyle.Fill;
            //            dockManager1.AddPanel(DockingStyle.Fill, dockPanel);
            if (caption == null)
            {
                ctl.TextChanged += (_, __) => dockPanel.Text = ctl.Text;
                dockPanel.Text = ctl.Text;
            }
            else
                dockPanel.Text = caption;
            ctl.Visible = true;
            dockPanel.Show();
            // Pour faire flotter :
            //dockPanel.DockedAsTabbedDocument = false;
            //dockPanel.MakeFloat(pt);
            //tabbedView1.Controller.Dock(dockPanel);

            return dockPanel;
        }

        private void dockManager1_ClosedPanel(object sender, DockPanelEventArgs e)
        {
            var dockPanel = e.Panel;
            if (dockPanel.Visibility == DockVisibility.Hidden)
                dockPanel.Dispose();
        }
    }
}