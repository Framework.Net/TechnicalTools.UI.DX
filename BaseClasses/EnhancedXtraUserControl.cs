﻿using System;
using System.ComponentModel;

using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;

using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX
{
    [ToolboxItem(false)]

    // Classe de base donnant accès a des fonctionalités générique et utile supplémentaires
    public class EnhancedXtraUserControl : XtraUserControl
    {
       // protected virtual void HandleExceptionInUI(string current_action_name, Exception ex)
       // {
       //     var frm = GetEnhancedOwnerForm();
       //     frm.HandleExceptionInUI(current_action_name, ex);
       // }

       // protected virtual DialogResult ShowError(string msg, string title = null, bool ask_try_again = false)
       // {
       //     var frm = GetEnhancedOwnerForm();
       //     return frm.ShowError(msg, title, ask_try_again);
       // }
       // protected virtual DialogResult ShowError(Exception ex, string title = null, bool ask_try_again = false)
       // {
       //     var frm = GetEnhancedOwnerForm();
       //     return frm.ShowError(ex, title, ask_try_again);
       // }
       // EnhancedXtraForm GetEnhancedOwnerForm()
       // {
       //     var frm = FindForm() as EnhancedXtraForm;
       //     Debug.Assert(frm != null, "Toutes les fenêtres de l'application sont censés être des EnhancedXtraForm ! Bizarre...");
       //     return frm;
       // }

       // /// <summary><see cref="EnhancedXtraForm.ExecuteAndDisplayException"/> </summary>
       //// [DebuggerHidden, DebuggerStepThrough]
       // public virtual bool ExecuteAndDisplayException(string action_name, Action action)
       // {
       //     var frm = (EnhancedXtraForm)FindForm();
       //     Debug.Assert(frm != null, "frm != null");
       //     return frm.ExecuteAndDisplayException(action_name, action);
       // }

        public EnhancedXtraUserControl()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (TrackInstances)
                ObjectTracker.Instance.Track(this);
        }
        public static bool TrackInstances { get; set; } = true;

        public static LayoutVisibility VisibleIf(bool cond)
        {
            return cond ? LayoutVisibility.Always : LayoutVisibility.Never;
        }
        public static BarItemVisibility MenuVisibleIf(bool cond)
        {
            return cond ? BarItemVisibility.Always : BarItemVisibility.Never;
        }
        public static DefaultBoolean EnabledIf(bool? cond)
        {
            return cond == true ? DefaultBoolean.True 
                 : cond == false ? DefaultBoolean.False
                 : DefaultBoolean.Default;
        }
    }
}
