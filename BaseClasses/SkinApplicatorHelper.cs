﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using TechnicalTools;
using TechnicalTools.Diagnostics;

namespace TechnicalTools.UI.DX.BaseClasses
{
    // Ce composant, droppé dans une fenetre (le plus souvent la fenetre de base) permet de forcer l'application du skin par defaut en mode design
    // VS 2012 souffre par ailleurs d'un b.u.g qui fait que le skin n'est pas applique en mode design
    // (cf https://www.devexpress.com/Support/Center/Question/Details/S170739)
    [ToolboxItem(true)]
    public class DesignTimeSkinApplicator : Component
    {
        ContainerControl _containerControl;

        public DesignTimeSkinApplicator()
        {
        }


        #region Just to get the reference to the owning Control (usually a Form) (From https://stackoverflow.com/a/371829/294998)

        public ContainerControl ContainerControl
        {
            get { return _containerControl; }
            set
            {
                _containerControl = value;
                // Le constructeur static permet que le code soit executé en mode design par Visual Studio
                // Ce moyen permet egalement d'appliquer le skin à TOUTE fenêtre. Si on mettait ce code dans le constructeur 
                // statique d'une form de base, on ne pourrait pas visualiser cette même avec le skin car VS interagit d'une facon particuliere avec les form.
                SkinApplicatorHelper.Install(_containerControl);
            }
        }

        public override ISite Site
        {
            get { return base.Site; }
            set
            {
                base.Site = value;

                IDesignerHost host = value?.GetService(typeof(IDesignerHost)) as IDesignerHost;
                IComponent componentHost = host?.RootComponent;
                if (componentHost is ContainerControl)
                    ContainerControl = componentHost as ContainerControl;
            }
        }

        #endregion

    }

    public static class SkinApplicatorHelper
    {
        public static string DefaultSkin { get; } = "DevExpress Style";

        /// <summary>
        /// "Theorical" means that the current devexpress skin may not be synchronized with the value returned by this method.
        /// But soon it will be the case.
        /// </summary>
        public static string GetTheoricalCurrentSkin()
        {
            return _CurrentSkin;
        }
        public static void SetCurrentSkin(string name)
        {
            _CurrentSkin = name;
            if (_CurrentSkin == null)
                return;

            lock (_ControlsByThreadId)
                foreach (var lst in _ControlsByThreadId.Values)
                {
                    var ctl = lst.FirstOrDefault();
                    if (ctl != null && ctl.IsHandleCreated && !ctl.IsDisposed)
                        ctl.BeginInvoke((Action)(() =>
                        {
                            try
                            {
                                UserLookAndFeel.Default.SkinName = _CurrentSkin ?? DefaultSkin;
                            }
                            catch (Exception ex)
                            {
                                ExceptionManager.Instance.NotifyException(ex, UnexpectedExceptionManager.eExceptionKind.Ignored);
                            }
                        }));
                }
        }
        static string _CurrentSkin;


        internal static void Install(ContainerControl control)
        {
            lock (_ControlsByThreadId)
            {
                var lst = _ControlsByThreadId.TryGetValueClass(Thread.CurrentThread.ManagedThreadId);
                if (lst == null)
                {
                    lst = new List<ContainerControl>(1);
                    _ControlsByThreadId.TryAdd(Thread.CurrentThread.ManagedThreadId, lst);
                }
                lst.Add(control);
            }
            control.Disposed += (sender, __) =>
            {
                lock (_ControlsByThreadId)
                {
                    var lst = _ControlsByThreadId.TryGetValueClass(Thread.CurrentThread.ManagedThreadId);
                    Debug.Assert(lst != null);
                    lst.Remove(control);
                    if (lst.Count == 0)
                        _ControlsByThreadId.TryRemove(Thread.CurrentThread.ManagedThreadId, out lst);
                }
            };

            if (_installed_in_current_thread.Value)
                return;
            DevExpress.UserSkins.BonusSkins.Register();

            // from : https://www.devexpress.com/Support/Center/Example/Details/E3920
            //var skinNames = new List<string>();
            //foreach (SkinContainer skin in SkinManager.Default.Skins)
            //    skinNames.Add(skin.SkinName);
            //var styleNames = new List<Tuple<string, LookAndFeelStyle>>();
            //foreach (LookAndFeelStyle style in Enum.GetValues(typeof(LookAndFeelStyle)))
            //    styleNames.Add(Tuple.Create(style.ToString(), style));
            //UserLookAndFeel.Default.Style = styleNames.Last().Item2; // Last is the default "Skin"
            //UserLookAndFeel.Default.SkinName = skinNames.First(); // first is the default "DevExpress Style)

            //SkinManager.Default.RegisterAssembly(typeof(DevExpress.UserSkins.MySkin).Assembly);

            SkinManager.EnableFormSkins();
            SkinManager.EnableMdiFormSkins();

            if (_CurrentSkin != null)
                UserLookAndFeel.Default.SkinName = _CurrentSkin;
            _installed_in_current_thread.Value = true;
        }
        static readonly ConcurrentDictionary<int, List<ContainerControl>> _ControlsByThreadId = new ConcurrentDictionary<int, List<ContainerControl>>();


        static readonly ThreadLocal<bool> _installed_in_current_thread = new ThreadLocal<bool>(() => false);

        // Voir aussi https://github.com/DevExpress-Examples/how-to-create-a-component-which-automatically-create-a-menu-for-all-registered-skins-e2058
        public static void InitSkinPopupMenu(BarSubItem skinMenu, bool includeBonusSkins = true, bool includeThemeSkins = true)
        {
            SkinHelper.InitSkinPopupMenu(skinMenu);

            UserLookAndFeel.Default.StyleChanged += DefaultOnStyleChanged;
            foreach (BarItemLink link in skinMenu.ItemLinks)
                if (link.Caption == DefaultSkin)
                { 
                    link.Caption += "   ( DEFAULT )";
                    break;
                }

            if (!includeBonusSkins)
                foreach (BarItemLink link in skinMenu.ItemLinks)
                    if (link.Caption == "Bonus Skins")
                    {
                        skinMenu.RemoveLink(link);
                        break;
                    }

            if (!includeThemeSkins)
                foreach (BarItemLink link in skinMenu.ItemLinks)
                    if (link.Caption == "Theme Skins")
                    {
                        skinMenu.RemoveLink(link);
                        break;
                    }
        }

        public static event Action<string> OnSkinChanged;

        static void DefaultOnStyleChanged(object o, EventArgs eventArgs)
        {
            SetCurrentSkin(UserLookAndFeel.Default.SkinName);
            OnSkinChanged?.Invoke(UserLookAndFeel.Default.SkinName);
        }
    }
}
