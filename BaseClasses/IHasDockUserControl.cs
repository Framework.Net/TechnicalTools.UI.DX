﻿using System;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;


namespace TechnicalTools.UI.DX
{
    // Indique que le control graphique permet d'afficher des sous control graphique 
    // sous forme d'onglet ou tuile réagencable
    public interface IHasDockUserControl //: IBaseControl
    {
        // En n'autorisant pas les autres type de control on s'assure que dans un CustomXtraUserControl, 
        // on ne pourra pas appeler DockAsTab alors qu'aucun parent ne pourra honorer la demande
        DockPanel DockAsDefault(XtraUserControlBase ctl, string caption = null);
        DockPanel DockAsDefault(BaseEdit be, string caption = null);
    }
}
