﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.XtraEditors;

namespace TechnicalTools.UI.DX
{
    public static class DefaultMessageBox
    {
        public static Func<Func<DialogResult>, DialogResult> Display { get; set; } = defaultMessageBoxDisplay => defaultMessageBoxDisplay();
        public static IWin32Window DefaultOwner { get; set; }

        public static void UseTheseSettingsWhile(Func<Func<DialogResult>, DialogResult> display, IWin32Window defaultOwner, Action action)
        {
            var displayBefore = Display;
            var defaultOwnerBefore = DefaultOwner;

            DefaultOwner = defaultOwner ?? DefaultOwner;
            Display = display ?? Display;
            try
            {
                action();
            }
            finally
            {
                DefaultOwner = defaultOwnerBefore;
                Display = displayBefore;
            }
        }

        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified text.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified owner and text.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text)
        {
            return Display(() => XtraMessageBox.Show(owner, text));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(owner, text, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified text and caption.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified owner, text and caption.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified text, caption and buttons.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption, MessageBoxButtons buttons)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption, buttons));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption, buttons, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified owner, text, caption and buttons.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified text, caption, buttons and icon.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption, buttons, icon));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of the message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption, buttons, icon, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified owner, text, caption, buttons and icon.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons, icon));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons, icon, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified text, caption, buttons, icon and default button.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption, buttons, icon, defaultButton));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(DefaultOwner, text, caption, buttons, icon, defaultButton, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified owner, text, caption, buttons, icon and default button.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons, icon, defaultButton));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons, icon, defaultButton, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings and text.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, owner and text.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, text and caption.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, owner, text and caption.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, text, caption and buttons.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption, MessageBoxButtons buttons)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption, buttons));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">The message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format a message box' text and caption. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption, MessageBoxButtons buttons, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption, buttons, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, owner, text, caption and buttons.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, MessageBoxButtons buttons)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, MessageBoxButtons buttons, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, text, caption, buttons and icon.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption, buttons, icon));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of amessage box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption, buttons, icon, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, owner, text, caption, buttons and icon.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons, icon));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons, icon, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, text, caption, buttons, icon and default button.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption, buttons, icon, defaultButton));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, DefaultOwner, text, caption, buttons, icon, defaultButton, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, text, caption, buttons, icon and default button.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons, icon, defaultButton));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">A value that specifies which buttons to display in the message box.
        /// 
        ///             </param><param name="icon">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies which icon to display in the message box.
        /// 
        /// 
        ///             </param><param name="defaultButton">One of the <see cref="T:System.Windows.Forms.MessageBoxDefaultButton"/> values that specifies the default button for the message box.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons, icon, defaultButton, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">An array of values that specify which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">The <see cref="T:System.Drawing.Icon"/> displayed in the message box.
        /// 
        ///             </param><param name="defaultButton">The zero-based index of the default button.
        /// 
        /// 
        ///             </param><param name="messageBeepSound">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies a system-alert level.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, DialogResult[] buttons, Icon icon, int defaultButton, MessageBoxIcon messageBeepSound, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons, icon, defaultButton, messageBeepSound, allowHtmlText));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified look and feel settings, owner, text, caption, buttons, icon, default button and plays the sound that corresponds to the specified system-alert level.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="lookAndFeel">A <see cref="T:DevExpress.LookAndFeel.UserLookAndFeel"/> object whose properties specify the look and feel of the message box.
        /// 
        ///             </param><param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">An array of values that specify which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">The <see cref="T:System.Drawing.Icon"/> displayed in the message box.
        /// 
        ///             </param><param name="defaultButton">The zero-based index of the default button.
        /// 
        /// 
        ///             </param><param name="messageBeepSound">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies a system-alert level.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(UserLookAndFeel lookAndFeel, IWin32Window owner, string text, string caption, DialogResult[] buttons, Icon icon, int defaultButton, MessageBoxIcon messageBeepSound)
        {
            return Display(() => XtraMessageBox.Show(lookAndFeel, owner, text, caption, buttons, icon, defaultButton, messageBeepSound));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified owner, text, caption, buttons, icon, default button and plays the sound that corresponds to the specified system-alert level.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as a dialog box's top-level window and owner.
        /// 
        ///             </param><param name="text">A string value that specifies the text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the message box's caption.
        /// 
        ///             </param><param name="buttons">An array of values that specify which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">The <see cref="T:System.Drawing.Icon"/> displayed in the message box.
        /// 
        ///             </param><param name="defaultButton">The zero-based index of the default button.
        /// 
        /// 
        ///             </param><param name="messageBeepSound">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies a system-alert level.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, DialogResult[] buttons, Icon icon, int defaultButton, MessageBoxIcon messageBeepSound)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons, icon, defaultButton, messageBeepSound));
        }
        /// <summary>
        /// 
        /// <para>
        /// Displays the XtraMessageBox with the specified settings.
        /// 
        /// </para>
        /// 
        /// </summary>
        /// <param name="owner">An object that serves as the top-level window and owner of a dialog box.
        /// 
        /// 
        ///             </param><param name="text">The text to display in the message box.
        /// 
        ///             </param><param name="caption">A string value that specifies the caption of a message box.
        /// 
        /// 
        ///             </param><param name="buttons">An array of values that specify which buttons to display in the message box.
        /// 
        /// 
        ///             </param><param name="icon">The <see cref="T:System.Drawing.Icon"/> displayed in the message box.
        /// 
        ///             </param><param name="defaultButton">The zero-based index of the default button.
        /// 
        /// 
        ///             </param><param name="messageBeepSound">One of the <see cref="T:System.Windows.Forms.MessageBoxIcon"/> values that specifies a system-alert level.
        /// 
        /// 
        ///             </param><param name="allowHtmlText">A value that specifies whether HTML tags can be used to format the text and caption of a message box. See <see cref="P:DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText"/> to learn more.
        /// 
        /// 
        ///             </param>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// 
        /// </returns>
        public static DialogResult Show(IWin32Window owner, string text, string caption, DialogResult[] buttons, Icon icon, int defaultButton, MessageBoxIcon messageBeepSound, DefaultBoolean allowHtmlText)
        {
            return Display(() => XtraMessageBox.Show(owner, text, caption, buttons, icon, defaultButton, messageBeepSound, allowHtmlText));
        }


    }
}
