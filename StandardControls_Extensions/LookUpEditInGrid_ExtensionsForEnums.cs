﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using DevExpress.XtraEditors.Repository;

using TechnicalTools.Tools;

using Internal = TechnicalTools.UI.DX.LookUpEdit_ExtensionsForObjects;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// Ce Helpeur expose simplifie le remplissage d'un RepositoryItemLookUpEdit dans un GridControl.
    /// </summary>
    public static class LookUpEditInGrid_ExtensionsForEnums
    {
        #region Generic for Objects

        #region RepositoryItemLookUpEdit

        /// <summary>
        /// Remplit un RepositoryItemLookUpEdit avec les valeurs d'un type Enum sous forme d'entier (l'objet du model manipule un entier au lieu d'une valeur type enum)
        /// </summary>
        /// <param name="lue"></param>
        /// <param name="removed_values">Valeurs à ignorer</param>
        public static void FillWithEnumValuesAsIntExceptFor<TEnum>(this RepositoryItemLookUpEdit lue, params TEnum[] removed_values)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValuesAsIntExceptFor works only for enum type !");
            ConfigureRepositoryItemLookUpEditAsDropDownList<Enum, long>(lue, LookUpEdit_ExtensionsForEnums.GetSpecificSourceExcept(removed_values), true);
        }

        /// <summary>
        /// Identique a FillWithEnumValuesAsIntExceptFor mais pour des valeurs spécifiques
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="lue"></param>
        /// <param name="values"></param>
        public static void FillWithEnumValuesAsInt<TEnum>(this RepositoryItemLookUpEdit lue, params TEnum[] values)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValuesAsInt works only for enum type !");
            if (values != null && values.Length > 0)
            {
                var all = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
                values = all.Except(values).ToArray();
            }
            ConfigureRepositoryItemLookUpEditAsDropDownList<Enum, long>(lue, LookUpEdit_ExtensionsForEnums.GetSpecificSourceExcept(values), true);
        }

        /// <summary>
        /// Remplit un RepositoryItemLookUpEdit avec les valeurs d'un type Enum (l'objet du model manipule une valeur fortement typé de type enum
        /// </summary>
        /// <param name="lue"></param>
        /// <param name="removed_values">Valeurs à ignorer</param>
        public static void FillWithEnumValuesExceptFor<TEnum>(this RepositoryItemLookUpEdit lue, params TEnum[] removed_values)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValuesExceptFor works only for enum type !");
            ConfigureRepositoryItemLookUpEditAsDropDownList<TEnum, long>(lue, LookUpEdit_ExtensionsForEnums.GetSpecificSourceExcept(removed_values));
        }

        public static void FillWithEnumValues<TEnum>(this RepositoryItemLookUpEdit lue, params TEnum[] values)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValues works only for enum type !");
            if (values != null && values.Length > 0)
            {
                var all = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
                values = all.Except(values).ToArray();
            }
            ConfigureRepositoryItemLookUpEditAsDropDownList<TEnum, long>(lue, LookUpEdit_ExtensionsForEnums.GetSpecificSourceExcept(values));
        }
        public static void FillWithEnumValues<TEnum>(this RepositoryItemLookUpEdit lue, IReadOnlyCollection<TEnum> allowedValues, bool sort_by_caption = true)
            where TEnum : struct
        {
            Debug.Assert(typeof(TEnum).IsEnum, "FillWithEnumValues works only for enum type !");

            if (allowedValues.Count == 0)
                allowedValues = Enum.GetValues(typeof(TEnum)).Cast<TEnum>().ToArray();
            ConfigureRepositoryItemLookUpEditAsDropDownList<TEnum, long>(lue, LookUpEdit_ExtensionsForEnums.GetSpecificSourceFor(allowedValues, sort_by_caption), sort_by_caption: sort_by_caption);
        }

        #endregion RepositoryItemLookUpEdit

        internal static void ConfigureRepositoryItemLookUpEditAsDropDownList<TEnum, TId>(RepositoryItemLookUpEdit lue, Internal.ItemList<Enum, long> source, bool bind_to_integer = false, bool sort_by_caption = true)
        {
            LookUpEditInGrid_ExtensionsForObjects.ConfigureRepositoryItemLookUpEditAsDropDownList(lue, source, sort_by_caption);
            if (!bind_to_integer)
                lue.ValueMember = GetMemberName.For((Item<object, TId> item) => item.Object);
        }

        // Cette classe est inutile, mais elle sert juste a faire un alias
        internal class Item<TObject, TId> : LookUpEdit_ExtensionsForObjects.Item<TObject, TId>
            where TObject : class
        {
        }

        #endregion Generic for Objects
    }
}
