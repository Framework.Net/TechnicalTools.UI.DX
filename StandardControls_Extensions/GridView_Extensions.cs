﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Drawing;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;


namespace TechnicalTools.UI.DX
{
    public static class GridView_Extensions
    {
        public static void DoWithRowObjectUnderMouse<TRowType>(this GridView view, Action<TRowType> action_on_row)
            where TRowType : class
        {
            DoWithRowObjectUnderMouse<TRowType>(view, (row, _, __) => action_on_row(row));
        }

        public static void DoWithRowObjectUnderMouse<TRowType>(this GridView view, Action<TRowType, GridColumn> action_on_row)
            where TRowType : class
        {
            DoWithRowObjectUnderMouse<TRowType>(view, (row, col, _) => action_on_row(row, col));
        }

        public static void DoWithRowObjectUnderMouse<TRowType>(this GridView view, Action<TRowType, GridColumn, int> action_on_row)
            where TRowType : class
        {
        
            Point mousePointClick = view.GridControl.PointToClient(Control.MousePosition);
            GridHitInfo gridInfo = view.CalcHitInfo(mousePointClick);

            //if (gridInfo.InRow || gridInfo.InRowCell)
            if (!gridInfo.InRowCell)
                return;

            int row_handle = gridInfo.RowHandle;
            if (typeof(TRowType) == typeof(DataRow) ||
                typeof(TRowType).IsSubclassOf(typeof(DataRow)))
            {
                var datarow = (TRowType)(object)view.GetFocusedDataRow();
                if (datarow != null)
                    action_on_row(datarow, gridInfo.Column, row_handle);
            }
            else
            {
                var datarow = (TRowType)view.GetFocusedRow();
                if (datarow != null)
                    action_on_row(datarow, gridInfo.Column, row_handle);
            }
        }

        // From https://www.devexpress.com/Support/Center/Question/Details/A1488
        // A partir de la v14.2, remplacer FindRowHandleByBusinessObject par "GridView.FindRow"
        public static int FindRowHandleByBusinessObject(this GridView gridView, object business_object)
        {
            if (business_object != null)
                for (int i = 0; i < gridView.DataRowCount; i++)
                {
                    var obj = gridView.GetRow(i);
                    if (business_object.Equals(obj))
                        return i;
                }
            return GridControl.InvalidRowHandle;
        }

        public static void FocusedBusinessObject(this GridView gridView, object business_object)
        {
            gridView.FocusedRowHandle = FindRowHandleByBusinessObject(gridView, business_object);
        }

        public static int FindRowHandleByDataRow(this GridView gridView, DataRow data_row)
        {
            if (data_row != null)
                for (int i = 0; i < gridView.DataRowCount; i++)
                {
                    var row = gridView.GetDataRow(i);
                    if (data_row == row)
                        return i;
                }
            return GridControl.InvalidRowHandle;
        }

        /// <summary>
        /// Get selected objects in grid by browsing all rows and group rows, recursively.
        /// The Row / object is considered selected if a least one cell of the row is selected
        /// </summary>
        /// <typeparam name="T">Your business type for datasource item</typeparam>
        /// <param name="view">The view where to get the selected business object behind row</param>
        /// <param name="excludeGroupWhereExplicitAnyItemIsSelected">Smart selection to follow intuition of user mind</param>
        /// <returns></returns>
        public static IEnumerable<T> GetSelectedBusinessObjects<T>(this GridView view, bool excludeGroupWhereExplicitAnyItemIsSelected = true, Func<int, T> getRow = null)
        {
            if (getRow == null)
                getRow = rh => (T)view.GetRow(rh);
            var srs = view.GetSelectedRows();

            var res = srs.Where(rh => rh >= 0).ToHashSet();

            foreach (var rh in srs.Where(rh => rh < 0))
            {
                var subSrs = GetDataRowHandles(view, rh).ToList();
                if (!excludeGroupWhereExplicitAnyItemIsSelected || // if developper disable smartSelection or...
                    !subSrs.Any(r => res.Contains(r))) // If none of sub row of the group is explicitely selected we add all the subset
                    foreach (var r in subSrs)
                        res.Add(r);
            }

            return res.Select(rh => (T)getRow(rh));
        }
        public static IEnumerable<int> GetDataRowHandles(this GridView view, int rowHandle)
        {
            if (rowHandle >= 0)
                return rowHandle.Yield();
            return _GetDataRowHandles(view, rowHandle);
        }
        static IEnumerable<int> _GetDataRowHandles(GridView view, int rowHandle)
        {
            int childsCount = view.GetChildRowCount(rowHandle);
            for (int i = 0; i < childsCount; ++i)
            {
                var h = view.GetChildRowHandle(rowHandle, i);
                if (h >= 0)
                    yield return h;
                else // another group (recursive call) or not (real row) 
                {
                    foreach (var hh in _GetDataRowHandles(view, h))
                        yield return hh;
                }
            }
        }

        /// <summary>
        /// Force le raffraichissement des cellules ainsi que des items ajoute ou supprimé
        /// Lorsqu'on set une datasource qui n'herite pas d'une classe de type container et qui implemente IEnumerable.
        /// La GridControl de DevExpress enumere la datasource et ne se raffraichit pas quand on ajoute ou supprime un item de ce datasource.
        /// Vous devez donc : 
        /// - Soit implementer IBindingList pour que DevExpress puisse ecouter les changement sur la collection
        /// - Soit appeler cette methode pour forcer la gridview a reenumerer les item de la collection pour les tranformer en ligne visible
        ///   (Mais dans ce cas l'etat de la vue : position de l'ascenseur, cellule ayant le focus, etc seront probablement perdu...)
        /// La meilleur solution est de faire en sorte que le type de votre datasource herite du type BindingList
        /// </summary>
        /// <param name="gv"></param>
        public static void RefreshCells(this GridView gv)
        {
            // C'est malheureusement le seul moyen (simple) de forcer un refresh des cellules d'une gridview :(
            // Pour plus compliqué et user friendly (on garde l'etat de la vue:  position de la scrollbar etc), voir : 
            // http://www.devexpress.com/Support/Center/CodeCentral/ViewExample.aspx?exampleId=E776
            // et / ou
            // http://www.devexpress.com/Support/Center/CodeCentral/ViewExample.aspx?exampleId=E1466
            var source = gv.GridControl.DataSource;
            gv.GridControl.DataSource = null;
            gv.GridControl.DataSource = source;
        }


        public static void IncreaseSizeOfEmbeddedNavigatorButtons(this GridControl gc, int factor = 2)
        {
            var copyDefaultImages = (ImageCollection)((ICloneable)gc.EmbeddedNavigator.Buttons.DefaultImageList).Clone();
            copyDefaultImages.ImageSize = new Size(copyDefaultImages.ImageSize.Width * 2, copyDefaultImages.ImageSize.Height * 2);
            gc.EmbeddedNavigator.Buttons.ImageList = copyDefaultImages;
        }


        public static void ExportToXlsxFileViaUI(this GridView gv)
        {
            Sfd.Filter = "Open excel file(*.xls)|*.xls|Open excel file(*.xlsx)|*.xlsx|Open excel file(*.pdf)|*.pdf";
            if (Sfd.ShowDialog() != DialogResult.OK)
                return;

            gv.OptionsPrint.AutoWidth = true;
            var link = new PrintableComponentLink(new PrintingSystem())
            {
                Component = gv.GridControl
            };
            if (Sfd.FileName != "")
            {
                string[] fn = Sfd.FileName.Split('.');
                if (fn.Length == 2)
                {
                    link.CreateDocument();
                    if (fn[1] == "xls")
                        link.PrintingSystem.ExportToXls(Sfd.FileName);
                    if (fn[1] == "xlsx")
                        link.PrintingSystem.ExportToXlsx(Sfd.FileName);
                    if (fn[1] == "pdf")
                        link.PrintingSystem.ExportToPdf(Sfd.FileName);
                }
            }
        }
        static XtraSaveFileDialog Sfd
        {
            get
            {
                return _sfd ?? (_sfd = new XtraSaveFileDialog());
            }
        }
        static XtraSaveFileDialog _sfd;

        public static void PrintViaUI(this GridView gv, string title = null,
                                                        bool landscape = false,
                                                        bool remove_cell_backcolor = false, 
                                                        string current_user = null,
                                                        string client_name = null,
                                                        bool? fit_all_columns_in_one_page = null,
                                                        params GridColumn[] columnToRepeats)
        {
            Debug.Assert(gv != null);
            void resetStyle(object sender, RowCellStyleEventArgs e) => e.Appearance.Options.UseBackColor = false;
            try
            {
                // TODO : En attente de https://www.devexpress.com/Support/Center/Question/Details/T165964
                //        pour proposer un truc un peu mieux quand toutes les colonnes ne peuvent pas tneir sur une page, et pour gerer les colonne fixé a gauche
                Debug.Assert(columnToRepeats == null || columnToRepeats.Length == 0, "Pas encore géré, en attente de Devexpress)");
                // TODO : Envoyer un bug report a DevExpress : Les valeur des noeuds representant la hierarchie dans un treelist sont tronqués dans le PrintPreview
                //        alors qu'au runtime le texte est entierement visible. Même en faisant un BestFitAllColumns
                var compoLink = new CompositeLink
                    {
                        PrintingSystem = new PrintingSystem(),
                        PaperKind = PaperKind.A4,
                        Landscape = landscape
                    };
                // TODO : Sauvegarder le layout de la gridview et le restituer à la fin pour pouvoir faire un BestFitColumns temporaire
                gv.OptionsPrint.SplitCellPreviewAcrossPages = true;
                gv.OptionsPrint.AutoWidth = fit_all_columns_in_one_page ?? gv.OptionsView.ColumnAutoWidth;
                // gridAssoResultView.OptionsPrint.AutoWidth = false;
                gv.OptionsPrint.UsePrintStyles = true;

                if (remove_cell_backcolor)
                {
                    gv.RowCellStyle += resetStyle;
                    //gv.AppearancePrint.Reset();
                    //gv.Appearance.Row.Options.UseBackColor = true;
                    //gv.Appearance.Row.Options.UseTextOptions = false;
                    //gv.OptionsPrint.UsePrintStyles = true;    
                }
            
                compoLink.Links.Clear();
                var printLink = new PrintableComponentLink
                {
                    Component = gv.GridControl
                };
                var phf = (PageHeaderFooter)(compoLink.PageHeaderFooter);
                phf.Header.Font = new Font(phf.Header.Font.FontFamily, 14.0f, FontStyle.Bold); 
                phf.Header.Content.Add(title ?? ""); // Top left
                phf.Header.Content.Add(""); // Top middle, TODO : Afficher les filtres (si il y en a)
                //phf.Header.Content.Add(string.IsNullOrWhiteSpace(client_name) ? "" : client_name + " (Proprietary and Confidential)"); // top right

                phf.Footer.Content.Add("[Date Printed]at[Time Printed]by[User Name]"); // Bottom left
                phf.Footer.Content.Add(""); // Bottom middle
                phf.Footer.Content.Add("Page[Page # of Pages #]"); // Bottom right

                compoLink.Links.Add(printLink);
                compoLink.Margins = new Margins(20, 20, 50, 40); // 40 : pour laisser de la place au texte des headers et footers
                compoLink.CreateDocument();
                compoLink.ShowPreview();
                //compositeLink.PrintingSystem.ExportToPdf(...);
                //System.Diagnostics.Process.Start(sFilePath);
            }
            finally
            {
                gv.RowCellStyle -= resetStyle;
            }

        }


        /// <summary>
        ///A Appeller lors de l'evenement CustomDrawCell, afin de changer la couleur de background des cellules lorsque 
        /// celle si sont mergé et la ligne courante a le focus
        /// </summary>
        /// <remarks>On nimplement pas cette fonctionalite avec une methode permettant d'Enable / de Disable la fonctonalité car il faudrait nous meme nous 
        /// abonner a l'evenement CustomDrawCell.. qui aurait lieu apres le handler du developpeur (code du designer).
        /// On risquerait donc d'overrider la couleur de fond qu'il aurait choisit</remarks>
        /// <param name="e"></param>
        /// <param name="view"></param>
        public static Color GetCellFocusedBackgroundColorOrCurrentOne(this RowCellCustomDrawEventArgs e, GridView view)
        {// from Inspired from https://www.devexpress.com/Support/Center/Example/Details/E1235
            var viewInfo = (GridViewInfo)view.GetViewInfo();
            if (e.RowHandle == view.FocusedRowHandle)
                return viewInfo.PaintAppearance.FocusedRow.BackColor;

            GridCellInfo cell = viewInfo.GetGridCellInfo(e.RowHandle, e.Column);
            if (cell == null)
                return e.Appearance.BackColor;
            if (!cell.IsMerged)
                return e.Appearance.BackColor;
            foreach (GridCellInfo ci in cell.MergedCell.MergedCells)
                if (ci.RowHandle == view.FocusedRowHandle)
                {
                    e.Appearance.Assign(viewInfo.PaintAppearance.FocusedRow);
                    return viewInfo.PaintAppearance.FocusedRow.BackColor;
                }
            return e.Appearance.BackColor;
        }
        public static Color GetCellFocusedBackgroundColorOrCurrentOne(this RowCellStyleEventArgs e, GridView view)
        {// from Inspired from https://www.devexpress.com/Support/Center/Example/Details/E1235
            var viewInfo = (GridViewInfo)view.GetViewInfo();
            if (e.RowHandle == view.FocusedRowHandle)
                return viewInfo.PaintAppearance.FocusedRow.BackColor;

            GridCellInfo cell = viewInfo.GetGridCellInfo(e.RowHandle, e.Column);
            if (cell == null)
                return e.Appearance.BackColor;
            if (!cell.IsMerged)
                return e.Appearance.BackColor;
            foreach (GridCellInfo ci in cell.MergedCell.MergedCells)
                if (ci.RowHandle == view.FocusedRowHandle)
                {
                    e.Appearance.Assign(viewInfo.PaintAppearance.FocusedRow);
                    return viewInfo.PaintAppearance.FocusedRow.BackColor;
                }
            return e.Appearance.BackColor;
        }

        public static GridColumn FindColumnUnderMouse(this GridView view, GridHitInfo hitInfo)
        {
            var gridInfo = (GridViewInfo)view.GetViewInfo();
            GridColumn columnTargeted = null;
            foreach (GridColumn column in view.Columns)
            {
                GridColumnInfoArgs colInfo = gridInfo.ColumnsInfo[column];
                if (colInfo != null &&  // colinfo is null if column is not visible
                    colInfo.Bounds.X <= hitInfo.HitPoint.X && hitInfo.HitPoint.X < colInfo.Bounds.Right)
                {
                    columnTargeted = column;
                    break;
                }
            }

            return columnTargeted;
        }

        public static GridGroupSummaryItem FindSummary(this GridView view, GridColumn column)
        {
            return view.GroupSummary.Cast<GridGroupSummaryItem>().FirstOrDefault(item => item.FieldName == column.FieldName);
        }
    }
}
