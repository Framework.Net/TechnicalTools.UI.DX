﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// Ce Helpeur expose les mêmes fonctionalités, avec la même philosophie, que le helper <see><cref>LookUpEdit_ExtensionsForEnums</cref></see>.
    /// Une fois que vous optez pour l'utilisation de ce helpeur, vous ne DEVEZ plus gérer manuellement 
    /// les propriétés EditValue, SelectedIndex etc de la LookUpEdit.
    /// Ce helpeur encapsule tout ce qui est nécessaire (sinon ajoutez une nouvelle méthode !).
    /// </summary>
    public static class LookUpEdit_ExtensionsForObjects
    {
        #region Generic for Objects

        #region LookUpEdit
        public static void FillWithObjects<TObject>(this LookUpEdit lue, IEnumerable<TObject> objects, bool with_empty = false)
            where TObject : class
        {
            FillWithObjects(lue, objects, o => o.ToString(), o => o, with_empty);
        }
        public static void FillWithTuples(this LookUpEdit lue, IEnumerable<Tuple<int,string>> values, bool with_empty = false)
        {
            lue.FillWithObjects(values, v=> v.Item2, v=>v.Item1, with_empty);
        }

        public static void FillWithTuples(this LookUpEdit lue, IEnumerable<Tuple<string, string>> values, bool with_empty = false)
        {
            lue.FillWithObjects(values, v => v.Item2, v => v.Item1, with_empty);
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static void FillWithValues<TValue>(this LookUpEdit lue, IEnumerable<TValue> values, bool with_empty = false)
            where TValue : struct
        {
            Debug.Assert(!typeof(TValue).IsEnum, "Pour remplir une LookUpEdit avec des enum utilisez FillWithEnumValues* (et les méthodes associées) au lieu de FillWithValues");
            Debug.Assert(!typeof(TValue).IsGenericType || typeof(TValue).GetGenericTypeDefinition() != typeof(KeyValuePair<,>), "Non géré, utilisez le type Tuple<,>");
            Debug.Assert(values != null);

            var strValues = values.Select(v =>
            {
                var res = v.ToString();
                // En convertissant les valeurs en string, on triche pour offrir la fonctionalité FillWithValues.
                // Normalement cela ne foncitonne qu'avec les objets
                // On s'assure donc qu'on peut reconvertir les valeurs dans le type attendu par l'utilisateur sans erreurs");
                Debug.Assert(EqualityComparer<TValue>.Default.Equals(v, (TValue)Convert.ChangeType(res, typeof(TValue))));
                return res;
            });
            FillWithObjects(lue, strValues, str => str, str => (TValue)Convert.ChangeType(str, typeof(TValue)));
        }
        [DebuggerHidden, DebuggerStepThrough]
        public static void FillWithObjects<TObject>(this LookUpEdit lue, IEnumerable<TObject> objects, Func<TObject, string> get_text, bool with_empty = false, bool sorted_by_caption = true)
            where TObject : class
        {
            FillWithObjects(lue, objects, get_text, o => o, with_empty, sorted_by_caption);
        }
        [DebuggerHidden, DebuggerStepThrough]
        public static void FillWithObjects<TObject, TId>(this LookUpEdit lue, IEnumerable<TObject> objects, Func<TObject, string> get_text, Func<TObject, TId> get_id, bool with_empty = false, bool sorted_by_caption = true)
            where TObject : class
        {
            Debug.Assert(get_text != null);

            TId id_to_reselect = default(TId);
            bool already_initialized = false;
            //DebugTools.Warning(lue.EditValue != DBNull.Value, 
            //                   "Attention ! DBNull a ete bindé sur la LookUpEdit " + lue.Name);
            if (lue.EditValue != null && lue.EditValue != DBNull.Value)
                if (lue.IsHandledByExtensions())
                {
                    var lst = lue.Properties.DataSource as IItemList;
                    Debug.Assert(lst != null);
                    if (lue.EditValue != lst.EmptyItem /*&& lue.EditValue != null*/)
                    {
                        Debug.Assert((Item)lue.EditValue != null);
                        object previous_id = lue.EditValue as int? ?? ((Item)lue.EditValue).GetId(); // TODO: Correction temporaire pour remplir une lookupedit dans une colonne lors du showneditor
                        Debug.Assert(previous_id != null);
                        if (previous_id.GetType() == typeof(TObject))
                        {
                            id_to_reselect = (TId)previous_id;
                            already_initialized = true;
                        }
                        // Peut etre a rajouter un jour
                        else if (((Item)lue.EditValue).GetObjectType() == typeof(TObject))
                        {
                            id_to_reselect = (TId)((Item)lue.EditValue).GetId();
                            already_initialized = true;
                        }
                    }
                }
                else if (lue.EditValue.GetType() == typeof (TId))
                {
                    id_to_reselect = (TId)lue.EditValue;
                    already_initialized = true;
                }


            var items = objects.Select(obj => new Item<TObject, TId>() {Id = get_id(obj), Object = obj, Caption = get_text(obj)});
            if (sorted_by_caption)
                    items = items.OrderBy(item => item.Caption);
            var choices = new ItemList<TObject, TId>(items) { IdType = typeof(TId), ObjectType = typeof(TObject) };
            if (with_empty)
            {
                var item = new Item<TObject, TId>() { Id = default(TId), Object = null, Caption = "" };
                choices.EmptyItem = item;
                choices.Insert(0, item);
            }

            ConfigureLookUpEditAsDropDownList(lue, choices, sorted_by_caption);
            lue.Install_InitializationChecks();


            if (already_initialized)
                SelectedValueAsObjectIdSet(lue, id_to_reselect, true);
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static TObject NullValue<TObject>(this LookUpEdit lue)
            where TObject : class
        {
            lue.LookUpEditChecksForObjectUse("NullValue", typeof(TObject));
            var lst = (IItemList)lue.Properties.DataSource;
            //Debug.Assert(lst.AllowEmpty, "Impossible pour la lookupedit " + lue.Name + " de récupérer la valeur de substitution lorsqu'aucune valeur n'est selectionnée " +
            //                              "car vous avez indiqué ne pas autoriser de selection vide lors de son remplissage (argument with_empty : false) !");
            return (TObject)lst.EmptyItem?.GetObject();
        }
        [DebuggerHidden, DebuggerStepThrough]
        public static void NullValueSet<TObject, TId>(this LookUpEdit lue, TObject null_value, TId null_id)
            where TObject : class
        {
            lue.LookUpEditChecksForObjectUse("NullValueSet", typeof(TObject), typeof(TId));
            var lst = (ItemList<TObject, TId>)lue.Properties.DataSource;
            //Debug.Assert(lst.AllowEmpty, "Impossible pour la lookupedit " + lue.Name + " de définir la valeur de substitution lorsqu'aucune valeur n'est selectionnée " +
            //                              "car vous avez indiqué ne pas autoriser de selection vide lors de son remplissage (argument with_empty : false) !");
            if (lst.EmptyItem == null)
                lst.EmptyItem = new Item<TObject, TId>() { Id = null_id, Object = null_value, Caption = lue.Properties.NullText };
            else
            {
                lst.EmptyItem.SetObject(null_value);
                lst.EmptyItem.SetId(null_id);
            }
        }
        [DebuggerHidden, DebuggerStepThrough]
        public static TObject SelectedValueAsObject<TObject>(this LookUpEdit lue)
            where TObject : class
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueAsObject", typeof(TObject));
            if (lue.EditValue == null)
            {
                var lst = (IItemList)lue.Properties.DataSource;
                if (!lst.AllowEmpty)
                    throw new Exception("SelectedValueAsObject: Cannot return a typed value because no value is selected, Please explicitely provide a default value if no item is selected !");
                return (TObject)lst.EmptyItem.GetObject();
            }
            return (TObject)((Item)lue.EditValue).GetObject();
        }
        [DebuggerHidden, DebuggerStepThrough]
        public static TObject SelectedValueAsObject<TObject>(this LookUpEdit lue, TObject defaultValueForNoSelection)
            where TObject : class
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueAsObject", typeof(TObject));
            var lst = (IItemList)lue.Properties.DataSource;
            if (lue.EditValue == null || lue.EditValue == lst.EmptyItem)
                return defaultValueForNoSelection;
            return (TObject)((Item)lue.EditValue).GetObject();
        }
        [DebuggerHidden, DebuggerStepThrough]
        public static TId SelectedValueAsObjectId<TId>(this LookUpEdit lue)
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueAsObjectId", null, typeof(TId));
            if (lue.EditValue == null)
            {
                var lst = (IItemList)lue.Properties.DataSource;
                if (!lst.AllowEmpty)
                    throw new Exception("SelectedValueAsObjectId: Cannot return id of a typed value because no value is selected, Please explicitely provide a default value if no item is selected !");
                return (TId)lst.EmptyItem.GetId();
            }
            return (TId)((Item)lue.EditValue).GetId();
        }
        [DebuggerHidden, DebuggerStepThrough]
        public static TId SelectedValueAsObjectId<TId>(this LookUpEdit lue, TId defaultValueForNoSelection)
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueAsObjectId", null, typeof(TId));
            var lst = (IItemList)lue.Properties.DataSource;
            if (lue.EditValue == null || lue.EditValue == lst.EmptyItem)
                return defaultValueForNoSelection;
            return (TId)((Item)lue.EditValue).GetId();
        }

        [DebuggerHidden][DebuggerStepThrough]
        public static void SelectedValueAsObjectSet<TObject>(this LookUpEdit lue, TObject obj, bool unselect_if_not_selectable = false)
            where TObject : class
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueAsObjectSet");
            var source = (IReadOnlyList<Item>)lue.Properties.DataSource;
            Type objType = source.Any() ? source.First().GetObjectType() : null;

            bool is_simple_type = IsValueType(objType);
            foreach (var item1 in source)
            {
                var item = (IItemWithObject<TObject>)item1;
                if (is_simple_type && obj.Equals(item.Object) || 
                    !is_simple_type && obj == item.Object)
                {
                    lue.EditValue = item;
                    ((IItemList)source).Initialized = true;
                    return;
                }
            }

            if (!unselect_if_not_selectable)
                throw new Exception($"Object \"{obj}\" is not selectable in LookUpEdit \"{lue.Name}\"!");

            ((IItemList)source).Initialized = true;
            lue.UnselectAnyObject();
        }
        static bool IsValueType(Type objType)
        {
            return objType != null &&
                    (objType.IsPrimitive ||
                    objType == typeof(string) ||
                    objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(Nullable<>));
        }


        [DebuggerHidden, DebuggerStepThrough]
        public static void SelectedValueAsObjectIdSet<TId>(this LookUpEdit lue, TId id, bool unselect_if_not_selectable = false)
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueAsObjectIdSet", null, typeof(TId));
            SelectedValueAsObjectIdSet_NoCheck(lue, id, unselect_if_not_selectable);
        }
        [DebuggerHidden, DebuggerStepThrough]
        static void SelectedValueAsObjectIdSet_NoCheck<TId>(this LookUpEdit lue, TId id, bool unselect_if_not_selectable = false)
        {
            var source = lue.Properties.DataSource as IReadOnlyList<Item>;
            Debug.Assert(source != null, "source != null");

            foreach (Item item in source)
                if (!item.IsNullItem() && (id is short ? (object)(int)(short)(object)id : id).Equals(item.GetId()))
                {
                    lue.EditValue = item;
                    ((IItemList)source).Initialized = true;
                    return;
                }
            if (!unselect_if_not_selectable)
                throw new Exception($"No object found with id \"{id}\" is LookUpEdit \"{lue.Name}\"!");

            ((IItemList)source).Initialized = true;
            lue.UnselectAnyObject();
        }

        /// <summary>
        /// Deselectionne la valeur selectionnée dans une lookupedit, la chaine vide est donc affiché et EditValue est à null
        /// 
        /// Cette fonction ne fonctionne que si la LookUpEdit a été remplie avec la méthode FillWithObjects.
        /// </summary>
        public static void UnselectAnyObject(this LookUpEdit lue)
        {
            UnselectAnyObject(lue, true);
        }
        public static void UnselectAnyObject<TObject, TId>(this LookUpEdit lue, TObject default_object_value, TId default_id_value)
            where TObject : class
        {
            NullValueSet(lue, default_object_value, default_id_value);
            UnselectAnyObject(lue, true);
        }
        internal static void UnselectAnyObject(this LookUpEdit lue, bool developper_explicitly_call)
        {
            lue.LookUpEditChecksForObjectUse("UnselectAnyObject");
            lue.EditValue = null;
            var lst = (IItemList)lue.Properties.DataSource;
            lst.Initialized |= lst.AllowEmpty || developper_explicitly_call;
        }

        /// <summary>
        /// Verifie que la lookupedit a ete bien utilisé par le developpeur => remonté d'erreur rapide
        /// </summary>
        /// <param name="lue"></param>
        /// <param name="action_name"></param>
        /// <param name="obj_type"></param>
        /// <param name="id_type"></param>
        [Conditional("DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        static void LookUpEditChecksForObjectUse(this LookUpEdit lue, string action_name, Type obj_type = null, Type id_type = null)
        {
            IItemList lst = lue.Properties.DataSource as IItemList;
            Debug.Assert(lst != null,
                $"Cannot use method \"{action_name}\" on lookupedit {lue.Name} because no extensions method named FillWith* has been used on it!");
            Debug.Assert(id_type == null || lst.IdType == id_type,
                $"Cannot use {(id_type == null ? "" : id_type.Name)} as Id type in lookupedit {lue.Name}, because lookupedit has been filled with object with Id of type {lst.IdType.Name}!" +
                         (lst.IdType == lst.ObjectType ? Environment.NewLine + "You probably did not used method FillWithObject with lambda that extract id from object!" : ""));
            Debug.Assert(obj_type == null || lst.ObjectType == obj_type,
                $"Cannot use {(obj_type == null ? "" : obj_type.Name)} as object type in lookupedit {lue.Name}, because lookupedit has been filled with object of type {lst.ObjectType.Name}!");
        }

        #region Item

        public interface IItem : IConvertible
        {
            object GetObject();
        }

        [DebuggerDisplay("Type<{GetIdType().Name} ==> {GetObjectType().Name}>  Id={GetId()} Caption={Caption}  Object=({GetObject()})")]
        internal abstract class Item : IItem
        {
            public          string Caption { get; set; }
            public abstract object GetObject();
            public abstract object GetId();
            public abstract void SetObject(object value);
            public abstract void SetId(object value);

            public abstract bool IsNullItem();
            public abstract Type GetIdType();
            public abstract Type GetObjectType();

            void EnsureIdType<T>()
            {
                if (GetIdType() != typeof(T))
                    throw new InvalidOperationException();
            }
            #region IConvertible Members

            public TypeCode GetTypeCode()
            {
                return Type.GetTypeCode(GetIdType());
               // throw new NotImplementedException();
            }

            public bool ToBoolean(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public byte ToByte(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public char ToChar(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public DateTime ToDateTime(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public decimal ToDecimal(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public double ToDouble(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public short ToInt16(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public int ToInt32(IFormatProvider provider)
            {
                EnsureIdType<int>();
                return Convert.ToInt32(GetId());
            }

            public long ToInt64(IFormatProvider provider)
            {
                EnsureIdType<long>();
                return Convert.ToInt64(GetId());
            }

            public sbyte ToSByte(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public float ToSingle(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public string ToString(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public object ToType(Type conversionType, IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public ushort ToUInt16(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public uint ToUInt32(IFormatProvider provider)
            {
                EnsureIdType<uint>();
                return Convert.ToUInt32(GetId());
            }

            public ulong ToUInt64(IFormatProvider provider)
            {
                EnsureIdType<ulong>();
                return Convert.ToUInt64(GetId());
            }

            #endregion
        }
        internal interface IItemWithId<out TId>
        {
            TId     Id     { get; }
        }
        internal interface IItemWithObject<out TObject>
            //where TObject : class
        {
            TObject Object { get; }
        }
        internal class Item<TObject, TId> : Item, IItemWithObject<TObject>, IItemWithId<TId>
            where TObject : class
        {
            public TId     Id     { get; set; }
            public TObject Object { get; set; }

            public override bool IsNullItem() { return Object == null; }
            public override Type GetIdType() { return typeof(TId); }
            public override Type GetObjectType() { return typeof(TObject); }

            public override object GetObject() { return Object; }
            public override object GetId() { return Id; }
            public override void SetObject(object value) { Object = (TObject)value; }
            public override void SetId(object value) { Id = value == null ? default(TId) : (TId)value; }
        }

        internal interface IItemList 
        {
            Type IdType      { get; }
            Type ObjectType  { get; }
            bool AllowEmpty  { get; }
            bool Initialized { get; set; }
            Item EmptyItem   { get; set; }
        }

        internal class ItemList<TObject, TId> : List<Item<TObject, TId>>, IItemList
            where TObject : class
        {
            public Type IdType { get; set; }
            public Type ObjectType { get; set; }
            public bool AllowEmpty { get { return EmptyItem != null; } }
            public bool Initialized { get; set; }
            public Item EmptyItem { get; set; }

            public ItemList() { }
            public ItemList(IEnumerable<Item<TObject, TId>> items)
            {
                foreach (var item in items)
                    Add(item);
            }
        }

        #endregion

        #endregion LookUpEdit

        internal static void ConfigureLookUpEditAsDropDownList<TObject, TId>(this LookUpEdit lue, ItemList<TObject, TId> source, bool sorted_by_caption = true)
            where TObject : class
        {
            // Fait de la prevention vis à vis du commentaire qui suit
            if (lue.Properties.DataSource == null) // Cette prevention ne peut avoir lieu que si la lookupedit n'a jamais eté utilisé
            {
                //var frm = lue.FindForm();
                //DebugTools.Warning(string.IsNullOrEmpty(lue.Properties.DisplayMember) && 
                //                   string.IsNullOrEmpty(lue.Properties.ValueMember) && 
                //                   (string.IsNullOrEmpty(lue.Properties.NullText) ||
                //                   lue.Text == lue.Properties.NullText && lue.EditValue == null),
                //                   string.Format("La lookupedit {0} {1} étant rempli à l'aide des extensions,", lue.Name, frm != null ? string.Format("de la form  {0}", frm.GetType().Name) : string.Format("du control {0}", lue.Parent.GetType().Name)) + Environment.NewLine +
                //                                 "les valeurs des propriétes de la lookupedit : \"DisplayMember\", \"ValueMember\" et \"NullText\" doivent être supprimés dans le designer !");
            }
            // On met ces valeurs à null pour forcer devexpress a raffraichir son etat interne (concernant la reflection surtout)
            // De temps en temps les proprietes des controles de devexpress utilisées pour la reflection ne fonctionnent pas si les valeurs ne passent pas de null => valeur non null
            // On le fait donc ici :
            lue.Properties.DisplayMember = null;
            if (lue.Properties.NullText != "[EditValue is null]")
            {
                lue.Properties.NullText = null;
            }
            lue.Properties.ValueMember = null;
            lue.Properties.DataSource = null;

            lue.Properties.DisplayMember = "Caption"; // GetMemberName.For((Item item) => item.Caption);
            lue.Properties.AllowNullInput = source.AllowEmpty ? DefaultBoolean.True : DefaultBoolean.False; // Empeche l'utilisateur de faire "Ctrl - Suppr", on gère le cas de la valeur vide dans la methode FillWith
            //lue.Properties.NullText = ""; // Même si on autorise pas, si on fait un SelectedValueAs*Set(une_valuer, true) la valeur EditValue sera a null !
            lue.Properties.DataSource = source;
            var col = new LookUpColumnInfo(lue.Properties.DisplayMember) { SortOrder = sorted_by_caption ? DevExpress.Data.ColumnSortOrder.Ascending : DevExpress.Data.ColumnSortOrder.None };
            lue.Properties.Columns.Clear();
            lue.Properties.Columns.Add(col);
            lue.Properties.ShowHeader = false;
            lue.Properties.ShowFooter = false;
            lue.Properties.DropDownRows = Math.Min(Math.Max(source.Count, 1), 50); // TODO : utiliser UseDropDownRowsAsMaxCount ?
        }

        // ReSharper disable AccessToModifiedClosure
        // ReSharper disable PossibleNullReferenceException
        // commun a LookUpEdit_ExtensionsForObjects et LookUpEdit_ExtensionsForEnums
        [Conditional("DEBUG")]
        internal static void Install_InitializationChecks(this LookUpEdit lue)
        {
            #region Verification que la lookupedit a bien ete chargé au demarrage
            // Declare les variables en avance afin que les closures fonctionnent (notemment sur les handlers)
            Form frm = null;
            EventHandler on_form_shown = null;
            EventHandler on_parent_changed = null;
            on_form_shown = (_, __) =>
            {
                frm.Shown -= on_form_shown;
                lue.ParentChanged -= on_parent_changed;

                var lst = lue.Properties.DataSource as IItemList;
                //string formName = lue.FindForm() == null ? "???" : lue.FindForm().Name;
                string nl = Environment.NewLine;
                Debug.Assert(lst.Initialized,
                             "Interface incohérente car non alignée avec le modèle de donnée." + nl +
                             "Merci d'initializer explicitement la LookUpEdit \"" + lue.Name + "\" !" + nl +
                             nl +
                             "Pour ce faire, dans Form_Load, ou une méthode équivalente, utilisez :" + nl +
                             " - soit une des methode SelectedValueAs{{Enum|Objects[Id]}}Set(...)," + nl +
                             " - soit une des methodes UnselectedAny{Enum|Object}(...)" + nl +
                             " - soit le binding de TrkBindingManager" + nl +
                             nl +
                             "INFO : Cet assert echoue le plus souvent parce que vous avez bindé une LookUpEdit avec un objet dont le champs bindé est à NULL." + nl +
                             "       Or, lors de l'utilisation de la methode FillWith{Enum|Objects}, via l'argument with_empty, vous n'avez pas explictement autorisé l'ajout d'un item permettant " + nl +
                             "       que cette valeur soit NULLABLE dans l'interface !");
                //if (lue.EditValue == null && !lst.AllowEmpty)
                //    Debug.Assert(lst.EmptyItem != null,
                //                 "La lookupedit \"" + lue.Name + "\" de la form \"" + formName + "\" a ete rempli en specifiant que la valeur null n'etait pas autorisé." + nl +
                //                 "Pourtant, vous avez choisi explicitement de deselectionner l'item actuellement selectionné et afficher une valeur vide." + nl +
                             
                //                 nl +
                //                 "Cas d'utilisation probable: Ce cas peut survenir parce qu'un nouvel objet a ete crée et que la lookupedit permet d'editer un de ces membres." + nl +
                //                 "                            Le membre de cet objet est non nullable mais vous ne souhaitez pas influencer le choix de l'utilisateur en choissant une valeur pour lui." + nl +
                //                 "                            Vous devez neanmoins en choisir pour que le code reste coherent" + nl +
                //                 "" + nl +
                //                 "DONC, Afin de gardez l'interface coherente, voud devez appeler, au choix :" + nl +
                //                 "   " + lue.Name + ".UnselectAny{Enum|Object}(default_value) // INdique explicitement votre volonté de ne pas sélectionner de valeur" + nl +
                //                 "ou bien pour indiquer au binding la valeur par defaut a choisir dans le cas ou vote objet a une valeur null" + nl +
                //                 "   " + lue.Name + ".NullValueSet(...)" + nl +
                //                 "Vous devrez, en plus, dans la méthode GetErrors (a overrider) de l'objet edité, verifier que ce champs a bien été renseigné par l'utilisateur" + nl +
                //                 nl + 
                //                 "Dans le cas ou ce champs peut legitimement etre non rempli (ie cela a du sens foncitonellement parlant)," + nl + 
                //                 "ajouter l'argument true lorsque vous remplisser la lookupedit pour indiquer que vous autoriser la valeur vide.");
            };

            on_parent_changed = (_, __) =>
            {
                if (frm != null && !frm.IsDisposed)
                    frm.Shown -= on_form_shown;
                lue.ParentChanged -= on_parent_changed;
                HandledLookUpEdits.Remove(lue);
                if (!lue.IsDisposed)
                    lue.Install_InitializationChecks();
            };

            frm = lue.FindForm();
            if (frm != null)
                frm.Shown += on_form_shown;
            lue.ParentChanged += on_parent_changed;
            lue.Disposed += (_, __) => HandledLookUpEdits.Remove(lue);
            if (!HandledLookUpEdits.Contains(lue))
                HandledLookUpEdits.Add(lue);
            #endregion
        }
        // ReSharper restore AccessToModifiedClosure
        // ReSharper restore PossibleNullReferenceException

        static readonly HashSet<LookUpEdit> HandledLookUpEdits = new HashSet<LookUpEdit>(); // Garde une trace des lookupedit gérés

        public static bool IsHandledByExtensions(this LookUpEdit lue)
        {
            return lue.Properties.DataSource is IItemList;
        }

        // When you dont know the type used to fill the lookupedit, use this one !
        // Avoid it !
        [Obsolete("Reservé pour le binding", false)]
        public static void SelectedValueForAnyTypeSet(this LookUpEdit lue, object value)
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueForAnyTypeSet");

            var lst = (IItemList)lue.Properties.DataSource;
            if (lst.ObjectType.IsEnum)
            {
                if (value == null)
                    lue.UnselectAnyEnum(false);
                else 
                    lue.SelectedValueAsEnumSet_NoCheck(value, true);
            }
            else 
            {
                if (value == null)
                    lue.UnselectAnyObject(false);
                else if (IsValueType(value.GetType()))
                    lue.SelectedValueAsObjectIdSet_NoCheck(value, true);
                else if (value is Item)
                    lue.SelectedValueAsObjectSet(((Item)value).GetObject(), true);
                else
                    lue.SelectedValueAsObjectSet(value, true);
            }
        }
        [Obsolete("A eviter", false)]
        public static object SelectedValueForAnyType(this LookUpEdit lue)
        {
            lue.LookUpEditChecksForObjectUse("SelectedValueForAnyType");
            var lst = (IItemList)lue.Properties.DataSource;
            return lue.EditValue == null || lue.EditValue == lst.EmptyItem 
                 ? null
                 : lst.ObjectType.IsEnum ? ((Item)lue.EditValue).GetObject()
                                         : ((Item)lue.EditValue).GetId(); // GetId() peut retourner l'id ou bien l'objet si le binding se fait directement via un objet
        }

        #endregion Generic for Objects
    }

}
