﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX
{
    /// <summary>
    /// Outil permettant garder une trace de quel élément de l'IHM représente quel objet.
    /// Les elements graphique peuvent etre quasiment n'importe quel type d'element graphique : Control, UserControl, Form, ...
    /// Les objets metiers peuvent etre de tout type.
    /// Cette classe permet par exemple d'éviter d'ouvrir plusieurs fois le même élément graphique pour un même objet.
    /// Voir aussi <see cref="ControlEditorFinder{TEditor, TBusinessObject}"/> et <see cref="FormEditorFinder{TEditor, TBusinessObject}"/>
    /// </summary>
    /// <typeparam name="TEditor">Type d'element graphique à se souvenir</typeparam>
    /// <typeparam name="TBusinessObject">Type d'objet métier affiché. En general le type de base des objet metier ou une interface de base.</typeparam>
    public class EditorFinder<TEditor, TBusinessObject>
        where TEditor : class, IComponent // IComponent : implementé par System.Windows.Forms.Control et System.ComponentModel.Component
        where TBusinessObject : class
    {
        static readonly TBusinessObject NoObject = null;

        // Recherche un editeur non lié a un objet
        public virtual TEditor FindEditor(Type editing_editor_type)
        {
            var mapping = FindBestMappingFor(editing_editor_type, NoObject);
            return mapping?.Editor;
        }
        // Recherche un editeur lié à un objet
        public virtual TEditor FindEditorFor(Type editing_editor_type, TBusinessObject edited_object)
        {
            Debug.Assert(edited_object != null);
            var mapping = FindBestMappingFor(editing_editor_type, edited_object);
            return mapping?.Editor;
        }
        protected Mapping FindBestMappingFor(Type editing_editor_type, TBusinessObject edited_object)
        {
            var mappings = FindEligibleMappings(editing_editor_type, edited_object);

            // On ne sait pas comment est géré BusinessObjectAreEquivalent dans la classe fille 
            // Plusieurs object different peut etre consideré comme etant les même (même ID...)
            // Dans ce cas, le meilleur éditeur à retourner s'avère être celui dont les objets ont la meme référence sinon, le premier fourni objet fourni:
            return mappings.OrderByDescending(m => m.Object == edited_object)  // Note : false étant trié avant true, on trie en "Descending"
                           .FirstOrDefault();
        }

        protected virtual IEnumerable<Mapping> FindEligibleMappings(Type editing_editor_type, TBusinessObject edited_object)
        {
            // On cherche...
            return _mappings.Where(m => editing_editor_type.IsInstanceOfType(m.Editor) // ...un editeur du même type que celui demandé...
                                     && BusinessObjectAreEquivalent(m.Object, edited_object)); // pour une reference d'objet donné
        }

        // Identifie si deux objets représentent les mêmes donnés.
        protected virtual bool BusinessObjectAreEquivalent(TBusinessObject m, TBusinessObject x)
        {
            return ReferenceEquals(m, x);
        }

        public virtual void RegisterEditor(TEditor editing_editor, TBusinessObject edited_object)
        {
            Debug.Assert(edited_object != null);
            Debug.Assert(FindBestMappingFor(editing_editor.GetType(), edited_object) == null, "Un editeur du même type existe deja pour cet objet !");
            var mapping = new Mapping()
            {
                Object = edited_object,
                Editor = editing_editor
            };
            _mappings.Add(mapping);
            Debug.Assert(FindBestMappingFor(editing_editor.GetType(), edited_object) == mapping, "BusinessObjectAreEquivalent is wrong !");
        }

        public virtual void RegisterEditor(TEditor editing_editor)
        {
            RegisterEditor(editing_editor, NoObject);
        }

        protected virtual void InstallAutoUnregister(TEditor editing_editor)
        {
            // Nothing
        }

        public virtual void UnregisterEditor(TEditor editing_editor, TBusinessObject edited_object)
        {
            Debug.Assert(edited_object != null);
            Mapping mapping = FindBestMappingFor(editing_editor.GetType(), edited_object);
            if (mapping != null)
                _mappings.Remove(mapping);
        }
        public virtual void UnregisterEditor(TEditor editing_editor)
        {
            UnregisterEditor(editing_editor, NoObject);
        }

        readonly List<Mapping> _mappings = new List<Mapping>();

        protected class Mapping
        {
            public TBusinessObject Object { get; set; }
            public TEditor Editor { get; set; }
        }
    }


    /// <summary>
    /// Customise EditorFinder pour les Control
    /// </summary>
    public class ControlEditorFinder<TEditor, TBusinessObject> : EditorFinder<TEditor, TBusinessObject>
        where TEditor : Control
        where TBusinessObject : class
    {
        protected override IEnumerable<Mapping> FindEligibleMappings(Type editing_editor_type, TBusinessObject edited_object)
        {
            return base.FindEligibleMappings(editing_editor_type, edited_object)
                       .Where(mapping => !mapping.Editor.IsDisposed);
        }

        public override void RegisterEditor(TEditor editing_editor, TBusinessObject edited_object)
        {
            base.RegisterEditor(editing_editor, edited_object);
            // Permet de ne pas avoir a appeler UnregisterEditor manuellement à chaque fois
            // Fonctionne pour la majorité des controle. Pour les form il peut y a une exception (voir FormEditorFinder)
            editing_editor.Disposed += (_, __) => UnregisterEditor(editing_editor, edited_object);
        }

        public override void RegisterEditor(TEditor editing_editor)
        {
            base.RegisterEditor(editing_editor);
            // Idem
            editing_editor.Disposed += (_, __) => UnregisterEditor(editing_editor); 
        }
    }

    /// <summary>
    /// Customise EditorFinder pour Form. Unregistered n'a plus besoin d'être appelé sur cette 
    /// </summary>
    public class FormEditorFinder<TForm, TBusinessObject> : ControlEditorFinder<TForm, TBusinessObject>
        where TForm : Form
        where TBusinessObject : class
    {
        public override void RegisterEditor(TForm editing_editor, TBusinessObject edited_object)
        {
            base.RegisterEditor(editing_editor, edited_object);
            // Permet de ne pas avoir a appeler UnregisterEditor manuellement à chaque fois
            // L'evenement "Disposed" n'est a priori pas déclenché pour les Form MDI (CF http://stackoverflow.com/questions/3097364/c-sharp-form-close-vs-form-dispose),
            // on utilise donc l'event Closed.
            editing_editor.Closed += (_, __) => UnregisterEditor(editing_editor, edited_object);
        }

        public override void RegisterEditor(TForm editing_editor)
        {
            base.RegisterEditor(editing_editor);
            // Idem
            editing_editor.Closed += (_, __) => UnregisterEditor(editing_editor);
        }
    }

}
