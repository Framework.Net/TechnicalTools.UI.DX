﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX.Tools
{
    public class TemporaryControlLocker : IDisposable
    {
        readonly Control _container; // The container to block 

        // Enabled states of all control inside container before we lock them
        // These states will be restored when Unblock is called
        readonly Dictionary<Control, ControlState> _ctlStates = new Dictionary<Control, ControlState>();

        sealed class ControlState
        {
            public Control      Owner;

            public bool         EnabledStateToRestore;
            public EventHandler OnEnabledChanged;
        }

        public TemporaryControlLocker(Control container)
        {
            Debug.Assert(container != null);
            _container = container;
            BlockControl(_container);
        }

        void IDisposable.Dispose()
        {
            if (_ctlStates.Count == 0)
                return;
            UnblockControl(_container);
        }


        bool BlockControl(Control ctl)
        {
            if (ctl is ScrollableControl)
            {
                bool itWorked = true;
                foreach (Control subctl in ctl.Controls)
                {
                    itWorked &= BlockControl(subctl);
                    Debug.Assert(itWorked, "Impossible de blocker un control. Etudier et rajouter cette gestion particulière ici si le control est standard." +
                                           "Si ce control est de type ScrollableControl, on peut blocker ses souscontrol)");
                }
                return itWorked;
            }
            else
            {
                _ctlStates.Add(ctl, new ControlState() { Owner = ctl, EnabledStateToRestore = ctl.Enabled, OnEnabledChanged = OnEnabledChange }); // save the state
                ctl.Enabled = false; // disable the control
                ctl.EnabledChanged += OnEnabledChange; // Install a handler to capture the state developpers want if enabled is changed
                ctl.Refresh();
                return !ctl.Enabled;
            }
        }
        void OnEnabledChange(object sender, EventArgs e)
        {
            var ctl = sender as Control;
            _ctlStates[ctl].EnabledStateToRestore = ctl.Enabled; // Capture the new wanted state by a developper
            ctl.EnabledChanged -= OnEnabledChange; // prevent recursivity
            try
            {
                ctl.Enabled = false;
                Debug.Assert(!ctl.Enabled);
            }
            finally
            {
                ctl.EnabledChanged += OnEnabledChange;
            }
        }
        void UnblockControl(Control ctl)
        {
            if (ctl is ScrollableControl)
                foreach (Control subctl in ctl.Controls)
                    UnblockControl(subctl);
            else
            {
                Debug.Assert(ctl == _ctlStates[ctl].Owner);

                ctl.EnabledChanged -= _ctlStates[ctl].OnEnabledChanged;
                ctl.Enabled = _ctlStates[ctl].EnabledStateToRestore;
                _ctlStates.Remove(ctl);
            }
        }
    }

    public class TemporaryControlGroupLocker : IDisposable
    {
        readonly List<TemporaryControlLocker> _group;

        public TemporaryControlGroupLocker(IEnumerable<Control> ctls)
        {
            _group = ctls.Select(ctl => new TemporaryControlLocker(ctl)).ToList();
        }

        void IDisposable.Dispose()
        {
            foreach (var handler in _group)
            {
                IDisposable asIDisposable = handler;
                asIDisposable.Dispose();
            }
        }
    }

}
