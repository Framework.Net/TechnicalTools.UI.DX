﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;


namespace TechnicalTools.UI.DX.Tools
{
    public class MirrorToTextboxTextWriter : TextWriter
    {
        StringBuilder _sb = new StringBuilder(1024);
        TextBox _txt;

        public MirrorToTextboxTextWriter(TextBox txt)
        {
            _txt = txt;
        }

        public override Encoding Encoding
        {
            get { return Encoding.Unicode; }
        }

        public override void Write(char value)
        {
            string str;
            lock (_sb)
            {
                _sb.Append(value);
                if (value != '\n')
                    return;
                str = _sb.ToString();
                _sb.Clear();
            }
            if (_txt.InvokeRequired)
                _txt.BeginInvoke((Action)(() => _txt.Text = str));
            else
                _txt.Text = str;
        }
    }
}
