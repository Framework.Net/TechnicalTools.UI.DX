﻿using System;
using System.Windows.Forms;

namespace TechnicalTools.UI.DX.Tools
{
    public class CursorChange : IDisposable
    {
        readonly Control _ctl;
        readonly bool _showWaitCursorBefore;

        public CursorChange(Control ctl, bool showWaitCursor)
        {
            _ctl = ctl;
            _showWaitCursorBefore = ctl.UseWaitCursor;

            ctl.UseWaitCursor = showWaitCursor;
            Application.DoEvents(); // Afin que le curseur change sinon il n'est pas mis à jour
        }

        public void Dispose()
        {
            _ctl.UseWaitCursor = _showWaitCursorBefore;
            Application.DoEvents(); // Afin que le curseur change sinon il n'est pas mis à jour
        }
    }
}
