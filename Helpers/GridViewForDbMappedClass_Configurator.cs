﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Linq;
using System.Windows.Forms;

using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Model;

using DataMapper;


namespace TechnicalTools.UI.DX.Helpers
{
    public abstract class GridViewForDbMappedClass_Configurator
    {
        protected Type _mappedType;
        protected IDbMapper _mapper;
        
        protected Control _owner;
        protected GridView _view;
        protected GridBand _band; // if view is AccessibleEvents BandedGridView, _band is not null
        // Si les propriétés FieldName des colonne de la grille ont besoin d'etre quelquechose comme "InnerProperty1.InnerProperty2.FinalPropertyToDisplay"
        // _rowTypeInnerPropertyName contientdra "InnerProperty1.InnerProperty2"
        protected string _rowTypeInnerPropertyName;

        private HashSet<GridColumn> _handledColumns; // Make this class cooperative with other columns customization

        protected GridViewForDbMappedClass_Configurator(Type mappedType)
        {
            _mappedType = mappedType;
        }

        public void Install(BandedGridView view, GridBand band, IDbMapper mapper, string rowTypeInnerPropertyName = null)
        {
            _Install(view, band, mapper, rowTypeInnerPropertyName);
        }
        public void Install(GridView view, IDbMapper mapper, string rowTypeInnerPropertyName = null)
        {
            _Install(view, null, mapper, rowTypeInnerPropertyName);
        }
        void _Install(GridView view, GridBand band, IDbMapper mapper, string rowTypeInnerPropertyName)
        {
            if (_view != null)
                Uninstall();
            _view = view;
            _band = band;
            _owner = _view.GridControl;
            _mapper = mapper;
            _rowTypeInnerPropertyName = rowTypeInnerPropertyName?.Trim();

            if (_rowTypeInnerPropertyName != null)
                Debug.Assert(!_rowTypeInnerPropertyName.Contains("."), "Not handled yet !");

            _view.BeginUpdate();

            _view.OptionsBehavior.Editable = false;
            _view.OptionsView.ColumnAutoWidth = false;
            _view.OptionsView.ShowAutoFilterRow = true;
            _view.OptionsView.RowAutoHeight = true;
            _view.OptionsView.ShowGroupPanel = true;
            _view.GridControl.ShowOnlyPredefinedDetails = true;
            _view.OptionsDetail.AllowExpandEmptyDetails = true; // Evite que MasterRowGetChildList soit appelé deux fois

            var colBefore = _view.Columns.ToList();
            BuildColumns(_view);
            var colAfter = _view.Columns.ToList();
            _handledColumns = colAfter.Except(colBefore).ToHashSet();

            SmartOrderColumns(view, _rowTypeInnerPropertyName);

            _view.EndUpdate();
        }

        void Uninstall()
        {
            _view = null;
        }

        protected virtual void BuildColumns(GridView view) // take gridview again because _view can be the model and not the real one if it is a subview
        {
            view.BeginUpdate();

            if (_band != null)
            {
                foreach(GridColumn col in _band.Columns.OfType<GridColumn>().ToList())
                    view.Columns.Remove(col);
                _band.Columns.Clear();
            }
            else
                view.Columns.Clear();

            var mtable = _mapper.GetTableMapping(_mappedType);
            foreach (var field in mtable.AllFields)
                AddColumn(view, field.PropertyInfo);

            view.CustomUnboundColumnData -= View_CustomUnboundColumnData;
            view.CustomUnboundColumnData += View_CustomUnboundColumnData;

            view.EndUpdate();
        }
        protected GridColumn AddColumn(GridView view, PropertyInfo pi)
        {
            string fieldname = (_rowTypeInnerPropertyName == null ? "" : _rowTypeInnerPropertyName + ".")
                             + pi.Name;
            GridColumn col;
            if (_band == null)
                col = new GridColumn() { Visible = true, FieldName = fieldname, UnboundType = pi.PropertyType.ToDevExpressGridViewUnboundType() };
            else
            {
                col = new BandedGridColumn() { Visible = true, FieldName = fieldname, UnboundType = pi.PropertyType.ToDevExpressGridViewUnboundType() };
                _band.Columns.Add((BandedGridColumn)col);
            }
            view.Columns.Add(col);
            if (_rowTypeInnerPropertyName != null)
                col.Caption = col.GetCaption().Substring(_rowTypeInnerPropertyName.Length + 1);
            if (pi.PropertyType.RemoveNullability() == typeof(CurrencyISO))
                col.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True; // allow user to sort and group by this column
            return col;
        }

        public virtual void SmartOrderColumns(GridView view, string innerPropertyPrefix = null)
        {
            //innerPropertyPrefix = innerPropertyPrefix == null ? null : innerPropertyPrefix + ".";

            //view.Columns[innerPropertyPrefix + nameof(ItemType.Id)].VisibleIndex = 0;
        }

        private void View_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Row == null)
                return; // TODO : Bug DX : Se produit quand les lignes du bas disparaissent
            Debug.Assert(!e.IsSetData, "Dont know how to handle setter yet ! Please disable the editability");
            if (e.IsGetData && _handledColumns.Contains(e.Column))
                e.Value = ReadValue(e.Row, e.Column.FieldName);
        }

        protected abstract object ReadValue(object row, string fieldName);

        // This method just exist because some mapped type are variant and specific to some database
        //void UpdateAccessors<TRealMappedType>();
    }
    public class GridViewForDbMappedClass_Configurator<TMappedType> : GridViewForDbMappedClass_Configurator
        where TMappedType : class, IAutoMappedDbObject
    {

        readonly Dictionary<string, Func<TMappedType, object>> _fastPropertyAccess = new Dictionary<string, Func<TMappedType, object>>();

        Func<object, TMappedType> _getInnerObject;

        public GridViewForDbMappedClass_Configurator()
            : base(typeof(TMappedType))
        {
        }


        protected override void BuildColumns(GridView view)
        {
            base.BuildColumns(view);

            var mtable = _mapper.GetTableMapping(_mappedType);
            foreach (var field in mtable.AllFields)
            {
                string fieldname = (_rowTypeInnerPropertyName == null ? "" : _rowTypeInnerPropertyName + ".")
                                 + field.PropertyInfo.Name;
                _fastPropertyAccess.Add(fieldname, field.GetDirectGetAccessor<TMappedType>());
            }
        }

        // This method just exist because some mapped type are variant and specific to some database
        public void UpdateAccessors<TRealMappedType>()
            where TRealMappedType : TMappedType
        {
            var mtable = _mapper.GetTableMapping(typeof(TRealMappedType));
            foreach (var field in mtable.AllFields)
            {
                string fieldname = (_rowTypeInnerPropertyName == null ? "" : _rowTypeInnerPropertyName + ".")
                                 + field.PropertyInfo.Name;
                _fastPropertyAccess[fieldname] = obj => field.GetDirectGetAccessor<TRealMappedType>();
            }
        }
        

        //private void View_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        //{
        //    Debug.Assert(!e.IsSetData, "Dont know how to handle setter yet ! Please disable the editability");
        //    if (e.IsGetData)
        //    {
        //        if (_getInnerObject == null)
        //        {
        //            var pi = e.Row.GetType().GetProperty(_rowTypeInnerPropertyName);
        //            _getInnerObject = obj => (TMappedType)pi.GetValue(obj);
        //        }
        //        e.Value = _fastPropertyAccess[e.Column.FieldName](_getInnerObject(e.Row));
        //    }
                
        //}
        protected override object ReadValue(object row, string fieldName)
        {
            if (_getInnerObject == null)
                if (string.IsNullOrWhiteSpace(_rowTypeInnerPropertyName))
                    _getInnerObject = obj => (TMappedType)obj;
                else
                {
                    var pi = row.GetType().GetProperty(_rowTypeInnerPropertyName);
                    _getInnerObject = obj => (TMappedType)pi.GetValue(obj);
                }

            var mappedobj = _getInnerObject(row);
            return ReadValue(mappedobj, fieldName);
        }
        protected virtual object ReadValue(TMappedType item, string fieldName)
        {
            return item == null
                 ? null
                 : _fastPropertyAccess[fieldName](item);
        }
    }
}
