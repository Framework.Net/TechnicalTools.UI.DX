﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Data;
using TechnicalTools.Extensions.Data;

namespace TechnicalTools.UI.DX.Helpers
{
    public static class TreeListHelper
    {
        #region Default intuitive formatting of columns

        public static void HideTechnicalProperties(TreeList tl, Type type = null)
        {
            if (type == null)
            {
                if (((tl.DataSource as IList)?.Count ?? 0) == 0)
                    return;
                type = ((IList)tl.DataSource)[0].GetType();
            }
            HideTechnicalPropertiesImmediately(tl, type);
        }
        internal static void HideTechnicalPropertiesImmediately(TreeList tl, Type type)
        {
            var properties = type.GetProperties();
            Debug.Assert(tl.Columns.Count > 0);
            foreach (TreeListColumn col in tl.Columns)
            {
                var prop = properties.FirstOrDefault(p => p.Name == col.FieldName);
                if (prop == null) // unbound column ?!
                    continue;
                if (prop.GetCustomAttributes(typeof(IsTechnicalPropertyAttribute), true).Length > 0)
                    col.Visible = false;
            }
        }

        public static void SetDefaultColumnFormatting(TreeList tl)
        {
            Debug.Assert(tl.DataSource != null, "L'utilisation de GridColumn.ColumnType ne peut fonctionner que si la datasource est setté");
            tl.BeginUpdate();
            foreach (TreeListColumn col in tl.Columns)
                SetDefaultColumnFormatting(col);
            tl.ShownEditor -= view_ShownEditor;
            tl.ShownEditor += view_ShownEditor;
            tl.EndUpdate();
        }
        public static void SetDefaultColumnFormatting(TreeListColumn col)
        {
            // TODO : How to apply to treelist, this code was for GridColumn
            //if (col.DisplayFormat.FormatString.Length == 0)
            //    if (col.ColumnType == typeof(DateTime) || col.ColumnType == typeof(DateTime?))
            //    {
            //        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            //        col.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            //    }
            //    else if (col.ColumnType == typeof(decimal) || col.ColumnType == typeof(decimal?))
            //    {
            //        col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            //        col.DisplayFormat.FormatString = "n2";
            //    }
            col.OptionsFilter.AutoFilterCondition = Type_Extensions.NumericTypesWithNullable.Contains(col.ColumnType)
                                                  ? AutoFilterCondition.Equals
                                                  : AutoFilterCondition.Contains;
            if (DxVersion.Major >= 17) // was not mandatory for version 16.1.5, but is for version 17.1.12 (for version in the range, exclusive, I don't know)
                col.FilterMode = Type_Extensions.NumericTypesWithNullable.Contains(col.ColumnType) ? ColumnFilterMode.Value : ColumnFilterMode.DisplayText;
        }
        public static readonly Version DxVersion = typeof(TreeList).Assembly.GetName(true).Version;

        static void view_ShownEditor(object sender, EventArgs e)
        {
            var view = (TreeList)sender;
            if (view.ActiveEditor is GridLookUpEdit grdLue)
            {
                grdLue.Popup += (_, __) =>
                {
                    var cellView = grdLue.Properties.View;
                    GridViewHelper.SetDefaultColumnFormatting(cellView);
                    if (cellView.OptionsView.ShowAutoFilterRow && cellView.Columns.Count > 0)
                    {
                        cellView.FocusedRowHandle = GridControl.AutoFilterRowHandle;
                        cellView.FocusedColumn = cellView.Columns[0];
                    }
                };
            }

        }
        #endregion Default intuitive formatting of columns


        public static UnboundColumnType ToDevExpressTreeListUnboundType(this Type t)
        {
            // Note: Code is sync with GridViewHelper.ToDevExpressTreeListUnboundType
            if (t.IsIntegerTypeOrNullable())
                return UnboundColumnType.Integer;
            if (t == typeof(string) || t == typeof(char) || t == typeof(char?))
                return UnboundColumnType.String;
            if (t.IsNumericTypeOrNullable())
                return UnboundColumnType.Decimal;
            if (t == typeof(bool) || t == typeof(bool?))
                return UnboundColumnType.Boolean;
            if (t == typeof(DateTime) || t == typeof(DateTime?) ||
                t == typeof(TimeSpan) || t == typeof(TimeSpan?))
                return UnboundColumnType.DateTime;
            return UnboundColumnType.Object;
        }
    }
}
