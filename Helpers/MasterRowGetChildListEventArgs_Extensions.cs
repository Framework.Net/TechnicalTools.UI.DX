﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;


namespace TechnicalTools.UI.DX.Helpers
{
    // Inspired from // From https://www.devexpress.com/Support/Center/Question/Details/T496636/gridview-how-to-display-an-indicator-in-the-detail-view-to-indicate-that-detail-data-is
    public static class MasterRowGetChildListEventArgs_Extensions
    {
        public static BindingList<TItem> SetChildListDefered<TItem>(this GridView masterView, string actionName, Func<IEnumerable<TItem>> getChildItems)
        {
            Debug.Assert(masterView.OptionsDetail.AllowExpandEmptyDetails, "Required because we return an empty datasource not yet loaded!");

            var details = new BindingList<TItem>();
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler((sender, e) =>
            {
                var items = getChildItems();
                masterView.GridControl.BeginInvoke((Action)(() =>
                {
                    details.AddRange(items);
                }));
            });
            worker.RunWorkerAsync(null);
            return details;
        }

        public static BindingList<TItem> SetChildListDefered<TItem>(this GridView masterView, MasterRowGetChildListEventArgs e, string actionName, Func<IEnumerable<TItem>> getChildItems)
        {
            var blst = new BindingList<TItem>();
            var detailView = (GridView)masterView.GetDetailView(e.RowHandle, e.RelationIndex);
            if (detailView == null)
            {
                //var frm = masterView.GridControl.FindForm();
                // TODO : Demander à devexpress
                //frm.SuspendDrawing(); // ca change rien 
                masterView.GridControl.SuspendDrawing();
                try
                {
                    masterView.GridControl.FindForm().ShowBusyWhileDoingUIWorkInPlace(actionName, pr =>
                    {
                        var lst = getChildItems();
                        blst.AddRange(lst);
                    });
                }
                finally
                {
                    masterView.GridControl.ResumeDrawing();
                    //frm.ResumeDrawing();
                }
            }
            else
            {
                Tuple<Task, DrawHelper> taskAndDrawer = taskByDetailView.GetValue(detailView, _ => Tuple.Create((Task)Task.Run(getChildItems), new DrawHelper(detailView)));
                var task = (Task<IEnumerable<TItem>>)taskAndDrawer.Item1;
                task.ContinueWith(t => detailView.GridControl.BeginInvoke((Action)(() =>
                {
                    if (t.Status != TaskStatus.RanToCompletion)
                        return;
                    blst.AddRange(t.Result);
                })));

                taskAndDrawer.Item2.Start();
            }
            return blst; 
        }
        static readonly ConditionalWeakTable<GridView, Tuple<Task, DrawHelper>> taskByDetailView = new ConditionalWeakTable<GridView, Tuple<Task, DrawHelper>>();
    
        class DrawHelper
        {
            readonly GridView _detailView;
            readonly AnimatedGifImageHelper _animatedGifImageHelper = new AnimatedGifImageHelper();
            readonly System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer { Interval = 100 };

            public DrawHelper(GridView detailView)
            {
                _detailView = detailView;
                _detailView.Disposed += _detailView_Disposed;
                _detailView.CustomDrawGroupPanel += detailView_CustomDrawGroupPanel;
                _timer.Tick += OnTick;
                _timer.Start();
            }
            public void Start()
            {
                _timer.Start();
            }

            public void Stop()
            {
                _timer.Stop();
                _detailView.InvalidateGroupPanel();
            }

            void StopAndUnwireEvents()
            {
                Stop();
                _detailView.Disposed -= _detailView_Disposed;
                _detailView.CustomDrawGroupPanel -= detailView_CustomDrawGroupPanel;
                _timer.Tick -= OnTick;
                _timer.Dispose();
            }
            void _detailView_Disposed(object sender, EventArgs e)
            {
                StopAndUnwireEvents();
            }

            void OnTick(object sender, EventArgs e)
            {
                if (_detailView.IsDisposing || (_detailView.GridControl?.IsDisposed ?? false))
                    return;
                _detailView.InvalidateGroupPanel();
            }

            private void detailView_CustomDrawGroupPanel(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
            {
                if (_timer.Enabled)
                {
                    e.DefaultDraw();
                    e.Graphics.DrawImage(_animatedGifImageHelper.GetNextFrame(), new Point(e.Bounds.Location.X + 300, e.Bounds.Location.Y));
                    e.Handled = true;
                }
            }
        }

        class AnimatedGifImageHelper
        {
            public AnimatedGifImageHelper()
            {
                gif = (Image)LoadingGif.Clone();
                frameDimension = new FrameDimension(gif.FrameDimensionsList[0]);
                frameCount = gif.GetFrameCount(frameDimension);
            }
            readonly FrameDimension frameDimension;
            readonly Image          gif;
            readonly int            frameCount;
                     int            currentFrame = -1;

            public Image GetFrame(int index)
            {
                gif.SelectActiveFrame(frameDimension, index);
                return gif.Clone() as Image;
            }

            public Image GetNextFrame()
            {
                currentFrame++;
                if (currentFrame >= frameCount || currentFrame < 1)
                    currentFrame = 0;

                return GetFrame(currentFrame);
            }


            public readonly Image LoadingGif = Image.FromStream(new MemoryStream(Convert.FromBase64String(LoadingGifInBase64)));
            // const string generated with :
            // byte[] bytes = File.ReadAllBytes("path to the gif file");
            // string file = Convert.ToBase64String(bytes);
            const string LoadingGifInBase64 = "R0lGODlhIAAgAPMAAP////+qAJXBLri5H52/Kqy8JOKwDdC0FIvDMoTENZq/LO6uB/yrAf+qAP+qAP+qACH5BAkKAAAAIf4aQ3JlYXRlZCB3aXRoIGFqYXhsb2FkLmluZm8AIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAIAAgAAAE5xDISWlhperN52JLhSSdRgwVo1ICQZRUsiwHpTJT4iowNS8vyW2icCF6k8HMMBkCEDskxTBDAZwuAkkqIfxIQyhBQBFvAQSDITM5VDW6XNE4KagNh6Bgwe60smQUB3d4Rz1ZBApnFASDd0hihh12BkE9kjAJVlycXIg7CQIFA6SlnJ87paqbSKiKoqusnbMdmDC2tXQlkUhziYtyWTxIfy6BE8WJt5YJvpJivxNaGmLHT0VnOgSYf0dZXS7APdpB309RnHOG5gDqXGLDaC457D1zZ/V/nmOM82XiHRLYKhKP1oZmADdEAAAh+QQJCgAAACwAAAAAIAAgAAAE6hDISWlZpOrNp1lGNRSdRpDUolIGw5RUYhhHukqFu8DsrEyqnWThGvAmhVlteBvojpTDDBUEIFwMFBRAmBkSgOrBFZogCASwBDEY/CZSg7GSE0gSCjQBMVG023xWBhklAnoEdhQEfyNqMIcKjhRsjEdnezB+A4k8gTwJhFuiW4dokXiloUepBAp5qaKpp6+Ho7aWW54wl7obvEe0kRuoplCGepwSx2jJvqHEmGt6whJpGpfJCHmOoNHKaHx61WiSR92E4lbFoq+B6QDtuetcaBPnW6+O7wDHpIiK9SaVK5GgV543tzjgGcghAgAh+QQJCgAAACwAAAAAIAAgAAAE7hDISSkxpOrN5zFHNWRdhSiVoVLHspRUMoyUakyEe8PTPCATW9A14E0UvuAKMNAZKYUZCiBMuBakSQKG8G2FzUWox2AUtAQFcBKlVQoLgQReZhQlCIJesQXI5B0CBnUMOxMCenoCfTCEWBsJColTMANldx15BGs8B5wlCZ9Po6OJkwmRpnqkqnuSrayqfKmqpLajoiW5HJq7FL1Gr2mMMcKUMIiJgIemy7xZtJsTmsM4xHiKv5KMCXqfyUCJEonXPN2rAOIAmsfB3uPoAK++G+w48edZPK+M6hLJpQg484enXIdQFSS1u6UhksENEQAAIfkECQoAAAAsAAAAACAAIAAABOcQyEmpGKLqzWcZRVUQnZYg1aBSh2GUVEIQ2aQOE+G+cD4ntpWkZQj1JIiZIogDFFyHI0UxQwFugMSOFIPJftfVAEoZLBbcLEFhlQiqGp1Vd140AUklUN3eCA51C1EWMzMCezCBBmkxVIVHBWd3HHl9JQOIJSdSnJ0TDKChCwUJjoWMPaGqDKannasMo6WnM562R5YluZRwur0wpgqZE7NKUm+FNRPIhjBJxKZteWuIBMN4zRMIVIhffcgojwCF117i4nlLnY5ztRLsnOk+aV+oJY7V7m76PdkS4trKcdg0Zc0tTcKkRAAAIfkECQoAAAAsAAAAACAAIAAABO4QyEkpKqjqzScpRaVkXZWQEximw1BSCUEIlDohrft6cpKCk5xid5MNJTaAIkekKGQkWyKHkvhKsR7ARmitkAYDYRIbUQRQjWBwJRzChi9CRlBcY1UN4g0/VNB0AlcvcAYHRyZPdEQFYV8ccwR5HWxEJ02YmRMLnJ1xCYp0Y5idpQuhopmmC2KgojKasUQDk5BNAwwMOh2RtRq5uQuPZKGIJQIGwAwGf6I0JXMpC8C7kXWDBINFMxS4DKMAWVWAGYsAdNqW5uaRxkSKJOZKaU3tPOBZ4DuK2LATgJhkPJMgTwKCdFjyPHEnKxFCDhEAACH5BAkKAAAALAAAAAAgACAAAATzEMhJaVKp6s2nIkolIJ2WkBShpkVRWqqQrhLSEu9MZJKK9y1ZrqYK9WiClmvoUaF8gIQSNeF1Er4MNFn4SRSDARWroAIETg1iVwuHjYB1kYc1mwruwXKC9gmsJXliGxc+XiUCby9ydh1sOSdMkpMTBpaXBzsfhoc5l58Gm5yToAaZhaOUqjkDgCWNHAULCwOLaTmzswadEqggQwgHuQsHIoZCHQMMQgQGubVEcxOPFAcMDAYUA85eWARmfSRQCdcMe0zeP1AAygwLlJtPNAAL19DARdPzBOWSm1brJBi45soRAWQAAkrQIykShQ9wVhHCwCQCACH5BAkKAAAALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiRMDjI0Fd30/iI2UA5GSS5UDj2l6NoqgOgN4gksEBgYFf0FDqKgHnyZ9OX8HrgYHdHpcHQULXAS2qKpENRg7eAMLC7kTBaixUYFkKAzWAAnLC7FLVxLWDBLKCwaKTULgEwbLA4hJtOkSBNqITT3xEgfLpBtzE/jiuL04RGEBgwWhShRgQExHBAAh+QQJCgAAACwAAAAAIAAgAAAE7xDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfZiCqGk5dTESJeaOAlClzsJsqwiJwiqnFrb2nS9kmIcgEsjQydLiIlHehhpejaIjzh9eomSjZR+ipslWIRLAgMDOR2DOqKogTB9pCUJBagDBXR6XB0EBkIIsaRsGGMMAxoDBgYHTKJiUYEGDAzHC9EACcUGkIgFzgwZ0QsSBcXHiQvOwgDdEwfFs0sDzt4S6BK4xYjkDOzn0unFeBzOBijIm1Dgmg5YFQwsCMjp1oJ8LyIAACH5BAkKAAAALAAAAAAgACAAAATwEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GGl6NoiPOH16iZKNlH6KmyWFOggHhEEvAwwMA0N9GBsEC6amhnVcEwavDAazGwIDaH1ipaYLBUTCGgQDA8NdHz0FpqgTBwsLqAbWAAnIA4FWKdMLGdYGEgraigbT0OITBcg5QwPT4xLrROZL6AuQAPUS7bxLpoWidY0JtxLHKhwwMJBTHgPKdEQAACH5BAkKAAAALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GAULDJCRiXo1CpGXDJOUjY+Yip9DhToJA4RBLwMLCwVDfRgbBAaqqoZ1XBMHswsHtxtFaH1iqaoGNgAIxRpbFAgfPQSqpbgGBqUD1wBXeCYp1AYZ19JJOYgH1KwA4UBvQwXUBxPqVD9L3sbp2BNk2xvvFPJd+MFCN6HAAIKgNggY0KtEBAAh+QQJCgAAACwAAAAAIAAgAAAE6BDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfYIDMaAFdTESJeaEDAIMxYFqrOUaNW4E4ObYcCXaiBVEgULe0NJaxxtYksjh2NLkZISgDgJhHthkpU4mW6blRiYmZOlh4JWkDqILwUGBnE6TYEbCgevr0N1gH4At7gHiRpFaLNrrq8HNgAJA70AWxQIH1+vsYMDAzZQPC9VCNkDWUhGkuE5PxJNwiUK4UfLzOlD4WvzAHaoG9nxPi5d+jYUqfAhhykOFwJWiAAAIfkECQoAAAAsAAAAACAAIAAABPAQyElpUqnqzaciSoVkXVUMFaFSwlpOCcMYlErAavhOMnNLNo8KsZsMZItJEIDIFSkLGQoQTNhIsFehRww2CQLKF0tYGKYSg+ygsZIuNqJksKgbfgIGepNo2cIUB3V1B3IvNiBYNQaDSTtfhhx0CwVPI0UJe0+bm4g5VgcGoqOcnjmjqDSdnhgEoamcsZuXO1aWQy8KAwOAuTYYGwi7w5h+Kr0SJ8MFihpNbx+4Erq7BYBuzsdiH1jCAzoSfl0rVirNbRXlBBlLX+BP0XJLAPGzTkAuAOqb0WT5AH7OcdCm5B8TgRwSRKIHQtaLCwg1RAAAOw==";
        }
    }
}
