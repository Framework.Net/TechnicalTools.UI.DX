﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Tools;


namespace TechnicalTools.UI.DX.Helpers
{
    public static class GridViewHelper
    {
        /// <summary>
        /// Add all public properties of type <paramref name="type"/> (and its base types) according to some attributes to the GridView
        /// or the BandedGridView (on the specified band or the last one).
        /// Properties are already formatted.
        ///
        /// We Re-implement devexpress "view.PopulateColumns(object list)" 
        /// because it does not work for BandedGridView
        /// </summary>
        /// <param name="view"></param>
        /// <param name="type"></param>
        /// <param name="band"></param>
        /// <param name="hideTechnicalColumns">Tell if we hide all technical properties</param>
        /// <returns>The columns created</returns>
        public static IEnumerable<GridColumn> PopulateColumnsFromDataAnnotations(this GridView view, Type type, GridBand band = null, bool hideTechnicalColumns = true)
        {
            var columnData = new List<ColumnData>();
            foreach (var prop in type.GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (!prop.CanRead)
                    continue;
                var browsableAtt = prop.GetCustomAttribute<BrowsableAttribute>();
                if (browsableAtt != null && !browsableAtt.Browsable)
                    continue;
                var technicalAtt = prop.GetCustomAttribute<IsTechnicalPropertyAttribute>();
                if (technicalAtt != null && technicalAtt.HideFromUser)
                    continue;

                var att = prop.GetCustomAttribute<DisplayAttribute>();
                var forceHide = hideTechnicalColumns && technicalAtt != null;
                columnData.Add(new ColumnData()
                {
                    fieldName = prop.Name,
                    visibleIndex = forceHide ? -1
                                 : (att?.GetOrder() ?? DefaultColumnDisplayOrder),
                    visibleByDefault = !forceHide && (att == null || att.GetOrder() >= 0),
                    caption = att?.Name,
                    toolTip = att?.Description,
                    // Allow to sort and group the column when it's object (Business Object)
                    allowSort = typeof(IComparable).IsAssignableFrom(prop.PropertyType)
                });
            }
            columnData = columnData.OrderBy(c => c.visibleIndex).ToList();

            var gv = new GridView();
            var bview = view as BandedGridView;
            if (band == null && bview != null)
            {
                if (bview.Bands.Count == 0)
                    bview.Bands.AddBand(" ");
                band = bview.Bands.LastOrDefault();
            }
            view.BeginUpdate();
            foreach (var cd in columnData)
            {
                // Let "Columns" creates the good object type (a class that inherits GridColumn)
                cd.column = view.Columns.AddField(cd.fieldName);
                if (cd.caption != null)
                    cd.column.Caption = cd.caption;
                if (cd.toolTip != null)
                    cd.column.ToolTip = cd.toolTip;
                if (cd.allowSort)
                    cd.column.OptionsColumn.AllowSort = DefaultBoolean.True;

                if (bview == null) // for EnhancedGridView
                    cd.column.AddColumnAtRight(view);
                else // for EnhancedBandedGridView
                    cd.column.AddColumnAtRight(band);
            }
            foreach (var cd in columnData)
                if (!cd.visibleByDefault)
                    cd.column.Visible = false;
            var columns = columnData.Select(cd => cd.column).ToList();
            GridViewHelper.SetDefaultColumnFormatting(view);
            view.EndUpdate();
            return columns;
        }
        public static int DefaultColumnDisplayOrder { get; } = 9999;
        class ColumnData
        {
            public string fieldName;
            public string caption;
            public string toolTip;
            public int visibleIndex;
            public bool visibleByDefault;
            public bool allowSort;
            public GridColumn column;
        }

        #region Default intuitive formatting of columns

        public static void HideTechnicalProperties(GridView view, Type type = null)
        {
            if (type == null)
            {
                if (((view.DataSource as IList)?.Count ?? 0) == 0)
                    return;
                type = ((IList)view.DataSource)[0].GetType();
            }
            HideTechnicalPropertiesImmediately(view, type);
        }
        internal static void HideTechnicalPropertiesImmediately(GridView view, Type type)
        {
            var properties = type.GetProperties();
            Debug.Assert(view.Columns.Count > 0);
            foreach (GridColumn col in view.Columns)
            {
                var prop = properties.FirstOrDefault(p => p.Name == col.FieldName);
                if (prop == null) // unbound column ?!
                    continue;
                if (prop.GetCustomAttributes(typeof(IsTechnicalPropertyAttribute), true).Length > 0)
                    col.Visible = false;
            }
        }

        /// <summary>
        /// Set default formating and return a way to undo the formatting handled by events.
        /// </summary>
        /// <param name="view"></param>
        /// <param name="columns">Specific columns to format, by default all columns are formatted</param>
        /// <returns>An action to unsubscribe events that are part of formatting for the specified column, or all column if null</returns>
        public static Action<GridColumn> SetDefaultColumnFormatting(GridView view, params GridColumn[] columns)
        {
            var concernedColumns = new HashSet<GridColumn>(ReferenceEqualityComparer<GridColumn>.Default);
            foreach (var col in (columns?.Length ?? 0) == 0 ? (IEnumerable<GridColumn>)view.Columns : columns)
                concernedColumns.Add(col);
            Debug.Assert(concernedColumns.Count != 0 || view.GridControl.DataSource != null, "L'utilisation de GridColumn.ColumnType ne peut fonctionner que si la datasource est setté");
            view.BeginUpdate();
            foreach (GridColumn col in columns)
                SetDefaultColumnFormatting(col);

            void view_ShownEditor(object sender, EventArgs e)
            {
                // Always use sender and not view in case developper move column to another view
                var v = (GridView)sender;
                if (!concernedColumns.Contains(v.FocusedColumn))
                    return;
                if (v.ActiveEditor is GridLookUpEdit grdLue)
                {
                    grdLue.Popup += (_, __) =>
                    {
                        var cellView = grdLue.Properties.View;
                        GridViewHelper.SetDefaultColumnFormatting(cellView);
                        if (cellView.OptionsView.ShowAutoFilterRow && cellView.Columns.Count > 0)
                        {
                            cellView.FocusedRowHandle = GridControl.AutoFilterRowHandle;
                            cellView.FocusedColumn = cellView.Columns[0];
                        }
                    };
                }
            }
            view.ShownEditor += view_ShownEditor;

            void view_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
            {                
                // Always use sender and not view in case developper move column to another view
                var v = (GridView)sender;
                if (!concernedColumns.Contains(v.FocusedColumn))
                    return;

                var r = e.RepositoryItem as RepositoryItemDateEdit;
                if (r != null)
                    r.CalendarTimeEditing = DefaultBoolean.True;
            }
            view.CustomRowCellEdit += view_CustomRowCellEdit;
            view.EndUpdate();

            return (GridColumn col) =>
            {
                concernedColumns.Remove(col);
                if (col == null || concernedColumns.Count == 0)
                {
                    view.ShownEditor -= view_ShownEditor;
                    view.CustomRowCellEdit -= view_CustomRowCellEdit;
                }
            };
        }
        

        public static void SetDefaultColumnFormatting(GridColumn col)
        {
            if (col.DisplayFormat.FormatString.Length == 0)
                if (col.ColumnType == typeof(DateTime) || col.ColumnType == typeof(DateTime?))
                {
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                    col.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
                }
                else if (col.ColumnType == typeof(decimal) || col.ColumnType == typeof(decimal?))
                {
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    col.DisplayFormat.FormatString = "n2";
                }
            col.OptionsFilter.AutoFilterCondition = Type_Extensions.NumericTypesWithNullable.Contains(col.ColumnType)
                                                  ? AutoFilterCondition.Equals
                                                  : col.ColumnType.RemoveNullability() == typeof(DateTime) || col.ColumnType.RemoveNullability() == typeof(TimeSpan)
                                                  ? AutoFilterCondition.Less
                                                  : AutoFilterCondition.Contains;
            if (DxVersion.Major >= 17) // was not mandatory for version 16.1.5, but is for version 17.1.12 (for version in the range, exclusive, I don't know)
                col.FilterMode = Type_Extensions.NumericTypesWithNullable.Contains(col.ColumnType) 
                               ? ColumnFilterMode.Value 
                               : col.ColumnType.RemoveNullability() == typeof(DateTime) || col.ColumnType.RemoveNullability() == typeof(TimeSpan)
                               ? ColumnFilterMode.Value
                               : ColumnFilterMode.DisplayText;
            if (col is BandedGridColumn bcol)
            {
                bcol.CustomizationCaption = bcol.OwnerBand == null ? "" : bcol.OwnerBand.Caption + " - ";
                bcol.CustomizationCaption += !string.IsNullOrWhiteSpace(bcol.Caption) ? bcol.Caption
                    : !bcol.FieldName.Contains(".") ? bcol.FieldName
                    : bcol.FieldName.Substring(1 + bcol.FieldName.LastIndexOf(".", StringComparison.Ordinal));
            }
        }
        public static readonly Version DxVersion = typeof(GridControl).Assembly.GetName(true).Version;
        

        #endregion Default intuitive formatting of columns

        public static RepositoryItemMemoEdit CreateMultilineRepositoryEdit(this GridView view)
        {
            view.OptionsView.RowAutoHeight = true;
            var repoMultiLineCentered = new RepositoryItemMemoEdit()
            {
                NullText = null,
                AutoHeight = true,
                WordWrap = true,
                // To limit the heigh of memoedit, you can use view.ResizeRow.MaxRowHeight (if view is Enhanced)
                // Note: "LinesCount" properties allows developper to force the height of row (even in case the text is smaller)
                // And the autofilter row seems to be also impacted... which is not pretty at all
            };
            repoMultiLineCentered.Appearance.Options.UseTextOptions = true;
            repoMultiLineCentered.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
            repoMultiLineCentered.Appearance.TextOptions.VAlignment = VertAlignment.Center;
            return repoMultiLineCentered;
        }

        public static RepositoryItemRichTextEdit CreateHtmlRepositoryEdit(GridView view)
        {
            view.OptionsView.RowAutoHeight = true;

            var repo = new RepositoryItemRichTextEdit
            {
                AllowFocused = false,
                AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True,
                ReadOnly = true,
                DocumentFormat = DevExpress.XtraRichEdit.DocumentFormat.Html,
                AutoHeight = true,
                Appearance = {TextOptions = {WordWrap = DevExpress.Utils.WordWrap.Wrap}}
            };
            //view.GridControl.RepositoryItems.AddRange(new RepositoryItem[] { repo });
            return repo;
        }


        public static void InstallGridViewDefaultBehavior(GridView view)
        {
            view.RowCellStyle -= GridView_RowCellStyle;
            view.RowCellStyle += GridView_RowCellStyle;
            view.DataSourceChanged -= View_DataSourceChanged;
            view.DataSourceChanged += View_DataSourceChanged;
        }
        static void GridView_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            var view = sender as GridView;
            e.Appearance.BackColor = e.GetCellFocusedBackgroundColorOrCurrentOne(view);
        }
        static void View_DataSourceChanged(object sender, EventArgs e)
        {
            var view = (GridView)sender;
            GridViewHelper.SetDefaultColumnFormatting(view);
            view.GridControl.BeginInvoke((Action)(() =>
            {
                var value = view.BestFitMaxRowCount;
                view.BestFitMaxRowCount = 10;
                try
                {
                    view.BestFitColumns();
                }
                finally
                {
                    view.BestFitMaxRowCount = value;
                }
            }));

            view.DataSourceChanged -= View_DataSourceChanged;
        }

        public static UnboundColumnType ToDevExpressGridViewUnboundType(this Type t)
        {
            // Note: Code is sync with GridViewHelper.ToDevExpressTreeListUnboundType
            if (t.IsIntegerTypeOrNullable())
                return UnboundColumnType.Integer;
            if (t == typeof(string) || t == typeof(char) || t == typeof(char?))
                return UnboundColumnType.String;
            if (t.IsNumericTypeOrNullable())
                return UnboundColumnType.Decimal;
            if (t == typeof(bool) || t == typeof(bool?))
                return UnboundColumnType.Boolean;
            if (t == typeof(DateTime) || t == typeof(DateTime?) ||
                t == typeof(TimeSpan) || t == typeof(TimeSpan?))
                return UnboundColumnType.DateTime;
            return UnboundColumnType.Object;
        }

        public static void CreateDefaultEnhancedGridView_On_MasterRowGetLevelDefaultView(object sender, MasterRowGetLevelDefaultViewEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            var view = (GridView)sender;
            var gc = view.GridControl;

            var dicoByObject = _subViewInfos.GetValue(view, v => new Dictionary<object, Dictionary<int, EnhancedGridView>>(ReferenceEqualityComparer<object>.Default));
            var businesObject = view.GetRow(e.RowHandle);
            if (!dicoByObject.TryGetValue(businesObject, out Dictionary<int, EnhancedGridView> subViewByObject))
            {
                subViewByObject = new Dictionary<int, EnhancedGridView>();
                dicoByObject.Add(businesObject, subViewByObject);
            }
            if (!subViewByObject.TryGetValue(e.RelationIndex, out EnhancedGridView gvSubView))
            {
                gvSubView = new EnhancedGridView(view.GridControl);
                gvSubView.OptionsBehavior.Editable = false;
                gvSubView.OptionsView.AllowCellMerge = true;
                gvSubView.OptionsView.ColumnAutoWidth = false;
                gvSubView.OptionsView.ShowGroupPanel = false;
                gvSubView.CustomDrawCell += (_, ee) =>
                {
                    ee.Appearance.BackColor = ee.GetCellFocusedBackgroundColorOrCurrentOne(gvSubView);
                };

                void onRowExpanded(object sender2, CustomMasterRowEventArgs ee)
                {
                    //Console.WriteLine("in h " + ee.RelationIndex); 
                    if (e.RelationIndex == ee.RelationIndex)
                    {
                        var gv = ((GridView)sender2).GetDetailView(ee.RowHandle, ee.RelationIndex) as EnhancedGridView;
                        // Note gv != gvSubView because of the way details view works with DX. But they are representing the same thing 
                        var backup = gv.BestFitMaxRowCount;
                        gv.BestFitMaxRowCount = 5;
                        gv.BestFitColumns();
                        gv.BestFitMaxRowCount = backup;
                        (sender as GridView).MasterRowExpanded -= onRowExpanded;
                    }
                }
                (sender as GridView).MasterRowExpanded -= onRowExpanded;
                (sender as GridView).MasterRowExpanded += onRowExpanded;
                (sender as GridView).MasterRowCollapsed += onRowExpanded;

                subViewByObject.Add(e.RelationIndex, gvSubView);
            }


            // Prevent Memory Leak 
            gc.Disposed += (_, __) =>
            {
                _subViewInfos.Remove(view);
            };


            //CustomMasterRowEventHandler hh = null; 
            //hh = (object sender2, CustomMasterRowEventArgs ee) => 
            //{ 

            //    (sender as GridView).MasterRowExpanded -= h; 
            //    (sender as GridView).MasterRowCollapsed -= hh; 
            //}; 
            //(sender as GridView).MasterRowExpanded += h; 
            //(sender as GridView).MasterRowCollapsed += hh; 

            e.DefaultView = gvSubView;
        }
        static readonly ConditionalWeakTable<GridView, Dictionary<object, Dictionary<int, EnhancedGridView>>> _subViewInfos = new ConditionalWeakTable<GridView, Dictionary<object, Dictionary<int, EnhancedGridView>>>();

    }
}
