﻿using System;
using System.ComponentModel;

using DevExpress.Utils.Menu;


namespace TechnicalTools.UI.DX
{
    // Note : DXPopupMenu is the base class for GridViewMenu and PivotGridMenu
    public static class DXPopupMenu_Extensions
    {
        public static void AddToGroup(this DXPopupMenu menu, ePopupMenuHeaderType headerType, DXMenuItem item)
        {
            DXPopupMenuHelper.Instance.AddToGroup(menu, headerType, item);
        }
        public static void InsertInGroup(this DXPopupMenu menu, ePopupMenuHeaderType headerType, int index, DXMenuItem item)
        {
            DXPopupMenuHelper.Instance.InsertInGroup(menu, headerType, index, item);
        }
    }
    public class DXPopupMenuHelper
    { 
        public static DXPopupMenuHelper Instance { get; protected set; } = new DXPopupMenuHelper();

        public virtual void AddToGroup(DXPopupMenu menu, ePopupMenuHeaderType headerType, DXMenuItem item)
        {
            var i = menu.Items.FindIndex(it => headerType.Equals((it as DXMenuHeaderItem)?.Tag));
            if (i < 0)
            {
                menu.Items.Insert(0, CreateBarHeader(headerType));
                i = 0;
            }
            i = menu.Items.FindIndex(i + 1, it => it is DXMenuHeaderItem);
            if (i < 0)
                i = menu.Items.Count;
            menu.Items.Insert(i, item);
            menu.BeforePopup -= Menu_BeforePopup;
            menu.BeforePopup += Menu_BeforePopup;
        }

        public virtual void InsertInGroup(DXPopupMenu menu, ePopupMenuHeaderType headerType, int index, DXMenuItem item)
        {
            var i = menu.Items.FindIndex(it => headerType.Equals((it as DXMenuHeaderItem)?.Tag));
            if (i < 0)
            {
                menu.Items.Insert(0, CreateBarHeader(headerType));
                i = 0;
            }
            var i2 = menu.Items.FindIndex(i + 1, it => it is DXMenuHeaderItem);
            if (i2 < 0)
                i2 = menu.Items.Count;
            if (index > i2 - i - 1)
                index = i2 - i - 1;
            menu.Items.Insert(i + 1 + index, item);
            menu.BeforePopup -= Menu_BeforePopup;
            menu.BeforePopup += Menu_BeforePopup;
        }

        protected virtual DXMenuHeaderItem CreateBarHeader(ePopupMenuHeaderType headerType)
        {
            return new DXMenuHeaderItem() { Caption = headerType.GetDescription(), Tag = headerType };
        }
        void Menu_BeforePopup(object sender, EventArgs e)
        {
            var items = (sender as DXPopupMenu).Items;
            for (int i = 0; i < items.Count; ++i)
                if (items[i] is DXMenuHeaderItem)
                    items[i].Visible = i + 1 < items.Count && !(items[i + 1] is DXMenuHeaderItem);
        }
    }

    public enum ePopupMenuHeaderType
    {
        [Description("Standard")]
        Standard,
        [Description("Custom")]
        Custom,
        [Description("Connectivity")]
        Connectivity,
        [Description("Debugging")]
        Debugging,
    }
}
