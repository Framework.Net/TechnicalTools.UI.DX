﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools.Tools;
using TechnicalTools.Model;


namespace TechnicalTools.UI.DX.Helpers
{
    /// <summary>
    /// Helper to add automatically exploration and flattenization of data to a gridview.
    /// This class apply to a gridview where data source is List of T.
    /// This is instances of IHierarchyDescription that defines how to customize gridview
    /// </summary>
    public partial class GridViewDataSourceFlattenizer
    {
        internal class Join
        {
            internal object[] parts;
            internal Join Clone()
            {
                var j = new Join();
                j.parts = (object[])parts.Clone();
                return j;
            }
            internal static readonly string LvlPrefix = nameof(Join<object>.Lvl0).Truncate(nameof(Join<object>.Lvl0).Length - 1);
            public virtual Type GetPartType(int i) { throw new InvalidOperationException(); }
        }
        internal class Join<T0>                                  : Join                                   { public T0 Lvl0 { get { return (T0)parts[0]; } } public override Type GetPartType(int i) { return i == 0 ? typeof(T0) : base.GetPartType(i); } }
        internal class Join<T0, T1>                              : Join<T0>                               { public T1 Lvl1 { get { return (T1)parts[1]; } } public override Type GetPartType(int i) { return i == 1 ? typeof(T1) : base.GetPartType(i); } }
        internal class Join<T0, T1, T2>                          : Join<T0, T1>                           { public T2 Lvl2 { get { return (T2)parts[2]; } } public override Type GetPartType(int i) { return i == 2 ? typeof(T2) : base.GetPartType(i); } }
        internal class Join<T0, T1, T2, T3>                      : Join<T0, T1, T2>                       { public T3 Lvl3 { get { return (T3)parts[3]; } } public override Type GetPartType(int i) { return i == 3 ? typeof(T3) : base.GetPartType(i); } }
        internal class Join<T0, T1, T2, T3, T4>                  : Join<T0, T1, T2, T3>                   { public T4 Lvl4 { get { return (T4)parts[4]; } } public override Type GetPartType(int i) { return i == 4 ? typeof(T4) : base.GetPartType(i); } }
        internal class Join<T0, T1, T2, T3, T4, T5>              : Join<T0, T1, T2, T3, T4>               { public T5 Lvl5 { get { return (T5)parts[5]; } } public override Type GetPartType(int i) { return i == 5 ? typeof(T5) : base.GetPartType(i); } }
        internal class Join<T0, T1, T2, T3, T4, T5, T6>          : Join<T0, T1, T2, T3, T4, T5>           { public T6 Lvl6 { get { return (T6)parts[6]; } } public override Type GetPartType(int i) { return i == 6 ? typeof(T6) : base.GetPartType(i); } }
        internal class Join<T0, T1, T2, T3, T4, T5, T6, T7>      : Join<T0, T1, T2, T3, T4, T5, T6>       { public T7 Lvl7 { get { return (T7)parts[7]; } } public override Type GetPartType(int i) { return i == 7 ? typeof(T7) : base.GetPartType(i); } }
        internal class Join<T0, T1, T2, T3, T4, T5, T6, T7, T8>  : Join<T0, T1, T2, T3, T4, T5, T6, T7>   { public T8 Lvl8 { get { return (T8)parts[8]; } } public override Type GetPartType(int i) { return i == 8 ? typeof(T8) : base.GetPartType(i); } }

        class DatasourceFlattenizationApplyer
        {
            readonly IHierarchyDescription[] _pathFromAncestorsToParent;
            readonly IProgress<string> _pr;
            readonly bool _allowLeftOuterJoin;

            public DatasourceFlattenizationApplyer(IHierarchyDescription[] pathFromAncestorsToParent, bool allowLeftOuterJoin, IProgress<string> pr)
            {
                _pathFromAncestorsToParent = pathFromAncestorsToParent;
                _allowLeftOuterJoin = allowLeftOuterJoin;
                _pr = pr;
            }
            readonly Stack<object> _parents = new Stack<object>();
            static readonly object _seed = new object();
            static readonly IReadOnlyCollection<object> _seedList = new List<object>();

            public IReadOnlyCollection<Join> Apply()
            {
                var optimizedPreloadedData = new Stack<IReadOnlyCollection<object>>();
                optimizedPreloadedData.Push(_seedList);
                foreach (var hierarchyLevel in _pathFromAncestorsToParent)
                {
                    var hierarchyLevelOptimized = hierarchyLevel as IHierarchyDescriptionOptimized;
                    if (hierarchyLevelOptimized == null)
                    {
                        optimizedPreloadedData.Push(new List<object>());
                        break;
                    }
                    var t = hierarchyLevelOptimized.PreLoadAllChildren(optimizedPreloadedData.Peek());
                    if (t == null)
                    {
                        optimizedPreloadedData.Push(new List<object>());
                        break;
                    }
                    optimizedPreloadedData.Push(t);
                }

                // The following is all about speed of data transformation / optimization,
                // so we use only very generic untyped types.
                // BUT, at the end, we generate a wrapper strongly typed (Join<...>).  
                // This allows to keep all feature of gridview using reflection (custom display, menu, exploring nested, chart etc)
                _parents.Push(_seed);
                _flattenizedModel = new object[_pathFromAncestorsToParent.Length];
                try
                {
                    var data = FlattenizeDatasourceThroughHierarchyBranch(0).ToArray();
                    var res = Convert(data, optimizedPreloadedData);
                    return res;
                }
                finally
                {
                    _flattenizedModel = null;
                    _parents.Clear();
                }
            }
            object[] _flattenizedModel;
            IEnumerable<object[]> FlattenizeDatasourceThroughHierarchyBranch(int depth)
            {
                var hierarchy = _pathFromAncestorsToParent[depth];
                var children = hierarchy.GetChildren(_parents) ?? Array.Empty<object>();
                if (_allowLeftOuterJoin && children.Count == 0 && depth > 0)
                    yield return (object[])_flattenizedModel.Clone();
                else if (depth == _pathFromAncestorsToParent.Length - 1)
                    foreach (var item in children)
                    {
                        var eo = (object[])_flattenizedModel.Clone();
                        eo[depth] = item;
                        yield return eo;
                    }
                else
                {
                    int cnt = 0;
                    foreach (var subParentItem in children)
                    {
                        _flattenizedModel[depth] = subParentItem;
                        _parents.Push(subParentItem);
                        foreach (var eo in FlattenizeDatasourceThroughHierarchyBranch(depth + 1))
                            yield return eo;
                        _parents.Pop();
                        ++cnt;
                        if (depth == 0)
                            _pr.Report(cnt + " row" + (cnt <= 1 ? "" : "s") + " done on " + children.Count);
                    }
                }
            }

            IReadOnlyCollection<Join> Convert(object[][] data, Stack<IReadOnlyCollection<object>> optimizedPreloadedData)
            {
                Type[] genericTypes = new Type[_pathFromAncestorsToParent.Length];
                for (var depth = 0; depth < _pathFromAncestorsToParent.Length; ++depth)
                {
                    var types = new List<Type>(); // Expected to be very tiny list so we dont use hashset etc
                    for (int i = 0; i < data.Length; ++i)
                    {
                        var part = data[i][depth];
                        if (part != null)
                        {
                            var t = part.GetType();
                            if (!types.Contains(t))
                                types.Add(t);
                        }
                    }
                    genericTypes[depth] = types.FirstOrDefault()
                                       ?? optimizedPreloadedData.ElementAt(optimizedPreloadedData.Count - depth - 1 - 1)?.GetType().GetOneImplementationOf(typeof(IEnumerable<>))?.GetGenericArguments().First()
                                       ?? typeof(object);
                }
                var baseJoinType = genericTypes.Length == 2 ? typeof(Join<,>)
                                    : genericTypes.Length == 3 ? typeof(Join<,,>)
                                    : genericTypes.Length == 4 ? typeof(Join<,,,>)
                                    : genericTypes.Length == 5 ? typeof(Join<,,,,>)
                                    : genericTypes.Length == 1 ? typeof(Join<>)
                                    : genericTypes.Length == 6 ? typeof(Join<,,,,,>)
                                    : genericTypes.Length == 7 ? typeof(Join<,,,,,,>)
                                    : genericTypes.Length == 8 ? typeof(Join<,,,,,,,>)
                                    : throw new NotImplementedException();
                var joinType = baseJoinType.MakeGenericType(genericTypes);
                var cons = DefaultObjectFactory.ConstructorFor(joinType);
                var result = Array.CreateInstance(joinType, data.Length);
                for (int i = 0; i < data.Length; ++i)
                {
                    var j = cons() as Join;
                    j.parts = data[i];
                    result.SetValue(j, i);
                }
                return (IReadOnlyCollection<Join>)result;
            }
        }
    }
}
