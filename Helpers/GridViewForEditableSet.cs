﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.BandedGrid;

using TechnicalTools.UI.EditableLayer;
using TechnicalTools.Model.Cache;
using TechnicalTools.Model;


namespace TechnicalTools.UI.DX.Helpers
{
    public abstract class GridViewForEditableSet
    {
        public class ColorsSetting : IDisposable
        {
            public Color EditedCellBackColor;
            public Pen   StrikeOutPen;
            public Color EditableFieldBackground;

            public static ColorsSetting Default = new ColorsSetting()
            {
                EditedCellBackColor = Color.LightGreen,
                StrikeOutPen = new Pen(Color.Red, 3),
                EditableFieldBackground = Color.MintCream,
            };

            public void Dispose() { StrikeOutPen.Dispose(); }
        }
        public ColorsSetting Colors { get; set; } = ColorsSetting.Default;

        protected class MetaDataInfo
        {
            public readonly IReadOnlyDictionary<string, bool> InterfacePropertyToReadOnly;

            interface IABase
            {
                int Foo { get; set; }
            }
            interface IA : IABase
            {
                new int Foo { get; set; }
            }
            class A : IA
            {
                int IA.Foo { get; set; }
                int IABase.Foo { get; set; }

                public int Foo { get; set; }
            }
            public MetaDataInfo(Type titem, Type iitem)
            {
                var mappings = titem.GetInterfaceMap(iitem);
                var interfacePropertyToReadOnly = new Dictionary<string, bool>();
                var properties = titem.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                for (int i = 0; i < mappings.TargetMethods.Length; ++i)
                {
                    var methodPropName = mappings.InterfaceMethods[i].Name;
                    if (!methodPropName.StartsWith("get_") &&
                        !methodPropName.StartsWith("set_"))
                        continue;
                    var key = methodPropName.Substring(4);
                    if (interfacePropertyToReadOnly.ContainsKey(key))
                        continue;
                    var mi = mappings.TargetMethods[i];
                    var curProperty = properties.FirstOrDefault(prop => prop.GetGetMethod(true) == mi 
                                                                     || prop.GetSetMethod(true) == mi);
                    if (curProperty == null)
                        continue;
                    var att = curProperty.GetCustomAttribute<System.ComponentModel.ReadOnlyAttribute>();
                    if (att != null)
                        interfacePropertyToReadOnly.Add(key, att.IsReadOnly);
                }
                InterfacePropertyToReadOnly = interfacePropertyToReadOnly;
            }

            public bool IsReadOnly(string interfacePropertyName)
            {
                return InterfacePropertyToReadOnly.TryGetValueStruct(interfacePropertyName) ?? false;
            }
        }

        public abstract void DefineSubstituteEditableColumn(GridColumn colPaymentAccount, string editedFieldName);
    }

    public class GridViewForEditableSet<TItem, IItem> : GridViewForEditableSet
        where TItem : class, IItem, IHasTypedIdReadable
        where IItem : class, ICopyable<IItem>, IHasTypedIdReadable
    {
        IEditableSet<TItem, IItem> _editableSet;
        GridControl _gc;
        GridView _gv;
        bool _onlyForMainInterface;
        readonly Dictionary<string, PropertyInfo> _propsDisplayed = new Dictionary<string, PropertyInfo>();
        readonly Dictionary<GridColumn, string> _substituteColumns = new Dictionary<GridColumn, string>();

        public event EventHandler ButtonEnabilityRefresh;

        public NavigatorButtonBase ValidateAndSaveButton { get { return _gc.EmbeddedNavigator.Buttons.EndEdit; } }
        public NavigatorButtonBase CancelEditButton      { get; private set; }
        public NavigatorButtonBase CommitButton          { get; private set; }

        static readonly MetaDataInfo MetaData = new MetaDataInfo(typeof(TItem), typeof(IItem));

        public GridViewForEditableSet<TItem, IItem> Install(GridView gv, IEditableSet<TItem, IItem> editableSet, bool onlyForMainInterface = false)
        {
            if (_editableSet != null)
                Uninstall();
            _editableSet = editableSet;

            var gvAlreadyConfigured = _gv == gv;
            _gv = gv;
            _gc = gv.GridControl;
            _onlyForMainInterface = onlyForMainInterface;

            _gc.Disposed += (_, __) => _editableSet.ItemReseted -= editableSet_ItemReseted;
            _editableSet.ItemReseted += editableSet_ItemReseted;

            if (!gvAlreadyConfigured)
            {
                if (gv is EnhancedGridView)
                    (gv as EnhancedGridView)._FreeColumn.AllowChangeMergeCell = false; // Incompatible avec gv_RowCellStyle
                gv.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;

                _gc.EmbeddedNavigator.Buttons.Append.Hint = "Add line";
                _gc.EmbeddedNavigator.Buttons.Append.Visible = editableSet.AllowNew;
                _gc.EmbeddedNavigator.Buttons.Append.Enabled = editableSet.AllowNew;
                _gc.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
                _gc.EmbeddedNavigator.Buttons.Edit.Visible = false;
                _gc.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
                _gc.EmbeddedNavigator.Buttons.First.Enabled = false;
                _gc.EmbeddedNavigator.Buttons.First.Visible = false;
                _gc.EmbeddedNavigator.Buttons.Last.Visible = false;
                _gc.EmbeddedNavigator.Buttons.Next.Visible = false;
                _gc.EmbeddedNavigator.Buttons.NextPage.Visible = false;
                _gc.EmbeddedNavigator.Buttons.Prev.Visible = false;
                _gc.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
                _gc.EmbeddedNavigator.Buttons.Remove.Hint = "Mark existing line to be deleted / Cancel new line";
                _gc.EmbeddedNavigator.Buttons.Remove.Visible = editableSet.AllowRemove;

                _gc.EmbeddedNavigator.TextStringFormat = typeof(TItem).Name.Uncamelify() + " {0} of {1}" + (_gc.EmbeddedNavigator is ControlNavigatorEnhanced ? " (visible) of {2} (total)" : "");
                _gc.EmbeddedNavigator.ButtonClick += gc_EmbeddedNavigator_ButtonClick;
                _gc.UseEmbeddedNavigator = true;

                if (gv.Columns.Count == 0 && gv.OptionsBehavior.AutoPopulateColumns)
                {
                    gv.Columns.Clear();
                    foreach (var prop in typeof(IItem).GetProperties())
                    {
                        if (prop.IsTechnicalPropertyHidden() || !prop.IsBrowsable())
                            continue;
                        _propsDisplayed.Add(prop.Name, prop);
                        GridColumn col = null;
                        if (!prop.PropertyType.IsSubclassOf(typeof(EditableSet)))
                        {
                            col = gv.Columns.AddVisible(prop.Name);
                            if (gv is BandedGridView bgv && bgv.Bands.Count > 0)
                                bgv.Bands[0].Columns.Add((BandedGridColumn)col);
                            col.OptionsColumn.AllowEdit = prop.CanWrite && !MetaData.IsReadOnly(prop.Name);
                        }
                        else // TODO
                        {

                        }
                        if (prop.GetCustomAttribute<DisplayAttribute>()?.Order < 0)
                            if (col != null)
                                col.Visible = false;
                    }

                    foreach (var otherInterface in typeof(IItem).GetAllInheritedInterfaces())
                        if (otherInterface.Assembly != typeof(IIdTuple).Assembly) // Ignore technical interface
                            foreach (var prop in otherInterface.GetProperties())
                                if (!_propsDisplayed.ContainsKey(prop.Name))
                                {
                                    if (prop.IsTechnicalPropertyHidden() || !prop.IsBrowsable())
                                        continue;
                                    // TODO : call GetInterfaceMapping
                                    _propsDisplayed.Add(prop.Name, prop);
                                    var col = gv.Columns.AddVisible(prop.Name);
                                    col.OptionsColumn.AllowEdit = prop.CanWrite && MetaData.IsReadOnly(prop.Name);
                                    col.OptionsColumn.ReadOnly = _onlyForMainInterface;

                                    if (prop.GetCustomAttribute<DisplayAttribute>()?.Order < 0)
                                        col.Visible = false;
                                }
                    // Here we just want to resize columns to match the content and title of all columns.
                    // caller can assign a small value (for example 50) to gv.BestFitMaxRowCount to speed up loading time
                    // But we can't do it right now because there is some different case to consider:
                    // 1) gv.GridControl.IsLoaded == true : This is the simplest case, we can call bestfitright now.
                    // 2) gv.GridControl.IsLoaded == false means that the gridview is not fully aware of all data.
                    //    This occurs when the current method (Install) is called in Load event of UserControl or Form
                    //    gv.GridControl.ForceInitialize exists but seems to not handle the case 3)
                    // 3) When gridview are in a tab that has never been displayed to user yet, 
                    //    gridcontrol and/or gridview have not been fully initialized neither.
                    // So we use the Loaded event that seems to be raised at the right time.
                    // This event is raised when user click for the first on a the never-displayed-tab
                    // There is no concurrency to handle neither because UI code is always executed on same thread
                    void onDataSourceLoaded(object _, EventArgs __)
                    {
                        if (gv.GridControl.IsLoaded && gv.DataSource != null)
                        {
                            gv.GridControl.Load -= onDataSourceLoaded;
                            gv.DataSourceChanged -= onDataSourceLoaded;
                            gv.BestFitColumns();
                        }
                    }
                    if (gv.GridControl.IsLoaded && gv.DataSource != null)
                        onDataSourceLoaded(gv.GridControl, EventArgs.Empty);
                    else
                    {
                        gv.GridControl.Load += onDataSourceLoaded;
                        gv.DataSourceChanged += onDataSourceLoaded;
                    }
                }
            }
            if (CommitButton == null || !_gc.EmbeddedNavigator.CustomButtons.Cast<NavigatorCustomButton>().Contains(CommitButton))
            {
                // We dont use EndEdit button because its Enable property is controled by DX. The button is only enabled when user is editing a cell
                CommitButton = new NavigatorCustomButton(9, "Validate and save all");
                _gc.EmbeddedNavigator.CustomButtons.AddRange(new[] { (NavigatorCustomButton)CommitButton });
                CommitButton.Visible = editableSet.AllowNew || editableSet.AllowEdit || editableSet.AllowRemove;
            }
            if (CancelEditButton == null || !_gc.EmbeddedNavigator.CustomButtons.Cast<NavigatorCustomButton>().Contains(CancelEditButton))
            {
                CancelEditButton = new NavigatorCustomButton(11, "Cancel edits on this line");
                _gc.EmbeddedNavigator.CustomButtons.AddRange(new[] { (NavigatorCustomButton)CancelEditButton });
                CancelEditButton.Visible = editableSet.AllowNew || editableSet.AllowEdit || editableSet.AllowRemove;
            }

            _editableSet.Commited += editableSet_Commited;
            gv.Disposed += (_, __) => _editableSet.Commited -= editableSet_Commited;

            _gc.DataSource = editableSet.EditingSet;

            // If you add an event here, add its unsubscribing below in Uninstall 
            UnsubscribeEvents(gv);
            SubscribeEvents(gv);

            RefreshButtonEnabilities(gv, gv.FocusedRowHandle);
            return this;
        }

        void SubscribeEvents(GridView gv)
        {
            gv.CustomDrawCell += gv_CustomDrawCell;
            gv.RowCellStyle += gv_RowCellStyle;
            gv.FocusedRowChanged += gv_FocusedRowChanged;
            gv.ShownEditor += gv_ShownEditor;
            gv.CellValueChanged += gv_CellValueChanged;
            gv.CustomRowFilter += gv_CustomRowFilter;
        }
        void UnsubscribeEvents(GridView gv)
        {
            gv.CustomDrawCell -= gv_CustomDrawCell;
            gv.RowCellStyle -= gv_RowCellStyle;
            gv.FocusedRowChanged -= gv_FocusedRowChanged;
            gv.ShownEditor -= gv_ShownEditor;
            gv.CellValueChanged -= gv_CellValueChanged;
            gv.CustomRowFilter -= gv_CustomRowFilter;
        }

        public override void DefineSubstituteEditableColumn(GridColumn colPaymentAccount, string editedFieldName)
        {
            if (_editableSet == null)
                throw new TechnicalException("GridViewForEditableSet must be Install first!", null);
            _substituteColumns.Add(colPaymentAccount, editedFieldName);
        }

        public void Uninstall()
        {
            _editableSet.ItemReseted -= editableSet_ItemReseted;

            if (_gc != null)
            {
                _propsDisplayed.Clear();
                _gc.DataSource = null;
                _gc.EmbeddedNavigator.ButtonClick -= gc_EmbeddedNavigator_ButtonClick;

                _gv.CustomDrawCell -= gv_CustomDrawCell;
                _gv.RowCellStyle -= gv_RowCellStyle;
                _gv.FocusedRowChanged -= gv_FocusedRowChanged;
                _gv.ShownEditor -= gv_ShownEditor;
                _gv.CellValueChanged -= gv_CellValueChanged;
                _gv.CustomRowFilter -= gv_CustomRowFilter;

                _gc = null;
            }
        }

        void gv_ShownEditor(object sender, EventArgs e)
        {
            var view = (GridView)sender;
            var editor = view.ActiveEditor;
            //editor.EditValueChanged -= gvActiveEditor_EditValueChanged;
            editor.EditValueChanged += gvActiveEditor_EditValueChanged;
        }
        void gv_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            RefreshButtonEnabilities(_gv, _gv.FocusedRowHandle);
        }

        void gvActiveEditor_EditValueChanged(object sender, EventArgs e)
        {
            if (sender is CheckEdit editor)
            {
                var view = (GridView)((GridControl)editor.Parent).FocusedView;
                view.PostEditor();
            }
        }

        public event EventHandler<CancelEventArgs> Committing;

        void gc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            var nav = (ControlNavigatorEnhanced)sender;
            var gc = (GridControl)nav.Parent;
            var gv = (GridView)gc.FocusedView; Debug.Assert(gv == _gv);
            if (e.Button.ButtonType == NavigatorButtonType.Append)
            {
                e.Handled = true; // prevent grid to create a new object
                // Create an instance that is already technically valid, 
                // so we can handle the editing process like editing
                IItem item = _editableSet.AddNew();
                gv.FocusedRowHandle = gv.FindRow(item);
            }
            else if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = true; // prevent grid to remove line for us
                var editableItem = (IItem)gv.GetFocusedRow();
                Remove(editableItem, gv, gv.FocusedRowHandle);
                gv.RefreshRow(gv.FocusedRowHandle);
            }
            else if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (e.Button == CommitButton) // Commit Button
                {
                    e.Handled = true; // prevent grid to do... something for us
                    var ee = new CancelEventArgs();
                    Committing?.Invoke(this, ee);
                    if (!ee.Cancel)
                        _editableSet.Commit();
                }
                else // Remove / Reset Edits
                {
                    e.Handled = true; // prevent grid to do... something for us
                    var editableItem = (IItem)gv.GetFocusedRow();
                    if (_editableSet.Added.Contains(editableItem))
                    {
                        _editableSet.ResetEditsOrRemove(editableItem);
                        gv.RefreshData();
                    }
                    else
                    {
                        _editableSet.ResetEditsOrRemove(editableItem);
                        gv.RefreshRow(gv.FocusedRowHandle); // refresh cell display
                        gv.LayoutChanged(); // To restore row height
                        RefreshButtonEnabilities(gv, gv.FocusedRowHandle);
                    }
                }
            }
        }

        public void Remove(IItem editableItem, GridView gv = null, int? itemRowHandle = null)
        {
            gv = gv ?? _gv;
            itemRowHandle = itemRowHandle ?? gv.FindRowHandleByBusinessObject(editableItem);
            _editableSet.Remove(editableItem);
            gv.RefreshData();
        }

        private void editableSet_ItemReseted(object sender, object item)
        {
            var rh = _gv.FindRow(item);
            if (rh != GridControl.InvalidRowHandle)
                RefreshButtonEnabilities(_gv, rh);
            else // means item has been removed from view 
                // We postpone the refresh because FocusedRowHandle stays the same and 
                // then FocusedRowChanged event won't be raised.
                _gc.BeginInvokeSafely(() => RefreshButtonEnabilities(_gv, _gv.FocusedRowHandle));
        } 

        [DebuggerHidden, DebuggerStepThrough]
        void gv_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle < 0)
                return;
            
            var view = (GridView)sender;
            var editable = (IItem)view.GetRow(e.RowHandle);
            if (editable == null) // can happen when the last line is removed, Devexpress seems to send event with a non existant rowhandle
                return;
            if (!view.IsCellSelected(e.RowHandle, e.Column))
            {
                var fieldname = _substituteColumns.TryGetValueClass(e.Column) ?? e.Column.FieldName;
                var prop = _propsDisplayed.ContainsKey(fieldname) ? _propsDisplayed[fieldname] : null;

                if (prop != null && (_editableSet.Added.Contains(editable) ||
                                     _editableSet.IsPropertyDirty(editable, fieldname, prop.DeclaringType)))
                {
                    e.Appearance.BackColor = Colors.EditedCellBackColor;
                    e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
                }
                else if (!e.Column.OptionsColumn.ReadOnly && e.Column.OptionsColumn.AllowEdit && _editableSet.AllowEdit)
                    e.Appearance.BackColor = Colors.EditableFieldBackground;
            }

            if (_editableSet.Removed.Contains(editable))
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Strikeout);
        }

        void gv_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            var view = (GridView)sender;
            var editable = (IItem)view.GetRow(e.RowHandle);
            if (_editableSet.Removed.Contains(editable))
            {
                e.DefaultDraw();
                e.Graphics.DrawLine(Colors.StrikeOutPen,
                                    new Point(e.Bounds.X, e.Bounds.Y + e.Bounds.Height / 2),
                                    new Point(e.Bounds.X + e.Bounds.Width, e.Bounds.Y + e.Bounds.Height / 2));
                e.Handled = true;
            }
            //e.Graphics.DrawLine(New Pen(Brushes.Black), New Point(0, (e.Bounds.Y) + 10), New Point(gcCME.Location.X + gcCME.Width, (e.Bounds.Y) + 10))
        }

        void gv_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var view = sender as GridView;
            if (view.GridControl.InvokeRequired)
            {
                view.GridControl.BeginInvoke((Action)(() => gv_FocusedRowChanged(sender, e)));
                return;
            }

            RefreshButtonEnabilities(view, e.FocusedRowHandle);
        }

        private void editableSet_Commited(object sender, EventArgs e)
        {
            RefreshButtonEnabilities(_gv, _gv.FocusedRowHandle);
        }

        void RefreshButtonEnabilities(GridView gv, int focusedRowhandle)
        {
            var buttons = gv.GridControl.EmbeddedNavigator.Buttons;

            if (focusedRowhandle < 0)
            {
                buttons.Remove.Enabled = false;
                CancelEditButton.Enabled = false;
            }
            else
            {
                var editableItem = (IItem)gv.GetRow(focusedRowhandle);
                if (editableItem == null) // can happen when we remove last row. Devexpress raise the event with the old last focusedRowhandle
                    return;
                CancelEditButton.Enabled = _editableSet.IsDirty(editableItem);
                buttons.Remove.Enabled = true;
            }
            CommitButton.Enabled = _editableSet.HasChanges;
            ButtonEnabilityRefresh?.Invoke(this, EventArgs.Empty);
        }

        void gv_CustomRowFilter(object sender, RowFilterEventArgs e)
        {
            // To prevent line from disapearing when user click '+' but already has a filter
            // We make new line always visible until they are commited
            var item = _editableSet.EditingSet[e.ListSourceRow];
            if (_editableSet.Added.Contains(item))
            {
                e.Visible = true;
                e.Handled = true;
            }
        }
    }
}
