﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using DevExpress.Data.Filtering;
using DevExpress.Data.Filtering.Helpers;
using DevExpress.XtraEditors.Filtering;
using DevExpress.XtraEditors.Repository;

using TechnicalTools.Extensions.Data;
using TechnicalTools.Model;

using DataMapper;


namespace TechnicalTools.UI.DX.Helpers
{
    public class CriteriaParserInitializer
    {
        public static readonly CriteriaParserInitializer Instance = new CriteriaParserInitializer();

        private CriteriaParserInitializer() { }

        public List<FilterColumn> BuildFilterColumns(Type type, bool allowInterface = false)
        {
            var lst = new List<FilterColumn>();
            foreach (var col in GetPropertiesAsColumns(type, null, allowInterface, new HashSet<Type>() { type }))
                lst.Add(col);
            return lst;
        }

        IEnumerable<UnboundFilterColumn> GetPropertiesAsColumns(Type propType, string parentPropName, bool allowInterface, HashSet<Type> typesMet)
        {
            parentPropName = parentPropName?.IfNotBlankAddSuffix(".");

            var properties = typeof(Exception).IsAssignableFrom(propType) 
                           ? propType.GetProperties().Where(p => p.Name != nameof(Exception.TargetSite))
                           : propType.GetProperties();
            foreach (var p in properties)
            {
                if (p.GetCustomAttributes(typeof(IsTechnicalPropertyAttribute), true).Length > 0)
                    continue;

                // DevExpress handles interface very badly for databinding, 
                // Actually DX uses the first instance of datasource to get reflection on the property with same name.
                // So if next item in datasource is of different concrete Type, mapping does not work, even when all type of all instance implement same interface
                if (p.PropertyType.IsInterface && !allowInterface)
                    continue;

                if ((p.PropertyType.TryGetNullableType() ?? p.PropertyType).IsValueType || p.PropertyType == typeof(string))
                {
                    yield return new UnboundFilterColumn(parentPropName + p.Name, parentPropName + p.Name, p.PropertyType,
                                                         GetTypeRelatedRepositoryItem(p.PropertyType),
                                                         // See comment in method RaisePopupMenuShowing
                                                         FilterColumnClauseClass.String /*GetTypeRelatedClauseClass(p.PropertyType)*/);
                    continue;
                }

                if (typeof(DynamicEnum).IsAssignableFrom(p.PropertyType) ||
                    typeof(IStaticEnum).IsAssignableFrom(p.PropertyType))
                {
                    yield return new UnboundFilterColumn(parentPropName + p.Name, parentPropName + p.Name, p.PropertyType,
                                     GetTypeRelatedRepositoryItem(p.PropertyType), FilterColumnClauseClass.String);
                    continue;
                }

                // Empèche une récursion infini si les données sont de la forme d'un graphe
                // Par exemple a cause d'un type qui se référence lui même via ses propriétés. Exemple : ITypedId.AsOrphan
                if (!typesMet.Contains(p.PropertyType))
                {
                    typesMet.Add(p.PropertyType);
                    foreach (var col in GetPropertiesAsColumns(p.PropertyType, p.Name, allowInterface, typesMet))
                        yield return col;
                }
            }
        }

        RepositoryItem GetTypeRelatedRepositoryItem(Type type)
        {
            if (type == typeof(string))
                return new RepositoryItemTextEdit();
            if ((type.TryGetNullableType() ?? type) == typeof(bool))
                return new RepositoryItemCheckEdit();
            if (Type_Extensions.IntegerTypesWithNullable.Contains(type))
                return new RepositoryItemSpinEdit();
            if (Type_Extensions.NumericTypesWithNullable.Contains(type))
            {
                var repo = new RepositoryItemTextEdit();
                repo.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
                repo.Mask.EditMask = @"-?\d{1,4}(\.\d+)?";
                return repo;
            }
            if ((type.TryGetNullableType() ?? type) == typeof(DateTime))
                return new RepositoryItemDateEdit();
            if ((type.TryGetNullableType() ?? type) == typeof(TimeSpan))
                return new RepositoryItemTimeSpanEdit();
            if ((type.TryGetNullableType() ?? type).IsEnum)
            {
                var tEnum = type.TryGetNullableType() ?? type;

                // important : Tell DW how to parse value that represet enum in filterstring
                // Moreover in "criteria tree", the value are correctly typed (and not string)
                EnumProcessingHelper.RegisterEnum(tEnum);


                var repo = new RepositoryItemLookUpEdit();
                var mgFillWithEnumValue = typeof(LookUpEditInGrid_ExtensionsForEnums)
                            .GetMethods(BindingFlags.Public | BindingFlags.Static)
                            .Where(m => m.Name == nameof(LookUpEditInGrid_ExtensionsForEnums.FillWithEnumValues))
                            .Where(m => m.GetParameters().Last().ParameterType.IsArray)
                            .FirstOrDefault();
                var mFillWithEnumValue = mgFillWithEnumValue.MakeGenericMethod(tEnum);
                mFillWithEnumValue.Invoke(null, new object[] { repo, null });
                return repo;
            }
            if (typeof(DynamicEnum).IsAssignableFrom(type))
            {
                var repo = new RepositoryItemLookUpEdit();
                var mgFillWithObjects = typeof(LookUpEditInGrid_ExtensionsForObjects).GetMethod(nameof(LookUpEditInGrid_ExtensionsForObjects.FillWithObjects));
                var spAll = type.GetProperty(nameof(DynamicEnum.DummyEnum.All), BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                var enumValues = (spAll.GetValue(null) as IEnumerable)
                                    .Cast<DynamicEnum>()
                                    .ToDictionary(de => DynamicEnum.GetEnumId(de));
                var mCast = typeof(Enumerable).GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(type);
                object objects = mCast.Invoke(null, new object[] { enumValues.Values });

                var getText = GetType().GetMethod(nameof(CreateGetTextDelegate), BindingFlags.NonPublic | BindingFlags.Static).MakeGenericMethod(type).Invoke(this, null);
                var getId = GetType().GetMethod(nameof(CreateIdentityDelegate), BindingFlags.NonPublic | BindingFlags.Static).MakeGenericMethod(type).Invoke(null, null);
                var mFillWithObjects = mgFillWithObjects.MakeGenericMethod(type, type);
                mFillWithObjects.Invoke(null, new object[] { repo, objects, getText, getId, false });

                if (!UserValueToStringHandlers.ContainsKey(type))
                {
                    UserValueToStringHandlers.Add(type, (_, e) =>
                    {
                        if (e.Value != null && type == e.Value.GetType())
                        {
                            var de = e.Value as DynamicEnum;
                            e.Tag = type.FullName;
                            // only the first part (id) hold the true used value
                            // We add also the caption in case someone else change the caption of the enumvalue in the futur.
                            // That way we are able to find what the original user wanted 
                            e.Data = "(" + DynamicEnum.GetEnumId(de) + ", " + de.Caption + ")";
                            e.Handled = true;
                        }
                    });
                }
                CriteriaOperator.UserValueToString += UserValueToStringHandlers[type];
                if (!UserValueParseHandlers.ContainsKey(type))
                {
                    UserValueParseHandlers.Add(type, (_, e) =>
                    {
                        if (e.Tag == type.FullName)
                        {
                            var enumId = long.Parse(e.Data.Remove(e.Data.IndexOf(", ")).Substring(1));
                            e.Value = enumValues[enumId];
                            var oldCaption = e.Data.Substring(e.Data.IndexOf(", ") + 2);
                            oldCaption = oldCaption.Remove(oldCaption.Length - 1);
                            if (oldCaption != enumValues[enumId].Caption)
                            {
                                // TODO : Alert user to make him save again ?
                            }

                            e.Handled = true;
                        }
                    });
                }
                CriteriaOperator.UserValueParse += UserValueParseHandlers[type];

                return repo;
            }
            if (typeof(IStaticEnum).IsAssignableFrom(type))
            {
                var repo = new RepositoryItemLookUpEdit();
                var mgFillWithObjects = typeof(LookUpEditInGrid_ExtensionsForObjects).GetMethod(nameof(LookUpEditInGrid_ExtensionsForObjects.FillWithObjects));
                var fieldByNames = type.GetFields(BindingFlags.Static | BindingFlags.Public)
                                       .Where(f => f.FieldType == type)
                                       .ToDictionary(f => f.Name);
                var mCast = typeof(Enumerable).GetMethod(nameof(Enumerable.Cast)).MakeGenericMethod(type);
                object objects = mCast.Invoke(null, new object[] { fieldByNames.Values.Select(f => f.GetValue(null)).ToList() });
                var getText = GetType().GetMethod(nameof(CreateGetStaticTextDelegate), BindingFlags.NonPublic | BindingFlags.Static).MakeGenericMethod(type).Invoke(this, null);
                var getId = GetType().GetMethod(nameof(CreateIdentityDelegate), BindingFlags.NonPublic | BindingFlags.Static).MakeGenericMethod(type).Invoke(null, null);
                var mFillWithObjects = mgFillWithObjects.MakeGenericMethod(type, type);
                mFillWithObjects.Invoke(null, new object[] { repo, objects, getText, getId, false });

                if (!UserValueToStringHandlers.ContainsKey(type))
                {
                    UserValueToStringHandlers.Add(type, (_, e) =>
                    {
                        if (e.Value != null && type == e.Value.GetType())
                        {
                            e.Tag = type.FullName;
                            e.Data = (e.Value as IStaticEnum).Id.ToString();
                            e.Handled = true;
                        }
                    });
                }
                CriteriaOperator.UserValueToString += UserValueToStringHandlers[type];
                if (!UserValueParseHandlers.ContainsKey(type))
                {
                    UserValueParseHandlers.Add(type, (_, e) =>
                    {
                        if (e.Tag == type.FullName)
                        {
                            e.Value = IStaticEnum_Extensions.GetValueSet(type)[e.Data].EnumValue;
                            e.Handled = true;
                        }
                    });
                }
                CriteriaOperator.UserValueParse += UserValueParseHandlers[type];

                return repo;
            }


            return new RepositoryItemTextEdit();
        }
        readonly Dictionary<Type, EventHandler<UserValueProcessingEventArgs>> UserValueParseHandlers = new Dictionary<Type, EventHandler<UserValueProcessingEventArgs>>();
        readonly Dictionary<Type, EventHandler<UserValueProcessingEventArgs>> UserValueToStringHandlers = new Dictionary<Type, EventHandler<UserValueProcessingEventArgs>>();
        static Func<T, string> CreateGetTextDelegate<T>() where T : DynamicEnum { return (T x) => x.Caption; }
        static Func<T, string> CreateGetStaticTextDelegate<T>() where T : IStaticEnum { return (T x) => x.ToString(); }
        static Func<T, T> CreateIdentityDelegate<T>() { return (T x) => x; }
    }
}
